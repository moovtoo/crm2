<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Locale extends Model
{
    //
    use SoftDeletes;

    protected $fillable =[
        'name',
        'flag',
        'code',
        'message'
    ];

    public function fields(){
        return [
            'index'=>[
                'order'=>'Order',
                'name'=>'Name',
                'sub_domains'=>'Sub-Domains',
            ],
            'store' =>[
                'name',
                'flag',
                'code',
                'message',
            ],
            'update' =>[
                'name',
                'code',
                'message',
            ],
        ];
    }
}
