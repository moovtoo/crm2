<?php

namespace App\Models;


class Settings extends Base
{
    protected $fillable =[
        'key',
        'value',
        'locale_id',
    ];
}
