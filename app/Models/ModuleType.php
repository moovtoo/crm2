<?php

namespace App\Models;
use Illuminate\Database\Eloquent\SoftDeletes;


class ModuleType extends Base
{
    use SoftDeletes;
    protected $fillable = ['name', 'slug', 'order', 'ref_id'];

}
