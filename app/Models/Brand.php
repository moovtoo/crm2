<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Brand  extends Base
{
    use SoftDeletes;
    protected $fillable = ['name','parent_id','photo'];

    public function parent(){
        return Brand::query()->where('id', $this->parent_id)->where('id','<>',$this->id)->get()->first();
    }


    public function fields(){
        return [
            'index'=>[
                'order'=>'Order',
                'photo'=>'Icon',
                'name'=>'Name',
                'parent'=>'Parent',
            ],
            'store' =>[
                'name',
                'parent_id'

            ],
            'update'=>[
                'name',
            ]
        ];
    }

}

