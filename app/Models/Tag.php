<?php

namespace App\Models;
use Illuminate\Database\Eloquent\SoftDeletes;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    use SoftDeletes;
    protected $fillable =[
        'name',
        'slug',
        'locale',
    ];

    
    public function language(){
        return $this->belongsTo(Locale::class,'locale');
    }

    public function articles(){
        return $this->belongsToMany(Article::class,'article_tags');
    }

    public function videos(){
        return $this->belongsToMany(Video::class,'videos_tags');
    }


    public function fields(){
        return [
            'index'=>[
                'order'=>'Order',
                'name'=>'Name',
                'locale'=>'Locale'
            ],
            'store' =>[
                'name',
                'locale',
            ]
        ];
    }
}
