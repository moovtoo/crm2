<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class ProjectModule extends Base
{
    use SoftDeletes;
    //
    protected $fillable = [
        'name','end_date','start_date','project_id','user_id','status_id','channel'
    ];

    public function status(){
        return $this->belongsTo(Status::class,'status_id');
    }
    public function users(){
        return $this->belongsToMany(User::class,'project_module_users');
    }
    public function user(){
        return $this->belongsTo(User::class,'user_id');
    }

    public function project(){
        return $this->belongsTo(Project::class , 'project_id');
    }
    public function projectTask(){
       return $this->hasMany(ProjectTask::class);
    }


    public function checkUser( $id ){
        $flag = false;
        $tasks = $this->projectTask;
        foreach ( $tasks as $task ){
            if ( $task->checkUser($id) ){
                $flag = true;
                break;
            }
        }
        return $this->users()->where('user_id',$id)->count()> 0 || $flag;
    }

    public function taskCount(){
        $tasks = ProjectTask::query()->where('project_module_id',$this->id)->get();
        $count = 0 ;
        foreach ( $tasks as $task ){
            $count += $task->estimation_date;
        }
        return $count;
    }
    public function fields(){
        return [
            'index'=>[
                'id'=>'ID',
                'name'=>'Name',
                'status_id'=>'Stage',
                'start_date'=>'Start Date',
                'end_date'=>'End Date',
                'task_count'=>'Estimation M/D',
                'use'=>'Team Leader',
                'channel'=>'Channel',

            ],
            'store' =>[
                'name',

            ],
            'update' =>[
                'name',

            ]
        ];
    }
}
