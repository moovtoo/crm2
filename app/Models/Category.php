<?php

namespace App\Models;
use Illuminate\Database\Eloquent\SoftDeletes;


class Category extends Base
{
    use SoftDeletes;
    protected $fillable = ['name','slug','type','parent_id','order'];

    public function parent(){
        return $this->belongsTo(Category::class,'parent_id');
    }
    
    public function articles(){
        return $this->belongsToMany(Article::class,'article_category');
    }

    public function fields(){
        return [
            'index'=>[
                'order'=>'Order',
                'name'=>'Name',
                'type'=>'Type',
                'parent'=>'Parent'
            ],
            'store' =>[
                'name',
                'type',
                'parent_id',
            ]
        ];
    }
}
