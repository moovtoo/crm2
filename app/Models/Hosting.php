<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class Hosting extends Base
{
    use SoftDeletes;

    protected $fillable = ['name','slug','order','status_id','domain_management_id','management_email','type_id','technology_id','renewal_date',
        'price','server_id','cpanel_code','cpanel_password','database_name','notes'];

    public function organization(){
        return $this->belongsTo(Organization::class,'organization_id');
    }
    public function status(){
        return $this->belongsTo(Status::class,'status_id');
    }
    public function domainManagement(){
        return $this->belongsTo(DomainManagement::class,'domain_management_id');
    }
    public function technology(){
        return $this->belongsTo(Technology::class,'technology_id');
    }

    public function domainType(){
        return $this->belongsTo(DomainType::class,'type_id');
    }


    public function fields(){
        return [
            'index'=>[
//                'order'=>'Order',
                'name'=>'Name',
//                'organization_id'=>'Organization',
                'status_id'=>'Status',
                'domain_management_id'=>'Domain Management',
                'technology_id'=>'Technology',
//                'type_id'=>'Domain Type',
                'renewal_date'=>'Renewal Date',
            ],
            'store' =>[
                'name',
//                'status_id',
//
            ],
        'update' =>[
                'name',
//            'status_id',
//
            ]
        ];
    }
}
