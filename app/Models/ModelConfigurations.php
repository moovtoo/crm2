<?php

namespace App\Models;

class ModelConfigurations extends Base
{
    //
    protected $fillable =[
        'module_id',
        'key',
        'value',
        'locale_id',
        'order'
    ];
}
