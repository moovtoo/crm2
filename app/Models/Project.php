<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class Project extends Base
{
    use SoftDeletes;

    protected $fillable = [
        'name', 'organization_id','end_date','start_date','project_manager_id','client_manager_id','status_id','description'
    ];
    public function organization(){
        return $this->belongsTo(Organization::class,'organization_id');
    }
    public function manager(){
        return $this->belongsTo(User::class,'project_manager_id');
    }
    public function contact(){
        return $this->belongsTo(Contact::class,'client_manager_id');
    }
    public function status(){
        return $this->belongsTo(Status::class,'status_id');
    }

    public function modules(){
        return $this->hasMany( ProjectModule::class , 'project_id');
    }
    
    public function users(){
        return $this->belongsToMany(User::class,'project_users');
    }


    public function getAllRelatedUsers (){
        return User::query()->whereRaw( 'users.id in  ( select project_users.user_id from project_users where project_users.project_id='
        .$this->id.') or users.id in  ( select project_module_users.user_id from project_module_users where project_module_users.project_module_id in '.
        '( select project_modules.id from project_modules where project_modules.project_id = '.$this->id.')) '.
            'or users.id in (select  project_task_users.user_id from project_task_users where project_task_users.project_task_id in '
        .' ( select project_tasks.id from project_tasks where project_tasks.project_module_id in '
        .' ( select project_modules.id from project_modules where project_modules.project_id = '.$this->id.'))) ')->get();
    }

    public function fields(){
        return [
            'index'=>[
//                'id'=>'ID',
                'name'=>'Name',
                'start_date'=>'Start Date',
                'end_date'=>'End Date',
                'client_manager_id'=>'Client Contact',
                'project_manager_id'=>'Client Leader',
                'status_id'=>'Status',
            ],
            'store' =>[
                'name',
                'end_date',
                'start_date',
                'client_manager_id',
                'project_manager_id',
                'organization_id',
                'status_id',
            ],
            'update' =>[
                'name',
                'end_date',
                'start_date',
                'client_manager_id',
                'project_manager_id',
                'organization_id',
                'status_id',
            ]
        ];
    }


}
