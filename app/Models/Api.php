<?php

namespace App\Models;
use Illuminate\Database\Eloquent\SoftDeletes;


class Api extends Base
{

    protected $fillable = ['name','slug','custom_select','custom_condition',
        'module_id','version', 'authenticated', 'response', 'type','order_by','paginate'];


    public function module(){
        return $this->belongsTo(Module::class ,'module_id');
    }
    public  function arguments (){
        return $this->hasMany(ApiArgument::class);
    }

    public function domains(){
        return $this->belongsToMany(Domain::class,'api_domains');
    }

    public function fields(){
        return [
            'index'=>[
                'order'=>'Order',
                'name'=>'Name',
                'module'=>'Module',
                'type'=>'Type',
            ],
            'store' =>[
                'name',
                'module_id',
            ],
            'update'=>[
                'name',
                'module_id',
            ]
        ];
    }

}
