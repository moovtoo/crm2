<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    //

    protected $fillable = [
        'name', 'module_id','slug'
    ];

    public function module(){
        return $this->belongsTo(Module::class ,'module_id');
    }
    public function fields(){
        return [
            'index'=>[
                'id'=>'ID',
                'name'=>'Name',
                'module'=>'Module',
//                'description'=>'Description',
            ],
            'store' =>[
                'name',
                'module_id',
            ],
            'update' =>[
                'name',

            ]
        ];
    }

}
