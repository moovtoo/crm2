<?php

namespace App\Models;
use Illuminate\Database\Eloquent\SoftDeletes;


class Article extends Base
{
    //
    use SoftDeletes;
    
    protected $fillable = [
        'name',
        'slug',
        'description',
        'content',
        'meta_title',
        'meta_description',
        'meta_url',
        'meta_image',
        'meta_type',
        'photo',
        'status',
        'locale_id',
        'date_created',
        'date_updated',
        'order'
    ];


    public function users(){
        return $this->belongsToMany(User::class,'article_users');
    }
    
    public function sites(){
        return $this->belongsToMany(Domain::class,'article_domains');
    }
    
    public function tags(){
        return $this->belongsToMany(Tag::class,'article_tags');
    }

    public function firstUser(){
        try{
            return $this->users()->first()->name;
        }catch (\Exception $e){
            return 'Unknown';
        }

    }

    public function language(){
        return $this->belongsTo(Locale::class,'locale_id');
    }

    public function categories(){
        return $this->belongsToMany(Category::class, 'article_category');
    }

    public function firstCategory(){
        try{
        return $this->categories()->first()->name;
        }catch (\Exception $e){
            return "Unknown";
        }
    }

    public function fields(){
        return [
            'index'=>[
                'order' => 'Order',
                'name'=>'Name',
                'author'=>'Author',
                'status'=>'Status',
                'category'=>'Category',
                'date'=>'Date'
            ],
            'store' =>[
                'name',
                'photo',
                'categories'
            ],
            'update' =>[
                'name',
                'categories'
            ]
        ];
    }

}
