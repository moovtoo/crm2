<?php

namespace App\Models;
use Illuminate\Database\Eloquent\SoftDeletes;


class Organization extends Base
{
    use SoftDeletes;
    //
    protected $fillable = [
        'name', 'description','user_id', 'country_id', 'city', 'slug', 'zip_code', 'image', 'company_image', 'sector_id', 'connected_since', 'address'
    ];

    public function country(){
        return $this->belongsTo( Country::class , 'country_id');
    }
    public function sector(){
        return $this->belongsTo( Sector::class , 'sector_id');
    }

    public function user(){
        return $this->belongsTo( User::class , 'user_id');
    }
    public function phones(){
        return $this->hasMany(OrganizationPhone::class);
    }
    public function contacts(){
        return $this->hasMany(Contact::class);
    }

    public function fields(){
        return [
            'index'=>[
                'id'=>'ID',
                'name'=>'Name',
                'user'=>'Assigned To',
                'country' => 'Country',
                'connected_since' => 'Connected Since',
            ],
            'store' =>[
                'name',
            ],
            'update' =>[
                'name',
            ]
        ];
    }
}
