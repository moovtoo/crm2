<?php

namespace App\Models;
use Illuminate\Database\Eloquent\SoftDeletes;


class Module extends Base
{
    use SoftDeletes;
    protected $fillable = ['name','table_name','slug','parent_id','order','ref_id', 'type_id', 'parent_id', 'controller', 'allow_locale', 'success_message', 'success_message_ref_id', 'update_message', 'update_message_ref_id', 'delete_message', 'delete_message_ref_id', 'error_message', 'error_message_ref_id'];

    public function parent(){
        return $this->belongsTo(ParentModule::class,'parent_id');
    }

    public function roleCheck($role){
        return RoleModules::query()->where('module_id',$this->id)->where('role_id',$role)->get()->first();
    }

    public function topParent($id = null,$arr=array()){
        $tp = ParentModule::query()->where('id',$id== null ?$this->parent_id : $id)->get()->first();
        if ( $tp == null )
            return $arr;
        if ( $tp->parent_id == null ){
            $arr[]=$tp->id;
            return $arr;
        }

        $arr[]=$tp->id;
        return  $this->topParent($tp->parent_id, $arr);
    }

    public function isAllowedModule($role_id){
        if($role_id == 1000){
            return true;
        }
        $rm = $this->roleCheck($role_id);
        if ( $rm == null )
            return false;

        foreach ($rm->toArray() as $k=> $v)
            if ( $v )
                return true;
        return false;
    }

    public function isAllowedModuleAdd($roles){
        foreach($roles as $role){
            if($role->id == 1000){
                return true;
            }
            $rm = $this->roleCheck($role->id);
            if ( $rm != null ){
                foreach ($rm->toArray() as $k=> $v)
                    if ( ($v && $k == 'add') )
                        return true;
            }
        }
        return false;
    }

    public function isAllowedModuleEdit($roles){
        foreach($roles as $role){
            if($role->id == 1000){
                return true;
            }
            $rm = $this->roleCheck($role->id);
            if ( $rm != null ){
                foreach ($rm->toArray() as $k=> $v)
                    if ( ($v && $k == 'edit') )
                        return true;
            }
        }
        return false;
    }

    public function isAuthModuleAdd($roles){
        foreach($roles as $role){
            if($role->id == 1000){
                return true;
            }
            $rm = $this->roleCheck($role->id);
            if ( $rm != null ){
                foreach ($rm->toArray() as $k=> $v)
                    if ( ($v && $k == 'add') )
                        return true;
            }
        }
        return false;
    }

    public function isAuthModuleEdit($roles){
        foreach($roles as $role){
            if($role->id == 1000){
                return true;
            }
            $rm = $this->roleCheck($role->id);
            if ( $rm != null ){
                foreach ($rm->toArray() as $k=> $v)
                    if ( ($v && $k == 'edit_all') )
                        return true;
            }
        }
        return false;
    }

    public function isAuthModuleDelete($roles){
        foreach($roles as $role){
            if($role->id == 1000){
                return true;
            }
            $rm = $this->roleCheck($role->id);
            if ( $rm != null ){
                foreach ($rm->toArray() as $k=> $v)
                    if ( ($v && $k == 'delete_all') )
                        return true;
            }
        }
        return false;
    }

    public function isAllowedModuleDelete($roles){
        foreach($roles as $role){
            if($role->id == 1000){
                return true;
            }
            $rm = $this->roleCheck($role->id);
            if ( $rm != null ){
                foreach ($rm->toArray() as $k=> $v)
                    if ( ($v && $k == 'delete_all') || ($v && $k == 'delete') )
                        return true;
            }
        }
        return false;
    }

}
