<?php

namespace App\Models;
use Illuminate\Database\Eloquent\SoftDeletes;


class Contact extends Base
{
    use SoftDeletes;
    protected $fillable = ['name','slug','private_email','business_email','business_position', 'sector_id', 'organization_id', 'country_id', 'notes','address_1','address_2','state','po_box','website','linkedin','facebook','twitter','instagram','pinterest'];

    public function country(){
        return $this->belongsTo(Country::class,'country_id');
    }
    public function sector(){
        return $this->belongsTo( Sector::class , 'sector_id');
    }
    
    public function organization(){
        return $this->belongsTo(Organization::class,'organization_id');
    }
    
    public function phones(){
        return $this->hasMany(ContactPhone::class);
    }

    public function fields(){
        return [
            'index'=>[
//                'order'=>'Order',
                'name'=>'Name',
                'organization'=>'Organization',
                'business_position'=>'Business Position',
                'country'=>'Country'
            ],
            'store' =>[
                'name',
            ],
            'update' =>[
                'name',
            ]
        ];
    }
}
