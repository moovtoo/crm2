<?php

namespace App\Models;
use Illuminate\Database\Eloquent\SoftDeletes;


class ContactPhone extends Base
{
    use SoftDeletes;
    protected $fillable = ['contact_id','phone','type_id','link'];

    public function contact(){
        return $this->belongsTo(Contact::class,'contact_id');
    }
    public function type(){
        return $this->belongsTo( ContactType::class , 'type_id');
    }

    public function fields(){
        return [
            'index'=>[
                'order'=>'Order',
                'contact'=>'Contact',
                'type'=>'Type',
                'phone'=>'Phone Number',
                'link'=>'Link'
            ],
            'store' =>[
                'contact_id',
                'type',
            ],
            'update' =>[
                'contact_id',
                'type',
            ]
        ];
    }
}
