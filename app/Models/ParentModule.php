<?php

namespace App\Models;
use Illuminate\Database\Eloquent\SoftDeletes;


class ParentModule extends Base
{
    use SoftDeletes;
    protected $fillable = ['name', 'slug', 'parent_id','order','icon','controller', 'description'];

    public function parent(){
        return $this->belongsTo(ParentModule::class,'parent_id');
    }

    public function parentChildren(){
        return $this->hasMany(ParentModule::class , 'parent_id');
    }
    
    public function children(){
        return Module::query()->where('parent_id', $this->id)->orderBy('order','asc')->get();
    }

    public function hasConfig(){
        $ids =  $this->getAllChildren();

        $modules = Module::query()->whereIn('parent_id',$ids)->get()->pluck('id');

        $config = ModelConfigurations::query()->whereIn('module_id',$modules)->get();

        return $config;

    }

    private function getAllChildren(){
        if ( $this->parentChildren()->count() == 0 )
            return array() ;
        else{
            return array_merge( array($this->id) , $this->getInternalChildren($this->id));
        }

    }
    private function getInternalChildren($id){
        $p = ParentModule::find($id);
        if ( $p->parentChildren()->count() == 0 ){
            return array();
        }else{
            foreach ($p->parentChildren as $c ){
                return array_merge( array($c->id),$this->getInternalChildren($c->id) );
            }
        }
    }

}
