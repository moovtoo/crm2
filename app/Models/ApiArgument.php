<?php

namespace App\Models;


class ApiArgument extends Base
{

    protected $fillable = ['key','api_id', 'required', 'type'];

    public  function api(){
        return $this->belongsTo(Api::class, 'api_id');
    }


    public function fields(){
        return [
            'index'=>[
                'order'=>'Order',
                'key'=>'Key',
                'type'=>'Type',
            ],
            'store' =>[
                'key',
                'type',
            ],
            'update'=>[
                'key',
                'type',
            ]
        ];
    }



}

