<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Country extends Base
{
    //
    protected $fillable =[
        'name',
        'slug',
        'ref_id',
        'code',
        'flag',
        'nationality'
    ];

    public function fields(){
        return [
            'index'=>[
                'order'=>'Order',
                'photo'=>'Flag',
                'name'=>'Title',
                'code'=>'Code',
                'created_at'=>'Created Date',
            ],
            'store' =>[
                'name',
            ],
            'update' =>[
                'name',
            ]


        ];
    }
}
