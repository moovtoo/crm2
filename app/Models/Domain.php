<?php

namespace App\Models;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Domain extends Base
{
    use SoftDeletes;

    protected $fillable = ['name','type','parent_id','order'];

    public function parent(){
        return $this->belongsTo(Domain::class,'parent_id');
    }
    
    public function subDomains(){
        return Domain::query()->where('parent_id', $this->id)->get();
    }
    
    public function articles(){
        return $this->belongsToMany(Article::class,'article_domains');
    }
    public function apis(){
        return $this->belongsToMany(Api::class,'api_domains');
    }


    public function fields(){
        return [
            'index'=>[
                'order'=>'Order',
                'name'=>'Name',
                'sub_domains'=>'Sub-Domains',
            ],
            'store' =>[
                'name',
                'type',
            ]
        ];
    }
}
