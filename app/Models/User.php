<?php

namespace App\Models;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\EloquentSortable\Sortable;
use Spatie\EloquentSortable\SortableTrait;
use App\Notifications\ResetPassword as ResetPasswordNotification;

class User extends Authenticatable implements Sortable
{
    use SoftDeletes;
    use Notifiable;
    //
    use SortableTrait;

    public $sortable = [
        'order_column_name' => 'order',
        'sort_when_creating' => true,
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','photo','status','role_id','created_at','position',
        'facebook', 'twitter','linkedin','instagram','blog','website','bio','provider_id','provider',
        'device_token','device_platform','api_token'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function isAdmin(){
        foreach($this->roles as $role){
            if($role->id == 1000 || $role->slug != 'user'){
                return true;
            }
        }
        return false;
    }

    public function articles(){
        return $this->belongsToMany(Article::class,'article_users');
    }
    public function roles(){
        return $this->belongsToMany(Role::class , 'user_roles');
    }
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordNotification($token));
    }
    public function  project(){
        return $this->belongsToMany(Project::class ,'project_users');
    }


    public function  RelatedModulesAndTasks( $project_id){
        $project = Project::query()->find($project_id);

        $modules_results = array();
        foreach ( $project->modules as $module ){
            if ( $module->checkUser($this->id) ){
                array_push($modules_results , $module);
            }
        }

        return $modules_results;


    }




    public function fields(){
        return [
            'index'=>[
//                'order' => 'Order',
                'order'=>'Order',
                'photo'=>'Photo',
                'name'=>'Full Name',
//                'email'=>'Email',
                'created_at'=>'Created Date',
                'status'=>'Status',
                'role'=>'Role',

//                'date'=>'Date'
            ],
            'store' =>[
                'name',
//                'photo',
                'email',
                'status',
                'role_id',
                'password',
            ],
            'update' =>[
                'name',
                'email',
                'status',
                'role_id',
            ]
        ];
    }
}
