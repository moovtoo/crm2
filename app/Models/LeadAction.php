<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class LeadAction extends Base
{
    use SoftDeletes;
    protected $fillable = [
        'name','attachment','user_id','contact_id','type','status_id','type_id','note','created_date','lead_id'
    ];

    public function user(){
        return $this->belongsTo(User::class,'user_id');
    }
    public function lead(){
        return $this->belongsTo(Lead::class,'lead_id');
    }
    public function type(){
        return $this->belongsTo( ContactType::class , 'type_id');
    }

    public function contact(){
        return $this->belongsTo(Contact::class,'contact_id');
    }
    public function status(){
        return $this->belongsTo(Status::class,'status_id');
    }

    public function fields(){
        return [
            'index'=>[
//                'id'=>'ID',
                'note'=>'Note',
//                'lead'=>'Company Name',
                'type'=>'Type',
                'status_id'=>'Status',
                'created_date'=>'Date',
                'contact_id'=>'Contact',
            ],
            'store' =>[
//                'name',
                'created_date',
                'contact_id',
                'status_id',
//                'lead_id',
                'type_id',
            ],
            'update' =>[
//                'name',
                'created_date',
                'contact_id',
                'status_id',
//                'lead_id',
                'type_id',
            ]
        ];
    }
    public function fields2(){
        return [
            'index'=>[
//                'id'=>'ID',
                'note'=>'Note',
                'lead'=>'Company Name',
//                'type'=>'Type',
                'status_id'=>'Status',
                'created_date'=>'Date',
                'contact_id'=>'Contact',
            ],
            'store' =>[
//                'name',
                'created_date',
                'contact_id',
                'status_id',
//                'lead_id',
                'type_id',
            ],
            'update' =>[
//                'name',
                'created_date',
                'contact_id',
                'status_id',
//                'lead_id',
                'type_id',
            ]
        ];
    }


}
