<?php

namespace App\Models;
use Illuminate\Database\Eloquent\SoftDeletes;


class Media extends Base
{
    use SoftDeletes;
    //
    protected $fillable =[
        'folder',
        'name',
        'url',
        'kind',
        'order'
    ];

}
