<?php

namespace App\Models;
use Illuminate\Database\Eloquent\SoftDeletes;


class OrganizationPhone extends Base
{
    use SoftDeletes;
    //
    protected $fillable = [
        'phone', 'type_id','link', 'organization_id','sector_id'
    ];

    public function organization(){
        return $this->belongsTo( Organization::class , 'organization_id');
    }
    public function type(){
        return $this->belongsTo( ContactType::class , 'type_id');
    }
    public function sector(){
        return $this->belongsTo( Sector::class , 'sector_id');
    }
    public function fields(){
        return [
            'index'=>[
                'order'=>'Order',
                'phone'=>'Phone',
                'type'=>'Type',
                'sector'=>'Sector',
                'organization' => 'Organization',
            ],
            'store' =>[
                'type',
                'organization_id',
            ],
            'update' =>[
                'type',
                'organization_id',
            ]
        ];
    }
}
