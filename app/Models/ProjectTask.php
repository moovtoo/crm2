<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class ProjectTask extends Base
{
    use SoftDeletes;
    //
    protected $fillable = [
        'name','end_date','start_date','project_module_id','estimation_date','cost','handled_by','comment','status_id'
    ];
    public function users(){
        return $this->belongsToMany(User::class,'project_task_users');
    }
    public function projectModule(){
        return $this->belongsTo(ProjectModule::class , 'project_module_id');
    }
    public function status(){
        return $this->belongsTo(Status::class,'status_id');
    }

    public function  checkUser($id){
        return $this->users()->where('user_id',$id)->count()> 0;
    }


    public function fields(){
        return [
            'index'=>[
//                'id'=>'ID',
                'name'=>'Name',
                'start_date'=>'Start Date',
                'end_date'=>'End Date',
                'estimation_date'=>'Estimation M/D',
                'cost'=>'Cost M/D',
                'handled_by'=>'Handled By',
                'status_id'=>'Status',
//                'comment'=>'Comment',

            ],
            'store' =>[
                'name',

            ],
            'update' =>[
                'name',

            ]
        ];
    }
}
