<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;

class Lead extends Base
{
    use SoftDeletes;
    protected $fillable = [
        'name', 'organization_id','origin_id','contact_id','user_id','status_id','description','created_date'
    ];
    //
    public function organization(){
        return $this->belongsTo(Organization::class,'organization_id');
    }
    public function origin(){
        return $this->belongsTo(Origin::class,'origin_id');
    }
    public function user(){
        return $this->belongsTo(User::class,'user_id');
    }
    public function contact(){
        return $this->belongsTo(Contact::class,'contact_id');
    }
    public function status(){
        return $this->belongsTo(Status::class,'status_id');
    }
    public function leadAction(){
        return $this->hasMany(LeadAction::class,'lead_id');
    }
    public function UpComingleadAction(){
        return LeadAction::query()->whereIn('status_id',[10,20])
            ->where('type_id',7)
            ->where('lead_id', $this->id)
            ->orderBy('created_date','asc')
            ->first();
    }

    public function UpComingleadAction2(){
        return LeadAction::query()->whereIn('status_id',[10,12,20])
            ->where('type_id','!=',7)
            ->where('lead_id', $this->id)
            ->where('created_date','>=',Carbon::now()->addDay(-90)->toDateString())
            ->orderBy('created_date','asc')
            ->first();
    }


    public function fields(){
        return [
            'index'=>[
                'name'=>'Title',
                'organization_id'=>'Organization',
                'contact_id'=>'Contact',
                'origin_id'=>'Origin',
                'status_id'=>'Status',
                'created_date'=>'Date',
            ],
            'store' =>[
                'name',
                'contact_id',
                'origin_id',
                'organization_id',
                'status_id',
                'created_date',
            ],
            'update' =>[
                'name',
                'contact_id',
                'origin_id',
                'organization_id',
                'status_id',
                'created_date',
            ]
        ];
    }
    public function fields2(){
        return [
            'index'=>[
                'name'=>'Title',
                'organization_id'=>'Organization',
                'contact_id'=>'Contact',
                'user_id'=>'Assigned To',
                'status_id'=>'Status',
                'created_date'=>'Date',
            ],
            'store' =>[
                'name',
                'contact_id',
                'origin_id',
                'organization_id',
                'status_id',
                'created_date',
            ],
            'update' =>[
                'name',
                'contact_id',
                'origin_id',
                'organization_id',
                'status_id',
                'created_date',
            ]
        ];
    }

}
