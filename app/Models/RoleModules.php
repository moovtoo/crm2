<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RoleModules extends Model
{

    //
    protected $fillable = [
        'role_id', 'module_id', 'add', 'edit', 'delete', 'show', 'edit_all', 'delete_all', 'show_all', 'status'
    ];

    public function module(){
        return $this->belongsTo(Module::class ,'module_id');
    }

    public function role(){
        return $this->belongsTo( Role::class );
    }
}
