<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Origin extends Base
{
    //
    protected $fillable =[
        'name',
        'slug',
        'department_id',
    ];

    public function fields(){
        return [
            'index'=>[
                'order'=>'Order',
                'name'=>'Name',
                'department'=>'Department',
            ],
            'store' =>[
                'name',
            ],
            'update' =>[
                'name',
            ]
        ];
    }

    public function department(){
        return $this->belongsTo(Department::class, 'department_id');
    }
}
