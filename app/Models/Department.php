<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Department extends Base
{
    //
    protected $fillable =[
        'name',
        'slug',
        'color',
        'tags',
    ];

    public function fields(){
        return [
            'index'=>[
                'order'=>'Order',
                'name'=>'Name',
                'color'=>'Color',
                'tags'=>'Tags',
            ],
            'store' =>[
                'name',
                'color',
            ],
            'update' =>[
                'name',
                'color',
            ]
        ];
    }
}
