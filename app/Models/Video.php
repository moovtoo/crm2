<?php

namespace App\Models;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Video extends Base
{
    use SoftDeletes;
    //
    protected $fillable = [

        'name',
        'description',
        'date_created',
        'image',
        'video',
        'meta_title',
        'meta_description',
        'meta_url',
        'meta_image',
        'meta_type',
        'status',

    ];

    public function tags(){
        return $this->belongsToMany(Tag::class,'videos_tags');
    }


    public function fields(){
        return [
            'index'=>[
                'id'=>'ID',
//                'image'=>'Image',
                'name'=>'Title',
                'status'=>'Status',
//                'uploaded_date'=>'Uploaded Date',
//                'description'=>'Description'
                'date_created'=>'Created Date',
            ],
            'store' =>[
                'video',
                'name',
//                'description',
            ],

            'update' =>[
                'name',
            ]


        ];
    }
}
