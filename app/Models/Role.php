<?php

namespace App\Models;
use Illuminate\Database\Eloquent\SoftDeletes;


class Role extends Base
{
    use SoftDeletes;
    //
    protected $fillable = [
        'name', 'description','order'
    ];

    public function modules(){
        return $this->hasMany( RoleModules::class , 'role_id');
    }

    public function module($id){
        return $this->hasMany( RoleModules::class , 'role_id')->where('module_id',$id)->get()->first();
    }
    public function users(){
        return $this->belongstoMany( User::class , 'user_roles');
    }


    public function fields(){
        return [
            'index'=>[
                'order'=>'Order',
                'name'=>'Name',
//                'description'=>'Description',
            ],
            'store' =>[
                'name',
                'description',
            ],
            'update' =>[
                'name',
                'description',

            ]
        ];
    }
}
