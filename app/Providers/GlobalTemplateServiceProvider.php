<?php

namespace App\Providers;

use App\Models\Category;
use App\Models\Domain;
use App\Models\Locale;
use App\Models\Settings;
use App\Models\User;
use App\Models\Tag;
use App\Models\ParentModule;
use App\Models\ModelConfigurations;
use Illuminate\Support\ServiceProvider;

class GlobalTemplateServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
        view()->composer(['layouts.app','auth.login','moovtoo-welcome','layouts.main-app','auth.passwords.reset','auth.passwords.email','auth.passwords.email'], function ($view) {
            $view->with('settings', $this->getSettings());
        });
        
        view()->composer(['layouts.partials.body-header'], function($view){
            $view->with('parentModules', $this->getParentModules());
        });

        view()->composer(['content-management.articles.article','content-management.articles.article-update','content-management.model-configurations.configurations'], function ($view) {
            $view->with('authors', $this->getAuthors());
            $view->with('categories', $this->getCategories());
            $view->with('locales', $this->getLocales());
            $view->with('domains', $this->getAllDomains());
            $view->with('tags', $this->getAllTags());
            $view->with('configuration', $this->getConfigurations(1));
        });

        view()->composer(['content-management.videos.video', 'content-management.videos.video-update'], function ($view){
            $view->with('tags', $this->getAllTags());
        });
        view()->composer(['content-management.tags.tag', 'content-management.tags.tag-update'], function ($view) {
            $view->with('locales', $this->getLocales());
        });

        view()->composer(['configurations.domains.domain', 'configurations.domains.domain-update'], function ($view) {
            $view->with('domains', $this->getDomains());
        });

        // view()->composer(['auth.login'], function ($view) {
        //     $view->with('settings', $this->getSettings());
        // });
        // view()->composer(['auth.passwords.email'], function ($view) {
        //     $view->with('settings', $this->getSettings());
        // });
    }

    private function getDomains(){
        return Domain::query()->where('type','domain')->get();
    }
    
    
    private function getAllDomains(){
        return Domain::query()->get();
    }
    
    private function getAllTags(){
        return Tag::query()->get();
    }

    private function getAuthors(){
        return User::query()->orderBy('name')->get();
    }
    private function getCategories(){
        return Category::query()->orderBy('name')->get();
    }

    private function getLocales(){
        return Locale::query()->orderBy('name')->get();
    }
    
    private function getSideModules($id){
        return ParentModule::query()->where('parent_id',$id)->orderBy('order','asc')->get();
    }
    
    private function getParentModules(){
        return ParentModule::query()->where('parent_id', null)->orderBy('order','asc')->get();
    }
    
    private function getParentModule($id){
        return ParentModule::query()->where('id', $id)->get()->first();
    }
    
    private function getConfigurations($model){
        $configuration = array();
        $configurations = ModelConfigurations::query()->where('module_id', $model)->get();
        foreach ($configurations as $config) {
            $configuration [$config->key] = $config->value;
        }
        return $configuration;
    }

    private function getSettings($locale_id = 1){
        $setting = array();
        $settings = Settings::query()->where('locale_id', $locale_id)->get();
        foreach ($settings as $set) {
            $setting [$set->key] = $set->value;
        }
//        $logo =array_has($setting, 'logo')?$setting['logo']:'';
        return $setting;
    }
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
