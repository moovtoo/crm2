<?php
/**
 * Created by PhpStorm.
 * User: rabeearkadan
 * Date: 12/11/18
 * Time: 4:57 PM
 */
namespace App\Utils;

use App\Models\Article;
use App\Models\Hosting;
use App\Models\Lead;
use App\Models\LeadAction;
use App\Models\OrganizationPhone;
use App\Models\Api;
use App\Models\ApiArgument;
use App\Models\Domain;
use App\Models\Media;
use App\Models\Project;
use App\Models\ProjectModule;
use App\Models\ProjectTask;
use App\Models\Role;
use App\Models\Status;
use App\Models\User;
use App\Models\Category;
use App\Models\Tag;
use App\Models\Video;
use App\Models\Organization;
use App\Models\Contact;
use App\Models\ContactPhone;
use App\Models\Department;
use App\Models\Origin;
use Illuminate\Support\Facades\URL;

trait Transform
{

    public function transformGeneric($item){
        $name  = (new \ReflectionClass($item))->getShortName();
        $name = 'transformIndex'.$name;
        if(method_exists($this, $name))
        {
            return $this->$name($item);
        }
    }

    public function transformIndexArticle(Article $article){
        $art = new Article();
        $art->order = $article->order;
        $art->id = $article->id;
        $art->name = $article->name;
        $art->status = $article->status;
        $art->author = $article->firstUser();
        $art->category = $article->firstCategory();
        $art->date =$article->date_created;
        return $art;
    }

    public function transformIndexVideo(Video $video){
        $vdu = new Video();
        $vdu->id = $video->id;
        $vdu->order = $video->order;
        $vdu->name = $video->name;
        $vdu->status = $video->status;
        $vdu->description = $video->description;
        $vdu->date_created = $video->date_created;
        $vdu->image = $video->image;
        $vdu->created_at = $video->created_at;
        return $vdu;
    }



    public function transformIndexUser(User $user){
        $use = new User();
        $use->id = $user->id;
        $use->order = $user->order;
        $use->name = $user->name;
        $use->photo = ($user->photo != null)? asset("$user->photo"): asset("images/avatar.jpg");
        $use->created_at = $user->created_at;
        $roles = '';
        $count = count($user->roles);
        foreach($user->roles as $rol){
            if(--$count != 0){
                $roles .= $rol->name.', ';
            }else{
                $roles .= $rol->name;
            }
        }
        $use->role = $roles;
        $use->status = $user->status;
        $use->position = $user->position;
        $use->facebook = $user->facebook;
        $use->twitter = $user->twitter;
        $use->linkedin = $user->linkedin;
        $use->instagram = $user->instagram;
        $use->website = $user->website;
        $use->blog = $user->blog;
        $use->bio = $user->bio;
        return $use;
    }

    public function transformIndexProject(Project $project){
        $pro = new Project();
        $pro->id = $project->id;
        $pro->name = $project->name;
        $pro->organization_id = ($project->organization)? $project->organization->name : '';
        $pro->client_manager_id = ($project->contact)? $project->contact->name : '';
        $pro->project_manager_id = ($project->manager)? $project->manager->name : '';
        $pro->status_id = ($project->status)? $project->status->name : '';
        $pro->description = $project->description;
        $pro->start_date = $project->start_date;
        $pro->end_date = $project->end_date;
        return $pro;
    }
    public function transformIndexLead(Lead $lead){
        $led = new Lead();
        $led->id = $lead->id;
        $led->name = $lead->name;
        $led->order= $lead->order;
        $led->organization_id = ($lead->organization)? $lead->organization: '';
        $led->origin_id = ($lead->origin)? $lead->origin->name : '';
        $led->contact_id = ($lead->contact)? $lead->contact: '';
        $led->user_id = ($lead->user)? $lead->user->name : '';
        $led->description = $lead->description;
        $led->status_id = ($lead->status)? $lead->status->name : '';
        $led->type_id = ($lead->type)? $lead->type->name : '';
        $led->created_date = $lead->created_date;
        return $led;
    }

    public function transformIndexLeadAction(LeadAction $leadAction){
        $ledac = new LeadAction();
        $ledac->id = $leadAction->id;
        $ledac->order = $leadAction->order;
        $ledac->name = $leadAction->name;
//        $ledac->lead_id = ($leadAction->lead )? $leadAction->lead->name : '';
        $ledac->organization_id = ($leadAction->organization)? $leadAction->organization->name : '';
        $ledac->contact_id = ($leadAction->contact)? $leadAction->contact->name : '';
        $ledac->user_id = ($leadAction->user)? $leadAction->user->name : '';
        $ledac->status_id = ($leadAction->status)? $leadAction->status->name : '';
        $ledac->note = strip_tags(($leadAction->note));
//        $ledac->note = strip_tags(htmlentities($leadAction->note, ENT_COMPAT,'ISO-8859-1', true));
        $ledac->type = ($leadAction->type)? $leadAction->type->name : '';
        $ledac->created_date = $leadAction->created_date;
        return $ledac;
    }

    public function transformIndexLeadAction2(LeadAction $leadAction){
        $ledac = new LeadAction();
        $ledac->id = $leadAction->id;
        $ledac->order = $leadAction->order;
//        $ledac->name = $leadAction->lead->name;
        $ledac->note = ($leadAction->lead )? $leadAction->lead->name : '';
        $ledac->lead = ($leadAction->lead)? $leadAction->lead->organization->name : '';
        $ledac->contact_id = ($leadAction->contact)? $leadAction->contact->name : '';
        $ledac->user_id = ($leadAction->user)? $leadAction->user->name : '';
        $ledac->status_id = ($leadAction->status)? $leadAction->status->name : '';
//        $ledac->note = strip_tags(($leadAction->note));
//        $ledac->note = strip_tags(htmlentities($leadAction->note, ENT_COMPAT,'ISO-8859-1', true));
        $ledac->type = ($leadAction->type)? $leadAction->type->name : '';
        $ledac->created_date = $leadAction->created_date;
        return $ledac;
    }


    public function transformIndexRole(Role $role){
        $rol = new Role();
        $rol->id = $role->id;
        $rol->order = $role->order;
        $rol->name = $role->name;
        $rol->description = $role->description;
        return $rol;
    }
    public function transformIndexProjectModule(ProjectModule $projectModule){
        $projectModul= new ProjectModule();
        $projectModul->id = $projectModule->id;
        $projectModul->order = $projectModule->order;
        $projectModul->start_date = $projectModule->start_date;
        $projectModul->end_date = $projectModule->end_date;
        $projectModul->channel = $projectModule->channel;
        $projectModul->task_count = $projectModule->taskCount();
//        $projectModul->name = $projectModule->name;
        $projectModul->name = ['name'=>$projectModule->name,'id'=>$projectModule->id];
        $projectModul->status_id = ($projectModule->status)? $projectModule->status->name : '';
        $projectModul->use = '';
        foreach ($projectModule->users as $r ){
            $projectModul->use .= $r->name.', ';
        }
        $projectModul->user_id = ($projectModule->user)? $projectModule->user->name : '';
        return $projectModul;
    }
    public function transformIndexProjectTask(ProjectTask $projectTask){
        $projecttsk= new ProjectTask();
        $projecttsk->id = $projectTask->id;
        $projecttsk->cost = $projectTask->cost;
        $projecttsk->start_date = $projectTask->start_date;
        $projecttsk->end_date = $projectTask->end_date;
        $projecttsk->comment = str_limit( $projectTask->comment , $limit = 15, $end = '...');
        $projecttsk->estimation_date = $projectTask->estimation_date;
        $projecttsk->status_id = ($projectTask->status)? $projectTask->status->name : '';
        $projecttsk->name = $projectTask->name;
        $projecttsk->handled_by = '';
        foreach ($projectTask->users as $r ){
            $projecttsk->handled_by .= $r->name.', ';
        }
        $projecttsk->user_id = ($projectTask->user)? $projectTask->user->name : '';
        return $projecttsk;
    }

    public function transformIndexOrigin(Origin $origin){
        $org = new Origin();
        $org->id = $origin->id;
        $org->order = $origin->order;
        $org->name = $origin->name;
        $org->department = ($origin->department)? $origin->department->name : '';
        return $org;
    }

    public function transformIndexApi(Api $api){
        $ap = new Api();
        $ap->id = $api->id;
        $ap->order = $api->order;
        $ap->name = $api->name;
        $ap->module = ($api->module)? $api->module->name : '';
        $ap->type = $api->type;
        return $ap;
    }
    
    public function transformIndexDepartment(Department $department){
        $dep = new Department();
        $dep->id = $department->id;
        $dep->order = $department->order;
        $dep->name = $department->name;
        $dep->color = $department->color;
        $dep->tags = $department->tags;
        return $dep;
    }
    
    public function transformIndexOrganization(Organization $organization){
        $org = new Organization();
        $org->id = $organization->id;
        $org->order = $organization->order;
        $org->name = $organization->name;
        $org->user = ($organization->user)? $organization->user->name : '';
        $org->sector = ($organization->sector)? $organization->sector->name : '';
        $org->country = ($organization->country)? $organization->country->name : '';
        $org->connected_since = $organization->connected_since;

        return $org;
    }

    public function transformIndexOrganizationPhone(OrganizationPhone $phone){
        $org = new OrganizationPhone();
        $org->id = $phone->id;
        $org->order = $phone->order;
        $org->phone = $phone->phone;
        $org->type = ($phone->type)? $phone->type->name : '';
        $org->sector = ($phone->serctor)? $phone->sector->name : '';
        $org->link = $phone->link;
        $org->organization = ($phone->organization)? $phone->organization->name : '';
        return $org;
    }
    
    public function transformIndexContact(Contact $contact){
        $cntct = new Contact();
        $cntct->id            = $contact->id;
        $cntct->order         = $contact->order;
        $cntct->name         = $contact->name;
        $cntct->business_email          = $contact->business_email;
        $cntct->business_position          = $contact->business_position;
        $cntct->private_email          = $contact->private_email;
        $cntct->sector          = $contact->sector;
        $cntct->country          = ($contact->country)? $contact->country->name : '';
        $cntct->organization  = ($contact->organization)? $contact->organization->name : '';
        $cntct->address_1 = $contact->address_1;
        $cntct->address_2 = $contact->address_2;
        $cntct->state = $contact->state;
        $cntct->po_box = $contact->po_box;
        $cntct->website = $contact->website;
        $cntct->linkedin = $contact->linkedin;
        $cntct->facebook = $contact->facebook;
        $cntct->twitter = $contact->twitter;
        $cntct->instagram = $contact->instagram;
        $cntct->pinterest = $contact->pinterest;
        return $cntct;
    }
    
    public function transformIndexContactPhone(ContactPhone $phone){
        $cntct = new ContactPhone();
        $cntct->id            = $phone->id;
        $cntct->order         = $phone->order;
        $cntct->contact         = ($phone->contact)? $phone->contact->name : '';
        $cntct->type          = ($phone->type)? $phone->type->name : '';
        $cntct->link          = $phone->link;
        $cntct->phone          = $phone->phone;
        return $cntct;
    }
    
    public function transformIndexApiArgument(ApiArgument $apiArgument){
        $ap         = new ApiArgument();
        $ap->id     = $apiArgument->id;
        $ap->order  = $apiArgument->order;
        $ap->key    = $apiArgument->key;
        $ap->required = $apiArgument->required;
        $ap->type   = $apiArgument->type;
        return $ap;
    }

    public function transformIndexMedia(Media $media){

        $m = new Media();

        $m->url = URL::asset( $media->url);
        $m->thumb = URL::asset( $media->url);
        $m->name = $media->name;
        $m->type = $media->kind;
        $m->id = $media->id;
        $m->tag = $media->folder;
        return $m;
    }



    public function transformIndexDomain(Domain $domain){

        $d = new Domain();
        $d->order = $domain->order;
        $d->id = $domain->id;
        $d->name = $domain->name;
        $d->type = $domain->type;
        $d->parent = $domain->parent == null ?'': $domain->parent->name;
        $d->sub_domains = $domain->subDomains();
        foreach($d->sub_domains as $sub){
            $sub->parent_name = $d->name;
        }

        return $d;
    }
    
    public function transformIndexCategory(Category $category){

        $d = new Category();
        $d->order = $category->order;
        $d->id = $category->id;
        $d->name = $category->name;
        $d->type = $category->type;
        $d->parent = $category->parent == null ?'': $category->parent->name;


        return $d;
    }
    public function transformIndexStatus(Status $status){
        $stat = new Status();
        $stat->id = $status->id;
        $stat->name = $status->name;
        $stat->module = ($status->module)? $status->module->name : '';
        return $stat;
    }

    public function transformIndexHosting(Hosting $hosting){
        $host = new Hosting();
        $host->id = $hosting->id;
        $host->name = $hosting->name;
//        $host->order= $hosting->order;
        $host->domain_management_id = ($hosting->domainManagement)? $hosting->domainManagement->name : '';
        $host->technology_id = ($hosting->technology)? $hosting->technology->name : '';
//        $host->contact_id = ($hosting->contact)? $hosting->contact->name : '';
//        $host->user_id = ($hosting->user)? $hosting->user->name : '';
//        $host->description = $hosting->description;
        $host->status_id = ($hosting->status)? $hosting->status->name : '';
        $host->type_id = ($hosting->domainType)? $hosting->domainType->name : '';
        $host->renewal_date = $hosting->renewal_date;
        return $host;
    }


    public function transformIndexTag(Tag $tag){

        $d = new Tag();
        $d->id = $tag->id;
        $d->order = $tag->order;
        $d->name = $tag->name;
        $d->locale = ($tag->language)? $tag->language->name : '';

        return $d;
    }

    public function changeImagePath($path){
        $path = str_replace('public\\','storage/',$path);
        $path = str_replace('public/','storage/',$path);

        return $path;
    }


}