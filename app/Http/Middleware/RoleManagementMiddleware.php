<?php

namespace App\Http\Middleware;

use App\Models\Module;
use App\Models\ParentModule;
use App\Models\Role;
use App\Models\RoleModules;
use App\Models\User;
use Closure;

class RoleManagementMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ( auth()->check()) {
           $roles = auth()->user()->roles;
            foreach($roles as $role){
                if($role->id == 1000){
                    $modules = Module::all();
                    $permissions = array();
                    foreach ($modules as $module ){
                        $rm = new RoleModules();
                        $rm->role_id = 1000;
                        $rm->module_id = $module->id;
                        $rm->show = true;
                        $rm->add = true;
                        $rm->edit = true;
                        $rm->delete = true;
                        $rm->edit_all = true;
                        $rm->delete_all = true;
                        $rm->show_all = true;
                        $permissions[] = $rm;
                    }
                    $request->permissions = $permissions;
                    $parent_module_ids = array();
                    $modules = Module::all();
                    foreach ($modules as $module ) {
                        foreach ($module->topParent() as $v){
                            if (!in_array($v, $parent_module_ids)) {
                                $parent_module_ids[] = $v;
                            }
                        }
                    }
                    $request->permission_role_id = 1000;
                    $request->permissions_parent_modules = $parent_module_ids;
                    return $next($request);
                }
                elseif ($role->id == 1){
                    $request->permission_role_id = 1;
                }
            }


            foreach($roles as $role){
                $request->permission_role = $role;
                $request->permissions = $role->modules;
                $parent_module_ids = array();
                $modules = Module::all();
                foreach ($modules as $module ) {
                    foreach ($module->topParent() as $v){
                        if (!in_array($v, $parent_module_ids)
                            && $module->isAllowedModule ( $role->id ) ) {
                            $parent_module_ids[] = $v;
                        }
                    }
                }
                $request->permissions_parent_modules = $parent_module_ids;
            }
        }
        return $next($request);
    }
}
