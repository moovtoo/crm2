<?php

namespace App\Http\Controllers\ProjectManagement\HostingSystem;

use App\Models\DomainManagement;
use App\Models\DomainType;
use App\Models\Module;
use App\Models\Organization;
use App\Models\Hosting;
use App\Models\Status;
use App\Models\Technology;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\DataTables;


class HostingController extends Controller
{



    public function index(Request $request){


        if ( $request->ajax() ){
            return DataTables::of($this->listModel(new Hosting()))->make(true);
        }
        $title = 'Hostings List';
        $add_button ='ADD A HOSTING';
        $filters = [
        ];
        $links = $this->parentLinks($this);
        $parent = $this->getParentModule($this);

        return view('project-management.hosting-system.hosting.hostings',compact('add_button','links','title','parent'));
    }



    public function show($hosting){

        $hosting = $this->findHostingById($hosting);
        $title = 'Hosting '.$hosting->name;
        $links = $this->parentLinks($this);
        $parent = $this->getParentModule($this);
        return view('project-management.hosting-system.hosting.hosting-show', compact('title','hosting','links','parent'));
    }


    public function create(){
        $module = $this->getModule($this);

        if(!$this->getModule($this)->isAuthModuleAdd(auth()->user()->roles))return redirect()->back();
        $model = new Hosting();
        $fields = $model->fields();
        $parent = $this->getParentModule($this);
        $organizations = Organization::all();
        $domainManagements = DomainManagement::all();
        $domainTypes= DomainType::all();
        $technologies= Technology::all();
//        $users = User::all();
        $statuses = Status::query()->where('module_id', $module->id)->get();
        $links = $this->parentLinks($this);
        return view('project-management.hosting-system.hosting.hosting',['title'=>'New Hosting','fields'=>$fields,'links'=>$links,
            'statuses'=>$statuses,'parent'=>$parent,'technologies'=>$technologies ,'domainManagements'=>$domainManagements,'domainTypes'=>$domainTypes]);
    }

    public function store(Request $request){
        $params = $request->except('_token');
        $params['slug'] = str_slug($request->input('name'));

        $hosting =  $this->createHosting($params);

        return ['status'=> 'success','hosting'=>$hosting];
    }

    public function edit($id){
        $module = $this->getModule($this);
        if ( request()->permissions_parent_modules == 1000 || $module->isAuthModuleEdit(auth()->user()->roles)){

        }else{
            return redirect()->back();
        }
        $hosting = $this->findHostingById($id);
        $model = new Hosting();
        $fields = $model->fields();
        $title = 'Update '.$hosting->name;
        $parent = $this->getParentModule($this);
        $domainManagements = DomainManagement::all();
        $domainTypes= DomainType::all();
        $technologies= Technology::all();

        $statuses = Status::query()->where('module_id', $module->id)->get();
        $links = $this->parentLinks($this);
        return view('project-management.hosting-system.hosting.hosting-update',['title'=>$title, 'fields'=>$fields,'technologies'=>$technologies,
            'links'=>$links,'hosting'=>$hosting,'parent'=>$parent,'domainManagements'=>$domainManagements,'domainTypes'=>$domainTypes
            ,'statuses'=>$statuses]);
    }

    public function editAuthorise($id){
        $module = $this->getModule($this);
        if ( request()->permissions_parent_modules == 1000 || $module->isAuthModuleEdit(auth()->user()->roles)){
            return response()->json(['status'=>'success']);
        }else{
            return response()->json(['status'=>'error','message'=> 'you are not authorised to do this action']);
        }
    }


    public function update(Request $request, $hosting){
        $params = $request->except('_token','users');
        $params['slug'] = str_slug($request->input('name'));

        $this->updateHosting($params, $hosting);
        $hosting = $this->findHostingById($hosting);
//        $hosting->users()->attach($request->input('users'));

        return ['status'=> 'success','hosting'=>$hosting];
    }

    public function destroy ( $hosting){
        if ( $this->getModule($this)->isAuthModuleDelete(auth()->user()->roles) ){
            if( $this->deleteHosting($hosting) ){
                return response()->json(['status'=>'success']);
            }else{
                return response()->json(['status'=>'error','message'=> 'something went wrong']);
            }
        }else{
            return response()->json(['status'=>'error','message'=> 'you are not authorised to do this action']);
        }

    }

    public function up($hosting){
        return Hosting::find($hosting)->moveOrderUp();

    }

    public function down($hosting){
        return Hosting::find($hosting)->moveOrderDown();
    }


    private function findHostingById(int $id) :Hosting
    {
        return Hosting::query()->where('id',$id)->get()->first();
    }


    private function createHosting(array $params)
    {
//        try {
            $module = new Hosting($params);
            $module->save();
            return $module;
//        } catch (\Exception $e) {
//            return null;
//        }
    }

    private function updateHosting(array $params, int $id)
    {
        try {
            return Hosting::query()->find($id)->update($params);
        } catch (\Exception $e) {
            return false;
        }
    }

    private function deleteHosting(int $id)
    {

        try {
            return Hosting::query()->find($id)->delete();
        } catch (\Exception $e) {
            return false;
        }
    }

    public function parentLinks($class){
        $module = Module::query()->where('controller_name',$this->getClassSubstring($class))->get()->first();
        $parent = $module->parent;
        $links[$module->name] = '';
        if($parent == null){
            return [];
        }else{

            do{
                $links[$parent->name] = '';
                $parent = $parent->parent;
            }while($parent->parent_id != null);
            $links[$parent->name] = $parent->slug;
        }
        return array_reverse($links);
    }


}
