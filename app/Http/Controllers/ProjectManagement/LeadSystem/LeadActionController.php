<?php

namespace App\Http\Controllers\ProjectManagement\LeadSystem;

use App\Models\Contact;
use App\Models\ContactType;
use App\Models\Lead;
use App\Models\Module;
use App\Models\Organization;
use App\Models\LeadAction;
use App\Models\Origin;
use App\Models\Status;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\DataTables;

class LeadActionController extends Controller
{



    public function index(Request $request){


        if ( $request->ajax() ){
            if($request->has('lead_id')){

                return DataTables::of($this->listModel(new LeadAction(), [['lead_id',$request->get('lead_id')]]))->make(true);
            }

            if($request->has('type_id') and $request->has('status_id')){

                $obs = LeadAction::query()
//                    ->where('created_date','>=',Carbon::now()->toDateString())
                    ->where('type_id',$request->get('type_id'))
                    ->where('status_id',$request->get('status_id'))->get();
                $result = $obs->map( function ($module){
                    return $this->transformIndexLeadAction2($module);
                })->all();
                return DataTables::of($result)->make(true);
            }
            return DataTables::of($this->listModel(new LeadAction()))->make(true);

        }

        $model = new LeadAction();
        $title = 'Event List';
        $fields = $model->fields();
//        $links = $this->parentLinks($this);
        $add_button ='Add Event';
        $filters = [
            'type' => ['Action', 'Sub']
        ];

        return view('configuration.lead-action.lead-action',compact('fields','title','add_button','filters'));
    }



    public function show($leadAction){

        $leadAction = $this->findLeadActionById($leadAction);
        $title = 'LeadAction '.$leadAction->name;
        $parent = $this->getParentModule(new LeadController());
//        $links = $this->parentLinks($this);
        return view('project-management.lead-system.lead-action.lead-action-show', compact('title','leadAction','parent'));
    }


    public function create( Request $request){
        $module = $this->getModule(new LeadController());

        if(!$this->getModule(new LeadController())->isAuthModuleAdd(auth()->user()->roles))return redirect()->back();
        $model = new LeadAction();
        $fields = $model->fields();
        $parent = $this->getParentModule(new LeadController());
        $organizations = Organization::query()->orderBy('name','asc')->get();
        $users = User::query()->orderBy('name','asc')->get();
        $leads= Lead::query()->orderBy('name','asc')->get();
        $statuses = Status::query()->where('module_id', 10)->orderBy('name','asc')->get();
        $contacts= Contact::query()->orderBy('name','asc')->get();
        $origins = Origin::query()->orderBy('name','asc')->get();
        $lead = Lead::query()->where('id', $request->get('lead_id'))->get()->first();
//        $links = $this->parentLinks($this);
        $contactTypes= ContactType::query()->where('module_id' ,10)->get();
        return view('project-management.lead-system.lead-action.lead-action',['title'=>'New Event','lead'=>$lead,'contactTypes'=>$contactTypes,'leads'=>$leads,'origins'=>$origins,'contacts'=>$contacts,'fields'=>$fields,'statuses'=>$statuses,'parent'=>$parent, 'organizations'=>$organizations, 'users'=>$users]);
    }

    public function store(Request $request){
        $params = $request->except('_token','attachment');
        $params['slug'] = str_slug($request->input('name'));
        if ( $request->has('attachment') and $request->input('attachment') != null){
            // $image = $this->uploadOne($request->file('photo'),'article');
            $params['attachment'] = $this->changeImagePath($request->input('attachment'));
        }

        $leadAction =  $this->createLeadAction($params);

        return ['status'=> 'success','leadAction'=>$leadAction];
    }

    public function edit($id){
        $module = $this->getModule(new LeadController());
        if ( request()->permissions_parent_modules == 1000 || $module->isAuthModuleEdit(auth()->user()->roles)){

        }else{
            return redirect()->back();
        }
        $leadAction = $this->findLeadActionById($id);
        $model = new LeadAction();
        $fields = $model->fields();
        $title = 'Update '.str_limit(strip_tags( $leadAction->note), $limit = 45, $end = '...');
        $parent = $this->getParentModule(new LeadController());
        $organizations = Organization::query()->orderBy('name','asc')->get();
        $users = User::query()->orderBy('name','asc')->get();
        $statuses = Status::query()->where('module_id', 10)->orderBy('name','asc')->get();
        $contacts = Contact::query()->orderBy('name','asc')->get();
        $origins = Origin::query()->orderBy('name','asc')->get();
        $leads = Lead::query()->orderBy('name','asc')->get();
        $contactTypes= ContactType::query()->where('module_id' ,10)->orderBy('name','asc')->get();
//        $links = $this->parentLinks($this);
        return view('project-management.lead-system.lead-action.lead-action-update',['title'=>$title, 'contactTypes'=>$contactTypes,'fields'=>$fields,'leads'=>$leads,'origins'=>$origins,'contacts'=>$contacts, 'leadAction'=>$leadAction,'parent'=>$parent, 'organizations'=>$organizations,'statuses'=>$statuses, 'users'=>$users]);
    }

    public function editAuthorise($id){
        $module = $this->getModule(new LeadController());
        if ( request()->permissions_parent_modules == 1000 || $module->isAuthModuleEdit(auth()->user()->roles)){
            return response()->json(['status'=>'success']);
        }else{
            return response()->json(['status'=>'error','message'=> 'you are not authorised to do this action']);
        }
    }


    public function update(Request $request, $leadAction){
        $params = $request->except('_token','attachment');
        $params['slug'] = str_slug($request->input('name'));
        if ( $request->has('attachment') and $request->input('attachment') != null){
            // $image = $this->uploadOne($request->file('photo'),'article');
            $params['attachment'] = $this->changeImagePath($request->input('attachment'));
        }

        $this->updateLeadAction($params, $leadAction);
        $leadAction = $this->findLeadActionById($leadAction);

        return ['status'=> 'success','leadAction'=>$leadAction];
    }

    public function destroy ( $leadAction){
        if ( $this->getModule(new LeadController())->isAuthModuleDelete(auth()->user()->roles) ){
            if( $this->deleteLeadAction($leadAction) ){
                return response()->json(['status'=>'success']);
            }else{
                return response()->json(['status'=>'error','message'=> 'something went wrong']);
            }
        }else{
            return response()->json(['status'=>'error','message'=> 'you are not authorised to do this action']);
        }

    }

    public function up($leadAction){
        return LeadAction::find($leadAction)->moveOrderUp();

    }

    public function down($leadAction){
        return LeadAction::find($leadAction)->moveOrderDown();
    }


    private function findLeadActionById(int $id) :LeadAction
    {
        return LeadAction::query()->where('id',$id)->get()->first();
    }


    private function createLeadAction(array $params)
    {
//        try {
            $module = new LeadAction($params);
            $module->save();
            return $module;
//        } catch (\Exception $e) {
//            return null;
//        }
    }

    private function updateLeadAction(array $params, int $id)
    {
        try {
            return LeadAction::query()->find($id)->update($params);
        } catch (\Exception $e) {
            return false;
        }
    }

    private function deleteLeadAction(int $id)
    {

        try {
            return LeadAction::query()->find($id)->delete();
        } catch (\Exception $e) {
            return false;
        }
    }
//    public function parentLinks($class){
//        $module = Module::query()->where('controller_name',$this->getClassSubstring($class))->get()->first();
//        $parent = $module->parent;
//        $links[$module->name] = '';
//        if($parent == null){
//            return [];
//        }else{
//
//            do{
//                $links[$parent->name] = '';
//                $parent = $parent->parent;
//            }while($parent->parent_id != null);
//            $links[$parent->name] = $parent->slug;
//        }
//        return array_reverse($links);
//    }

}
