<?php

namespace App\Http\Controllers\ProjectManagement\LeadSystem;

use App\Models\Contact;
use App\Models\LeadAction;
use App\Models\Module;
use App\Models\Organization;
use App\Models\Lead;
use App\Models\Origin;
use App\Models\Status;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\DataTables;

class LeadController extends Controller
{

    public function __construct()
    {

    }
    public function trans(Lead $lead){
        $led = new Lead();
        $led->id = $lead->id;
        $led->name = $lead;
        $led->order= $lead->order;
        $led->organization_id = ($lead->organization)? $lead->organization->name : '';
        $led->origin_id = ($lead->origin)? $lead->origin->name : '';
        $led->contact_id = ($lead->UpComingleadAction())? $lead->UpComingleadAction()->contact: '';
//        $led->contact_id = ($lead->contact)? $lead->contact : '';
        $led->user_id = ($lead->user)? $lead->user->name : '';
        $led->description = $lead->description;
        $led->status_id = ($lead->UpComingleadAction())? $lead->UpComingleadAction()->status->name : '';
        $led->type_id = ($lead->UpComingleadAction())? $lead->UpComingleadAction()->type->name : '';
//        $led->type_id = ($lead->type)? $lead->type->name : '';
//        $led->created_date = $lead->created_date;
        $led->created_date = ($lead->UpComingleadAction())? $lead->UpComingleadAction()->created_date : '';
        return $led;
    }
    public function trans2(Lead $lead){
        $led = new Lead();
        $led->id = $lead->id;
        $led->name = $lead;
        $led->order= $lead->order;
        $led->organization_id = ($lead->organization)? $lead->organization->name : '';
        $led->origin_id = ($lead->origin)? $lead->origin->name : '';
        $led->contact_id = ($lead->UpComingleadAction2())? $lead->UpComingleadAction2()->contact: '';
//        $led->contact_id = ($lead->contact)? $lead->contact : '';
        $led->user_id = ($lead->user)? $lead->user->name : '';
        $led->description = $lead->description;
        $led->status_id = ($lead->UpComingleadAction2())? $lead->UpComingleadAction2()->status->name : '';
        $led->type_id = ($lead->UpComingleadAction2())? $lead->UpComingleadAction2()->type->name : '';
//        $led->type_id = ($lead->type)? $lead->type->name : '';
        $led->created_date = ($lead->UpComingleadAction2())? $lead->UpComingleadAction2()->created_date : '';
//        $led->created_date = $lead->created_date ;
        return $led;
    }

    public function index(Request $request){


        if ( $request->ajax() ){
            if($request->has('status_id') and $request->has('type_id')){
                $status_id[] = $request->input('status_id');
                $type_id = $request->input('type_id');
                $results = Lead::query()->whereHas('leadAction', function($q) use($status_id,$type_id) {
                    $q->where('lead_actions.status_id', 10);
                    $q->where('lead_actions.type_id', $type_id);
                    $q->orwhere('lead_actions.status_id' , 20);
                    $q->where('lead_actions.type_id', $type_id);
                })->get();
                $result = $results->map( function ($module){
                    return $this->trans($module);
                })->all();

                return DataTables::of($result)->make(true);

            }
            elseif($request->has('status_id')){
                $status_id[] = $request->input('status_id');
//                $type_id = $request->input('type_id');
                $results = Lead::query()->whereHas('leadAction', function($q) use($status_id) {
                    $q->where('lead_actions.status_id' , 10);
                    $q->where('lead_actions.type_id', '!=',7);
                    $q->where('created_date','>=',Carbon::now()->addDay(-90)->toDateString());
                    $q->orwhere('lead_actions.status_id' , 12);
                    $q->where('lead_actions.type_id', '!=',7);
                    $q->where('created_date','>=',Carbon::now()->addDay(-90)->toDateString());
                    $q->orwhere('lead_actions.status_id' , 20);
                    $q->where('lead_actions.type_id', '!=',7);
                    $q->where('created_date','>=',Carbon::now()->addDay(-90)->toDateString());
                    $q->whereNull('deleted_at');
                    $q->orderBy('created_date','asc');
                })
                    ->get();
                $result = $results->map( function ($module){
                    return $this->trans2($module);
                })->all();

                //dd($results);

                return DataTables::of($result)->make(true);

            }

            return DataTables::of($this->listModel(new Lead()))->make(true);
        }
        $title = 'Leads List';
        $add_button ='ADD A LEAD';
        $filters = [
        ];
        $links = $this->parentLinks($this);
        $parent = $this->getParentModule($this);
        return view('project-management.lead-system.lead.leads',compact('add_button','links','title','parent'));
    }




    public function show($lead){

        $lead = $this->findLeadById($lead);
        $title = 'Lead '.$lead->name;
        $parent = $this->getParentModule($this);
        return view('project-management.lead-system.lead.lead-show', compact('title','lead','links','parent'));
    }


    public function create(){
        $module = $this->getModule($this);

        if(!$this->getModule($this)->isAuthModuleAdd(auth()->user()->roles))return redirect()->back();
        $model = new Lead();
        $fields = $model->fields();
        $parent = $this->getParentModule($this);
        $organizations = Organization::query()->orderBy('name','asc')->get();
        $users =  User::where('id','!=',1)->orderBy('name','asc')->get();
        $statuses = Status::query()->where('module_id', 11)->orderBy('name','asc')->get();
        $contacts= Contact::query()->orderBy('name','asc')->get();
        $origins = Origin::query()->orderBy('name','asc')->get();
        $links = $this->parentLinks($this);
        return view('project-management.lead-system.lead.lead',['title'=>'New Lead','links'=>$links,'origins'=>$origins,'contacts'=>$contacts,'fields'=>$fields,'statuses'=>$statuses,'parent'=>$parent, 'organizations'=>$organizations, 'users'=>$users]);
    }

    public function store(Request $request){
        $params = $request->except('_token');
        $params['slug'] = str_slug($request->input('name'));

        $lead =  $this->createLead($params);

        return ['status'=> 'success','lead'=>$lead];
    }

    public function edit($id){
        $module = $this->getModule($this);
        if ( request()->permissions_parent_modules == 1000 || $module->isAuthModuleEdit(auth()->user()->roles)){

        }else{
            return redirect()->back();
        }
        $lead = $this->findLeadById($id);
        $model = new Lead();
        $fields = $model->fields();
        $title = 'Update '.$lead->name;
        $parent = $this->getParentModule($this);
        $organizations = Organization::query()->orderBy('name','asc')->get();
        $users =  User::where('id','!=',1)->orderBy('name','asc')->get();
        $statuses = Status::query()->where('module_id', 11)->orderBy('name','asc')->get();
        $contacts = Contact::query()->orderBy('name','asc')->get();
        $origins = Origin::query()->orderBy('name','asc')->get();
        $links = $this->parentLinks($this);

        return view('project-management.lead-system.lead.lead-update',['title'=>$title, 'fields'=>$fields,'links'=>$links,'origins'=>$origins,'contacts'=>$contacts, 'lead'=>$lead,'parent'=>$parent, 'organizations'=>$organizations,'statuses'=>$statuses, 'users'=>$users]);
    }

    public function editAuthorise($id){
        $module = $this->getModule($this);
        if ( request()->permissions_parent_modules == 1000 || $module->isAuthModuleEdit(auth()->user()->roles)){
            return response()->json(['status'=>'success']);
        }else{
            return response()->json(['status'=>'error','message'=> 'you are not authorised to do this action']);
        }
    }


    public function update(Request $request, $lead){
        $params = $request->except('_token');
        $params['slug'] = str_slug($request->input('name'));

        $this->updateLead($params, $lead);

        return ['status'=> 'success','lead'=>$lead];
    }

    public function destroy ( $lead){
        if ( $this->getModule($this)->isAuthModuleDelete(auth()->user()->roles) ){
            if( $this->deleteLead($lead) ){
                return response()->json(['status'=>'success']);
            }else{
                return response()->json(['status'=>'error','message'=> 'something went wrong']);
            }
        }else{
            return response()->json(['status'=>'error','message'=> 'you are not authorised to do this action']);
        }

    }

    public function up($lead){
        return Lead::find($lead)->moveOrderUp();

    }

    public function down($lead){
        return Lead::find($lead)->moveOrderDown();
    }


    private function findLeadById(int $id) :Lead
    {
        return Lead::query()->where('id',$id)->get()->first();
    }


    private function createLead(array $params)
    {
        try {
            $module = new Lead($params);
            $module->save();
            return $module;
        } catch (\Exception $e) {
            return null;
        }
    }

    private function updateLead(array $params, int $id)
    {
        try {
            return Lead::query()->find($id)->update($params);
        } catch (\Exception $e) {
            return false;
        }
    }

    private function deleteLead(int $id)
    {

        try {
            return Lead::query()->find($id)->delete();
        } catch (\Exception $e) {
            return false;
        }
    }

    public function parentLinks($class){
        $module = Module::query()->where('controller_name',$this->getClassSubstring($class))->get()->first();
        $parent = $module->parent;
        $links[$module->name] = '';
        if($parent == null){
            return [];
        }else{

            do{
                $links[$parent->name] = '';
                $parent = $parent->parent;
            }while($parent->parent_id != null);
            $links[$parent->name] = $parent->slug;
        }
        return array_reverse($links);
    }
}
