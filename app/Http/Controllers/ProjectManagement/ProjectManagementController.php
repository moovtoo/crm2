<?php

namespace App\Http\Controllers\ProjectManagement;

use App\Http\Controllers\Controller;

class ProjectManagementController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    
    public function show()
    {
        $parent = $this->getParentModule($this);
        return view('project-management.project-management',compact('parent'));
    }

}
