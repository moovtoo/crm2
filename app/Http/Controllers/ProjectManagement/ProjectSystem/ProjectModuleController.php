<?php

namespace App\Http\Controllers\ProjectManagement\ProjectSystem;

use App\Models\Contact;
use App\Models\ContactType;
use App\Models\Project;
use App\Models\Module;
use App\Models\Organization;
use App\Models\ProjectModule;
use App\Models\Origin;
use App\Models\ProjectTask;
use App\Models\Role;
use App\Models\Status;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\DataTables;

class ProjectModuleController extends Controller
{



    public function index(Request $request){


        if ( $request->ajax() ){
            if($request->has('project_id')){
                if( $request->permission_role_id == 1000 || $request->permission_role_id == 1)
                return DataTables::of($this->listModel(new ProjectModule(), [['project_id',$request->get('project_id')]]))->make(true);
                else
                    $id = Auth::id();
                $tasks = ProjectTask::query()->whereHas('users', function ($q) use ($id){
                    $q->where('project_task_users.user_id' , $id);
                })->get();
                $modules = ProjectModule::query()->whereHas('users', function ($q) use ($id) {
                    $q->where('project_module_users.user_id', $id);
                })->get();

                $projectModules = array();
                $projectModule_ids = array();
                foreach ( $tasks as $task ){
                    if ( !in_array( $task->projectModule->id,$projectModule_ids)){
                        $projectModules[] =  $this->transformIndexProjectModule($task->projectModule);
                        $projectModule_ids[] = $task->projectModule->id;
                    }
                }
                foreach ( $modules as $module ){
                    if ( !in_array( $module->id,$projectModule_ids)){
                        $projectModules[] =  $this->transformIndexProjectModule($module);
                        $projectModule_ids[] = $module->id;
                    }
                }
                return DataTables::of($projectModules,[['project_id',$request->get('project_id')]])->make(true);
            }

            return DataTables::of($this->listModel(new ProjectModule()))->make(true);

        }

        $model = new ProjectModule();
        $title = 'Event List';
        $fields = $model->fields();
//        $links = $this->parentLinks($this);
        $add_button ='Add Event';
        $filters = [
            'type' => ['Action', 'Sub']
        ];

        return view('project-management.project-system.project-module.project-module',compact('fields','title','add_button','filters'));
    }



    public function show($projectModule){

        $projectModule = $this->findProjectModuleById($projectModule);
        $title = 'ProjectModule '.$projectModule->name;
        $parent = $this->getParentModule(new ProjectController());
//        $links = $this->parentLinks($this);
        return view('project-management.project-system.project-module.project-module-show', compact('title','projectModule','parent'));
    }


    public function create( Request $request){
        $module = $this->getModule(new ProjectController());

        if(!$this->getModule(new ProjectController())->isAuthModuleAdd(auth()->user()->roles))return redirect()->back();
        $model = new ProjectModule();
        $fields = $model->fields();
        $parent = $this->getParentModule(new ProjectController());
        $ids = Role::query()->where('name', 'like', '%TeamLeader%')->orWhere('name', 'like', '%TeamMember%')->pluck('id');
        $users = User::query()->whereHas('roles', function($q) use($ids){
            $q->whereIn('roles.id', $ids);
        })->orderBy('name','asc')->get();
//        $projects= Project::query()->orderBy('name','asc')->get();
        $statuses = Status::query()->where('module_id', 14)->orderBy('name','asc')->get();
        $project = Project::query()->where('id', $request->get('project_id'))->get()->first();
        $links = $this->parentLinks($this);
        return view('project-management.project-system.project-module.project-module',['title'=>'New Module',
            'project'=>$project,'links'=>$links,'fields'=>$fields,'statuses'=>$statuses, 'parent'=>$parent, 'users'=>$users ,'module'=>$module]);
    }

    public function store(Request $request){
        $params = $request->except('_token','users');
        $params['slug'] = str_slug($request->input('name'));


        $projectModule =  $this->createProjectModule($params);
        $projectModule->users()->attach($request->input('users'));

        return ['status'=> 'success','projectModule'=>$projectModule];
    }

    public function edit($id){
        $module = $this->getModule(new ProjectController());
        if ( request()->permissions_parent_modules == 1000 || $module->isAuthModuleEdit(auth()->user()->roles)){

        }else{
            return redirect()->back();
        }
        $projectModule = $this->findProjectModuleById($id);
        $model = new ProjectModule();
        $fields = $model->fields();
        $title = 'Update '.str_limit(strip_tags( $projectModule->name), $limit = 45, $end = '...');
        $parent = $this->getParentModule(new ProjectController());
        $ids = Role::query()->where('name', 'like', '%TeamLeader%')->orWhere('name', 'like', '%TeamMember%')->pluck('id');
        $users = User::query()->whereHas('roles', function($q) use($ids){
            $q->whereIn('roles.id', $ids);
        })->orderBy('name','asc')->get();
        $statuses = Status::query()->where('module_id', 14)->orderBy('name','asc')->get();
        $projects = Project::query()->orderBy('name','asc')->get();
        $links = $this->parentLinks($this);
        return view('project-management.project-system.project-module.project-module-update',['title'=>$title,
            'fields'=>$fields,'projects'=>$projects,'links'=>$links, 'projectModule'=>$projectModule,'parent'=>$parent,
            'statuses'=>$statuses, 'users'=>$users,'module'=>$module]);
    }

    public function editAuthorise($id){
        $module = $this->getModule(new ProjectController());
        if ( request()->permissions_parent_modules == 1000 || $module->isAuthModuleEdit(auth()->user()->roles)){
            return response()->json(['status'=>'success']);
        }else{
            return response()->json(['status'=>'error','message'=> 'you are not authorised to do this action']);
        }
    }


    public function update(Request $request, $projectModule){
        $params = $request->except('_token','users');
        $params['slug'] = str_slug($request->input('name'));

        $this->updateProjectModule($params, $projectModule);
        $projectModule = $this->findProjectModuleById($projectModule);
        $projectModule->users()->sync($request->input('users'));

        return ['status'=> 'success','projectModule'=>$projectModule];
    }

    public function destroy ( $projectModule){
        if ( $this->getModule(new ProjectController())->isAuthModuleDelete(auth()->user()->roles) ){
            if( $this->deleteProjectModule($projectModule) ){
                return response()->json(['status'=>'success']);
            }else{
                return response()->json(['status'=>'error','message'=> 'something went wrong']);
            }
        }else{
            return response()->json(['status'=>'error','message'=> 'you are not authorised to do this action']);
        }

    }

    public function up($projectModule){
        return ProjectModule::find($projectModule)->moveOrderUp();

    }

    public function down($projectModule){
        return ProjectModule::find($projectModule)->moveOrderDown();
    }


    private function findProjectModuleById(int $id) :ProjectModule
    {
        return ProjectModule::query()->where('id',$id)->get()->first();
    }


    private function createProjectModule(array $params)
    {
//        try {
        $module = new ProjectModule($params);
        $module->save();
        return $module;
//        } catch (\Exception $e) {
//            return null;
//        }
    }

    private function updateProjectModule(array $params, int $id)
    {
        try {
            return ProjectModule::query()->find($id)->update($params);
        } catch (\Exception $e) {
            return false;
        }
    }

    private function deleteProjectModule(int $id)
    {

        try {
            return ProjectModule::query()->find($id)->delete();
        } catch (\Exception $e) {
            return false;
        }
    }
    public function parentLinks($class){
        $module = Module::query()->where('controller_name',$this->getClassSubstring($class))->get()->first();
        $parent = $module->parent;
        $links[$module->name] = '';
        if($parent == null){
            return [];
        }else{

            do{
                $links[$parent->name] = '';
                $parent = $parent->parent;
            }while($parent->parent_id != null);
            $links[$parent->name] = $parent->slug;
        }
        return array_reverse($links);
    }

}
