<?php

namespace App\Http\Controllers\ProjectManagement\ProjectSystem;

use App\Models\Project;
use App\Models\ProjectModule;
use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProjectDashBoardController extends Controller
{
    //
         public function index(Request $request){

            $projects = Project::all();
//           $title = 'Project '.$project->name;
            $parent = $this->getParentModule($this);
             $ids = Role::query()->where('name', 'like', '%TeamLeader%')
                 ->orWhere('name', 'like', '%TeamMember%')
                 ->orWhere('name', 'like', '%ClientLeader%')
                 ->pluck('id');
             $users = User::query()->whereHas('roles', function($q) use($ids){
                 $q->whereIn('roles.id', $ids);
             })->orderBy('name','asc')->get();
             $projectModules = ProjectModule::all();
            return view('project-management.project-system.project-dash-board', compact('title','projects','links','parent'
            ,'projectModules','users'));

         }

}


