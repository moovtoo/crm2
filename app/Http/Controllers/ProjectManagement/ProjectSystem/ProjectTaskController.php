<?php

namespace App\Http\Controllers\ProjectManagement\ProjectSystem;

use App\Models\Contact;
use App\Models\ContactType;
use App\Models\Project;
use App\Models\Module;
use App\Models\Organization;
use App\Models\ProjectModule;
use App\Models\ProjectTask;
use App\Models\Origin;
use App\Models\Role;
use App\Models\Status;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\DataTables;

class ProjectTaskController extends Controller
{



    public function index(Request $request){


        if ( $request->ajax() ){
            if($request->has('project_module_id')){
                if( $request->permission_role_id == 1000 || $request->permission_role_id == 1)
                return DataTables::of($this->listModel(new ProjectTask(), [['project_module_id',$request->get('project_module_id')]]))->make(true);
                else {
                    $id = Auth::id();
                    $tasks = ProjectTask::query()->whereHas('users', function ($q) use ($id) {
                        $q->where('project_task_users.user_id', $id);
                    })->get()->map(function ($module) {
                        return $this->transformIndexProjectTask($module);
                    })->all();
                    return DataTables::of($tasks, [['project_module_id', $request->get('project_module_id')]])->make(true);
                }
            }

            return DataTables::of($this->listModel(new ProjectTask()))->make(true);

        }

        $model = new ProjectTask();
        $title = 'Event List';
        $fields = $model->fields();
//        $links = $this->parentLinks($this);
        $add_button ='Add Event';
        $filters = [
            'type' => ['Action', 'Sub']
        ];

        return view('project-management.project-system.project-task.project-task',compact('fields','title','add_button','filters'));
    }



    public function show($projectTask){

        $projectTask = $this->findProjectTaskById($projectTask);
        $title = 'ProjectTask '.$projectTask->name;
        $parent = $this->getParentModule(new ProjectController());
//        $links = $this->parentLinks($this);
        return view('project-management.project-system.project-task.project-task-show', compact('title','projectTask','parent'));
    }


    public function create( Request $request){
        $module = $this->getModule(new ProjectController());
        if(!$this->getModule(new ProjectController())->isAuthModuleAdd(auth()->user()->roles))return redirect()->back();
        $model = new ProjectTask();
        $fields = $model->fields();
        $parent = $this->getParentModule(new ProjectController());
        $ids = Role::query()->where('name', 'like', '%TeamLeader%')->orWhere('name', 'like', '%TeamMember%')->pluck('id');
        $users = User::query()->whereHas('roles', function($q) use($ids){
            $q->whereIn('roles.id', $ids);
        })->orderBy('name','asc')->get();
        $statuses = Status::query()->where('module_id', 15)->orderBy('name','asc')->get();
        $projectModule = ProjectModule::query()->where('id', $request->get('project_module_id'))->get()->first();
        $links = $this->parentLinks($this);
        $title = 'New Task For '.str_limit(strip_tags( $projectModule->name), $limit = 45, $end = '...');
        return view('project-management.project-system.project-task.project-task',['title'=>$title,
            'projectModule'=>$projectModule,'fields'=>$fields,'statuses'=>$statuses, 'parent'=>$parent,
            'links'=>$links,'users'=>$users,'module'=>$module]);
    }

    public function store(Request $request){
        $params = $request->except('_token','users');
        $params['slug'] = str_slug($request->input('name'));


        $projectTask =  $this->createProjectTask($params);
        $projectTask->users()->attach($request->input('users'));

        return ['status'=> 'success','projectTask'=>$projectTask];
    }

    public function edit($id){
        $module = $this->getModule(new ProjectController());
        if ( request()->permissions_parent_modules == 1000 || $module->isAuthModuleEdit(auth()->user()->roles)){

        }else{
            return redirect()->back();
        }
        $projectTask = $this->findProjectTaskById($id);
        $model = new ProjectTask();
        $fields = $model->fields();
        $title = 'Update '.str_limit(strip_tags( $projectTask->name), $limit = 45, $end = '...');
        $parent = $this->getParentModule(new ProjectController());
        $ids = Role::query()->where('name', 'like', '%TeamLeader%')->orWhere('name', 'like', '%TeamMember%')->pluck('id');
        $statuses = Status::query()->where( 'module_id', 15)->orderBy('name','asc')->get();
        $users = User::query()->whereHas('roles', function($q) use($ids){
            $q->whereIn('roles.id', $ids);
        })->orderBy('name','asc')->get();
        $links = $this->parentLinks($this);
        return view('project-management.project-system.project-task.project-task-update',['title'=>$title,
            'fields'=>$fields, 'projectTask'=>$projectTask,'parent'=>$parent,
            'links'=>$links,'users'=>$users ,'module'=>$module,'statuses'=>$statuses]);
    }

    public function editAuthorise($id){
        $module = $this->getModule(new ProjectController());
        if ( request()->permissions_parent_modules == 1000 || $module->isAuthModuleEdit(auth()->user()->roles)){
            return response()->json(['status'=>'success']);
        }else{
            return response()->json(['status'=>'error','message'=> 'you are not authorised to do this action']);
        }
    }


    public function update(Request $request, $projectTask){
        $params = $request->except('_token','users');
        $params['slug'] = str_slug($request->input('name'));

        $this->updateProjectTask($params, $projectTask);
        $projectTask = $this->findProjectTaskById($projectTask);
        $projectTask->users()->sync($request->input('users'));

        return ['status'=> 'success','projectTask'=>$projectTask];
    }

    public function destroy ( $projectTask){
        if ( $this->getModule(new ProjectController())->isAuthModuleDelete(auth()->user()->roles) ){
            if( $this->deleteProjectTask($projectTask) ){
                return response()->json(['status'=>'success']);
            }else{
                return response()->json(['status'=>'error','message'=> 'something went wrong']);
            }
        }else{
            return response()->json(['status'=>'error','message'=> 'you are not authorised to do this action']);
        }

    }

    public function up($projectTask){
        return ProjectTask::find($projectTask)->moveOrderUp();

    }

    public function down($projectTask){
        return ProjectTask::find($projectTask)->moveOrderDown();
    }


    private function findProjectTaskById(int $id) :ProjectTask
    {
        return ProjectTask::query()->where('id',$id)->get()->first();
    }


    private function createProjectTask(array $params)
    {
//        try {
        $module = new ProjectTask($params);
        $module->save();
        return $module;
//        } catch (\Exception $e) {
//            return null;
//        }
    }

    private function updateProjectTask(array $params, int $id)
    {
        try {
            return ProjectTask::query()->find($id)->update($params);
        } catch (\Exception $e) {
            return false;
        }
    }

    private function deleteProjectTask(int $id)
    {

        try {
            return ProjectTask::query()->find($id)->delete();
        } catch (\Exception $e) {
            return false;
        }
    }
    public function parentLinks($class){
        $module = Module::query()->where('controller_name',$this->getClassSubstring($class))->get()->first();
        $parent = $module->parent;
        $links[$module->name] = '';
        if($parent == null){
            return [];
        }else{

            do{
                $links[$parent->name] = '';
                $parent = $parent->parent;
            }while($parent->parent_id != null);
            $links[$parent->name] = $parent->slug;
        }
        return array_reverse($links);
    }

}
