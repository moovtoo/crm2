<?php

namespace App\Http\Controllers\ProjectManagement\ProjectSystem;

use App\Models\Contact;
use App\Models\Module;
use App\Models\Organization;
use App\Models\Project;
use App\Models\ProjectModule;
use App\Models\ProjectTask;
use App\Models\Role;
use App\Models\Status;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\DataTables;


class ProjectController extends Controller
{



    public function index(Request $request){


        if ( $request->ajax() ){
//            dd($request->permission_role_id);
            if( $request->permission_role_id == 1000 || $request->permission_role_id == 1)

            {
                return DataTables::of($this->listModel(new Project()))->make(true);
            }
            else {
                $id = Auth::id();
                $tasks = ProjectTask::query()->whereHas('users', function ($q) use ($id) {
                    $q->where('project_task_users.user_id', $id);
                })->get();

                $modules = ProjectModule::query()->whereHas('users', function ($q) use ($id) {
                    $q->where('project_module_users.user_id', $id);
                })->get();

                $projects = array();
                $project_ids = array();
                foreach ($tasks as $task) {
                    if (!in_array($task->projectModule->project->id, $project_ids)) {
                        $projects[] = $this->transformIndexProject($task->projectModule->project);
                        $project_ids[] = $task->projectModule->project->id;
                    }
                }
                foreach ($modules as $module) {
                    if (!in_array($module->project->id, $project_ids)) {
                        $projects[] = $this->transformIndexProject($module->project);
                        $project_ids[] = $module->project->id;
                    }
                }
                return DataTables::of($projects)->make(true);
            }
        }
        $title = 'Projects List';
        $add_button ='ADD A PROJECT';
        $filters = [
        ];
        $links = $this->parentLinks($this);
        $parent = $this->getParentModule($this);

        return view('project-management.project-system.project.projects',compact('add_button','links','title','parent','projects'));
    }



    public function show($project){


    }


    public function create(){
        $module = $this->getModule($this);

        if(!$this->getModule($this)->isAuthModuleAdd(auth()->user()->roles))return redirect()->back();
        $model = new Project();
        $fields = $model->fields();
        $parent = $this->getParentModule($this);
        $organizations = Organization::all();
        $ids = Role::query()->where('name', 'like', '%ClientLeader%')->pluck('id');
        $users = User::query()->whereHas('roles', function($q) use($ids){
            $q->whereIn('roles.id', $ids);
        })->orderBy('name','asc')->get();
        $contacts= Contact::query()->orderBy('name','asc')->get();
        $statuses = Status::query()->where('module_id', $module->id)->get();
        $links = $this->parentLinks($this);
        return view('project-management.project-system.project.project',['title'=>'New Project','fields'=>$fields,'links'=>$links,'contacts'=>$contacts
            ,'statuses'=>$statuses,'parent'=>$parent, 'organizations'=>$organizations, 'users'=>$users]);
    }

    public function store(Request $request){
        $params = $request->except('_token');
        $params['slug'] = str_slug($request->input('name'));

        $project =  $this->createProject($params);

        return ['status'=> 'success','project'=>$project];
    }

    public function edit($id){
        $module = $this->getModule($this);
        if ( request()->permissions_parent_modules == 1000 || $module->isAuthModuleEdit(auth()->user()->roles)){

        }else{
            return redirect()->back();
        }
        $project = $this->findProjectById($id);
        $model = new Project();
        $fields = $model->fields();
        $title = 'Update '.$project->name;
        $parent = $this->getParentModule($this);
        $organizations = Organization::all();
        $idsProject = Role::query()->where('name', 'like', '%TeamLeader%')
            ->orWhere('name', 'like', '%TeamMember%')
            ->orWhere('name', 'like', '%ClientLeader%')
            ->pluck('id');
        $usersProject = User::query()->whereHas('roles', function($q) use($idsProject){
            $q->whereIn('roles.id', $idsProject);
        })->orderBy('name','asc')->get();
        $ids = Role::query()->where('name', 'like', '%ClientLeader%')->pluck('id');
        $users = User::query()->whereHas('roles', function($q) use($ids){
            $q->whereIn('roles.id', $ids);
        })->orderBy('name','asc')->get();
        $contacts = Contact::query()->where('organization_id',$project->organization_id)->get();
        $statuses = Status::query()->where('module_id', $module->id)->get();
        $links = $this->parentLinks($this);
        return view('project-management.project-system.project.project-update',['title'=>$title, 'fields'=>$fields,
            'contacts'=>$contacts,'links'=>$links, 'project'=>$project,'parent'=>$parent,
            'organizations'=>$organizations,'statuses'=>$statuses, 'users'=>$users,'usersProject'=>$usersProject]);
    }

    public function editAuthorise($id){
        $module = $this->getModule($this);
        if ( request()->permissions_parent_modules == 1000 || $module->isAuthModuleEdit(auth()->user()->roles)){
            return response()->json(['status'=>'success']);
        }else{
            return response()->json(['status'=>'error','message'=> 'you are not authorised to do this action']);
        }
    }


    public function update(Request $request, $project){
        $params = $request->except('_token','users');
        $params['slug'] = str_slug($request->input('name'));

        $this->updateProject($params, $project);
        $project = $this->findProjectById($project);
        $project->users()->attach($request->input('users'));

        return ['status'=> 'success','project'=>$project];
    }

    public function destroy ( $project){
        if ( $this->getModule($this)->isAuthModuleDelete(auth()->user()->roles) ){
            if( $this->deleteProject($project) ){
                return response()->json(['status'=>'success']);
            }else{
                return response()->json(['status'=>'error','message'=> 'something went wrong']);
            }
        }else{
            return response()->json(['status'=>'error','message'=> 'you are not authorised to do this action']);
        }

    }

    public function up($project){
        return Project::find($project)->moveOrderUp();

    }

    public function down($project){
        return Project::find($project)->moveOrderDown();
    }


    private function findProjectById(int $id) :Project
    {
        return Project::query()->where('id',$id)->get()->first();
    }


    private function createProject(array $params)
    {
        try {
            $module = new Project($params);
            $module->save();
            return $module;
        } catch (\Exception $e) {
            return null;
        }
    }

    private function updateProject(array $params, int $id)
    {
        try {
            return Project::query()->find($id)->update($params);
        } catch (\Exception $e) {
            return false;
        }
    }

    private function deleteProject(int $id)
    {

        try {
            return Project::query()->find($id)->delete();
        } catch (\Exception $e) {
            return false;
        }
    }

    public function parentLinks($class){
        $module = Module::query()->where('controller_name',$this->getClassSubstring($class))->get()->first();
        $parent = $module->parent;
        $links[$module->name] = '';
        if($parent == null){
            return [];
        }else{

            do{
                $links[$parent->name] = '';
                $parent = $parent->parent;
            }while($parent->parent_id != null);
            $links[$parent->name] = $parent->slug;
        }
        return array_reverse($links);
    }


}
