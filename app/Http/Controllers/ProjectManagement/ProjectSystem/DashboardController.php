<?php

namespace App\Http\Controllers\ProjectManagement\ProjectSystem;

use App\Models\Lead;
use App\Models\LeadAction;
use App\Models\Module;
use App\Models\ParentModule;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\DataTables;

class DashboardController extends Controller
{

    public function index(Request $request){


        if ( $request->ajax() ){
            return DataTables::of($this->listModel(new LeadAction()))->make(true);
        }
        $title = 'Dashboard';
        $add_button ='ADD ';
        $filters = [
        ];
//        $leadActions = LeadAction::all();
        $leads= Lead::all();

        $parent = ParentModule::query()->where('slug','project-management')->first();
        return view('project-management.project-system.dashboard',compact('add_button','title','parent','leads'));
    }


    public function parentLinks($class){
        $module = Module::query()->where('controller_name',$this->getClassSubstring($class))->get()->first();
        $parent = $module->parent;
        $links[$module->name] = '';
        if($parent == null){
            return [];
        }else{

            do{
                $links[$parent->name] = '';
                $parent = $parent->parent;
            }while($parent->parent_id != null);
            $links[$parent->name] = $parent->slug;
        }
        return array_reverse($links);
    }

}
