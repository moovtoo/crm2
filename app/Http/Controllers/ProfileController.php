<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;

class ProfileController extends Controller
{
    //




    public function edit()
    {
        $user = Auth()->user();
        return view('user.edit-profile',compact('user'));
    }


    public function update(Request $request){
        $params = $request->except('_token','password','photo');

        $validator = Validator::make($request->all(), [
            'password' => 'required_with:old_password|nullable|min:6|confirmed',
        ]);
        if ($validator->fails()) {
            return ['status' => 'error', 'messages' => $validator->errors()->getMessages()];
        }
        $u = Auth()->user();
        if ($request->input('old_password') != null && !Hash::check( $request->input('old_password'),$u->password )) {
            return ['status' => 'error', 'messages' => ["old_password" => 'Password not match']];
        }else if($request->input('password') != null){
            $params['password'] =  Hash::make($request->input('password'));
        }
        if ( $request->has('photo') and $request->input('photo') != null ){
            $params['photo'] = $this->changeImagePath($request->input('photo'));
        }


        $this->updateUser($params);
        return ['status' => 'success'];
    }

    private function updateUser(array $params)
    {
        try {
            return Auth()->user()->update($params);
        } catch (\Exception $e) {
            return false;
        }
    }

    public function removeImage(){
        try {
            $user =Auth()->user();
            $user->photo = null;
            $user->save();
            return redirect()->back();
        } catch (\Exception $e) {
            return redirect()->back();
        }
    }



}
