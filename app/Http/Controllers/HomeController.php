<?php

namespace App\Http\Controllers;

use App\Models\Contact;
use App\Models\ParentModule;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\sendContact;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $parentModules = ParentModule::query()->whereNull('parent_id')->orderBy('order')->get();

        return view('home', compact('parentModules'));
    }

    public function mailContact(Request $request)
    {
        Mail::to('rabee.m.arkadan@gmail.com')
            ->send(new sendContact($request));

         return json_encode(['contactMessage' => 'Thank you for your message, it has been sent to our team who will take note and will return to you as soon as possible.']);
    }

    public function ajaxSelect(Request $request){
        if($request->has('organization_id')){
            $contacts = Contact::query()->where('organization_id', $request->get('organization_id'));
            if($request->has('q')){
                $contacts = $contacts->where('name', 'like', '%'.$request->get('q').'%');
            }
            $contacts = $contacts->orderBy('name', 'asc')->pluck('id','name');
            return json_encode($contacts);
        }
    }
}
