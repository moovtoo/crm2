<?php

namespace App\Http\Controllers;

use App\Models\Module;
use App\Models\ParentModule;
use App\Models\RoleModules;
use App\Utils\Transform;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests,Transform;


    public function getClassSubstring($class){
        $classNameWithNamespace = get_class($class);
        return substr($classNameWithNamespace, strrpos($classNameWithNamespace, '\\')+1);
    }

    private function getParent($id){
        $p = ParentModule::query()->where('id',$id)->get()->first();
        if ( $p == null )
            return null;
        else{
            if ( $p->parent_id == null)
                return $p;
            else
                return $this->getParent($p->parent_id);
        }
    }
    function getParentModule($class){
//        Log::info($this->getClassSubstring($class));
        $leaf = Module::query()->where('controller_name',$this->getClassSubstring($class))->get()->first();
        if ( $leaf == null ){
            $leaf = ParentModule::query()->where('controller_name',str_replace('Controller','',$this->getClassSubstring($class)))->get()->first();
            $p = $this->getParent( $leaf->id);
        }else{
            $p = $this->getParent( $leaf->parent_id);
        }


        return $p;
    }

    public function getModule($class){
        return Module::query()->where('controller_name',$this->getClassSubstring($class))->get()->first();
    }

    public function listModel($model,$conditions = array() , $order=[], $limit = 0, $relation = null, $relation_id = null, $relation_operator = '='){

        $msql=  $model::query();

        if ( !empty($conditions))
            $msql->where($conditions);


        $name  = (new \ReflectionClass($model))->getShortName();
       $module = Module::query()->where('name',$name)->get()->first();

       if ( $module != null && request()->permissions_parent_modules != 1000 ){
           foreach(auth()->user()->roles as $role){
                $permission = RoleModules::query()->where('role_id',$role->id )
                    ->where('module_id',$module->id)->get()->first();
                if ( $module->user_related == 1 && $permission != null && $permission->show_all == 0 ){

                    if ( in_array('user_id',$model->getFillable())){
                        $msql->where('user_id',auth()->id());
                    }else{
                        $ownerId = auth()->id();
                        $msql->whereHas('users', function($q) use($ownerId) {
                            $q->where('users.id', $ownerId);
                        });
                    }

                }
           }
       }
       if($relation != null){
            $msql->whereHas($relation, function($q) use($relation, $relation_id, $relation_operator){
                $key = $relation.'.id';
                $q->where($key, $relation_operator, $relation_id);
            });
        }
        if ( !empty($order) )
            $msql->orderBy($order[0],$order[1]);
        if ( $limit != 0 )
            $msql->limit($limit);

        $models = $msql->get();
        $result = $models->map( function ($module){
            return $this->transformGeneric($module);
        })->all();
        return $result;
    }


    private function pickPermission( $permissions , $module_id){
        if ( !is_array($permissions))
            return null;
        foreach ($permissions as $permission ){
            if ( $permission->module_id == $module_id ){
                return $permission;
            }
        }
        return null;
    }

}

