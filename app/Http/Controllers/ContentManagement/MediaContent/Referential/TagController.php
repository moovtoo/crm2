<?php

namespace App\Http\Controllers\ContentManagement\MediaContent\Referential;

use App\Models\Module;
use App\Models\Tag;
use App\Models\Locale;
use App\Utils\TextField;
use App\Utils\Transform;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\DataTables;

class TagController extends Controller
{
    //


    public function index(Request $request){


        if ( $request->ajax() ){
            return DataTables::of($this->listModel(new Tag()))->make(true);
        }

        $title = 'Tags List';
        $add_button ='ADD A TAG';
        $parent = $this->getParentModule($this);
        $links = $this->parentLinks($this);

        return view('content-management.media-content.referential.tag.tags',compact('add_button','links','parent','title'));
    }



    public function show($tag){

        $tag = $this->findTagById($tag);
        $title = 'Tag '.$tag->name;
        $parent = $this->getParentModule($this);
        $links = $this->parentLinks($this);
        return view('content-management.media-content.referential.tag.tag-show', compact('title','links','tag','parent'));
    }


    public function create(){
//        $fields = $this->TagFields();
        if(!$this->getModule($this)->isAuthModuleAdd(auth()->user()->roles))return redirect()->back();
        $parent = $this->getParentModule($this);
        $locales = Locale::all();
        $links = $this->parentLinks($this);
        return view('content-management.media-content.referential.tag.tag',['title'=>'New Tag','links'=>$links,'parent'=>$parent, 'locales' => $locales]);
    }

    public function store(Request $request){
        $params = $request->except('_token');
        $params['slug'] = str_slug($request->input('name'));

        $tag =  $this->createTag($params);

        return $tag;
    }

    public function edit($id){
        $module = $this->getModule($this);
        if ( request()->permissions_parent_modules == 1000 || $module->isAuthModuleEdit(auth()->user()->roles)
            || $this->findTagById($id)->user_id == auth()->id()
            || $this->findTagById($id)->users()->where('user_id',auth()->id() )->count()>0 ){

        }else{
            return redirect()->back();
        }
        $tag = $this->findTagById($id);
        $title = 'Update '.$tag->name;
        $parent = $this->getParentModule($this);
        $locales = Locale::all();
        $links = $this->parentLinks($this);

        return view('content-management.media-content.referential.tag.tag-update',['title'=>$title,'links'=>$links, 'tag'=>$tag,'parent'=>$parent, 'locales' => $locales]);
    }

    public function editAuthorise($id){
        $module = $this->getModule($this);
        if ( request()->permissions_parent_modules == 1000 ||
            $module->isAuthModuleEdit(auth()->user()->roles)
            || $this->findTagById($id)->user_id == auth()->id()
            || $this->findTagById($id)->users()->where('user_id',auth()->id() )->count()>0){
            return response()->json(['status'=>'success']);
        }else{
            return response()->json(['status'=>'error','message'=> 'you are not authorised to do this action']);
        }
    }


    public function update(Request $request, $tag){
        $params = $request->except('_token');
        $params['slug'] = str_slug($request->input('name'));

        $this->updateTag($params, $tag);

        return $tag;
    }

    public function destroy ($tag){
        if ( $this->getModule($this)->isAuthModuleDelete(auth()->user()->roles) ){
            if( $this->deleteTag($tag) ){
                return response()->json(['status'=>'success']);
            }else{
                return response()->json(['status'=>'error','message'=> 'something went wrong']);
            }

        }else{
            return response()->json(['status'=>'error','message'=> 'you are not authorised to do this action']);
        }

    }

    public function up($tag){
        return Tag::find($tag)->moveOrderUp();

    }
    public function down($tag){
        return Tag::find($tag)->moveOrderDown();
    }


    private function findTagById(int $id) :Tag
    {
            return Tag::query()->where('id',$id)->get()->first();
    }


    private function createTag(array $params)
    {
        try {

            $module = new Tag($params);
            $module->save();
            return $module;
        } catch (\Exception $e) {
            return null;
        }
    }

    private function updateTag(array $params, int $id)
    {
        try {
            return Tag::query()->find($id)->update($params);
        } catch (\Exception $e) {
            return false;
        }
    }

    private function deleteTag(int $id)
    {
        try {
            return Tag::query()->find($id)->delete();
        } catch (\Exception $e) {
            return false;
        }
    }
    public function parentLinks($class){
        $module = Module::query()->where('controller_name',$this->getClassSubstring($class))->get()->first();
        $parent = $module->parent;
        $links[$module->name] = '';
        if($parent == null){
            return [];
        }else{

            do{
                $links[$parent->name] = '';
                $parent = $parent->parent;
            }while($parent->parent_id != null);
            $links[$parent->name] = $parent->slug;
        }
        return array_reverse($links);
    }

}
