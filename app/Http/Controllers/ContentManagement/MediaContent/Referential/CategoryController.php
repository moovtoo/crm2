<?php

namespace App\Http\Controllers\ContentManagement\MediaContent\Referential;

use App\Models\Category;
use App\Models\Module;
use App\Utils\TextField;
use App\Utils\Transform;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\DataTables;

class CategoryController extends Controller
{
    //



    public function index(Request $request){


        if ( $request->ajax() ){
            return DataTables::of($this->listModel(new Category()))->make(true);
        }

        $categories = Category::query()->where('parent_id', null)->pluck('name')->toArray();
//        $fields = $this->CategoryFields();
        $add_button ='ADD A CATEGORY';
        $filters = [
            'type' => ['Category', 'Sub-Category'],
            'parent' => $categories
        ];
        $parent = $this->getParentModule($this);
        $title = 'Category List';
        $links = $this->parentLinks($this);

        return view('content-management.media-content.referential.category.categories',compact('fields','links','title','add_button','filters','parent'));
    }



    public function show($category){

        $category = $this->findCategoryById($category);
        $title = 'Category '.$category->name;
        $parent = $this->getParentModule($this);
        $links = $this->parentLinks($this);

        return view('content-management.media-content.referential.category.category-show', compact('title','links','category','parent'));
    }


    public function create(){
        if(!$this->getModule($this)->isAuthModuleAdd(auth()->user()->roles))return redirect()->back();
        $parent = $this->getParentModule($this);
        $categories = Category::all();
        $links = $this->parentLinks($this);

        return view('content-management.media-content.referential.category.category',['title'=>'New Category','links'=>$links,'parent'=>$parent, 'categories'=>$categories]);
    }

    public function store(Request $request){
        $params = $request->except('_token','photo');
        $params['slug'] = str_slug($request->input('name'));

        if ( $request->has('photo') and $request->input('photo') != ''){
            // $image = $this->uploadOne($request->file('photo'),'article');
            $params['photo'] = $this->changeImagePath($request->input('photo'));
        }

        $category =  $this->createCategory($params);

        return $category;
    }

    public function edit($id){
        $module = $this->getModule($this);
        if ( request()->permissions_parent_modules == 1000 || $module->isAuthModuleEdit(auth()->user()->roles)
            || $this->findCategoryById($id)->user_id == auth()->id()
            || $this->findCategoryById($id)->users()->where('user_id',auth()->id() )->count()>0 ){

        }else{
            return redirect()->back();
        }
        $category = $this->findCategoryById($id);
        $title = 'Update '.$category->name;
        $parent = $this->getParentModule($this);
        $categories = Category::all();
        $links = $this->parentLinks($this);
        return view('content-management.media-content.referential.category.category-update',['title'=>$title,'links'=>$links, 'category'=>$category,'parent'=>$parent, 'categories'=>$categories]);
    }
    public function editAuthorise($id){
        $module = $this->getModule($this);
        if ( request()->permissions_parent_modules == 1000 ||
            $module->isAuthModuleEdit(auth()->user()->roles)
            || $this->findCategoryById($id)->user_id == auth()->id()
            || $this->findCategoryById($id)->users()->where('user_id',auth()->id() )->count()>0){
            return response()->json(['status'=>'success']);
        }else{
            return response()->json(['status'=>'error','message'=> 'you are not authorised to do this action']);
        }
    }


    public function update(Request $request, $category){
        $params = $request->except('_token','photo');
        $params['slug'] = str_slug($request->input('name'));

        if ( $request->has('photo') and $request->input('photo') != '' ){
            // $image = $this->uploadOne($request->file('photo'),'article');
            $params['photo'] = $this->changeImagePath($request->input('photo'));
        }

//        return json_encode($params);

        $this->updateCategory($params, $category);

        return $category;
    }



    public function destroy ( $category){
        if ( $this->getModule($this)->isAuthModuleDelete(auth()->user()->roles) ){
            if( $this->deleteCategory($category) ){
                return response()->json(['status'=>'success']);
            }else{
                return response()->json(['status'=>'error','message'=> 'something went wrong']);
            }

        }else{
            return response()->json(['status'=>'error','message'=> 'you are not authorised to do this action']);
        }

    }


    public function up($category){
        return Category::find($category)->moveOrderUp();

    }
    public function down($category){
        return Category::find($category)->moveOrderDown();
    }


    private function findCategoryById(int $id) :Category
    {
            return Category::query()->where('id',$id)->get()->first();
    }

    private function createCategory(array $params)
    {
        try {

            $module = new Category($params);
            $module->save();
            return $module;
        } catch (\Exception $e) {
            return null;
        }
    }

    private function updateCategory(array $params, int $id)
    {
        try {
            return Category::query()->find($id)->update($params);
        } catch (\Exception $e) {
            return false;
        }
    }

    private function deleteCategory(int $id)
    {
        try {
            return Category::query()->find($id)->delete();
        } catch (\Exception $e) {
            return false;
        }
    }

    public function removeImage($id){
        try {
            $category = Category::query()->find($id);
            $category->photo = null;
            $category->save();
            return redirect()->back();
        } catch (\Exception $e) {
            return redirect()->back();
        }
    }
    public function parentLinks($class){
        $module = Module::query()->where('controller_name',$this->getClassSubstring($class))->get()->first();
        $parent = $module->parent;
        $links[$module->name] = '';
        if($parent == null){
            return [];
        }else{

            do{
                $links[$parent->name] = '';
                $parent = $parent->parent;
            }while($parent->parent_id != null);
            $links[$parent->name] = $parent->slug;
        }
        return array_reverse($links);
    }

}
