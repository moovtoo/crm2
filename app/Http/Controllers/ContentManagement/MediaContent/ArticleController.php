<?php

namespace App\Http\Controllers\ContentManagement\MediaContent;

use App\Models\Article;
use App\Models\Domain;
use App\Models\Locale;
use App\Models\Module;
use App\Models\Tag;
use App\Models\User;
use App\Models\Category;
use App\Utils\TextField;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\Validator;

class ArticleController extends Controller
{
    //



    public function index(Request $request){


        if ( $request->ajax() ){
            return DataTables::of($this->listModel(new Article(),[],['order','desc']))->make(true);
        }

        $authors_filter = User::query()->pluck('name')->toArray();
        $categories_filter = Category::query()->pluck('name')->toArray();
        $add_button ='ADD AN ARTICLE';
        $filters = [
            'author' => $authors_filter,
            'category' => $categories_filter
        ];

        $parent = $this->getParentModule($this);
        $title = 'Article List';
        $links = $this->parentLinks($this);

        return view('content-management.media-content.article.articles',compact('add_button','title','links', 'filters','parent'));
    }



    public function show($article){

        $article = $this->findArticleById($article);
        $title = 'Article '.$article->name;
        $parent = $this->getParentModule($this);
        $links = $this->parentLinks($this);
        return view('content-management.media-content.article.article-show', compact('title','links','article','parent'));
    }


    public function create(){
        if(!$this->getModule($this)->isAuthModuleAdd(auth()->user()->roles))return redirect()->back();
//        $fields = $this->ArticleFields();
        $parent = $this->getParentModule($this);
        $authors = User::all();
        $locales = Locale::all();
        $domains = Domain::all();
        $categories = Category::all();
        $links = $this->parentLinks($this);
        $tags = Tag::all();
        return view('content-management.media-content.article.article',['title'=>'New Article','parent'=>$parent
            ,'authors'=>$authors,'locales'=>$locales,'links'=>$links,'domains'=>$domains,'categories'=>$categories,'tags'=>$tags]);
    }

    public function store(Request $request){
        $params = $request->except('_token','users','categories','photo','sites','meta_image', 'tags');
        $params['slug'] = str_slug($request->input('name'));
        $validator = Validator::make($request->all(),[
            'name'=>'required|unique:articles',
            'categories'=>'required',
            'photo'=>'required',
            ]);
        if ( $validator->fails() ){
            return ['status'=>'error','messages'=>$validator->errors()->getMessages()];
        }

        if ( $request->has('photo') and $request->input('photo') != ''){
            // $image = $this->uploadOne($request->file('photo'),'article');
            $params['photo'] = $this->changeImagePath($request->input('photo'));
        }
        
        if ( $request->has('meta_image') and $request->input('meta_image') != ''){
            // $image = $this->uploadOne($request->file('photo'),'article');
            $params['meta_image'] = $this->changeImagePath($request->input('meta_image'));
        }

        $article =  $this->createArticle($params);
        $article->categories()->attach($request->input('categories'));
        $article->users()->attach($request->input('users'));
        $article->sites()->attach($request->input('sites'));
        $article->tags()->attach($request->input('tags'));

        return ['status'=> 'success','article'=>$article];
    }

    public function edit($id){
        $module = $this->getModule($this);
        if ( request()->permissions_parent_modules == 1000 || $module->isAuthModuleEdit(auth()->user()->roles)
            || $this->findArticleById($id)->user_id == auth()->id()
            || $this->findArticleById($id)->users()->where('user_id',auth()->id() )->count()>0 ){

        }else{
            return redirect()->back();
        }
        $article = $this->findArticleById($id);
        $links = $this->parentLinks($this);
        $fields = $this->ArticleFields();
        $title = 'Update '.$article->name;
        $parent = $this->getParentModule($this);
        $authors = User::all();
        $locales = Locale::all();
        $domains = Domain::all();
        $categories = Category::all();
        $tags = Tag::all();

        return view('content-management.media-content.article.article-update',['title'=>$title, 'fields'=>$fields, 'article'=>$article,'parent'=>$parent
            ,'authors'=>$authors,'links'=>$links,'locales'=>$locales,'domains'=>$domains,'categories'=>$categories,'tags'=>$tags]);
    }

    public function editAuthorise($id){
        $module = $this->getModule($this);
        if ( request()->permissions_parent_modules == 1000 ||
            $module->isAuthModuleEdit(auth()->user()->roles)
            || $this->findArticleById($id)->user_id == auth()->id()
            || $this->findArticleById($id)->users()->where('user_id',auth()->id() )->count()>0 ) {

            return response()->json(['status'=>'success']);
        }else{
            return response()->json(['status'=>'error','message'=> 'you are not authorised to do this action']);
        }
    }


    public function update(Request $request, $article){
        $params = $request->except('_token','users','categories', 'sites','photo', 'meta_image', 'tags');
        $params['slug'] = str_slug($request->input('name'));

        $validator = Validator::make($request->all(),[
            'name'=>'required|unique:articles,name,'.$article,
            'categories'=>'required',
            ]);
        if ( $validator->fails() ){
            return ['status'=>'error','messages'=>$validator->errors()->getMessages()];
        }

        if ( $request->has('photo') and $request->input('photo') != '' ){
            // $image = $this->uploadOne($request->file('photo'),'article');
            $params['photo'] = $this->changeImagePath($request->input('photo'));
        }
        
        if ( $request->has('meta_image') and $request->input('meta_image') != '' ){
            // $image = $this->uploadOne($request->file('photo'),'article');
            $params['meta_image'] = $this->changeImagePath($request->input('meta_image'));
        }

//        return json_encode($params);

        $this->updateArticle($params, $article);
        $article = $this->findArticleById($article);
        $article->categories()->sync($request->input('categories'));
        $article->users()->sync($request->input('users'));
        $article->sites()->sync($request->input('sites'));
        $article->tags()->sync($request->input('tags'));

        return ['status'=> 'success','article'=>$article];
    }

    public function destroy ( $article){
        if ( $this->getModule($this)->isAuthModuleDelete(auth()->user()->roles) ){
            if( $this->deleteArticle($article) ){
                return response()->json(['status'=>'success']);
            }else{
                return response()->json(['status'=>'error','message'=> 'something went wrong']);
            }

        }else{
            return response()->json(['status'=>'error','message'=> 'you are not authorised to do this action']);
        }

    }

    public function up($article){
        return Article::find($article)->moveOrderUp();

    }

    public function down($article){
        return Article::find($article)->moveOrderDown();
    }


    private function findArticleById(int $id) :Article
    {
            return Article::query()->where('id',$id)->get()->first();
    }


    private function createArticle(array $params)
    {
        try {

            $module = new Article($params);
            $module->save();
            return $module;
        } catch (\Exception $e) {
            return null;
        }
    }

    private function updateArticle(array $params, int $id)
    {
        try {
            return Article::query()->find($id)->update($params);
        } catch (\Exception $e) {
            return false;
        }
    }

    private function deleteArticle(int $id)
    {

        try {
            return Article::query()->find($id)->delete();
        } catch (\Exception $e) {
            return false;
        }
    }

    public function removeImage($id){
        try {
            $article = Article::query()->find($id);
            $article->photo = null;
            $article->save();
            return redirect()->back();
        } catch (\Exception $e) {
            return redirect()->back();
        }
    }
    
    public function removeMetaImage($id){
        try {
            $article = Article::query()->find($id);
            $article->meta_image = null;
            $article->save();
            return redirect()->back();
        } catch (\Exception $e) {
            return redirect()->back();
        }
    }
    public function parentLinks($class){
        $module = Module::query()->where('controller_name',$this->getClassSubstring($class))->get()->first();
        $parent = $module->parent;
        $links[$module->name] = '';
        if($parent == null){
            return [];
        }else{

            do{
                $links[$parent->name] = '';
                $parent = $parent->parent;
            }while($parent->parent_id != null);
            $links[$parent->name] = $parent->slug;
        }
        return array_reverse($links);
    }

}
