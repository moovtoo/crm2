<?php

namespace App\Http\Controllers\OrganizationsManagement\ContactSystem;

use App\Models\Contact;
use App\Models\ContactPhone;
use App\Models\ContactType;
use App\Models\Module;
use App\Utils\TextField;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\Validator;

class ContactPhoneController extends Controller
{
    //



    public function index(Request $request){


        if ( $request->ajax() ){
            if($request->has('contact_id')){
                return DataTables::of($this->listModel(new ContactPhone(), [['contact_id',$request->get('contact_id')]]))->make(true);
            }
            return DataTables::of($this->listModel(new ContactPhone()))->make(true);

        }
        $add_button ='ADD A CONTACT PHONE';
        $filters = [
        ];
        $links = $this->parentLinks($this);
        $parent = $this->getParentModule(new ContactController());
        $title = 'Contact Phone List';

        return view('organizations-management.contact-system.contact-phone.contact-phones',compact('add_button','links','title','parent'));
    }



    public function show($contactPhone){

        $contactPhone = $this->findContactPhoneById($contactPhone);
        $title = 'Contact Phone for '.$contactPhone->contact->name;
        $parent = $this->getParentModule(new ContactController());
        $links = $this->parentLinks($this);
        return view('organizations-management.contact-system.contact-phone.contact-phone-show', compact('title','links','contactPhone','parent'));
    }


    public function create(){
        $module = $this->getModule($this);
        if(!$this->getModule($this)->isAuthModuleAdd(auth()->user()->roles))return redirect()->back();
        $model = new ContactPhone();
        $fields = $model->fields();
        $links = $this->parentLinks($this);
        $parent = $this->getParentModule(new ContactController());
        $contacts = Contact::all();
        $contactTypes= ContactType::query()->where('module_id' ,$module->id )->get();
        return view('organizations-management.contact-system.contact-phone.contact-phone',['title'=>'New Organization','links'=>$links,'fields'=>$fields,'contactTypes'=>$contactTypes,'parent'=>$parent, 'contacts'=>$contacts]);
    }

    public function store(Request $request){
        $params = $request->except('_token');

        $contactPhone =  $this->createContactPhone($params);

        return ['status'=> 'success','contactPhone'=>$contactPhone];
    }

    public function edit($id){
        $module = $this->getModule($this);
        if ( request()->permissions_parent_modules == 1000 || $module->isAuthModuleEdit(auth()->user()->roles)){

        }else{
            return redirect()->back();
        }
        $contactPhone = $this->findContactPhoneById($id);
        $model = new ContactPhone();
        $fields = $model->fields();
        $title = 'Update Contact Phone for '.$contactPhone->contact->name;
        $parent = $this->getParentModule(new ContactController());
        $contacts = Contact::all();
        $contactTypes= ContactType::query()->where('module_id' ,$module->id )->get();
        $links = $this->parentLinks($this);

        return view('organizations-management.contact-system.contact-phone.contact-phone-update',['title'=>$title,'links'=>$links
            ,'contactTypes'=>$contactTypes, 'fields'=>$fields, 'contactPhone'=>$contactPhone,'parent'=>$parent, 'contacts'=>$contacts]);
    }

    public function editAuthorise($id){
        $module = $this->getModule($this);
        if ( request()->permissions_parent_modules == 1000 || $module->isAuthModuleEdit(auth()->user()->roles)){
            return response()->json(['status'=>'success']);
        }else{
            return response()->json(['status'=>'error','message'=> 'you are not authorised to do this action']);
        }
    }


    public function update(Request $request, $contactPhone){
        $params = $request->except('_token');

        $this->updateContactPhone($params, $contactPhone);

        return ['status'=> 'success','contactPhone'=>$contactPhone];
    }

    public function destroy ( $contactPhone){
        if ( $this->getModule($this)->isAuthModuleDelete(auth()->user()->roles) ){
            if( $this->deleteContactPhone($contactPhone) ){
                return response()->json(['status'=>'success']);
            }else{
                return response()->json(['status'=>'error','message'=> 'something went wrong']);
            }
        }else{
            return response()->json(['status'=>'error','message'=> 'you are not authorised to do this action']);
        }

    }

    public function up($contactPhone){
        return ContactPhone::find($contactPhone)->moveOrderUp();

    }

    public function down($contactPhone){
        return ContactPhone::find($contactPhone)->moveOrderDown();
    }


    private function findContactPhoneById(int $id) :ContactPhone
    {
            return ContactPhone::query()->where('id',$id)->get()->first();
    }


    private function createContactPhone(array $params)
    {
        try {
            $module = new ContactPhone($params);
            $module->save();
            return $module;
        } catch (\Exception $e) {
            return null;
        }
    }

    private function updateContactPhone(array $params, int $id)
    {
        try {
            return ContactPhone::query()->find($id)->update($params);
        } catch (\Exception $e) {
            return false;
        }
    }

    private function deleteContactPhone(int $id)
    {

        try {
            return ContactPhone::query()->find($id)->delete();
        } catch (\Exception $e) {
            return false;
        }
    }
    public function parentLinks($class){
        $module = Module::query()->where('controller_name',$this->getClassSubstring($class))->get()->first();
        $parent = $module->parent;
        $links[$module->name] = '';
        if($parent == null){
            return [];
        }else{

            do{
                $links[$parent->name] = '';
                $parent = $parent->parent;
            }while($parent->parent_id != null);
            $links[$parent->name] = $parent->slug;
        }
        return array_reverse($links);
    }

}
