<?php

namespace App\Http\Controllers\OrganizationsManagement\ContactSystem;

use App\Models\ContactType;
use App\Models\Module;
use App\Models\Organization;
use App\Models\Contact;
use App\Models\ContactPhone;
use App\Models\Country;
use App\Models\Sector;
use App\Utils\TextField;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\Validator;

class ContactController extends Controller
{
    //
    public function __construct()
    {

    }


    public function index(Request $request){


        if ( $request->ajax() ){
            return DataTables::of($this->listModel(new Contact()))->make(true);
        }

        $add_button ='ADD A CONTACT';
        $filters = [
        ];
        $links = $this->parentLinks($this);
        $parent = $this->getParentModule($this);
        $title = 'Contact List';

        return view('organizations-management.contact-system.contact.contacts',compact('add_button','links','title','parent'));
    }



    public function show($contact){
        $module = $this->getModule($this);
        $contact = $this->findContactById($contact);
        $title = 'Contact '.$contact->name;
        $parent = $this->getParentModule($this);
        $links = $this->parentLinks($this);
        $contactPhones= $contact->phones;
        $contactTypes= ContactType::query()->where('module_id' ,$module->id )->get();
        return view('organizations-management.contact-system.contact.contact-show', compact('title','links',
            'contactTypes','contactPhones','contact','parent'));
    }


    public function create(){
        $module = $this->getModule($this);
        if(!$this->getModule($this)->isAuthModuleAdd(auth()->user()->roles))return redirect()->back();

        $model = new Contact();
        $fields = $model->fields();
        $parent = $this->getParentModule($this);
        $organizations = Organization::all();
        $countries = Country::all();
        $sectors= Sector::all();
        $links = $this->parentLinks($this);
        $contactTypes= ContactType::query()->where('module_id' ,$module->id )->get();
        return view('organizations-management.contact-system.contact.contact',['title'=>'New Contact','links'=>$links,'fields'=>$fields,'contactTypes'=>$contactTypes,'sectors'=>$sectors,'parent'=>$parent, 'organizations'=>$organizations, 'countries'=>$countries]);
    }

    public function store(Request $request){
        $params = $request->except('_token','type_id', 'phone');
        $params['slug'] = str_slug($request->input('name'));

        $contact =  $this->createContact($params);
        $types = $request->input('type_id');
        $phones = $request->input('phone');
        foreach($phones as $key => $value){
            $contactPhone = new ContactPhone();
            $contactPhone->phone = $value;
            $contactPhone->type_id = $types[$key];
            $contactPhone->contact_id = $contact->id;
            $contactPhone->save();
        }

        return ['status'=> 'success','contact'=>$contact];
    }

    public function edit($id){
        $module = $this->getModule($this);
        if ( request()->permissions_parent_modules == 1000 || $module->isAuthModuleEdit(auth()->user()->roles)){

        }else{
            return redirect()->back();
        }
        $contact = $this->findContactById($id);
        $model = new Contact();
        $fields = $model->fields();
        $title = 'Update '.$contact->name;
        $parent = $this->getParentModule($this);
        $organizations = Organization::all();
        $countries = Country::all();
        $sectors= Sector::all();
        $links = $this->parentLinks($this);
        $contactTypes= ContactType::query()->where('module_id' ,$module->id )->get();
        $contactPhones= $contact->phones;

        return view('organizations-management.contact-system.contact.contact-update',['title'=>$title, 'links'=>$links,'fields'=>$fields, 'contactTypes'=>$contactTypes,'sectors'=>$sectors,'contact'=>$contact,'parent'=>$parent, 'organizations'=>$organizations, 'countries'=>$countries, 'contactPhones' => $contactPhones]);
    }

    public function editAuthorise($id){
        $module = $this->getModule($this);
        if ( request()->permissions_parent_modules == 1000 || $module->isAuthModuleEdit(auth()->user()->roles)){
            return response()->json(['status'=>'success']);
        }else{
            return response()->json(['status'=>'error','message'=> 'you are not authorised to do this action']);
        }
    }


    public function update(Request $request, $contact){
        $params = $request->except('_token');
        $params['slug'] = str_slug($request->input('name'));

        $this->updateContact($params, $contact);
        $contact = $this->findContactById($contact);
        foreach($contact->phones as $phone){
            $phone->delete();
        }

        $types = $request->input('type_id');
        $phones = $request->input('phone');
        foreach($phones as $key => $value){
            $contactPhone = new ContactPhone();
            $contactPhone->phone = $value;
            $contactPhone->type_id = $types[$key];
            $contactPhone->contact_id = $contact->id;
            $contactPhone->save();
        }

        return ['status'=> 'success','contact'=>$contact];
    }

    public function destroy ( $contact){
        if ( $this->getModule($this)->isAuthModuleDelete(auth()->user()->roles) ){
            if( $this->deleteContact($contact) ){
                return response()->json(['status'=>'success']);
            }else{
                return response()->json(['status'=>'error','message'=> 'something went wrong']);
            }
        }else{
            return response()->json(['status'=>'error','message'=> 'you are not authorised to do this action']);
        }

    }

    public function up($contact){
        return Contact::find($contact)->moveOrderUp();

    }

    public function down($contact){
        return Contact::find($contact)->moveOrderDown();
    }


    private function findContactById(int $id) :Contact
    {
            return Contact::query()->where('id',$id)->get()->first();
    }


    private function createContact(array $params)
    {
        try {
            $module = new Contact($params);
            $module->save();
            return $module;
        } catch (\Exception $e) {
            return null;
        }
    }

    private function updateContact(array $params, int $id)
    {
        try {
            return Contact::query()->find($id)->update($params);
        } catch (\Exception $e) {
            return false;
        }
    }

    private function deleteContact(int $id)
    {

        try {
            return Contact::query()->find($id)->delete();
        } catch (\Exception $e) {
            return false;
        }
    }
    public function parentLinks($class){
        $module = Module::query()->where('controller_name',$this->getClassSubstring($class))->get()->first();
        $parent = $module->parent;
        $links[$module->name] = '';
        if($parent == null){
            return [];
        }else{

            do{
                $links[$parent->name] = '';
                $parent = $parent->parent;
            }while($parent->parent_id != null);
            $links[$parent->name] = $parent->slug;
        }
        return array_reverse($links);
    }

}
