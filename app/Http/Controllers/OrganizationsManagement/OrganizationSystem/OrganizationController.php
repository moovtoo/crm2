<?php

namespace App\Http\Controllers\OrganizationsManagement\OrganizationSystem;

use App\Models\Contact;
use App\Models\ContactType;
use App\Models\Module;
use App\Models\Organization;
use App\Models\OrganizationPhone;
use App\Models\Sector;
use App\Models\User;
use App\Models\Country;
use App\Utils\TextField;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\Validator;

class OrganizationController extends Controller
{
    //



    public function index(Request $request){


        if ( $request->ajax() ){
            return DataTables::of($this->listModel(new Organization()))->make(true);
        }

        $add_button ='ADD AN ORGANIZATION';
        $filters = [
        ];

        $parent = $this->getParentModule($this);
        $title = 'Organization List';
        $links = $this->parentLinks($this);

        return view('organizations-management.organization-system.organization.organizations',compact('add_button','links','title','parent'));
    }



    public function show($organization){

        $organization = $this->findOrganizationById($organization);
        $title = 'Organization '.$organization->name;
        $parent = $this->getParentModule($this);
        $links = $this->parentLinks($this);
//        $contacts= Contact::query()->where('organization_id' ,$organization)->get();

        return view('organizations-management.organization-system.organization.organization-show', compact('title','links','organization','parent'));
    }


    public function create(){
        $module = $this->getModule($this);
        if(!$this->getModule($this)->isAuthModuleAdd(auth()->user()->roles))return redirect()->back();
        $model = new Organization();
        $fields = $model->fields();
        $parent = $this->getParentModule($this);
        $users = User::all();
        $sectors = Sector::all();
        $countries = Country::all();
        $links = $this->parentLinks($this);
        $contactTypes= ContactType::query()->where('module_id' ,5)->get();
        return view('organizations-management.organization-system.organization.organization',['title'=>'New Organization',
            'links'=>$links,'fields'=>$fields,'parent'=>$parent,'sectors'=>$sectors ,
            'contactTypes'=>$contactTypes,'users'=>$users, 'countries'=>$countries]);
    }

    public function store(Request $request){
        $params = $request->except('_token','image', 'company_image');
        $params['slug'] = str_slug($request->input('name'));

        if ( $request->has('image') and $request->input('image') != null){
            $params['image'] = $this->changeImagePath($request->input('image'));
        }
        
        if ( $request->has('company_image') and $request->input('company_image') != null){
            $params['company_image'] = $this->changeImagePath($request->input('company_image'));
        }

        $organization =  $this->createOrganization($params);
        $types = $request->input('type_id');
        $phones = $request->input('phone');
        foreach($phones as $key => $value){
            $organizationPhone = new OrganizationPhone();
            $organizationPhone->phone = $value;
            $organizationPhone->type_id = $types[$key];
            $organizationPhone->organization_id = $organization->id;
            $organizationPhone->save();
        }

        return ['status'=> 'success','organization'=>$organization];
    }

    public function edit($id){
        $module = $this->getModule($this);
        if ( request()->permissions_parent_modules == 1000 || $module->isAuthModuleEdit(auth()->user()->roles)
            || $this->findOrganizationById($id)->user->id == Auth()->user()->id ){

        }else{
            return redirect()->back();
        }
        $organization = $this->findOrganizationById($id);
        $model = new Organization();
        $fields = $model->fields();
        $title = 'Update '.$organization->name;
        $parent = $this->getParentModule($this);
        $users = User::all();
        $countries = Country::all();
        $sectors = Sector::all();
        $links = $this->parentLinks($this);
        $contactTypes= ContactType::query()->where('module_id' ,5)->get();
        $organizationPhones= $organization->phones;

        return view('organizations-management.organization-system.organization.organization-update',['title'=>$title,
            'links'=>$links, 'fields'=>$fields,'sectors'=>$sectors, 'organization'=>$organization,
            'contactTypes'=>$contactTypes,'parent'=>$parent, 'users'=>$users, 'organizationPhones'=>$organizationPhones,'countries'=>$countries]);
    }

    public function editAuthorise($id){
        $module = $this->getModule($this);
        if ( request()->permissions_parent_modules == 1000 || $module->isAuthModuleEdit(auth()->user()->roles)
            || $this->findOrganizationById($id)->user->id == Auth()->user()->id ){
            return response()->json(['status'=>'success']);
        }else{
            return response()->json(['status'=>'error','message'=> 'you are not authorised to do this action']);
        }
    }


    public function update(Request $request, $organization){
        $params = $request->except('_token','image', 'company_image');
        $params['slug'] = str_slug($request->input('name'));

        if ( $request->has('image') and $request->input('image') != null ){
            $params['image'] = $this->changeImagePath($request->input('image'));
        }
        
        if ( $request->has('company_image') and $request->input('company_image') != null ){
            $params['company_image'] = $this->changeImagePath($request->input('company_image'));
        }

        $this->updateOrganization($params, $organization);
        $organization = $this->findOrganizationById($organization);
        foreach($organization->phones as $phone){
            $phone->delete();
        }

        $types = $request->input('type_id');
        $phones = $request->input('phone');
        foreach($phones as $key => $value){
            $organizationPhone = new OrganizationPhone();
            $organizationPhone->phone = $value;
            $organizationPhone->type_id = $types[$key];
            $organizationPhone->organization_id = $organization->id;
            $organizationPhone->save();
        }


        return ['status'=> 'success','organization'=>$organization];
    }

    public function destroy ( $organization){
        if ( $this->getModule($this)->isAuthModuleDelete(auth()->user()->roles) ){
            if( $this->deleteOrganization($organization) ){
                return response()->json(['status'=>'success']);
            }else{
                return response()->json(['status'=>'error','message'=> 'something went wrong']);
            }
        }else{
            return response()->json(['status'=>'error','message'=> 'you are not authorised to do this action']);
        }

    }

    public function up($organization){
        return Organization::find($organization)->moveOrderUp();

    }

    public function down($organization){
        return Organization::find($organization)->moveOrderDown();
    }


    private function findOrganizationById(int $id) :Organization
    {
            return Organization::query()->where('id',$id)->get()->first();
    }


    private function createOrganization(array $params)
    {
        try {
            $module = new Organization($params);
            $module->save();
            return $module;
        } catch (\Exception $e) {
            return null;
        }
    }

    private function updateOrganization(array $params, int $id)
    {
        try {
            return Organization::query()->find($id)->update($params);
        } catch (\Exception $e) {
            return false;
        }
    }

    private function deleteOrganization(int $id)
    {

        try {
            return Organization::query()->find($id)->delete();
        } catch (\Exception $e) {
            return false;
        }
    }
    public function parentLinks($class){
        $module = Module::query()->where('controller_name',$this->getClassSubstring($class))->get()->first();
        $parent = $module->parent;
        $links[$module->name] = '';
        if($parent == null){
            return [];
        }else{

            do{
                $links[$parent->name] = '';
                $parent = $parent->parent;
            }while($parent->parent_id != null);
            $links[$parent->name] = $parent->slug;
        }
        return array_reverse($links);
    }

}
