<?php

namespace App\Http\Controllers\OrganizationsManagement\OrganizationSystem;

use App\Models\ContactType;
use App\Models\Module;
use App\Models\Organization;
use App\Models\OrganizationPhone;
use App\Models\User;
use App\Models\Country;
use App\Utils\TextField;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\Validator;

class OrganizationPhoneController extends Controller
{
    //



    public function index(Request $request){


        if ( $request->ajax() ){
            return DataTables::of($this->listModel(new OrganizationPhone()))->make(true);
        }

        $add_button ='ADD AN ORGANIZATION PHONE';
        $filters = [
        ];
        $title = 'Organization Phone List';
        $parent = $this->getParentModule($this);
        $links = $this->parentLinks($this);

        return view('organizations-management.organization-system.organization-phone.organization-phones',compact('add_button','links','title','parent'));
    }



    public function show($phone){

        $phone = $this->findOrganizationPhoneById($phone);
        $title = 'Phone '.$phone->organization->name;
        $parent = $this->getParentModule($this);
        $links = $this->parentLinks($this);
        return view('organizations-management.organization-system.organization-phone.organization-phone-show', compact('title','phone','links','parent'));
    }


    public function create(){
        $module = $this->getModule($this);
        if(!$this->getModule($this)->isAuthModuleAdd(auth()->user()->roles))return redirect()->back();
        $model = new OrganizationPhone();
        $fields = $model->fields();
        $parent = $this->getParentModule($this);
        $organizations = Organization::all();
        $contactTypes= ContactType::query()->where('module_id' ,$module->id )->get();
        $links = $this->parentLinks($this);

        return view('organizations-management.organization-system.organization-phone.organization-phone',['title'=>'New Organization Phone','links'=>$links,'contactTypes'=>$contactTypes,'fields'=>$fields,'parent'=>$parent, 'organizations'=>$organizations]);
    }

    public function store(Request $request){
        $params = $request->except('_token');

        $phone =  $this->createOrganizationPhone($params);

        return ['status'=> 'success','phone'=>$phone];
    }

    public function edit($id){
        $module = $this->getModule($this);
        if ( request()->permissions_parent_modules == 1000 || $module->isAuthModuleEdit(auth()->user()->roles) ){

        }else{
            return redirect()->back();
        }
        $phone = $this->findOrganizationPhoneById($id);
        $model = new OrganizationPhone();
        $fields = $model->fields();
        $title = 'Update Organization Phone For '.$phone->organization->name;
        $parent = $this->getParentModule($this);
        $organizations = Organization::all();
        $contactTypes= ContactType::query()->where('module_id' ,$module->id )->get();
        $links = $this->parentLinks($this);


        return view('organizations-management.organization-system.organization-phone.organization-phone-update',['title'=>$title, 'links'=>$links,'fields'=>$fields, 'contactTypes'=>$contactTypes,'phone'=>$phone,'parent'=>$parent, 'organizations'=>$organizations]);
    }

    public function editAuthorise($id){
        $module = $this->getModule($this);
        if ( request()->permissions_parent_modules == 1000 || $module->isAuthModuleEdit(auth()->user()->roles)){
            return response()->json(['status'=>'success']);
        }else{
            return response()->json(['status'=>'error','message'=> 'you are not authorised to do this action']);
        }
    }


    public function update(Request $request, $phone){
        $params = $request->except('_token');

        $this->updateOrganizationPhone($params, $phone);

        return ['status'=> 'success','phone'=>$phone];
    }

    public function destroy ( $phone){
        if ( $this->getModule($this)->isAuthModuleDelete(auth()->user()->roles) ){
            if( $this->deleteOrganizationPhone($phone) ){
                return response()->json(['status'=>'success']);
            }else{
                return response()->json(['status'=>'error','message'=> 'something went wrong']);
            }
        }else{
            return response()->json(['status'=>'error','message'=> 'you are not authorised to do this action']);
        }

    }

    public function up($phone){
        return OrganizationPhone::find($phone)->moveOrderUp();

    }

    public function down($phone){
        return OrganizationPhone::find($phone)->moveOrderDown();
    }


    private function findOrganizationPhoneById(int $id) :OrganizationPhone
    {
            return OrganizationPhone::query()->where('id',$id)->get()->first();
    }


    private function createOrganizationPhone(array $params)
    {
        try {
            $module = new OrganizationPhone($params);
            $module->save();
            return $module;
        } catch (\Exception $e) {
            return null;
        }
    }

    private function updateOrganizationPhone(array $params, int $id)
    {
        try {
            return OrganizationPhone::query()->find($id)->update($params);
        } catch (\Exception $e) {
            return false;
        }
    }

    private function deleteOrganizationPhone(int $id)
    {

        try {
            return OrganizationPhone::query()->find($id)->delete();
        } catch (\Exception $e) {
            return false;
        }
    }
    public function parentLinks($class){
        $module = Module::query()->where('controller_name',$this->getClassSubstring($class))->get()->first();
        $parent = $module->parent;
        $links[$module->name] = '';
        if($parent == null){
            return [];
        }else{

            do{
                $links[$parent->name] = '';
                $parent = $parent->parent;
            }while($parent->parent_id != null);
            $links[$parent->name] = $parent->slug;
        }
        return array_reverse($links);
    }

}
