<?php

namespace App\Http\Controllers\OrganizationsManagement;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Domain;
use App\Models\Locale;
use App\Models\ModelConfigurations;
use App\Models\Sector;
use App\Models\User;
use Illuminate\Http\Request;

class OrganizationsManagementController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    
    public function show()
    {
        $parent = $this->getParentModule($this);
        return view('organizations-management.organizations-management',compact('parent'));
    }

    public function configuration(){

        $parent = $this->getParentModule($this);
        $configurations = $this->listConfigurations($parent);
        $locales = Locale::all();
        $authors =  User::all();
        $categories = Category::all();
        $domains = Domain::all();
        $sectors = Sector::all();
        return view('organizations-management.configurations',compact('configurations','parent','sectors','locales','authors','categories','domains'));
    }


    public function managementStore(Request $request, $module_id)
    {
        if ( $request->has('locale_id') and $request->input('locale_id') != ''){
            $this->updateConfiguration($request->input('locale_id') , 'locale_id', $module_id);
        }
        if ( $request->has('author_id') and $request->input('author_id') != ''){
            $this->updateConfiguration($request->input('author_id') , 'author_id', $module_id);
        }
        if ( $request->has('category_id') and $request->input('category_id') != ''){
            $this->updateConfiguration($request->input('category_id') , 'category_id', $module_id);
        }
        if ( $request->has('status') and $request->input('status') != ''){
            $this->updateConfiguration($request->input('status') , 'status', $module_id);
        }
        if ( $request->has('domain_id') and $request->input('domain_id') != ''){
            $this->updateConfiguration($request->input('domain_id') , 'domain_id', $module_id);
        }
    }



    private function listConfigurations($parent){
        $configuration = array();
        $configurations = $parent->hasConfig();

        foreach ($configurations as $config) {
            $configuration [$config->key] = $config->value;
        }
        return $configuration;
    }


    private  function updateConfiguration($value , $key, $module_id): bool
    {
        try {
            $module = ModelConfigurations::query()->where('key', $key)->where('module_id', $module_id)->first();
            if ($module == null ){
                $module = new ModelConfigurations(['module_id'=>$module_id,'key'=>$key,'value'=>$value]);
                $module->save();
                return true;
            }else{
                $module->value = $value;
                $module->save();
                return true;
            }

        } catch (\Exception $e) {
            return false;
        }
    }

}
