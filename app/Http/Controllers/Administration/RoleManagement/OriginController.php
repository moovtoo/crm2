<?php

namespace App\Http\Controllers\Administration\RoleManagement;

use App\Models\Module;
use App\Models\Origin;
use App\Models\Department;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use Yajra\DataTables\DataTables;

class OriginController extends Controller
{
    //

    public function index(Request $request){


        if ( $request->ajax() ){
            return DataTables::of($this->listModel(new Origin()))->make(true);
        }

        $title = 'Origin List';
        $add_button ='ADD AN ORIGIN';
        $parent = $this->getParentModule($this);
        $links = $this->parentLinks($this);

        return view('administration.role-management.origin.origins',compact('add_button','parent','title','links'));
    }

    public function show($origin){

        $origin = $this->findOriginById($origin);
        $title = 'Origin '.$origin->name;
        $links = $this->parentLinks($this);
        $parent = $this->getParentModule($this);
        return view('administration.role-management.origin.origin-show', compact('title','origin','links','parent'));
    }


    public function create(){
        if(!$this->getModule($this)->isAuthModuleAdd(auth()->user()->roles))return redirect()->back();

        $parent = $this->getParentModule($this);
        $model = new Origin();
        $links = $this->parentLinks($this);
        $fields = $model->fields();
        $departments = Department::all();
        return view('administration.role-management.origin.origin',['title'=>'New Origin','links'=>$links,'fields' => $fields,'parent'=>$parent, 'departments' => $departments]);
    }

    public function store(Request $request){
        $params = $request->except('_token');
        $params['slug'] = str_slug($request->input('name'));

        $origin =  $this->createOrigin($params);

        return ['status' => 'success', 'origin' => $origin];
    }

    public function edit($origin)
    {
        $module = $this->getModule($this);
        if ( request()->permissions_parent_modules == 1000 || $module->isAuthModuleEdit(auth()->user()->roles)){

        }else{
            return redirect()->back();
        }

        $origin = $this->findOriginById($origin);

        $title = 'Update ' . $origin->name;
        $parent = $this->getParentModule($this);
        $model = new Origin();
        $fields = $model->fields();
        $departments = Department::all();
        $links = $this->parentLinks($this);

        return view('administration.role-management.origin.origin-update', ['title' => $title,'links'=>$links, 'fields' => $fields, 'origin' => $origin, 'departments' => $departments, 'parent' => $parent]);
    }

    public function editAuthorise($id){
        $module = $this->getModule($this);
        if ( request()->permissions_parent_modules == 1000 || $module->isAuthModuleEdit(auth()->user()->roles)){
                return response()->json(['status'=>'success']);
        }else{
            return response()->json(['status'=>'error','message'=> 'you are not authorised to do this action']);
        }
    }

    public function update(Request $request, $origin){
        $params = $request->except('_token');
        $params['slug'] = str_slug($request->input('name'));

        $this->updateOrigin($params, $origin);
        $origin = $this->findOriginById($origin);
        return ['status' => 'success', 'origin' => $origin];
    }

    public function destroy ( $origin){
        if ( $this->getModule($this)->isAuthModuleDelete(auth()->user()->roles) ){
            if( $this->deleteOrigin($origin) ){
                return response()->json(['status'=>'success']);
            }else{
                return response()->json(['status'=>'error','message'=> 'something went wrong']);
            }
        }else{
            return response()->json(['status'=>'error','message'=> 'you are not authorised to do this action']);
        }

    }

    public function up($origin){
        return Origin::find($origin)->moveOrderUp();

    }
    public function down($origin){
        return Origin::find($origin)->moveOrderDown();
    }

    private function findOriginById(int $id) :Origin
    {
        return Origin::query()->where('id',$id)->get()->first();
    }

    private function createOrigin(array $params)
    {
       try {
            $module = new Origin($params);
            $module->save();
            return $module;
       } catch (\Exception $e) {
           return null;
       }
    }

    private function updateOrigin(array $params, int $id)
    {
        try {
            return Origin::query()->find($id)->update($params);
        } catch (\Exception $e) {
            return false;
        }
    }

    private function deleteOrigin(int $id)
    {

        try {
            return Origin::query()->find($id)->delete();
        } catch (\Exception $e) {
            return false;
        }
    }
    public function parentLinks($class){
        $module = Module::query()->where('controller_name',$this->getClassSubstring($class))->get()->first();
        $parent = $module->parent;
        $links[$module->name] = '';
        if($parent == null){
            return [];
        }else{

            do{
                $links[$parent->name] = '';
                $parent = $parent->parent;
            }while($parent->parent_id != null);
            $links[$parent->name] = $parent->slug;
        }
        return array_reverse($links);
    }
}
