<?php

namespace App\Http\Controllers\Administration\RoleManagement;

use App\Models\Module;
use App\Models\Department;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use Yajra\DataTables\DataTables;

class DepartmentController extends Controller
{
    //

    public function index(Request $request){


        if ( $request->ajax() ){
            return DataTables::of($this->listModel(new Department()))->make(true);
        }


        $add_button ='ADD A DEPARTMENT';
        $title = 'Department List';

        $parent = $this->getParentModule($this);
        $links = $this->parentLinks($this);

        return view('administration.role-management.department.departments',compact('add_button','links','parent','title'));
    }

    public function show($department){

        $department = $this->findDepartmentById($department);
        $title = 'Department '.$department->name;

        $parent = $this->getParentModule($this);
        $links = $this->parentLinks($this);
        return view('administration.role-management.department.department-show', compact('title','links','department','parent'));
    }


    public function create(){
        if(!$this->getModule($this)->isAuthModuleAdd(auth()->user()->roles))return redirect()->back();

        $parent = $this->getParentModule($this);
        $model = new Department();
        $fields = $model->fields();
        $links = $this->parentLinks($this);
        return view('administration.role-management.department.department',['title'=>'New Department','links'=>$links,'fields' => $fields,'parent'=>$parent]);
    }

    public function store(Request $request){
        $params = $request->except('_token');
        $params['slug'] = str_slug($request->input('name'));
        $params['tags'] = implode('|', $request->input('tags'));

        $department =  $this->createDepartment($params);

        return ['status' => 'success', 'department' => $department];
    }

    public function edit($department)
    {
        $module = $this->getModule($this);
        if ( request()->permissions_parent_modules == 1000 || $module->isAuthModuleEdit(auth()->user()->roles)){

        }else{
            return redirect()->back();
        }

        $department = $this->findDepartmentById($department);
        
        $department->tags = explode('|', $department->tags);

        $title = 'Update ' . $department->name;
        $parent = $this->getParentModule($this);
        $model = new Department();
        $fields = $model->fields();
        $links = $this->parentLinks($this);

        return view('administration.role-management.department.department-update', ['title' => $title,'links'=>$links, 'fields' => $fields,'department' => $department, 'parent' => $parent]);
    }

    public function editAuthorise($id){
        $module = $this->getModule($this);
        if ( request()->permissions_parent_modules == 1000 || $module->isAuthModuleEdit(auth()->user()->roles)){
                return response()->json(['status'=>'success']);
        }else{
            return response()->json(['status'=>'error','message'=> 'you are not authorised to do this action']);
        }
    }

    public function update(Request $request, $department){
        $params = $request->except('_token');
        $params['slug'] = str_slug($request->input('name'));
        $params['tags'] = implode('|', $request->input('tags'));

        $this->updateDepartment($params, $department);
        $department = $this->findDepartmentById($department);
        return ['status' => 'success', 'department' => $department];
    }

    public function destroy ( $department){
        if ( $this->getModule($this)->isAuthModuleDelete(auth()->user()->roles) ){
            if( $this->deleteDepartment($department) ){
                return response()->json(['status'=>'success']);
            }else{
                return response()->json(['status'=>'error','message'=> 'something went wrong']);
            }
        }else{
            return response()->json(['status'=>'error','message'=> 'you are not authorised to do this action']);
        }

    }

    public function up($department){
        return Department::find($department)->moveOrderUp();

    }
    public function down($department){
        return Department::find($department)->moveOrderDown();
    }

    private function findDepartmentById(int $id) :Department
    {
        return Department::query()->where('id',$id)->get()->first();
    }

    private function createDepartment(array $params)
    {
       try {
            $module = new Department($params);
            $module->save();
            return $module;
       } catch (\Exception $e) {
           return null;
       }
    }

    private function updateDepartment(array $params, int $id)
    {
        try {
            return Department::query()->find($id)->update($params);
        } catch (\Exception $e) {
            return false;
        }
    }

    private function deleteDepartment(int $id)
    {

        try {
            return Department::query()->find($id)->delete();
        } catch (\Exception $e) {
            return false;
        }
    }
    public function parentLinks($class){
        $module = Module::query()->where('controller_name',$this->getClassSubstring($class))->get()->first();
        $parent = $module->parent;
        $links[$module->name] = '';
        if($parent == null){
            return [];
        }else{

            do{
                $links[$parent->name] = '';
                $parent = $parent->parent;
            }while($parent->parent_id != null);
            $links[$parent->name] = $parent->slug;
        }
        return array_reverse($links);
    }

}
