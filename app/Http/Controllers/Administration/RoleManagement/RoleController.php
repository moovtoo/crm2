<?php

namespace App\Http\Controllers\Administration\RoleManagement;

use App\Models\Module;
use App\Models\Role;
use App\Models\ParentModule;
use App\Models\RoleModules;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use Yajra\DataTables\DataTables;

class RoleController extends Controller
{
    //

    public function index(Request $request){


        if ( $request->ajax() ){
            return DataTables::of($this->listModel(new Role(),[['id','!=','1000']]))->make(true);
        }

        $title = 'Role List';
        $add_button ='Add Role';
        $parent = $this->getParentModule($this);
        $links = $this->parentLinks($this);
        return view('administration.role-management.role.roles',compact('add_button','links','parent','title'));
    }

    public function show($role){

        $role = $this->findRoleById($role);
        $title = 'Role '.$role->name;
        $modules = Module::all();
        $links = $this->parentLinks($this);
        $parent = $this->getParentModule($this);
        $parents = ParentModule::all();
        return view('administration.role-management.role.role-show', compact('title','links','user','role','modules','parent', 'parents'));
    }


    public function create(){
        if(!$this->getModule($this)->isAuthModuleAdd(auth()->user()->roles))return redirect()->back();
        $parent = $this->getParentModule($this);
        $modules = Module::all();
        $links = $this->parentLinks($this);
        $parents = ParentModule::all();
        return view('administration.role-management.role.role',['title'=>'New Role','links'=>$links,'parent'=>$parent,'modules'=>$modules, 'parents'=>$parents]);
    }

    public function store(Request $request){
        $params = $request->except('_token','permissions');
        $params['slug'] = str_slug($request->input('name'));

        $role =  $this->createRole($params);

        if($request->has('permissions') and $request->input('permissions') != null){
            foreach ($request->input('permissions') as $module => $permissions){
                $par ['role_id'] = $role->id;
                $par ['module_id'] = $module;
                Log::info($permissions);
                foreach ($permissions as $k=>$v){
                    $par[$k]= 1;
                }
                Log::info($par);
                $this->createRoleModule($par);
            }
        }

        return $role;

    }

    public function edit($role)
    {
        $module = $this->getModule($this);
        if ( request()->permissions_parent_modules == 1000 || $module->isAuthModuleEdit(auth()->user()->roles)
            || $this->findRoleById($role)->user_id == auth()->id()
            || $this->findRoleById($role)->users()->where('user_id',auth()->id() )->count()>0 ){

        }else{
            return redirect()->back();
        }

        $role = $this->findRoleById($role);
        
        $parents = ParentModule::all();

        $title = 'Update ' . $role->name;
        $parent = $this->getParentModule($this);
        $modules = Module::all();
        $links = $this->parentLinks($this);

        return view('administration.role-management.role.role-update', ['title' => $title, 'links'=>$links,'role' => $role, 'parent' => $parent, 'modules'=> $modules, 'parents'=>$parents]);
    }

    public function editAuthorise($id){
        $module = $this->getModule($this);
        if ( request()->permissions_parent_modules == 1000 || $module->isAuthModuleEdit(auth()->user()->roles)
            || $this->findRoleById($id)->user_id == auth()->id()
        || $this->findRoleById($id)->users()->where('user_id',auth()->id() )->count()>0 ){
                return response()->json(['status'=>'success']);
        }else{
            return response()->json(['status'=>'error','message'=> 'you are not authorised to do this action']);
        }
    }

    public function update(Request $request, $role){
        $params = $request->except('_token');
        $params['slug'] = str_slug($request->input('name'));

        $this->updateRole($params, $role);
        $role = $this->findRoleById($role);
        $role->modules()->delete();
        foreach ($request->input('permissions') as $module => $permissions){
            $params ['role_id'] = $role->id;
            $params ['module_id'] = $module;
            foreach ($permissions as $k=>$v){
                $params[$k]= 1;
            }
            $this->createRoleModule($params);
        }
        return $role;
    }

    public function destroy ( $role){
        if ( $this->getModule($this)->isAuthModuleDelete(auth()->user()->roles) ){
            if( $this->deleteRole($role) ){
                return response()->json(['status'=>'success']);
            }else{
                return response()->json(['status'=>'error','message'=> 'something went wrong']);
            }
        }else{
            return response()->json(['status'=>'error','message'=> 'you are not authorised to do this action']);
        }

    }

    public function up($role){
        return Role::find($role)->moveOrderUp();

    }
    public function down($role){
        return Role::find($role)->moveOrderDown();
    }

    private function findRoleById(int $id) :Role
    {
        return Role::query()->where('id',$id)->get()->first();

    }

    private function createRole(array $params)
    {
       try {

            $module = new Role($params);
            $module->save();
            return $module;
       } catch (\Exception $e) {
           return null;
       }
    }

    private function updateRole(array $params, int $id)
    {
        try {
            return Role::query()->find($id)->update($params);
        } catch (\Exception $e) {
            return false;
        }
    }

    private function deleteRole(int $id)
    {

        try {
            return Role::query()->find($id)->delete();
        } catch (\Exception $e) {
            return false;
        }
    }

    private function createRoleModule($params){
        return RoleModules::create($params);
    }



    public function parentLinks($class){
        $module = Module::query()->where('controller_name',$this->getClassSubstring($class))->get()->first();
        $parent = $module->parent;
        $links[$module->name] = '';
        if($parent == null){
            return [];
        }else{

            do{
                $links[$parent->name] = '';
                $parent = $parent->parent;
            }while($parent->parent_id != null);
            $links[$parent->name] = $parent->slug;
        }
        return array_reverse($links);
    }


}
