<?php

namespace App\Http\Controllers\Administration;

use App\Http\Controllers\Controller;

class AdministrationController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function show()
    {
        $parent = $this->getParentModule($this);
        return view('administration.administration',compact('parent'));
    }

}
