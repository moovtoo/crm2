<?php

namespace App\Http\Controllers\Administration\SystemAdministration;

use App\Models\Module;
use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    //


    public function index(Request $request){


        if ( $request->ajax() ){
            return DataTables::of($this->listModel(new User(),[], [], 0, 'roles', 1000, '!='))->make(true);
        }

        $roles = Role::query()->pluck('name')->toArray();
        $add_button ='Add User';
        $filters = [
            'status' => ['Active', 'Inactive'],
            'role' => $roles
        ];
        $parent = $this->getParentModule($this);
        $title = 'Users List';
        $links = $this->parentLinks($this);
        return view('administration.system-administration.user.users',compact('add_button','links', 'filters','title','parent'));
    }


    public function show($user){

        $user = $this->findUserById($user);
        $title = 'User '.$user->name;
        $parent = $this->getParentModule($this);
        $links = $this->parentLinks($this);

        return view('administration.system-administration.user.user-show', compact('title','links','user','parent'));
    }


    public function create(){
        if(!$this->getModule($this)->isAuthModuleAdd(auth()->user()->roles))return redirect()->back();
        $roles = Role::query()->orderBy('name')->get();
        $parent = $this->getParentModule($this);
        $links = $this->parentLinks($this);
        return view('administration.system-administration.user.user',['title'=>'New User','roles'=>$roles,'links'=>$links,'parent'=>$parent]);
    }

    public function store(Request $request){
        $params = $request->except('_token','password','photo','roles');
        $params['slug'] = str_slug($request->input('name'));
        $params['password'] = Hash::make($request->input('password'));
        $validator = null;
        if ( $params['email'] != null){
            $user = User::onlyTrashed()->where('email',$params['email'] )->get()->first();
            if ( $user != null  ){
                $validator =  Validator::make($request->all(), [
                    'name' => ['required', 'string', 'max:255'],
                    'email' => ['required', 'string', 'email', 'max:255'],
                    'password' => ['required', 'string', 'min:6'],
                ]);
            }
        }

        if ( $validator == null )
        $validator = Validator::make($request->all(),[
            'email'=>'required|unique:users',
            'password'=>'required|min:6',
            'name'=>'required',
            'status'=>'required',
            ]);

        if ( $validator->fails() ){
            return ['status'=>'error','messages'=>$validator->errors()->getMessages()];
        }
        if ( $request->has('photo') and $request->input('photo') != null){
            $params['photo'] = $this->changeImagePath($request->input('photo'));
        }
        $user =  $this->createUser($params);
        $user->roles()->attach($request->input('roles'));

        return ['status'=> 'success','user'=>$user];
    }


    public function edit($id){
        $module = $this->getModule($this);
        if ( request()->permissions_parent_modules == 1000 || $module->isAuthModuleEdit(auth()->user()->roles)
            || $this->findUserById($id)->user_id == auth()->id()
            || $this->findUserById($id)->users()->where('user_id',auth()->id() )->count()>0 ){

        }else{
            return redirect()->back();
        }
        $editUser = $this->findUserById($id);
        $roles = Role::query()->orderBy('name')->get();

        $title = 'Update '.$editUser->name;
        $parent = $this->getParentModule($this);

        return view('administration.system-administration.user.user-update',['title'=>$title, 'editUser'=>$editUser,'roles'=>$roles,'parent'=>$parent]);
    }

    public function editAuthorise($id){
        $module = $this->getModule($this);
        if ( request()->permissions_parent_modules == 1000 ||
            $module->isAuthModuleEdit(auth()->user()->roles)
            || $this->findUserById($id)->user_id == auth()->id()
            || $this->findUserById($id)->users()->where('user_id',auth()->id() )->count()>0){
            return response()->json(['status'=>'success']);
        }else{
            return response()->json(['status'=>'error','message'=> 'you are not authorised to do this action']);
        }
    }

    public function update(Request $request, $user){
        $params = $request->except('_token','password','photo','roles');
        $params['slug'] = str_slug($request->input('name'));

        if ( $request->has('photo') and $request->input('photo') != null ){
            $params['photo'] = $this->changeImagePath($request->input('photo'));
        }


        $this->updateUser($params, $user);
        $user = $this->findUserById($user);
        $user->roles()->sync($request->input('roles'));
        return ['status'=> 'success','user'=>$user];
    }
    public function destroy ( $user){
        if ( $this->getModule($this)->isAuthModuleDelete(auth()->user()->roles) ){
            if( $this->deleteUser($user) ){
                return response()->json(['status'=>'success']);
            }else{
                return response()->json(['status'=>'error','message'=> 'something went wrong']);
            }
        }else{
            return response()->json(['status'=>'error','message'=> 'you are not authorised to do this action']);
        }

    }

    public function removeImage($id){
        try {
            $user = User::query()->where('id',$id)->get()->first();
            $user->photo = null;
            $user->save();
            return redirect()->back();
        } catch (\Exception $e) {
            return redirect()->back();
        }
    }


    public function up($user){
        return User::find($user)->moveOrderUp();

    }
    public function down($user){
        return User::find($user)->moveOrderDown();
    }

    private function findUserById(int $id) :User
    {
        return User::query()->where('id',$id)->get()->first();
    }
    

    private function createUser(array $params)
    {
        try {
            if ( $params['email']!= ''){
                $user = User::onlyTrashed()->where('email',$params['email'] )->get()->first();
                if ( $user != null ){
                    $user = User::onlyTrashed()->where('email',$params['email'])->get()->first()->restore();
                    $user = User::query()->where('email',$params['email'])->get()->first();
                    $user->name = $params['name'];
                    $user->password = $params['password'];
                    $user->role_id = $params['role_id'];
                    $user->status = $params['status'];
                    $user->save();
                    return $user;
                }
            }
            $module = new User($params);
            $module->save();
            return $module;
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }
    private function updateUser(array $params, int $id)
    {
        try {
            return User::query()->find($id)->update($params);
        } catch (\Exception $e) {
            return false;
        }
    }
    private function deleteUser(int $id)
    {

        try {
            return User::query()->find($id)->delete();
        } catch (\Exception $e) {
            return false;
        }
    }
    public function parentLinks($class){
        $module = Module::query()->where('controller_name',$this->getClassSubstring($class))->get()->first();
        $parent = $module->parent;
        $links[$module->name] = '';
        if($parent == null){
            return [];
        }else{

            do{
                $links[$parent->name] = '';
                $parent = $parent->parent;
            }while($parent->parent_id != null);
            $links[$parent->name] = $parent->slug;
        }
        return array_reverse($links);
    }

}
