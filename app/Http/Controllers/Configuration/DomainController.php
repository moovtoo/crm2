<?php

namespace App\Http\Controllers\Configuration;

use App\Models\Domain;
use App\Utils\TextField;
use App\Utils\Transform;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\Facades\DataTables;

class DomainController extends  Controller
{
    //

    use Transform;


    public function index(Request $request){


        if ( $request->ajax() ){
            return DataTables::of($this->listDomains())->make(true);
        }

        $model = new Domain();
        $fields = $model->fields();
        $add_button ='Add Domain';
        $filters = [
            'type' => ['Domain', 'Sub']
        ];

        return view('configuration.domains.domains',compact('fields','add_button','filters'));
    }


    public function show($domain){

        $domain = $this->findDomainById($domain);
        $title = 'Domain '.$domain->name;

        return view('configuration.domains.domain-show', compact('title','domain'));
    }

    public function create(){

        $model = new Domain();
        $fields = $model->fields();
        return view('configuration.domains.domain',['title'=>'New Domain','fields'=>$fields]);
    }

    public function editAuthorise($id){
        return response()->json(['status'=>'success']);
    }
    
    public function store(Request $request){
        $params = $request->except('_token');

        $domain =  $this->createDomain($params);
        return $domain;
    }

    public function edit($domain){
        $domain = $this->findDomainById($domain);

        $model = new Domain();
        $fields = $model->fields();

        $title = 'Update '.$domain->name;

        return view('configuration.domains.domain-update',['title'=>$title, 'fields'=>$fields, 'domain'=>$domain]);
    }


    public function update(Request $request, $domain){
        $params = $request->except('_token');
        $params['slug'] = str_slug($request->input('name'));

        $this->updateDomain($params, $domain);
        $domain = $this->findDomainById($domain);

        return $domain;
    }

    public function destroy ( $domain){
        if( $this->deleteDomain($domain) ){
            return "success";
        }else{
            return 'failed';
        }
    }

    public function up($domain){
        return Domain::find($domain)->moveOrderUp();

    }
    public function down($domain){
        return Domain::find($domain)->moveOrderDown();
    }


    private function findDomainById(int $id) :Domain
    {
        return Domain::query()->where('id',$id)->get()->first();
    }

    private function listDomains($id = null)
    {
        if ( $id  == null ){
            $modulesPage =  Domain::query()
                ->where('parent_id', null)
                ->orderBy('order','desc')
                ->get();
            $modules = $modulesPage->map( function (Domain $module){
                return $this->transformIndexDomain($module);
            })->all();
        }else{
            $modulesPage =  Domain::query()
                ->where('parent_id', null)
                ->orderBy('order','desc')
                ->get();
            $modules = $modulesPage->map( function (Domain $module){
                return $this->transformIndexDomain($module);
            })->all();
        }

        return $modules;
    }

    private function createDomain(array $params)
    {
        try {

            $domain = new Domain($params);
            $domain->save();
            return $domain;
        } catch (\Exception $e) {
            return null;
        }
    }

    private function updateDomain(array $params, int $id)
    {
        try {
            return Domain::query()->find($id)->update($params);
        } catch (\Exception $e) {
            return false;
        }
    }

    private function deleteDomain(int $id)
    {

//        try {
            return Domain::query()->find($id)->delete();
//        } catch (\Exception $e) {
//            return false;
//        }
    }

}
