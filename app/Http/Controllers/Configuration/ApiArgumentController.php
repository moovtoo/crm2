<?php

namespace App\Http\Controllers\Configuration;

use App\Models\ApiArgument;
use App\Models\Api;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\Facades\DataTables;

class ApiArgumentController extends  Controller
{
    //



    public function index(Request $request){


        if ( $request->ajax() ){
            if($request->has('api_id')){
                return DataTables::of($this->listModel(new ApiArgument(), [['api_id',$request->get('api_id')]]))->make(true);
            }
            return DataTables::of($this->listModel(new ApiArgument()))->make(true);
        }

        $model = new ApiArgument();
        $fields = $model->fields();
        $add_button ='Add Api Argument';
        $filters = [
            'type' => ['Api', 'Sub']
        ];

        return view('configuration.api-argument.api-argument',compact('fields','add_button','filters'));
    }


    public function show($apiArgument){

        $apiArgument = $this->findApiArgumentById($apiArgument);
        $title = 'ApiArgument '.$apiArgument->name;

        return view('configuration.api-argument.api-argument-show', compact('title','apiArgument'));
    }

    public function create(){
        $model = new ApiArgument();
        $fields = $model->fields();
        return view('configuration.api-argument.api-argument',['title'=>'New Api Argument','fields'=>$fields]);
    }

    public function store(Request $request){
        $params = $request->except('_token');

        $apiArgument =  $this->createApiArgument($params);
        return ['status'=> 'success','apiArgument'=>$apiArgument];
    }

    public function edit($apiArgument){
        $apiArgument = $this->findApiArgumentById($apiArgument);
        $model = new ApiArgument();
        $fields = $model->fields();
        $title = 'Update '.$apiArgument->name;

        return view('configuration.api-argument.api-argument-update',['title'=>$title, 'fields'=>$fields, 'apiArgument'=>$apiArgument]);
    }
    
    public function editAuthorise($id){
        return response()->json(['status'=>'success']);
    }

    public function update(Request $request, $apiArgument){
        $params = $request->except('_token');
        $params['slug'] = str_slug($request->input('name'));

        $this->updateApiArgument($params, $apiArgument);
        $apiArgument = $this->findApiArgumentById($apiArgument);

        return ['status'=> 'success','apiArgument'=>$apiArgument];
    }

    public function destroy ( $apiArgument){
        if( $this->deleteApiArgument($apiArgument) ){
            return "success";
        }else{
            return 'failed';
        }
    }

    public function up($apiArgument){
        return ApiArgument::find($apiArgument)->moveOrderUp();

    }
    public function down($apiArgument){
        return ApiArgument::find($apiArgument)->moveOrderDown();
    }


    private function findApiArgumentById(int $id) :ApiArgument
    {
        return ApiArgument::query()->where('id',$id)->get()->first();
    }


    private function createApiArgument(array $params)
    {
        try {

            $apiArgument = new ApiArgument($params);
            $apiArgument->save();
            return $apiArgument;
        } catch (\Exception $e) {
            return null;
        }
    }

    private function updateApiArgument(array $params, int $id)
    {
        try {
            return ApiArgument::query()->find($id)->update($params);
        } catch (\Exception $e) {
            return false;
        }
    }

    private function deleteApiArgument(int $id)
    {

       try {
            return ApiArgument::query()->find($id)->delete();
       } catch (\Exception $e) {
           return false;
       }
    }

}
