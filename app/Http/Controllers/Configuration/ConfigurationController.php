<?php

namespace App\Http\Controllers\Configuration;

use App\Models\Settings;
use App\Models\Locale;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class ConfigurationController extends Controller
{


    public function show(){
        return view('configuration.configuration');
    }


    public function indexBranding(){

        $settings = $this->listSetting();
        return view('configuration.configurations.branding',compact('settings'));
    }

    public function brandingStore(Request $request ){

        $this->updateSetting($request->input('cms_title') , 'cms_title');

        if ( $request->has('logo') and $request->input('logo') != ''){
            $this->updateSetting($this->changeImagePath($request->input('logo')) , 'logo');
        }
        if ( $request->has('background') and $request->input('background') != '' ){
            $this->updateSetting($this->changeImagePath($request->input('background')) , 'background');
        }
        if ( $request->has('favicon') and $request->input('favicon') != ''){
            $this->updateSetting($this->changeImagePath($request->input('favicon')) , 'favicon');
        }

        $settings = $this->listSetting();
        return view('configuration.configurations.branding',compact('settings'));
    }

    public function indexDomain(){

        $settings = $this->listSetting();
        return view('configuration.configurations.domain',compact('settings'));
    }

    public function domainStore(Request $request ){

        $this->updateSetting($request->input('domain_name') , 'domain_name');

        $settings = $this->listSetting();
        return view('configuration.configurations.domain',compact('settings'));
    }
    
    public function indexSuperUser(){

        $settings = $this->listSetting();
        return view('configuration.configurations.super-user',compact('settings'));
    }

    public function superUserStore(Request $request ){

        $this->updateSetting($request->input('name') , 'name');
        $this->updateSetting($request->input('email') , 'email');
        if ( $request->has('profile') ){
            $this->updateSetting($this->changeImagePath($request->input('profile')) , 'profile');
        }

        $validator = Validator::make($request->all(), [
            'password' => 'required_with:old_password|nullable|min:6|confirmed',
        ]);
        if ($validator->fails()) {
            return ['status' => 'error', 'messages' => $validator->errors()->getMessages()];
        }
        $settings = $this->listSetting();
        if ($request->input('old_password') !='' && !Hash::check( $request->input('old_password'),$settings['password'])) {
            return ['status' => 'error', 'messages' => ["old_password" => 'Password not match']];
        }
        $this->updateSetting(Hash::make($request->input('password')) , 'password');

        return ['status' => 'success'];
    }

    public function removeImage($key, $locale = 1){
        try {
            $module = Settings::query()->where('key', $key )->where('locale_id',$locale)->first();
            $module->value = null;
            $module->save();
            return redirect()->back();

        } catch (\Exception $e) {
            return redirect()->back();
        }
    }
    
    public function indexLanguage(){

        $settings = $this->listSetting();
        $locales = Locale::all();
        return view('configuration.configurations.languages',compact('settings', 'locales'));
    }
    
    public function languageChange($language){

        $this->updateSetting($language , 'language');

        $settings = $this->listSetting();
        return redirect()->route('configuration.language',compact('settings'));
    }

    public function createLang(){
        $model = new Locale();
        $fields = $model->fields();

        $settings = $this->listSetting();
        return view('configuration.configurations.language',compact('settings', 'fields'));
    }
    
    public function storeLanguage(Request $request){
        $params = $request->except('_token','flag');

        if($request->has('flag') and $request->input('flag') != null){
            $params['flag'] = $this->changeImagePath($request->input('flag'));
        }

        $language =  $this->createLanguage($params);
        return ['status'=>'success', 'language'=>$language];
    }

    private function createLanguage(array $params)
    {
        try {
            $language = new Locale($params);
            $language->save();
            return $language;
        } catch (\Exception $e) {
            return null;
        }
    }

    private function listSetting($locale_id = 1){
        $setting = array();
        $settings = Settings::query()->where('locale_id', $locale_id)->get();
        foreach ($settings as $set) {
            $setting [$set->key] = $set->value;
        }
        return $setting;
    }

    private function createSetting(array $params): Settings
    {
        try {

            $module = new Settings($params);
            $module->save();
            return $module;
        } catch (\Exception $e) {
            return null;
        }
    }

    private  function updateSetting($value , $key , $locale= 1): bool
    {
        try {
            $module = Settings::query()->where('key', $key )->where('locale_id',$locale)->first();
            if ($module == null ){
                $module = new Settings(['key'=>$key,'value'=>$value,'locale_id'=>$locale]);
                $module->save();
                return true;
            }else{
                $module->value = $value;
                $module->save();
                return true;
            }

        } catch (\Exception $e) {
            return false;
        }
    }
    
}
