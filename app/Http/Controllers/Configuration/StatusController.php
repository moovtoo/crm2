<?php

namespace App\Http\Controllers\Configuration;
use App\Models\Module;
use App\Models\Status;

use App\Utils\TextField;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;



class StatusController extends Controller
{

    //
    public function index(Request $request){


        if ( $request->ajax() ){
            return DataTables::of($this->listModel(new Status()))->make(true);
        }


//        $fields = $this->StatusFields();
        $model = new Status();

        $add_button ='Add Status';
        $fields = $model->fields();

//        $parent = $this->getParentModule($this);

        return view('configuration.status.statuses',compact('fields','add_button'));
    }
    public function show($status){

        $status = $this->findStatusById($status);
        $title = 'Status '.$status->name;
        $modules = Module::all();

//        $parent = $this->getParentModule($this);
        return view('configuration.status.status-show', compact('title','user','status','modules'));
    }


    public function create(){
//        if(!$this->getModule($this)->isAuthModuleAdd(auth()->user()->roles))
//            return redirect()->back();
//        $fields = $this->StatusFields();
//        $parent = $this->getParentModule($this);
        $modules = Module::all();
        return view('configuration.status.status',['title'=>'New Status','modules'=>$modules]);
    }

    public function store(Request $request){
        $params = $request->except('_token');
        $params['slug'] = str_slug($request->input('name'));
        $status =  $this->createStatus($params);
        return ['status'=> 'success', 'statuse' =>$status];

    }

    public function edit($status)
    {
        $module = $this->getModule($this);
        if ( request()->permissions_parent_modules == 1000 || $module->isAuthModuleEdit(auth()->user()->roles)){

        }else{
            return redirect()->back();
        }
        $status = $this->findStatusById($status);
//        $fields = $this->StatusFields();
        $title = 'Update ' . $status->name;
//        $parent = $this->getParentModule($this);
        $modules = Module::all();

        return view('configuration.status.status-update', ['title' => $title,  'status' => $status,  'modules'=> $modules]);
    }
    public function editAuthorise($id){
        $module = $this->getModule($this);
        if ( request()->permissions_parent_modules == 10000 ||
            $module->isAuthModuleEdit(auth()->user()->roles)){
            return response()->json(['status'=>'success']);
        }else{
            return response()->json(['status'=>'error','message'=> 'you are not authorised to do this action']);
        }
    }


    public function update(Request $request, $status){
        $params = $request->except('_token');
        $params['slug'] = str_slug($request->input('name'));

        $this->updateStatus($params, $status);
        $status = $this->findStatusById($status);
        return ['status'=> 'success','statuse'=>$status];
    }

    public function destroy ( $status){
        if ( $this->getModule($this)->isAuthModuleDelete(auth()->user()->roles) ){
            if( $this->deleteStatus($status) ){
                return response()->json(['status'=>'success']);
            }else{
                return response()->json(['status'=>'error','message'=> 'something went wrong']);
            }
        }else{
            return response()->json(['status'=>'error','message'=> 'you are not authorised to do this action']);
        }

    }

    public function up($status){
        return Status::find($status)->moveOrderUp();

    }
    public function down($status){
        return Status::find($status)->moveOrderDown();
    }

    private function findStatusById(int $id) :Status
    {
        return Status::query()->where('id',$id)->get()->first();

    }

    private function createStatus(array $params)
    {
        try {

            $module = new Status($params);
            $module->save();
            return $module;

        } catch (\Exception $e) {
            return null;
        }
    }

    private function updateStatus(array $params, int $id)
    {
        try {
            return Status::query()->find($id)->update($params);
        } catch (\Exception $e) {
            return false;
        }
    }

    private function deleteStatus(int $id)
    {

        try {
            return Status::query()->find($id)->delete();
        } catch (\Exception $e) {
            return false;
        }
    }







}
