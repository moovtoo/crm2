<?php

namespace App\Http\Controllers\Configuration;

use App\Models\Api;
use App\Models\ApiArgument;
use App\Models\Module;
use App\Utils\TextField;
use App\Utils\Transform;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\Facades\DataTables;

class ApiController extends  Controller
{
    //


    public function index(Request $request){


        if ( $request->ajax() ){
            return DataTables::of($this->listModel(new Api()))->make(true);
        }


        $model = new Api();
        $fields = $model->fields();
        $add_button ='Add Api';
        $filters = [
            'type' => ['Api', 'Sub']
        ];

        return view('configuration.apis.apis',compact('fields','add_button','filters'));
    }


    public function show($api){

        $api = $this->findApiById($api);
        $title = 'Api '.$api->name;

        return view('configuration.apis.api-show', compact('title','api'));
    }

    public function editAuthorise($id){
        return response()->json(['status'=>'success']);
    }

    public function create(){
        $model = new Api();
        $fields = $model->fields();
        $modules = Module::all();
        return view('configuration.apis.api',['title'=>'New Api','fields'=>$fields,'modules'=>$modules]);
    }

    public function store(Request $request){
        $params = $request->except('_token');
        $params['slug'] = str_slug($request->input('name'));
        $params['type']=  trim(strtoupper($request->input('type')));

        $api =  $this->createApi($params);
        return ['status'=> 'success','api'=>$api];
    }

    public function edit($api){

        $api = $this->findApiById($api);
        $model = new Api();
        $fields = $model->fields();
        $title = 'Update '.$api->name;
        $modules = Module::all();

        return view('configuration.apis.api-update',['title'=>$title, 'fields'=>$fields, 'api'=>$api, 'modules'=>$modules]);
    }


    public function update(Request $request, $api){
        $params = $request->except('_token');
        $params['slug'] = str_slug($request->input('name'));
        $params['type']=  trim(strtoupper($request->input('type')));

        $this->updateApi($params, $api);
        $api = $this->findApiById($api);

        return ['status'=> 'success','api'=>$api];
    }

    public function destroy ( $api){
        if( $this->deleteApi($api) ){
            return "success";
        }else{
            return 'failed';
        }
    }

    public function up($api){
        return Api::find($api)->moveOrderUp();

    }
    public function down($api){
        return Api::find($api)->moveOrderDown();
    }


    private function findApiById(int $id) :Api
    {
        return Api::query()->where('id',$id)->get()->first();
    }

    private function listApis($id = null)
    {
        if ( $id  == null ){
            $modulesPage =  Api::query()

                ->orderBy('order','desc')
                ->get();
            $modules = $modulesPage->map( function (Api $module){
                return $this->transformIndexApi($module);
            })->all();
        }else{
            $modulesPage =  Api::query()
                ->orderBy('order','desc')
                ->get();
            $modules = $modulesPage->map( function (Api $module){
                return $this->transformIndexApi($module);
            })->all();
        }

        return $modules;
    }

    private function createApi(array $params)
    {
        try {

            $api = new Api($params);
            $api->save();
            return $api;
        } catch (\Exception $e) {
            return null;
        }
    }

    private function updateApi(array $params, int $id)
    {
        try {
            return Api::query()->find($id)->update($params);
        } catch (\Exception $e) {
            return false;
        }
    }

    private function deleteApi(int $id)
    {

       try {
            return Api::query()->find($id)->delete();
       } catch (\Exception $e) {
           return false;
       }
    }


    public function apis(Request $request , $version , $slug){

        $api = Api::query()->where('slug',$slug)->where('version',$version)->first();
        if ( $api == null || $api->type != $request->method() ){
            if ( $request->acceptsJson()){
                return response()->json(['status'=>'error', 'message'=>'apis request not exist.']);
            }else{
                return response(['status'=>'error', 'message'=>'apis request not exist.']);
            }
        }
        if ( $api->authenticated == 1 &&
            !($request->has('access_token')
                || $request->header('access_token') ) ){
            if ( $request->acceptsJson()){
                return response()->json(['status'=>'error', 'message'=>'this request needs auth request']);
            }else{
                return response(['status'=>'error', 'message'=>'this request needs auth request.']);
            }

        }
        if( $api->authenticated == 1 )
        {
            if ( Auth::guard('api')->user() == null ){
                if ( $request->acceptsJson()){
                    return response()->json(['status'=>'error', 'message'=>'cannot fetch user']);
                }else{
                    return response(['status'=>'error', 'message'=>'cannot fetch user']);
                }
            }
        }


        $messages = array() ;
        foreach ( $api->arguments as $argument ){
            if ( $argument->required && !$request->has($argument->key)){
                $messages[$argument->key] ="This field is required";
            }
        }

        if ( !empty($messages)){
            if ( $request->acceptsJson()){
                return response()->json(['status'=>'error', 'message'=>$messages]);
            }else{
                return response(['status'=>'error', 'message'=>$messages]);
            }
        }

        $module = $api->module;
        $model = '\App\Models\\'.str_replace('Controller','',$module->controller_name);



        if ( $api->type == 'GET'){
            $sql = $model::query();

            if ( $api->custom_select )
                $sql->select($api->custom_select);

            foreach ($api->arguments as $argument){
                if ( $argument->type == 'or')
                    $sql->orWhere($argument->key,$argument->operator , $request->input($argument->key));
            }

            $sql->orderBy($api->order_by);

            if ( $api->paginate == 0 ){
                $result = $sql->get();
            }else{
                $result = $sql->paginate($api->paginate);
            }

            if ( $request->acceptsJson()){
                return response()->json(['status'=>'success', 'data'=>$result]);
            }else{
                return response(['status'=>'success', 'data'=>$result]);
            }
        }elseif( $api->type == 'POST'){
            $sql = $model::query();

            $params = array();
            foreach ($api->arguments as $argument){
                $params[$argument->key]= $request->input($argument->key);
            }

            $result = $sql->create($params);

            if ( $request->acceptsJson()){
                return response()->json(['status'=>'success', 'data'=>$result]);
            }else{
                return response(['status'=>'success', 'data'=>$result]);
            }
        }elseif( $api->type == 'PUT' ){
            $sql = $model::query();

            $params = array();
            $update = array();
            foreach ($api->arguments as $argument){
                if ( $argument->updated_key)
                    $update[$argument->key] = $request->input($argument->key);
                else
                    $params[$argument->key]= $request->input($argument->key);
            }

            if ( !empty($update)){
                if ( $request->acceptsJson()){
                    return response()->json(['status'=>'error', 'message'=>"Updating Key is missing"]);
                }else{
                    return response(['status'=>'error', 'message'=>"Updating Key is missing"]);
                }
            }

            $result = $sql->where($update)->first()->update($params);

            if ( $request->acceptsJson()){
                return response()->json(['status'=>'success', 'data'=>$result]);
            }else{
                return response(['status'=>'success', 'data'=>$result]);
            }
        }elseif($api->type == 'DELETE'){
            $sql = $model::query();

            $update = array();
            foreach ($api->arguments as $argument){
                if ( $argument->updated_key)
                    $update[$argument->key] = $request->input($argument->key);
            }

            if ( !empty($update)){
                if ( $request->acceptsJson()){
                    return response()->json(['status'=>'error', 'message'=>"Updating Key is missing"]);
                }else{
                    return response(['status'=>'error', 'message'=>"Updating Key is missing"]);
                }
            }

            $result = $sql->where($update)->first()->delete();

            if ( $request->acceptsJson()){
                return response()->json(['status'=>'success', 'data'=>$result]);
            }else{
                return response(['status'=>'success', 'data'=>$result]);
            }
        }



    }
}
