<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCountriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('countries', function (Blueprint $table) {
//            $table->increments('id');
//            $table->string('name');
//            $table->string('slug');
//            $table->integer('order')->default(0);
//            $table->unsignedInteger('ref_id')->nullable();
//            $table->string('code')->nullable();
//            $table->string('flag')->nullable();
//            $table->string('nationality')->nullable();
            $table->increments('id');
            $table->string('name');
            $table->string('iso')->unique();
            $table->string('iso3')->nullable();
            $table->integer('numcode')->nullable();
            $table->integer('phonecode');
            $table->integer('status');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('countries');
    }
}
