<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function($table) {
            $table->string('position')->nullable();
        });
        Schema::table('users', function($table) {
            $table->string('facebook')->nullable();
        });
        Schema::table('users', function($table) {
            $table->string('twitter')->nullable();
        });
        Schema::table('users', function($table) {
            $table->string('linkedin')->nullable();
        });
        Schema::table('users', function($table) {
            $table->string('instagram')->nullable();
        });
        Schema::table('users', function($table) {
            $table->string('website')->nullable();
        });
        Schema::table('users', function($table) {
            $table->string('blog')->nullable();
        });
        Schema::table('users', function($table) {
            $table->text('bio')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            //
        });
    }
}
