<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLeadActionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lead_actions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('order')->default(0);
            $table->string('name');
            $table->string('slug')->nullable();
            $table->text('attachment')->nullable();
            $table->integer('user_id')->nullable();
            $table->integer('contact_id');
            $table->integer('status_id');
            $table->integer('type_id');
            $table->integer('lead_id');
            $table->text('note')->nullable();
            $table->date('created_date');
            $table->softDeletes();
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
