<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArticlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('articles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('slug');
            $table->text('description');
            $table->text('content');
            $table->text('meta_title')->nullable();
            $table->text('meta_description')->nullable();
            $table->text('meta_url')->nullable();
            $table->string('meta_image')->nullable();
            $table->text('meta_type')->nullable();
            $table->string('photo')->nullable();
            $table->enum('status',['pending','approved','published','suspended']);
            $table->unsignedInteger('locale_id')->default(1);
            $table->unsignedInteger('order')->default(1);
            $table->date('date_created')->nullable();
            $table->date('date_updated')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('articles');
    }
}
