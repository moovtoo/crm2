<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHostingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hostings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('order')->default(0);
            $table->string('name');
            $table->string('slug');
            $table->date('starting_date')->nullable();
            $table->date('end_date')->nullable();
            $table->integer('domain_management_id')->nullable();
            $table->tinyInteger('management_email')->nullable();
            $table->integer('organization_id')->nullable();
            $table->integer('contact_id')->nullable();
            $table->integer('status_id')->nullable();
            $table->tinyInteger('ssl_type')->nullable();
//            $table->date('ssl_installation_date');
//            $table->date('ssl_end_date');
            $table->integer('type_id')->nullable();
            $table->integer('technology_id')->nullable();
//            $table->date('creation_date');
            $table->date('renewal_date');
            $table->integer('price')->nullable();
            $table->integer('server_id')->nullable();
//            $table->integer('sub_domain_id')->nullable();
            $table->string('cpanel_code')->nullable();
            $table->string('cpanel_password')->nullable();
            $table->string('database_name')->nullable();
            $table->text('notes')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hostings');
    }
}
