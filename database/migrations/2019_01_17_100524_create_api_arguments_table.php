    <?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApiArgumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('api_arguments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('order')->default(0);
            $table->unsignedInteger('api_id');
            $table->string('key');
            $table->string('operator')->default('=');
            $table->boolean('updated_key')->default(false);
            $table->boolean('required')->default(false);
            $table->enum('type',['or','and']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('api_arguments');
    }
}
