<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModules extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('modules', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('table_name');
            $table->string('slug');
            $table->string('controller_name');
            $table->integer('order')->default(0);
            $table->unsignedInteger('ref_id')->nullable();
            $table->unsignedInteger('type_id')->nullable();
            $table->unsignedInteger('parent_id')->nullable();
            $table->boolean('allow_locale')->default(1);
            $table->boolean('user_related')->default(1);
            $table->text('success_message')->nullable();
            $table->unsignedInteger('success_message_ref_id')->nullable();
            $table->text('update_message')->nullable();
            $table->unsignedInteger('update_message_ref_id')->nullable();
            $table->text('delete_message')->nullable();
            $table->unsignedInteger('delete_message_ref_id')->nullable();
            $table->text('error_message')->nullable();
            $table->unsignedInteger('error_message_ref_id')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('modules');
    }
}
