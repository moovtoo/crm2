<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLeadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
            Schema::create('leads', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('order')->default(0);
                $table->string('name');
                $table->string('slug')->nullable();
                $table->integer('organization_id');
                $table->integer('orogin_id')->nullable();
                $table->integer('user_id')->nullable();
                $table->integer('contact_id');
                $table->integer('status_id')->nullable();
                $table->text('description')->nullable();
                $table->date('created_date')->nullable();
                $table->softDeletes();
                $table->timestamps();
            });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('leads');
    }
}
