<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('apis', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('slug');
            $table->text('custom_select')->nullable();
            $table->text('custom_condition')->nullable();
            $table->unsignedInteger('module_id');
            $table->unsignedInteger('order')->default(0);
            $table->string('version')->default('v1');
            $table->boolean('authenticated')->default(false);
            $table->string('response')->default('json');
            $table->string('type')->default('GET');
            $table->string('order_by')->default('id');
            $table->integer('paginate')->default(0);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('apis');
    }
}
