<?php

use Illuminate\Database\Seeder;

class SectorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('sectors')->insert([
            'name' => "Distribution",
        ]);
        DB::table('sectors')->insert([
            'name' => "Consulting",
        ]);
        DB::table('sectors')->insert([
            'name' => "Finance",
        ]);
        DB::table('sectors')->insert([
            'name' => "E-Commerce",
        ]);
        DB::table('sectors')->insert([
            'name' => "Software",
        ]);
        DB::table('sectors')->insert([
            'name' => "Media",
        ]);
        DB::table('sectors')->insert([
            'name' => "Tourisme",
        ]);
        DB::table('sectors')->insert([
            'name' => "Chambre de Commerce",
        ]);
        DB::table('sectors')->insert([
            'name' => "Agency",
        ]);
        DB::table('sectors')->insert([
            'name' => "Telecom",
        ]);
        DB::table('sectors')->insert([
            'name' => "Chatbot",
        ]);
        DB::table('sectors')->insert([
            'name' => "Bâtiments",
        ]);
        DB::table('sectors')->insert([
            'name' => "Blockchain",
        ]);
        DB::table('sectors')->insert([
            'name' => "Start-up",
        ]);
        DB::table('sectors')->insert([
            'name' => "Transport",
        ]);
        DB::table('sectors')->insert([
            'name' => "Incubateur",
        ]);
        DB::table('sectors')->insert([
            'name' => "Production House",
        ]);

        DB::table('sectors')->insert([
            'name' => "Studio Photo",
        ]);
        DB::table('sectors')->insert([
            'name' => "Robot",
        ]);
        DB::table('sectors')->insert([
            'name' => "Alimentation",
        ]);
        DB::table('sectors')->insert([
            'name' => "Platforme",
        ]);
        DB::table('sectors')->insert([
            'name' => "Beaute",
        ]);
        DB::table('sectors')->insert([
            'name' => "Education",
        ]);

        DB::table('sectors')->insert([
            'name' => "Fashion",
        ]);
        DB::table('sectors')->insert([
            'name' => "HR",
        ]);
        DB::table('sectors')->insert([
            'name' => "Geolocalisation",
        ]);
        DB::table('sectors')->insert([
            'name' => "IOT",
        ]);
        DB::table('sectors')->insert([
            'name' => "Pépinières",
        ]);
        DB::table('sectors')->insert([
            'name' => "Culture",
        ]);
        DB::table('sectors')->insert([
            'name' => "Sante",
        ]);
        DB::table('sectors')->insert([
            'name' => "Presse",
        ]);

        DB::table('sectors')->insert([
            'name' => "Club",
        ]);

        DB::table('sectors')->insert([
            'name' => "Developpement Urbain",
        ]);

        DB::table('sectors')->insert([
            'name' => "Sport",
        ]);

        DB::table('sectors')->insert([
            'name' => "Furnisher",
        ]);

        DB::table('sectors')->insert([
            'name' => "Mobile",
        ]);

    }
}
