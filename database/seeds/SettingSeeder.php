<?php

use Illuminate\Database\Seeder;

class SettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('settings')->insert([
            'key' => "logo",
            'value' =>'',
            'order'=>'1',
        ]);
        DB::table('settings')->insert([
            'key' => "title",
            'value' =>'',
            'order'=>'1',
        ]);
        DB::table('settings')->insert([
            'key' => "email",
            'value' =>'',
            'order'=>'1',
        ]);
        DB::table('settings')->insert([
            'key' => "logo_mobile",
            'value' =>'',
            'order'=>'1',
        ]);
        DB::table('settings')->insert([
            'key' => "favicon",
            'value' =>'',
            'order'=>'1',
        ]);
        DB::table('settings')->insert([
            'key' => "facebook",
            'value' =>'',
            'order'=>'1',
        ]);
        DB::table('settings')->insert([
            'key' => "youtube",
            'value' =>'',
            'order'=>'1',
        ]);
    }

}
