<?php

use Illuminate\Database\Seeder;

class TechnologiesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('technologies')->insert([

            'id' => "1",
            'name' => "PHP/MySQL",

        ]);
        DB::table('technologies')->insert([

            'id' => "2",
            'name' => "PHP/Mysql+Zend",

        ]);
        DB::table('technologies')->insert([

            'id' => "3",
            'name' => "DRUPAL",

        ]);
        DB::table('technologies')->insert([

            'id' => "4",
            'name' => "Laravel",

        ]);
        DB::table('technologies')->insert([

            'id' => "5",
            'name' => "SDK",

        ]);
        DB::table('technologies')->insert([

            'id' => "6",
            'name' => "PHP",

        ]);
    }
}
