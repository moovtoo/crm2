<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('users')->insert([
            'name' => "Admin Admin",
            'email' => "admin@admin.com",
            'status' =>'active',
            'password' => bcrypt('adminadmin'),
            'order'=>'1',
            'role_id'=>'1000',
        ]);
        DB::table('users')->insert([
            'name' => "Moderator",
            'email' => "moderator@admin.com",
            'status' =>'active',
            'password' => bcrypt('adminadmin'),
            'order'=>'2',
        ]);
    }
}
