<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         $this->call(LocaleSeeder::class);
         $this->call(RoleSeeder::class);
         $this->call(SettingSeeder::class);
         $this->call(UserSeeder::class);
         $this->call(ModuleSeeder::class);
         $this->call(ParentModuleSeeder::class);
         $this->call(CountriesSeeder::class);
         $this->call(StatusSeeder::class);
         $this->call(SectorSeeder::class);
         $this->call(ContactTypeSeeder::class);
         $this->call(DomainSeeder::class);
         $this->call(DomainTypeSeeder::class);
         $this->call(TechnologiesSeeder::class);
    }
}
