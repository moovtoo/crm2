<?php

use Illuminate\Database\Seeder;

class DomainTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('domain_Types')->insert([

            'id' => "1",
            'name' => "Website",

        ]);
        DB::table('domain_Types')->insert([

            'id' => "2",
            'name' => "Viewer",

        ]);
        DB::table('domain_Types')->insert([

            'id' => "3",
            'name' => "Email Only",

        ]);
        DB::table('domain_Types')->insert([

            'id' => "4",
            'name' => "CRM",

        ]);
    }
}
