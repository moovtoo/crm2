<?php

use Illuminate\Database\Seeder;

class ModuleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('modules')->insert([
            'id'=>'1',
            'name' => "User",
            'table_name'=>'users',
            'slug'=>'user',
            'controller_name'=>'UserController',
            'parent_id'=>'6',
        ]);
        DB::table('modules')->insert([
            'id'=>'2',
            'name' => "Role",
            'table_name'=>'roles',
            'slug'=>'role',
            'controller_name'=>'RoleController',
            'parent_id'=>'7',
        ]);
        DB::table('modules')->insert([
            'id'=>'3',
            'name' => "Organization",
            'table_name'=>'organizations',
            'slug'=>'organization',
            'controller_name'=>'OrganizationController',
            'parent_id'=>'5',
        ]);
        DB::table('modules')->insert([
            'id'=>'4',
            'name' => "Organization Phone",
            'table_name'=>'organization_phones',
            'slug'=>'organization-phone',
            'controller_name'=>'OrganizationPhoneController',
            'parent_id'=>'5',
        ]);
        DB::table('modules')->insert([
            'id'=>'5',
            'name' => "Contact",
            'table_name'=>'contacts',
            'slug'=>'contact',
            'controller_name'=>'ContactController',
            'parent_id'=>'4',
        ]);

        DB::table('modules')->insert([
            'id'=>'6',
            'name' => "Department",
            'table_name'=>'departments',
            'slug'=>'department',
            'controller_name'=>'DepartmentController',
            'parent_id'=>'7',
        ]);
        DB::table('modules')->insert([
            'id'=>'7',
            'name' => "Origin",
            'table_name'=>'origins',
            'slug'=>'origin',
            'controller_name'=>'OriginController',
            'parent_id'=>'7',
        ]);
        DB::table('modules')->insert([
            'id'=>'8',
            'name' => "Project",
            'table_name'=>'projects',
            'slug'=>'project',
            'controller_name'=>'ProjectController',
            'parent_id'=>'10',
        ]);
        DB::table('modules')->insert([
            'id'=>'9',
            'name' => "Team",
            'table_name'=>'teams',
            'slug'=>'team',
            'controller_name'=>'TeamController',
//            'parent_id'=>'10',
        ]);
        DB::table('modules')->insert([
            'id'=>'10',
            'name' => "Leads Action",
            'table_name'=>'lead_actions',
            'slug'=>'lead-action',
            'controller_name'=>'LeadController',
//            'parent_id'=>'10',
        ]);
        DB::table('modules')->insert([
            'id'=>'11',
            'name' => "Lead",
            'table_name'=>'leads',
            'slug'=>'lead',
            'controller_name'=>'LeadController',
            'parent_id'=>'10',
        ]);
        DB::table('modules')->insert([
            'id'=>'12',
            'name' => "Hosting",
            'table_name'=>'hostings',
            'slug'=>'hosting',
            'controller_name'=>'HostingController',
            'parent_id'=>'13',
        ]);
    }
}
