<?php

use Illuminate\Database\Seeder;

class StatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('statuses')->insert([
            'id'=>'1',
            'name' => "Pending",
            'slug'=>'pending',
            'module_id'=>'9',
        ]);
        DB::table('statuses')->insert([
            'id'=>'2',
            'name' => "Approved",
            'slug'=>'approved',
            'module_id'=>'9',
        ]);
        DB::table('statuses')->insert([
            'id'=>'3',
            'name' => "Pending",
            'slug'=>'pending',
            'module_id'=>'11',
        ]);
        DB::table('statuses')->insert([
            'id'=>'4',
            'name' => "On Going",
            'slug'=>'on-going',
            'module_id'=>'11',
        ]);
        DB::table('statuses')->insert([
            'id'=>'5',
            'name' => "Completed",
            'slug'=>'completed',
            'module_id'=>'11',
        ]);
        DB::table('statuses')->insert([
            'id'=>'6',
            'name' => "Lost",
            'slug'=>'lost',
            'module_id'=>'11',
        ]);
        DB::table('statuses')->insert([
            'id'=>'7',
            'name' => "Deal/Project",
            'slug'=>'deal-project',
            'module_id'=>'11',
        ]);
        DB::table('statuses')->insert([
            'id'=>'8',
            'name' => "Completed",
            'slug'=>'completed',
            'module_id'=>'10',
        ]);
        DB::table('statuses')->insert([
            'id'=>'9',
            'name' => "Pending",
            'slug'=>'pending',
            'module_id'=>'10',
        ]);
        DB::table('statuses')->insert([
            'id'=>'10',
            'name' => "Upcoming",
            'slug'=>'upcoming',
            'module_id'=>'10',
        ]);
        DB::table('statuses')->insert([
            'id'=>'11',
            'name' => "Cancelled",
            'slug'=>'cancelled',
            'module_id'=>'10',
        ]);
        DB::table('statuses')->insert([
            'id'=>'12',
            'name' => "Waiting Answer",
            'slug'=>'waiting-answer',
            'module_id'=>'10',
        ]);
        DB::table('statuses')->insert([
            'id'=>'13',
            'name' => "On Progress",
            'slug'=>'on-progress',
            'module_id'=>'10',
        ]);
        DB::table('statuses')->insert([
            'id'=>'14',
            'name' => "On Progress",
            'slug'=>'on-progress',
            'module_id'=>'10',
        ]);
        DB::table('statuses')->insert([
            'id'=>'15',
            'name' => "In creation",
            'slug'=>'in-creation',
            'module_id'=>'8',
        ]);
        DB::table('statuses')->insert([
            'id'=>'16',
            'name' => "Online",
            'slug'=>'online',
            'module_id'=>'12',
        ]);
        DB::table('statuses')->insert([
            'id'=>'17',
            'name' => "UM",
            'slug'=>'um',
            'module_id'=>'12',
        ]);
        DB::table('statuses')->insert([
            'id'=>'18',
            'name' => "UC",
            'slug'=>'uc',
            'module_id'=>'12',
        ]);
        DB::table('statuses')->insert([
            'id'=>'19',
            'name' => "TEST",
            'slug'=>'test',
            'module_id'=>'12',
        ]);
    }
}
