<?php

use Illuminate\Database\Seeder;

class DomainSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('domain_managements')->insert([

            'id' => "1",
            'name' => "by Client",

        ]);
        DB::table('domain_managements')->insert([

            'id' => "2",
            'name' => "Namesilo",

        ]);
        DB::table('domain_managements')->insert([

            'id' => "3",
            'name' => "Godaddy",

        ]);
        DB::table('domain_managements')->insert([

            'id' => "4",
            'name' => "TierraNet",

        ]);
    }
}
