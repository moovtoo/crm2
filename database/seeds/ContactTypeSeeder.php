<?php

use Illuminate\Database\Seeder;

class ContactTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('contact_types')->insert([
            'name' => "Mobile",
            'module_id' => "5",
        ]);
        DB::table('contact_types')->insert([
            'name' => "Phone",
            'module_id' => "5",
        ]);
        DB::table('contact_types')->insert([
            'name' => "Fax",
            'module_id' => "5",
        ]);
        DB::table('contact_types')->insert([
            'name' => "Call",
            'module_id' => "10",
        ]);
        DB::table('contact_types')->insert([
            'name' => "Soutenance orale",
            'module_id' => "10",
        ]);
        DB::table('contact_types')->insert([
            'name' => "Introduction Groupe",
            'module_id' => "10",
        ]);
        DB::table('contact_types')->insert([
            'name' => "Proposition Commerciale",
            'module_id' => "10",
        ]);
        DB::table('contact_types')->insert([
            'name' => "Recall",
            'module_id' => "10",
        ]);
        DB::table('contact_types')->insert([
            'name' => "Contrat",
            'module_id' => "10",
        ]);
        DB::table('contact_types')->insert([
            'name' => "Implication Editeur",
            'module_id' => "10",
        ]);
        DB::table('contact_types')->insert([
            'name' => "Email envoyé",
            'module_id' => "10",
        ]);
        DB::table('contact_types')->insert([
            'name' => "Présentation Offre",
            'module_id' => "10",
        ]);
        DB::table('contact_types')->insert([
            'name' => "Présentation",
            'module_id' => "10",
        ]);
        DB::table('contact_types')->insert([
            'name' => "Email",
            'module_id' => "10",
        ]);
        DB::table('contact_types')->insert([
            'name' => "Meeting",
            'module_id' => "10",
        ]);
        DB::table('contact_types')->insert([
            'name' => "14 à envoyer",
            'module_id' => "10",
        ]);
        DB::table('contact_types')->insert([
            'name' => "RFP Reçu",
            'module_id' => "10",
        ]);
        DB::table('contact_types')->insert([
            'name' => "Mobile",
            'module_id' => "4",
        ]);
        DB::table('contact_types')->insert([
            'name' => "Phone",
            'module_id' => "4",
        ]);
        DB::table('contact_types')->insert([
            'name' => "Fax",
            'module_id' => "4",
        ]);


    }
}
