<?php

use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('roles')->insert([
            'id' => '1000',
            'name' => "admin",
            'description' => "admin",
            'order'=>'1',
        ]);
        DB::table('roles')->insert([
            'id' => '1',
            'name' => "employee",
            'description' => "employee",
            'order'=>'2',
        ]);
    }
}