<?php

use Illuminate\Database\Seeder;

class LocaleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('locales')->insert([
            'name' => "English",
            'code' =>'en',
            'order'=>'1',
        ]);
    }

}
