<?php

use Illuminate\Database\Seeder;

class ParentModuleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //1
        DB::table('parent_modules')->insert([
            'name' => "Organizations Management",
            'slug' => "organizations-management",
            'controller_name'=>'OrganizationsManagement',
            'icon' => 'images/ContentManagement.svg',
            'inverse_icon' => 'images/ContentManagementInverse.svg',
        ]);

        //2
        DB::table('parent_modules')->insert([
            'name' => "Administration",
            'slug' => "administration",
            'controller_name'=>'Administration',
            'icon' => 'images/Administration.svg',
            'inverse_icon' => 'images/AdministrationInverse.svg',
        ]);
        //3
        DB::table('parent_modules')->insert([
            'name' => "Configuration",
            'slug' => "configuration",
            'controller_name'=>'Configuration',
            'icon' => 'images/Configuration.svg',
            'inverse_icon' => 'images/ConfigurationInverse.svg'
        ]);
        //4
        DB::table('parent_modules')->insert([
            'name' => "Contact System",
            'slug' => "contact-system",
            'controller_name'=>'ContactSystem',
            'icon' => 'images/ModuleManagement.svg',
            'inverse_icon' => 'images/ModuleManagementInverse.svg',
            'parent_id'=>1,
        ]);

        //5
        DB::table('parent_modules')->insert([
            'name' => "Organization System",
            'slug' => "organization-system",
            'controller_name'=>'OrganizationSystem',
            'parent_id'=>1,
        ]);
        //6
        DB::table('parent_modules')->insert([
            'name' => "System Administration",
            'slug' => "system-administration",
            'controller_name'=>'SystemAdministration',
            'parent_id'=>2,
        ]);
        //7
        DB::table('parent_modules')->insert([
            'name' => "Role Management",
            'slug' => "role-management",
            'controller_name'=>'RoleManagement',
            'parent_id'=>2,
        ]);
        //8
        DB::table('parent_modules')->insert([
            'name' => "Referential",
            'slug' => "referential",
            'controller_name'=>'Referential',
            'parent_id'=>1,
        ]);
        
        //9
        DB::table('parent_modules')->insert([
            'name' => "Project Management",
            'slug' => "project-management",
            'controller_name'=>'ProjectManagement',
            'icon' => 'images/ModuleManagement.svg',
        ]);
        //10
        DB::table('parent_modules')->insert([
            'name' => "Project System",
            'slug' => "project-system",
            'controller_name'=>'ProjectSystem',
            'parent_id'=>9,
        ]);
        //11
        DB::table('parent_modules')->insert([
            'name' => "lead Management",
            'slug' => "lead-management",
            'controller_name'=>'LeadManagement',
            'parent_id'=>9,
        ]);
        //12
//        DB::table('parent_modules')->insert([
//            'name' => "Hosting Management",
//            'slug' => "hosting-management",
//            'controller_name'=>'HostingManagement',
//            'icon' => 'images/ModuleManagement.svg',
//        ]);
        //13
        DB::table('parent_modules')->insert([
            'name' => "Hosting System",
            'slug' => "hosting-system",
            'controller_name'=>'LeadSystem',
            'parent_id'=>9,
        ]);
        //14
        DB::table('parent_modules')->insert([
            'name' => "Lead System",
            'slug' => "lead-system",
            'controller_name'=>'LeadSystem',
            'parent_id'=>9,
        ]);

    }
}

