@extends('layouts.main-app')

@section('content')
    <div class="container">
        <div class="content">


            <div class="row">
                <div class="container full-menu-title">{!!__('cms.home.content-management-system')!!} </div>
            </div>

            <div class="row">
                <div class="container">
                    <div class="row menu-dashboard">
                        @if ( request()->permission_role_id == 1000)
                            @foreach($parentModules as $parentModule)
                                <div class="col home-box">
                                    <div class="full-menu-box">
                                        <div class="full-menu-box-image">
                                            <a class=""  href="{{route($parentModule->slug)}}" >
                                                <img src="{{asset($parentModule->icon)}}" >
                                            </a>
                                        </div>
                                        <div class="full-menu-box-title">
                                            <a class=""  href="{{route($parentModule->slug)}}" >
                                                {{$parentModule->name}}
                                            </a>
                                        </div>
                                        {{-- <div class="full-menu-box-link-box">
                                            <a class="full-menu-box-button-blue"  href="{{route($parentModule->slug)}}" >{{__('cms.home.get-started')}}</a>
                                        </div> --}}
                                    </div>
                                </div>
                                @if ( $loop->index == 3 )
                    </div>
                    <div class="row">
                        @endif
                        @endforeach
                        @else
                            @php($i=0)
                            @foreach ($parentModules as $parentModule )
                                @if ( in_array($parentModule->id , request()->permissions_parent_modules))
                                    <div class="col home-box">
                                        <div class="full-menu-box">
                                            <div class="full-menu-box-image">
                                                <a class=""  href="{{route($parentModule->slug)}}" >
                                                    <img src="{{asset($parentModule->icon)}}" />
                                                </a>
                                            </div>
                                            <div class="full-menu-box-title">{{$parentModule->name}}</div>
                                            {{-- <div class="full-menu-box-link-box">
                                                <a class="full-menu-box-button-blue"  href="{{route($parentModule->slug)}}" >{{__('cms.home.get-started')}}</a>
                                            </div> --}}
                                        </div>
                                    </div>
                                    @php($i++)
                                @endif
                                @if ( $i == 3 )
                    </div>
                    <div class="row">
                        @endif


                        @endforeach
                        @endif
                    </div>

                </div>

            </div>
        </div>
    </div>
@endsection
