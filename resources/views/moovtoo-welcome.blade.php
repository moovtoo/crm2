<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>@if(isset($settings['cms_title'])) {{ $settings['cms_title']}}
            @else{{ config('app.name', 'Laravel') }}@endif</title>

        <link rel="shortcut icon" type="image/x-icon" href="@if(isset($settings['favicon'])) {{asset($settings['favicon'])}} @else  {{asset('images/login_logo.svg')}} @endif">

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">
        <link rel="icon" href="{{ asset($settings['favicon'])}}">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: inline-block;
                justify-content: center;
                margin: auto 0;
                vertical-align: middle;
            }
            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
            .moovtoo-bg{
                background-size: cover;
                background-repeat: no-repeat;
                height:100vh;
                position: relative;
                background-image: url("{{asset('moovtoo/welcome.gif')}}");
            }
            .moovtoo-logo{
                padding: 30px 0 0 30px;
            }
            .text-intro {
                font-size: 33px;
                font-family: sans-serif;
                color: white;
                margin-top: 100px;
                max-width: 600px;
            }
            .contact {
                background-color: #1cb3e6;
                display: inline-block;
                padding: 10px 30px;
                color: white;
                font-family: sans-serif;
                font-size: 18px;
                cursor: pointer;
            }
            .input-label{
                font-family: sans-serif;
                font-size: 15px;
                color: gray;
                padding: 5px 0;
            }
            .input {
                width: 100%;
                height: 40px;
                background-color: #dee6e7;
                border: 0;
            }
            .bottom-right{
                position: absolute;
                right: 0;
                display: inline-flex;
                bottom: 0;
                padding: 0;
            }
            .mobile-image img{
                max-width: 350px;
            }
            .form-contact{
                background-color: white;
                width: 70%;
                margin: 25px auto 0 auto;
            }
            .contact-btn{
                display: block;
                max-width: 135px;
                text-align: center;
            }
            [id*="error-"]{
                color:#ff2c2c;
            }
            @media (max-width: 991px) {
                #form{
                    width: 100%;
                }
            }
            @media(max-width: 767px){
                .mobile-image img{
                    display: none;
                }
                .moovtoo-bg{
                    background-image: url("{{asset('moovtoo/mobile.gif')}}");
                }
                .text-intro {
                    font-size: 25px;
                }
                .bottom-right{
                   text-align: center;
                   margin-bottom: 50px;
                }
                .contact{
                    margin-top: 20px;
                }
                .form-contact{
                    width: 95%;
                }
            }
        </style>
    </head>
    <body class="moovtoo-bg">
        <div class="bottom-right">
            <div class="flex-center" id="text">
                <div class="text-intro">
                    L’intelligence artificielle pour un tourisme authentique... et durable
                </div>
                <div class="contact" onclick="showForm()" >
                    Contactez-nous
                </div>
            </div>
            <div class="mobile-image">
                <img src={{asset('moovtoo/mobile-app.gif')}}>
            </div>
        </div>
        <div class="row" style="padding: 0;margin: 0;">
            <div class="col-md-7 flex-center" style="height: 90vh;display: none;" id="form">
                <div class="content form-contact">
                    <div style="padding: 5%;text-align: left">
                        <div class="input-label" >
                            Name
                        </div>
                        <input type="text" id="name" class="input">
                        <label id="error-name"></label>

                        <div class="input-label" >
                            Phone
                        </div>
                        <input type="text" id="phone" class="input" >
                        <label id="error-phone"></label>

                        <div class="input-label">
                            Email
                        </div>
                        <input type="email" id="email" class="input" >
                        <label id="error-email"></label>

                        <div class="input-label">
                            Message
                        </div>
                        <textarea type="text" id="message" class="input" style="height: 100px" ></textarea>
                        <label id="error-message"></label>

                        
                        <div class="contact contact-btn" onclick="contactUs()" >
                            Submit
                        </div>
                    </div>
                </div>

            </div>
        </div>

    <script
            src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
            integrity="sha256-3edrmyuQ0w65f8gfBsqowzjJe2iM6n0nKciPUp8y+7E="
            crossorigin="anonymous"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://netdna.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>

    <script>
        function showForm(){
            document.getElementById('form').style.display = "block";
            document.getElementById('text').style.display = "none";
        }
        function contactUs(){
            var pattern = new RegExp(/^(("[\w-+\s]+")|([\w-+]+(?:\.[\w-+]+)*)|("[\w-+\s]+")([\w-+]+(?:\.[\w-+]+)*))(@((?:[\w-+]+\.)*\w[\w-+]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][\d]\.|1[\d]{2}\.|[\d]{1,2}\.))((25[0-5]|2[0-4][\d]|1[\d]{2}|[\d]{1,2})\.){2}(25[0-5]|2[0-4][\d]|1[\d]{2}|[\d]{1,2})\]?$)/i);

            var email =  $('#email').val(),
                name = $('#name').val(),
                phone = $('#phone').val(),
                message = $('#message').val();


            var error = false;

            if(name == ''){
                $('#error-name').html('This field is required');
                error = true;
            }else{
                $('#error-name').html('');
            }

            if(email == ''){
                $('#error-email').html('This field is required');
                error = true;
            }else if(!pattern.test(email)){
                $('#error-email').html('Enter a valid email');
                error = true;
            }else{
                $('.help-email').html('');
            }

            if(phone == ''){
                $('#error-phone').html('This field is required');
                error = true;
            }else{
                $('#error-phone').html('');
            }
            if(message == ''){
                $('#error-message').html('This field is required');
                error = true;
            }else{
                $('#error-message').html('');
            }

            if(!error){
                $.ajax({
                    type: 'POST',
                    url: '{{route('contact.post')}}',
                    data: {
                        _token : "{{ csrf_token() }}",
                        email : email,
                        name : name,
                        telephone: phone,
                        message : message,
                    },

                }).done(function (result) {
                    $('#email').val('');
                    $('#name').val('');
                    $('#phone').val('');
                    $('#message').val('');

                }).fail(function (data) {
                    
                });
            }

        }
    </script>
    </body>
</html>
