<div id="list-{{$parent->slug}}-{{$module->slug}}" @if(str_contains(Request::path(), '/'.$module->slug)) style="display: block" @endif>
@if ( $module->children()->count() >  0)
    @foreach ($module->children() as $mod)
        @if (request()->permission_role_id ==1000 or $mod->isAllowedModule(request()->permission_role->id)and $mod->is_sticky == 1)
        <div>
            <a href="javascript:void(0)" class="list-group-item @if(Request::is("cms/$parent->slug/$module->slug/$mod->slug*")) active @endif">
                {{$mod->name}}
            </a>
            <ul class="list-group-item-dropdown" @if(str_contains(Request::path(), '/'.$mod->slug)) style="display: block" @endif>
                @include('layouts.partials.side-menu-leaf',['p'=>$parent,'mm'=>$module,'m'=>$mod, 'url'=>$url.'.'.$mod->slug,
                'allow_add' => (request()->permission_role_id ==1000 or $mod->isAllowedModuleAdd(auth()->user()->roles))])
            </ul>
        </div>
        @endif
    @endforeach
@endif

@if ( $module->parentChildren()->count() >  0)
    @foreach ($module->parentChildren as $mod)
            @if ( in_array($mod->id , request()->permissions_parent_modules))
                <div>
                    <a href="javascript:void(0)" class="list-group-item list-title-open" id="{{$module->slug}}-{{$mod->slug}}">
                        {{$mod->name}}
                    </a>
                </div>
                @include ('layouts.partials.side-menu-root', ['parent'=>$mod->parent , 'module'=>$mod, 'url'=>$url.'.'.$mod->slug])
            @endif
    @endforeach
@endif
</div>
