<nav class="navbar navbar-expand-md navbar-light navbar-laravel">
    <div class="container">
        <a class="navbar-brand" href="{{ route('home') }}">
            <img src="@if(isset($settings['logo'])) {{asset($settings['logo'])}} @else  {{asset('images/login_logo.svg')}} @endif" class="img-responsive">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <!-- Left Side Of Navbar -->
            <ul class="navbar-nav mr-auto">

            </ul>

            <!-- Right Side Of Navbar -->
            <ul class="navbar-nav ml-auto" style="float: right">
                <!-- Authentication Links -->
                <li class="header-search">
                    <img src="{{asset('images/Notifications.svg')}}" class="img-responsive">
                </li>
                <li class="header-search">
                    <img src="{{asset('images/Chat.svg')}}" class="img-responsive">
                </li>
                {{-- <li class="header-search">
                    <img src="{{asset('images/search.png')}}" class="img-responsive">
                </li> --}}
                @guest
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                    </li>
                    @if (Route::has('register'))
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                        </li>
                    @endif
                @else
                    <li class="nav-item dropdown">
                        <div class="user-header">
                            <img src="@if(isset(Auth::user()->photo) and Auth::user()->photo != '') {{asset(Auth::user()->photo)}} @else {{asset('images/Avatar.svg')}} @endif" class="img-responsive header-user-photo">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown"aria-haspopup="true" aria-expanded="false" v-pre>
                                Hello, {{ Auth::user()->name }}
                            </a>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                         document.getElementById('logout-form').submit();">
                                    {{ __('Logout') }}
                                </a>
                                <a class="dropdown-item" href="{{route ('profile')}}">
                                    {{ __('Edit Profile') }}
                                </a>
    
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </div>
                        </div>
                    </li>
                    <li class="header-logout">
                        <img src="{{asset('images/logout.svg')}}" class="img-responsive" onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();">
                    </li>
                @endguest
            </ul>
        </div>
    </div>
</nav>