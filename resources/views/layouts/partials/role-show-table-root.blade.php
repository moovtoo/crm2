@if($parent->parentChildren->count() > 0)
    @foreach($parent->parentChildren as $module)
        @if($module->parentChildren->count() == 0 and $module->children()->count() == 0)

        @else
            <tr data-list="{{$parent->id}}" style="display:none">
                <td colspan="6" class="role-dropdown-toggle" data-target="{{$module->id}}">
                    <span>{{$module->name}}<i class="fa fa-angle-down"></i></span>
                </td>
            </tr>
            @if($module->children()->count() > 0)
                @include('layouts.partials.role-show-table-rows', ['children'=>$module->children(), 'mod'=>$module])
            @endif
            @if($module->parentChildren->count() > 0)
                @include('layouts.partials.role-show-table-root', ['parent'=>$module])
            @endif
        @endif
    @endforeach
@endif