<div class="row bottom-btns">
    <div class="article-action-buttons">
        @if ( isset($update) and $update )
            <div class="form-group form-btn-actions">
                <div class="form-control form-submit-btn" id="form-update">Update</div>
            </div>
        @endif
        @if ( isset($edit) and $edit )
            <div class="form-group form-btn-actions">
                <div class="form-control form-submit-btn" id="form-edit">Edit</div>
            </div>
        @endif
        @if ( isset($submit) and $submit)
            <div class="form-group form-btn-actions">
                <div class="form-control form-submit-btn" id="form-submit">Submit</div>
            </div>
        @endif
        @if(isset($reset) and $reset)
            <div class="form-group form-btn-actions">
                <div class="form-control form-submit-btn" id="form-reset">Reset</div>
            </div>
        @endif
        @if ( !isset($back) or $back)
            <div class="form-group form-btn-actions">
                <div  class="form-control form-submit-btn" id="form-back">Back</div>
            </div>
        @endif
    </div>
</div>