@if($allow_add)
    @if($m->slug != 'project-dashBoard')
    <li>
         <a href="{{route($url.'.create')}}"
          @if(str_contains(Request::path(), '/'.$mod->slug.'/create')) class="active" @endif>
        <span class="plus">+</span> Add a {{$m->name}}</a>
    </li>
    @endif
@endif

<li><a href="{{route($url.'.index')}}"
       @if(array_slice(explode('/', Request::path()), -1, 1)[0] == $mod->slug) class="active" @endif>{{$m->name}} list</a>
</li>
