@foreach ($children as $child)
    <tr data-list="{{$mod->id}}" style="display:none">
        <td><input type="checkbox" class="role-checkbox" data-target="{{$child->id}}" data-parent-check="{{$mod->id}}"  />
            &nbsp;<span>{{$child->name}}</span></td>
        <td><input type="checkbox" name="permissions[{{$child->id }}][add]" data-col-check="add" data-check="{{$child->id}}"
            @if (isset($role))
                @if( $child->roleCheck($role->id)!= null and $child->roleCheck($role->id)->add == 1 ) checked @endif
            @endif
            />
            &nbsp;</td>

        <td><input type="checkbox" name="permissions[{{$child->id }}][show_all]" data-col-check="show" data-check="{{$child->id}}"
            @if (isset($role))
                @if( $child->roleCheck($role->id)!= null and $child->roleCheck($role->id)->show_all == 1 ) checked @endif
            @endif
            />
            &nbsp;<span class="role-permission-label">All records</span> <br/>
            @if($child->user_related)<input type="checkbox" name="permissions[{{$child->id }}][show]" data-col-check="show" data-check="{{$child->id}}"
            @if (isset($role))
                @if( $child->roleCheck($role->id)!= null and $child->roleCheck($role->id)->show == 1 ) checked @endif
            @endif
            />
            <span class="role-permission-label">Only user record</span>@endif</td>

        <td><input type="checkbox" name="permissions[{{$child->id }}][edit_all]" data-col-check="edit" data-check="{{$child->id}}"
            @if (isset($role))
                @if( $child->roleCheck($role->id)!= null and $child->roleCheck($role->id)->edit_all == 1 ) checked @endif
            @endif
            />
            &nbsp;<span class="role-permission-label">All records </span><br/>
            @if($child->user_related)<input type="checkbox" name="permissions[{{$child->id }}][edit]" data-col-check="edit" data-check="{{$child->id}}"
            @if (isset($role))
                @if( $child->roleCheck($role->id)!= null and $child->roleCheck($role->id)->edit == 1 ) checked @endif
            @endif
            /> 
            <span class="role-permission-label">Only user record</span>@endif</td>

        <td><input type="checkbox" name="permissions[{{$child->id }}][delete_all]" data-col-check="delete" data-check="{{$child->id}}"
            @if (isset($role))
                @if( $child->roleCheck($role->id)!= null and $child->roleCheck($role->id)->delete_all == 1 ) checked @endif
            @endif
            />
            &nbsp;<span class="role-permission-label">All records</span> <br/>
            @if($child->user_related)<input type="checkbox" name="permissions[{{$child->id }}][delete]" data-col-check="delete" data-check="{{$child->id}}"
            @if (isset($role))
                @if( $child->roleCheck($role->id)!= null and $child->roleCheck($role->id)->delete == 1 ) checked @endif
            @endif
            /> 
            <span class="role-permission-label">Only user record</span>@endif</td>

        <td><input type="checkbox" name="permissions[{{$child->id }}][status]" data-col-check="status" data-check="{{$child->id}}"
            @if (isset($role))
                @if( $child->roleCheck($role->id)!= null and $child->roleCheck($role->id)->status == 1 ) checked @endif
            @endif
            /> </td>
    </tr>
@endforeach