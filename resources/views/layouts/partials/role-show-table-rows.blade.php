@foreach ($children as $child)
    <tr data-list="{{$mod->id}}" style="display:none">
        <td><span>{{$child->name}}</span></td>
        <td>@if (isset($role))
                @if( $child->roleCheck($role->id)!= null and $child->roleCheck($role->id)->add == 1 ) 
                    <i class="fa fa-check"></i> 
                @else 
                    <i class="fa fa-close"></i> 
                @endif
            @endif
            &nbsp;</td>

        <td>@if (isset($role))
                @if( $child->roleCheck($role->id)!= null and $child->roleCheck($role->id)->show_all == 1 ) 
                    <i class="fa fa-check"></i> 
                @else 
                    <i class="fa fa-close"></i> 
                @endif
            @endif
            &nbsp;<span class="role-permission-label">All records</span> <br/>
            @if($child->user_related)
                @if (isset($role))
                    @if( $child->roleCheck($role->id)!= null and $child->roleCheck($role->id)->show == 1 ) 
                        <i class="fa fa-check"></i> 
                    @else 
                        <i class="fa fa-close"></i> 
                    @endif
                @endif
            <span class="role-permission-label">Only user record</span>@endif</td>

        <td>@if (isset($role))
                @if( $child->roleCheck($role->id)!= null and $child->roleCheck($role->id)->edit_all == 1 ) 
                    <i class="fa fa-check"></i> 
                @else 
                    <i class="fa fa-close"></i> 
                @endif
            @endif
            &nbsp;<span class="role-permission-label">All records </span><br/>
            @if($child->user_related)
                @if (isset($role))
                    @if( $child->roleCheck($role->id)!= null and $child->roleCheck($role->id)->edit == 1 ) 
                        <i class="fa fa-check"></i> 
                    @else 
                        <i class="fa fa-close"></i> 
                    @endif
                @endif 
            <span class="role-permission-label">Only user record</span>@endif</td>

        <td>@if (isset($role))
                @if( $child->roleCheck($role->id)!= null and $child->roleCheck($role->id)->delete_all == 1 ) 
                    <i class="fa fa-check"></i> 
                @else 
                    <i class="fa fa-close"></i> 
                @endif
            @endif
            &nbsp;<span class="role-permission-label">All records</span> <br/>
            @if($child->user_related)
                @if (isset($role))
                    @if( $child->roleCheck($role->id)!= null and $child->roleCheck($role->id)->delete == 1 ) 
                        <i class="fa fa-check"></i> 
                    @else 
                        <i class="fa fa-close"></i> 
                    @endif
                @endif
            <span class="role-permission-label">Only user record</span>@endif</td>

        <td>@if (isset($role))
                @if( $child->roleCheck($role->id)!= null and $child->roleCheck($role->id)->status == 1 ) 
                    <i class="fa fa-check"></i> 
                @else 
                    <i class="fa fa-close"></i> 
                @endif
            @endif</td>
    </tr>
@endforeach