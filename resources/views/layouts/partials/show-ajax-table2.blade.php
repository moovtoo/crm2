@php($fields = $model->fields2())

<div class="container-fluid" style="padding: 0;">
    <div class="col-md-12 " style="padding: 0;">
        <table class="table ajax-table" id="{{$table}}" cellspacing="0" width="100%" style="margin-top:0!important;">
            <thead>
            <tr>
                @foreach($fields['index'] as $k=>$field)
                    @if($loop->index == 0)
                        <th>Title</th>
                    @else
                        <th>{{$field}}</th>
                    @endif
                @endforeach
                {{--<th>Actions</th>--}}
            </tr>
            </thead>
        </table>
    </div>
</div>
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-head-orange"></div>
            <div class="modal-body delete-modal-body">
                <img src="{{asset('images/Close.svg')}}" class="img-responsive">
                <h4>Are you sure you want to delete this <span id="delete-type"></span></h4>
                <div class="modal-btns">
                    <button type="button" class="btn" id="confirmDelete">Yes</button>
                    <button type="button" class="btn" data-dismiss="modal">No</button>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="message-box" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-head-orange"></div>
            <div class="modal-body delete-modal-body">
                <img src="{{asset('images/Close.svg')}}" class="img-responsive">
                <h4 id="message"></h4>
                <div class="modal-btns">
                    {{-- <button type="button" class="btn" id="confirmDelete">Yes</button> --}}
                    <button type="button" class="btn" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</div>
@push('css')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.18/datatables.min.css"/>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs/jq-3.3.1/jszip-2.5.0/dt-1.10.18/af-2.3.2/b-1.5.4/b-colvis-1.5.4/b-flash-1.5.4/b-html5-1.5.4/b-print-1.5.4/cr-1.5.0/fc-3.2.5/fh-3.1.4/kt-2.5.0/r-2.2.2/rg-1.1.0/rr-1.2.4/sc-1.5.0/sl-1.2.6/datatables.min.css"/>
@endpush
@push('js')
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/v/bs-3.3.7/jq-3.3.1/jszip-2.5.0/dt-1.10.18/af-2.3.2/b-1.5.4/b-colvis-1.5.4/b-flash-1.5.4/b-html5-1.5.4/b-print-1.5.4/cr-1.5.0/fc-3.2.5/fh-3.1.4/kt-2.5.0/r-2.2.2/rg-1.1.0/rr-1.2.4/sc-1.5.0/sl-1.2.6/datatables.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <script>
        $(function() {


            $('#{{$table}}').dataTable({
                processing: false,
                serverSide: true,
                autoWidth:         true,
                paging:         true,
                bFilter: false,
                bLengthChange: false,
                order: [[ @if(isset($custom_order)) {{$custom_order}} ,'asc' @else 0,'desc'  @endif]],
                ajax: '{{ $route}}{!! isset($params)? $params:'' !!}',
                columns: [
                        @foreach($fields['index'] as $k=>$field)
                        @if ( $loop->index == 0)
                    { data: '{{$k}}', name: '{{$k}}', width:'180px'  },
                        @elseif($k == 'photo' || $k == 'icon')
                    { data: '{{$k}}', render: function(data){
                            return '<img src="'+data+'" class="img-responsive user-img-list">';
                        }, name: '{{$k}}'  },
                        @elseif($k == 'sub_domains')
                    { data: '{{$k}}', render: function(data){
                            var s = '';
                            $.each(data, function(key, value){
                                s += '<p class="p-subdomain"><a class="sub-link" href="https://'+value.name+'" target="_blank">'+ value.name +'.'+ value.parent_name +'</a> <span class="table-subdomain pull-right"><a href="javascript:void(0)" class="sub-domain-edit" id="'+value.id+'">Edit</a> | <a href="javascript:void(0)" class="sub-domain-delete" id="'+value.id+'">Remove</a></span></p>';
                            })
                            s += '<p class="p-subdomain-add"><span class="table-subdomain"><a href="javascript:void(0)" class="add-subdomain">Add sub domain</a></p>'
                            return s;
                        }, name: '{{$k}}'  },
                        @else
                    { data: '{{$k}}', name: '{{$k}}', render: function (data){
                            return data != null && data.length > 30 ? data.substr( 0, 30 )+'…':data;
                        }
                    },
                        @endif

                        @endforeach
                    {{--{ data: 'Action', name: 'Action',width:'10%',--}}
                        {{--defaultContent:--}}

                            {{--'<a href="javascript:void(0)" class="record-order-up"><img src="'+'{{asset("images/arrow-up.svg")}}'+'" class="img-responsive" style="display: inline-block;max-width: 16px;"> </a>\n'--}}
                            {{--+'<a href="javascript:void(0)" class="record-order-down"><img src="'+'{{asset("images/arrow-down.svg")}}'+'" class="img-responsive" style="display: inline-block;max-width: 16px;"> </a>\n'--}}
                            {{--+'<a href="javascript:void(0)" class="record-show"><img src="'+'{{asset("images/view.svg")}}'+'" class="img-responsive" style="display: inline-block;max-width: 16px;"> </a>\n'--}}
                                {{--@if ( request()->permission_role_id ==1000 or $module->isAllowedModuleEdit(request()->permission_role->id) )--}}
                            {{--+'<a href="javascript:void(0)" class="record-edit"><img src="'+'{{asset("images/edit.svg")}}'+'" class="img-responsive" style="display: inline-block;max-width: 16px;"> </a>\n'--}}
                                {{--@endif--}}
                                {{--@if ( request()->permission_role_id ==1000 or $module->isAllowedModuleDelete(request()->permission_role->id) )--}}
                            {{--+'<a href="javascript:void(0)" class="record-delete"><img src="'+'{{asset("images/remove.svg")}}'+'" class="img-responsive" style="display: inline-block;max-width: 15px;margin-left:5px;"> </a>'--}}
                        {{--@endif--}}
                    {{--}--}}
                ],
                // columnDefs: [
                //     {
                //         "targets": [1],
                //         "visible": false,
                //         "searchable": false
                //     }],
                columnDefs: [
                    { orderable: false, targets: -1 }
                ],
                preInit: function(e, settings ){
                    var div = $('#{{$table}}_wrapper').children('div').first();
                    $(div).hide();
                },
                initComplete: function(settings, json) {
                    var div = $('#{{$table}}_wrapper').children('div').first();
                    // div.children('div').each(function() {
                    //     $(this).removeClass('col-sm-6');
                    //     $(this).addClass('col-sm-4');
                    //     $(this).css('margin','auto');
                    // });
                    div.css('background-color','#fbfbfb');
                    div.css('padding','6px');

                    @if(isset($filters))
                    div.html(
                        '<div class="col-md-1" style="display:flex;">'+
                        '<span style="margin:auto 10px;font-size:15px;">Filter by</span>'+
                        '</div>'
                    );
                    @foreach($filters as $key => $value)
                    div.append(
                        '<div class="col-md-3 select2-filter" style="display:flex;">'+
                        '<select type="text" class="form-control select2" id="{{$key}}" name="{{$key}}" style="width:100%;margin: auto;">'+
                        '<option value="">Select {{$key}}</option>'+
                            @foreach($value as $val)
                                '<option value="{{$val}}">{{$val}}</option>'+
                            @endforeach
                                '</select>'+
                        '</div>'
                    );
                    @endforeach
                    @if(count($filters) < 2)
                    div.append(
                        '<div class="col-md-3 select2-filter" style="display:flex;">'+
                        '</div>'
                    );
                    @endif
                    @else
                    div.append(
                        '<div class="col-md-7 select2-filter" style="display:flex;">'+
                        '</div>'
                    );
                    @endif
                    // div.html(
                    //     '<div class="col-md-1" style="display:flex;">'+
                    //     '<span style="margin:auto 10px;font-size:15px;">Filter by</span>'+
                    //     '</div>'+
                    //     '<div class="col-md-3 select2-filter" style="display:flex;">'+
                    //     '<select type="text" class="form-control select2" id="category" name="category" style="width:100%;margin: auto;">'+
                    //     '<option value="">Select Category</option>'+
                    //     '<option value="category">Category</option>'+
                    //     '</select>'+
                    //     '</div>'+
                    //     '<div class="col-md-3 select2-filter" style="margin-left:20px;display:flex;">'+
                    //     '<select type="text" class="form-control select2 " id="status" name="status" style="width:100%;margin: auto;">'+
                    //     '<option value="">Select Status</option>'+
                    //     '<option value="Pending">Pending</option>'+
                    //     '</select>'+
                    //     '</div>'
                    // );

                    $('.select2').select2({
                        width:'100%'
                    });
                    {{--div.append(--}}
                    {{--'<div class="col-md-3 col-md-offset-1">'+--}}
                    {{--'<div class="add-btn pull-right" id="add-button">'+--}}
                    {{--@if ( request()->permission_role_id ==1000 or $module->isAllowedModuleEdit(request()->permission_role->id) )--}}
                    {{--' <div class="add-btn-plus">+</div>'+--}}
                    {{--'<div class="add-btn-title">{{$add_button?: ''}}</div>'+--}}
                    {{--@endif--}}
                    {{--'</div>'+--}}
                    {{--'</div>'--}}
                    {{--);--}}
                    $('#add-button div').click(function () {
                        location.href = '{{$route}}/create{{isset($params)?$params:""}}';
                    });
                    var div = $('#{{$table}}_wrapper').children('div').eq(2).css('padding','0 18px');

                }
            });

            $('#{{$table}}').on('click', 'a.record-show', function (e) {
                location.href = '{{$route}}/'+$('#{{$table}}').DataTable().row( $(this).parents('tr') ).data().id;
            } );


            $('#{{$table}}').on('click', 'a.record-edit', function (e) {
                var id = $('#{{$table}}').DataTable().row( $(this).parents('tr') ).data().id;
                $.ajax({
                    url: "{{$route}}/"+id+"/edit/authorize",
                    type:'GET',
                    dataType: 'json',
                    data: {
                        "_token": "{{ csrf_token() }}",
                    },
                    success: function(result){
                        if ( result.status == 'success')
                            location.href = '{{$route}}/'+id+"/edit";
                        else{
                            $('#message').html(result.message);
                            $('#message-box').modal('show');

                        }
                    },
                    error: function (data) {
                        $('#{{$table}}').DataTable().ajax.reload();
                        console.log(data);
                    }
                });

            } );

            $('#{{$table}}').on('click', 'a.sub-domain-edit', function (e) {
                location.href = '{{$route}}/'+$(this).attr('id')+"/edit";

                $.ajax({
                    url: "{{$route}}/"+id,
                    type:'DELETE',
                    dataType: 'json',
                    data: {
                        "_token": "{{ csrf_token() }}",
                    },
                    success: function(result){
                        if ( result.status == 'success')
                            $('#{{$table}}').DataTable().ajax.reload();
                        else{
                            $('#message').html(result.message);
                            $('#message-box').modal('show');

                        }
                        // if(result.fail == "Category"){
                        //     $('#deleteModalFail').modal('show');
                        // }
                    },
                    error: function (data) {
                        $('#{{$table}}').DataTable().ajax.reload();
                        console.log(data);
                    }
                });
            });

            $('#{{$table}}').on('click', 'a.add-subdomain', function (e) {
                location.href = '{{$route}}/create?id='+$('#{{$table}}').DataTable().row( $(this).parents('tr') ).data().id;
            });


            $('#{{$table}}').on('click', 'a.record-order-up', function (e) {
                $.ajax({url: "{{$route}}/"+$('#{{$table}}').DataTable().row( $(this).parents('tr') ).data().id+"/up",
                    success: function(result){
                        $('#{{$table}}').DataTable().ajax.reload();
                        console.log(result);
                    }
                });
            } );
            $('#{{$table}}').on('click', 'a.record-order-down', function (e) {
                $.ajax({url: "{{$route}}/"+$('#{{$table}}').DataTable().row( $(this).parents('tr') ).data().id+"/down",
                    success: function(result){
                        $('#{{$table}}').DataTable().ajax.reload();
                        console.log(result);
                    }
                });
            } );
            // Delete a record
            $('#{{$table}}').on('click', 'a.record-delete', function(e) // <-- add an event variable
            {
                e.preventDefault();
                var id =$('#{{$table}}').DataTable().row( $(this).parents('tr') ).data().id;
                $('#delete-type').html('{{$type}}');
                $('#deleteModal').modal('show');
                $('#confirmDelete').on('click',function(){
                    $('#deleteModal').modal('hide');
                    $.ajax({
                        url: "{{$route}}/"+id,
                        type:'DELETE',
                        dataType: 'json',
                        data: {
                            "_token": "{{ csrf_token() }}",
                        },
                        success: function(result){
                            if ( result.status == 'success')
                                $('#{{$table}}').DataTable().ajax.reload();
                            else{
                                $('#message').html(result.message);
                                $('#message-box').modal('show');

                            }
                            // if(result.fail == "Category"){
                            //     $('#deleteModalFail').modal('show');
                            // }
                        },
                        error: function (data) {
                            $('#{{$table}}').DataTable().ajax.reload();
                            console.log(data);
                        }
                    });
                })
            });

            $('#{{$table}}').on('click', 'a.sub-domain-delete', function(e) // <-- add an event variable
            {
                e.preventDefault();
                var id =$(this).attr('id');
                $('#delete-type').html('{{$type}}');
                $('#deleteModal').modal('show');
                $('#confirmDelete').on('click',function(){
                    $('#deleteModal').modal('hide');
                    $.ajax({
                        url: "{{$route}}/"+id,
                        type:'DELETE',
                        dataType: 'json',
                        data: {
                            "_token": "{{ csrf_token() }}",
                        },
                        success: function(result){
                            $('#{{$table}}').DataTable().ajax.reload();
                            // if(result.fail == "Category"){
                            //     $('#deleteModalFail').modal('show');
                            // }
                        },
                        error: function (data) {
                            $('#{{$table}}').DataTable().ajax.reload();
                            console.log(data);
                        }
                    });
                })
            });

            $('.select2').select2({
                width:'100%'
            });

        });
    </script>
@endpush