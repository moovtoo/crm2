<div class="form-group">
    <label for="module_type" class="control-label">{{$title}}</label>
    <select type="text" class="form-control select2" id="{{$id}}" name="{{$id}}">
        <option value="">Select {{$title}}</option>
        <option value="1" @if(isset($selected) and $selected==1) selected @endif>Yes</option>
        <option value="0" @if(isset($selected) and $selected==0) selected @endif>No</option>
    </select>
    <label id="{{$id}}-error" class="control-label error"></label>
</div>