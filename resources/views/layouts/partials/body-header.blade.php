<section class="tab-header">
    <div class="row tab-header-menu" >
        <div class="container">

            <div class="row">
                @if ( request()->permission_role_id == 1000)

                    @foreach($parentModules as $parentModule)
                        <div class="col tab-header-menu-item @if(Request::segment(2) == $parentModule->slug) active @endif" onclick="location.href='{{route("$parentModule->slug")}}'"  >
                            <div class="tab-header-menu-image-box">
                                <img src="{{asset($parentModule->inverse_icon)}}" class="tab-header-menu-image"/>
                            </div>
                            <div class="tab-header-menu-title">{{$parentModule->name}}</div>
                        </div>
                        @if ( $loop->index == 3 )
            </div>
            <div class="row">
                @endif
                @endforeach
            @else
                    @php($i=0)
                    @foreach ($parentModules as $parentModule )
                        @if ( in_array($parentModule->id , request()->permissions_parent_modules))
                            <div class="col tab-header-menu-item" onclick="location.href='{{route("$parentModule->slug")}}'">
                                <div class="tab-header-menu-image-box">
                                    <img src="{{asset($parentModule->icon)}}" class="tab-header-menu-image"/>
                                </div>
                                <div class="tab-header-menu-title @if(Request::segment(1)==$parentModule->slug) active @endif">{{$parentModule->name}}</div>
                            </div>
                            @php($i++)
                        @endif
                        @if ( $i == 3 )
            </div>
            <div class="row">
                @endif

                @endforeach
                @endif
            </div>


        </div>
    </div>



</section>
