<div class="col-sm-4 col-md-3 col-lg-2 sidebar">
    <div class="mini-submenu">
        <i class="fa fa-angle-double-right"></i>
    </div>
    <span class="pull-right" id="slide-submenu">
        <i class="fa fa-angle-double-left"></i>
    </span>

    <div class="list-group">
        <div class="side-menu-user">
            <img src="@if(isset(Auth::user()->photo) and Auth::user()->photo != '') {{asset(Auth::user()->photo)}} @else {{asset('images/Avatar.svg')}} @endif" class="img-responsive header-user-photo">
            <a href="{{route ('profile')}}">
                Hello {{ Auth::user()->name }}
            </a>
        </div>
        @if(str_contains(Request::path(), '/project-management')  )
            @if(request()->permission_role_id == 1000 or request()->permission_role_id == 1)
                 <div href="#" class="list-group-item main-title">
                       <a href="{{route('project-management/dashboard.index')}}" class="title" >
                            Dashboard
                        </a>
                 </div>
             @endif
        @endif
        @foreach ($parent->parentChildren as $module)
            @if ( in_array($module->id , request()->permissions_parent_modules))
                <div href="#" class="list-group-item main-title">
                    @if($module->icon != null)
                        <span class="pull-left">
                            <img src="{{asset("$module->icon")}}">
                        </span>
                    @endif
                    <a href="javascript:void(0)" class="title" id="{{$parent->slug}}-{{$module->slug}}">
                        {{$module->name}}
                    </a>
                </div>
                @if($module->children()->count() == 0 and $module->parentChildren()->count() == 0)

                @else
                    @include('layouts.partials.side-menu-root', ['parent'=>$parent , 'module'=>$module,
                            'url'=>$parent->slug.'.'.$module->slug])
                @endif
            @endif
        @endforeach
        @if ( $parent->hasConfig()->count() > 0 )
            <div href="#" class="list-group-item main-title">
                <a href="{{route("$parent->slug.configuration")}}" class="title">
                    General Configurations
                </a>
            </div>
        @endif
    </div>
</div>
