<footer class="login-footer">
    <ul class="login-footer-list">
        <li>
            <a class="footer-link" href="http://proximityagency.co" target="_blank">© 2019 Proximity Agency</a>
        </li>
        <li>
            <a class="footer-link"  href="#">About</a>
        </li>
    </ul>
</footer>