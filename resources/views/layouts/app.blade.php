<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@if(isset($settings['cms_title'])) {{ $settings['cms_title']}}
        @else{{ config('app.name', 'Laravel') }}@endif</title>

    <link rel="shortcut icon" type="image/x-icon" href="@if(isset($settings['favicon'])) {{asset($settings['favicon'])}} @else  {{asset('images/login_logo.svg')}} @endif">

    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">
    <link rel="shortcut icon" type="image/x-icon" href="{{asset($settings['favicon'])}}">
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />

    <link href="{{ asset('css/main.css') }}" rel="stylesheet">
    <link href="{{ asset('css/inputfile.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.18/datatables.min.css"/>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs/jq-3.3.1/jszip-2.5.0/dt-1.10.18/af-2.3.2/b-1.5.4/b-colvis-1.5.4/b-flash-1.5.4/b-html5-1.5.4/b-print-1.5.4/cr-1.5.0/fc-3.2.5/fh-3.1.4/kt-2.5.0/r-2.2.2/rg-1.1.0/rr-1.2.4/sc-1.5.0/sl-1.2.6/datatables.min.css"/>

    {{--<script src="{{ asset('js/app.js') }}" defer></script>--}}
    @stack('css')
</head>
<body>
<div id="app">
    @include('layouts.partials.header')

    <main class="py-4" style="padding-top:65px!important;">
        @include('layouts.partials.body-header')
        <div class="row" style="padding: 0;margin: 0">
            @if(Request::segment(2) == "configuration")
                @include('configuration.side-menu')
            @elseif(Request::segment(2) == "user")
                @include('user.side-menu')
            @else
                @include('layouts.partials.side-menu')
            @endif
            <div class="col-md-9 col-lg-10" style="padding: 0" id="main-view">
                <div class="row tab-header-title-box">
                    <div class="tab-header-title">
                        {{$title?? 'Admin Page'}}
                    </div>
                    <div class="tab-header-segment">
                        @if(isset($links))
                            @php($linkSegment = '')
                            @foreach ($links as $key => $value)
                                <a class="header-segment @if($value == '') no-cursor @endif" href="@if($value == '') javascript:void(0) @else{{route('home').$linkSegment.'/'.$value}}@endif">{{$key}}</a> >
                                @php($linkSegment .= '/'.$value)
                            @endforeach
                            {{$title}}
                        @endif
                    </div>
                </div>
                @yield('content')
            </div>
        </div>
    </main>
</div>


    <script
            src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
            integrity="sha256-3edrmyuQ0w65f8gfBsqowzjJe2iM6n0nKciPUp8y+7E="
            crossorigin="anonymous"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://netdna.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/v/bs-3.3.7/jq-3.3.1/jszip-2.5.0/dt-1.10.18/af-2.3.2/b-1.5.4/b-colvis-1.5.4/b-flash-1.5.4/b-html5-1.5.4/b-print-1.5.4/cr-1.5.0/fc-3.2.5/fh-3.1.4/kt-2.5.0/r-2.2.2/rg-1.1.0/rr-1.2.4/sc-1.5.0/sl-1.2.6/datatables.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>


    <script>
        function openTimeTab(clicked, id){
            target = $(clicked).parent().find('.full-tab-content[data-id="'+id+'"]');
            if($(target).css('display') == "none"){
                $(target).slideDown(500);
                $(target).find('full-tab-arrow').find('i').removeClass('fa-angle-down');
                $(target).find('full-tab-arrow').find('i').addClass('fa-angle-up');
            }else{
                $(target).slideUp(500);
                $(target).find('full-tab-arrow').find('i').addClass('fa-angle-down');
                $(target).find('full-tab-arrow').find('i').removeClass('fa-angle-up');
            }
        }
        $(function(){

            $('#navbarDropdown').on('click', function () {
                if($('.dropdown-menu').css('display') == 'none'){
                    $('.dropdown-menu').show();
                }else{
                    $('.dropdown-menu').hide();
                }
            })




            $('.role-dropdown-toggle span').on('click', function(){
                div = $(this).parent();
                target = $(div).attr('data-target');
                if($('[data-list="'+target+'"]').css('display') == 'none'){
                    $('[data-list="'+target+'"]').show(400);
                    $(div).find('i').removeClass('fa-angle-down');
                    $(div).find('i').addClass('fa-angle-up');
                }else{
                    $('[data-list="'+target+'"]').hide(400);
                    closeRoleChildren($('[data-list="'+target+'"]'));
                    $(div).find('i').removeClass('fa-angle-up');
                    $(div).find('i').addClass('fa-angle-down');
                }
            })
            function closeRoleChildren(children){
                $(children).each(function(key, value){
                    if($(value).find('.role-dropdown-toggle').length != 0){
                        child = $('[data-list="'+$(value).find('.role-dropdown-toggle').attr('data-target')+'"]');
                        $(child).each(function(key, value2){
                            if($(value2).find('.role-dropdown-toggle').length != 0){
                                childTarget = $(value2).find('.role-dropdown-toggle').attr('data-target');
                                closeRoleChildren($('[data-list="'+childTarget+'"]'));
                            }
                        })
                        if($(child).css('display') != "none"){
                            $(child).hide(400);
                            $(value).find('td').find('i').removeClass('fa-angle-up');
                            $(value).find('td').find('i').addClass('fa-angle-down');
                        }
                    }else{
                        if($(value).css('display') != "none"){
                            $(value).hide(400); 
                            $(value).find('td').find('i').removeClass('fa-angle-up');
                            $(value).find('td').find('i').addClass('fa-angle-down');
                        }
                    }
                })
            }

            $('.role-parent-checkbox').on('change', function(){
                check(this);
            })
            $('.role-checkbox').on('change', function(){
                childCheck(this);
            })
            $('.role-col-check').on('change',function(){
                target = $(this).attr('data-target');
                div = this;
                $('input[data-col-check="'+target+'"]').each(function(key, value){
                    if(div.checked){
                        $(value).prop('checked',true);
                    }else{
                        $(value).prop('checked',false);
                    }
                })
            })

            function check(checkbox){
                if(checkbox.checked) {
                    var id = $(checkbox).attr('data-parent-target');
                    $('input[data-parent-check="'+id+'"]').prop('checked',true);
                    $.each($('input[data-parent-check="'+id+'"]'), function(key, value){
                        $(value).prop('checked',true);
                        if($(value).attr('data-parent-target') != "undefined"){
                            check(value);
                        }
                        if($(value).attr('data-target') != "undefined"){
                            childCheck(value);
                        }
                    })
                }
                else {
                    var id = $(checkbox).attr('data-parent-target');
                    $('input[data-parent-check="'+id+'"]').prop('checked',false);
                    $.each($('input[data-parent-check="'+id+'"]'), function(key, value){
                        $(value).prop('checked',false);
                        if($(value).attr('data-parent-target') != "undefined"){
                            check(value);
                        }
                        if($(value).attr('data-target') != "undefined"){
                            childCheck(value);
                        }
                    })
                }
            }
            
            function childCheck(checkbox){
                if(checkbox.checked) {
                    var id = $(checkbox).attr('data-target');
                    $('input[data-check="'+id+'"]').prop('checked',true);
                    $.each($('input[data-check="'+id+'"]'), function(key, value){
                        $(value).prop('checked',true);
                        if($(value).attr('data-target') != "undefined"){
                            childCheck(value);
                        }
                    })
                }
                else {
                    var id = $(checkbox).attr('data-target');
                    $('input[data-check="'+id+'"]').prop('checked',false);
                    $.each($('input[data-check="'+id+'"]'), function(key, value){
                        $(value).prop('checked',false);
                        if($(value).attr('data-target') != "undefined"){
                            childCheck(value);
                        }
                    })
                }
            }

            try{
                // main-table
                if ( $('#parent-image').length > 0) {
                    $('#main-view').css('margin-left', '-' + $('#slide-submenu').next('.list-group').parent().width() / 2 + 'px');

                }
                $('#slide-submenu').on('click',function() {

                    if ( $('#parent-image').length ){
                        $('#main-view').css('visibility','hidden');
                    }
                    $(this).next('.list-group').fadeOut('slide',function(){
                        $('.mini-submenu').fadeIn();
                        $('#slide-submenu').fadeOut();
                        $('#main-view').toggleClass('col-md-12 col-md-9');
                        $('#main-view').toggleClass('col-lg-12 col-lg-10');


                    });
                    if ( $('#parent-image').length > 0 ){
                        $('#main-view').css('margin-left', 0);
                        setTimeout(function () {
                            $('#main-view').css('visibility','visible');
                        }, 400)
                    }

                    $(this).next('.list-group').parent().css('height','auto');

                });

                $('.mini-submenu').on('click',function(){
                    $(this).next().next('.list-group').toggle('slide');
                    $(this).next().next('.list-group').parent().css('height','-webkit-fill-available');
                    $('.mini-submenu').fadeOut();
                    $('#slide-submenu').fadeIn();
                    $('#main-view').toggleClass('col-md-9 col-md-12');
                    $('#main-view').toggleClass('col-lg-10 col-lg-12');

                    if ( $('#parent-image').length > 0 ) {

                        $('#main-view').css('visibility','hidden');
                        $('#main-view').css('margin-left', '-' + $('#slide-submenu').next('.list-group').parent().width() / 2 + 'px');
                        $('#main-view').css('visibility','visible');

                    }

                })
            }catch (e){

            }

            $('.list-group-item').on('click',function(){
                if($(this).next('.list-group-item-dropdown').css('display') == 'none'){
                    $(this).next('.list-group-item-dropdown').show(500);
                }else{
                    $(this).next('.list-group-item-dropdown').hide(500);
                }
            })

            $('a.list-title-open').on('click',function(){
                var id = $(this).attr('id');
                if($('#list-'+id).css('display') == "none"){
                    $('#list-'+id).show(500);
                }else{
                    $('#list-'+id).hide(500);
                }
            })
            
            $('.list-group-item a.title').on('click',function(){
                var id = $(this).attr('id');
                if($('#list-'+id).css('display') == "none"){
                    $('*[id^="list-"]').hide(500);
                    $('#list-'+id).show(500);
                }else{
                    $('#list-'+id).hide(500);
                }
            })

        });

    </script>
    @stack('js')
</body>
</html>
