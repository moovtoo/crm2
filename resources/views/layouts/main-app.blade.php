<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@if(isset($settings['cms_title'])) {{ $settings['cms_title']}}
        @else{{ config('app.name', 'Laravel') }}@endif</title>

    <link rel="shortcut icon" type="image/x-icon" href="@if(isset($settings['favicon'])) {{asset($settings['favicon'])}} @else  {{asset('images/login_logo.svg')}} @endif">

    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">
    <link rel="shortcut icon" type="image/x-icon" href="{{asset($settings['favicon'])}}">
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />

    <link href="{{ asset('css/main.css') }}" rel="stylesheet">
    <link href="{{ asset('css/inputfile.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.18/datatables.min.css"/>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs/jq-3.3.1/jszip-2.5.0/dt-1.10.18/af-2.3.2/b-1.5.4/b-colvis-1.5.4/b-flash-1.5.4/b-html5-1.5.4/b-print-1.5.4/cr-1.5.0/fc-3.2.5/fh-3.1.4/kt-2.5.0/r-2.2.2/rg-1.1.0/rr-1.2.4/sc-1.5.0/sl-1.2.6/datatables.min.css"/>

    {{--<script src="{{ asset('js/app.js') }}" defer></script>--}}
    @stack('css')
</head>
<body>
<div id="app">
    @include('layouts.partials.header')

    <main class="py-4" style="padding-top:65px!important;">
        @if(Request::path() != 'cms')
            @include('layouts.partials.body-header')
            <div class="row" style="padding: 0;margin: 0;background: #fafafa;">
                @if(Request::segment(2) == "configuration")
                    @include('configuration.side-menu')
                @else
                    @include('layouts.partials.side-menu')
                @endif
                <div class="col-md-9 col-lg-10" style="padding: 0" id="main-view">
                    {{-- <div class="row tab-header-title-box">
                        <div class="tab-header-title middle">
                            {{$title?? 'Admin Page'}}
                        </div>
                    </div> --}}
                    @yield('content')
                </div>
            </div>
        @else
            @yield('content')
        @endif
    </main>
</div>


<script
        src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha256-3edrmyuQ0w65f8gfBsqowzjJe2iM6n0nKciPUp8y+7E="
        crossorigin="anonymous"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://netdna.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/v/bs-3.3.7/jq-3.3.1/jszip-2.5.0/dt-1.10.18/af-2.3.2/b-1.5.4/b-colvis-1.5.4/b-flash-1.5.4/b-html5-1.5.4/b-print-1.5.4/cr-1.5.0/fc-3.2.5/fh-3.1.4/kt-2.5.0/r-2.2.2/rg-1.1.0/rr-1.2.4/sc-1.5.0/sl-1.2.6/datatables.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>


<script>
    $(function(){

        $('#navbarDropdown').on('click', function () {
            if($('.dropdown-menu').css('display') == 'none'){
                $('.dropdown-menu').show();
            }else{
                $('.dropdown-menu').hide();
            }
        })

        try{
            // main-table
            if ( $('#parent-image').length > 0) {
                $('#main-view').css('margin-left', '-' + $('#slide-submenu').next('.list-group').parent().width() / 2 + 'px');

            }
            $('#slide-submenu').on('click',function() {

                if ( $('#parent-image').length ){
                    $('#main-view').css('visibility','hidden');
                }
                $(this).next('.list-group').fadeOut('slide',function(){
                    $('.mini-submenu').fadeIn();
                    $('#slide-submenu').fadeOut();
                    $('#main-view').toggleClass('col-md-12 col-md-9');
                    $('#main-view').toggleClass('col-lg-12 col-lg-10');


                });
                if ( $('#parent-image').length > 0 ){
                    $('#main-view').css('margin-left', 0);
                    setTimeout(function () {
                        $('#main-view').css('visibility','visible');
                    }, 400)
                }

                $(this).next('.list-group').parent().css('height','auto');

            });

            $('.mini-submenu').on('click',function(){
                $(this).next().next('.list-group').toggle('slide');
                $(this).next().next('.list-group').parent().css('height','-webkit-fill-available');
                $('.mini-submenu').fadeOut();
                $('#slide-submenu').fadeIn();
                $('#main-view').toggleClass('col-md-9 col-md-12');
                $('#main-view').toggleClass('col-lg-10 col-lg-12');

                if ( $('#parent-image').length > 0 ) {

                    $('#main-view').css('visibility','hidden');
                    $('#main-view').css('margin-left', '-' + $('#slide-submenu').next('.list-group').parent().width() / 2 + 'px');
                    $('#main-view').css('visibility','visible');

                }

            })
        }catch (e){

        }

        $('.list-group-item').on('click',function(){
            if($(this).next('.list-group-item-dropdown').css('display') == 'none'){
                $(this).next('.list-group-item-dropdown').show(500);
            }else{
                $(this).next('.list-group-item-dropdown').hide(500);
            }
        })

        $('a.list-title-open').on('click',function(){
            var id = $(this).attr('id');
            if($('#list-'+id).css('display') == "none"){
                $('#list-'+id).show(500);
            }else{
                $('#list-'+id).hide(500);
            }
        })

        $('.list-group-item a.title').on('click',function(){
            var id = $(this).attr('id');
            if($('#list-'+id).css('display') == "none"){
                $('*[id^="list-"]').hide(500);
                $('#list-'+id).show(500);
            }else{
                $('#list-'+id).hide(500);
            }
        })

    });

</script>
@stack('js')
</body>
</html>
