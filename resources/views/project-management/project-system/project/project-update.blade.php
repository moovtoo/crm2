@extends('layouts.app')
@php($slug = 'project')
@php($module = \App\Models\Module::query()->where('slug',$slug)->get()->first())
@if( $module != null )
    @php($name = '\App\Models\\'.str_replace('Controller','',$module->controller_name))
    @php($model = new $name())
    @php($fields = $model->fields())
@endif
@section('content')
    <form id="main-form" enctype="multipart/form-data">
        <div class="tab">
            <div class="tab-links active" id="tab-1" onclick="openTab(1)">Modules</div>
            <div class="tab-links" id="tab-2" onclick="openTab(2)">Project Properties</div>
            <div class="tab-links" id="tab-3" onclick="openTab(3)">Project Reporting</div>
        </div>
        <input type="hidden" name="_method" value="put">
        @csrf
                <div id="1" class="tab-content tab-table" style="display:block;">
                    <div class="tab-table">
                        <div class="col-md-12 tab-table">
                            @include('layouts.partials.module-ajax-table', ['route'=>route('project-module.index'), 'params'=>'?project_id='.$project->id,'active'=>'project','type' => 'project', 'add_button'=>'ADD A MODULE','model'=>new \App\Models\ProjectModule()])
                        </div>
                    </div>
                </div>
                <div id="2" class=" tab-content " >
                    <div class="row">
                        <div class="container">
                            <div class="col-md-11">
                                <div class="form-group">
                                    <label for="name" class="control-label">Name<span class="star-required">*</span></label>
                                    <input type="text" class="form-control" id="name" name="name" value="{{$project->name}}">
                                    <label id="name-error" class="control-label error"></label>
                                </div>
                                <div class="form-group">
                                    <label for="project_manager_id" class="control-label">Client Leader</label>
                                    <select type="text" class="form-control select2" id="project_manager_id" name="project_manager_id">
                                        @foreach($users as $user)
                                            <option value="{{$user->id}}" @if($user->id == $project->project_manager_id) selected @endif>{{$user->name}}</option>
                                        @endforeach
                                    </select>
                                    <label id="project_manager_id-error" class="control-label error"></label>
                                </div>

                                <div class="form-group">
                                    <label for="organization_id" class="control-label">Organization</label>
                                    <select type="text" class="form-control select2" id="organization_id" name="organization_id">
                                        <option value="">Select Organization</option>
                                        @foreach($organizations as $organization)
                                            <option value="{{$organization->id}}" @if($organization->id == $project->organization_id) selected @endif >{{$organization->name}}</option>
                                        @endforeach
                                    </select>
                                    <label id="organization_id-error" class="control-label error"></label>
                                </div>




                                <div class="form-group">
                                    <label for="client_manager_id" class="control-label">Client Contact</label>
                                    <select type="text" class="form-control select2" id="client_manager_id" name="client_manager_id">

                                        @foreach($contacts as $contact)
                                            <option value="{{$contact->id}}" @if($contact->id == $project->project_manager_id) selected @endif  >{{$contact->name}}</option>
                                        @endforeach
                                    </select>
                                    <label id="client_manager_id-error" class="control-label error"></label>
                                </div>

                                <div class="form-group">
                                    <label for="start_date" class="control-label">Start Date</label>
                                    <div class="date-box">
                                        <div class="date-icon-box" >
                                            <span class="fa fa-calendar" id="start_date_icon"></span>
                                        </div>
                                        <input type="text" class="form-control" id="start_date" name="start_date"   data-date-format="DD-MM-YYYY" value="{{$project->start_date}}">
                                    </div>
                                    <label id="start_date-error" class="control-label error"></label>
                                </div>

                                <div class="form-group">
                                    <label for="end_date" class="control-label">End Date</label>
                                    <div class="date-box">
                                        <div class="date-icon-box" >
                                            <span class="fa fa-calendar" id="end_date_icon"></span>
                                        </div>
                                        <input type="text" class="form-control" id="end_date" name="end_date"  data-date-format="DD-MM-YYYY" value="{{$project->end_date}}">
                                    </div>
                                    <label id="end_date-error" class="control-label error"></label>
                                </div>

                                <div class="form-group">
                                    <label for="status_id" class="control-label">Project Status</label>
                                    <select type="text" class="form-control select2" id="status_id" name="status_id">
                                        <option value="">Select Status</option>
                                        @foreach($statuses as $status)
                                            <option value="{{$status->id}}" @if($status->id == $project->status_id) selected @endif>{{$status->name}}</option>
                                        @endforeach
                                    </select>
                                    <label id="status_id-error" class="control-label error"></label>
                                </div>
                                <div class="form-group">
                                    <label for="description" class="control-label">Description</label>
                                    <textarea class="form-control" id="description" name="description" >{{$project->description}}</textarea>
                                    <label id="description-error" class="control-label error"></label>
                                </div>
                            </div>
                        </div>
                        @include('layouts.partials.form-action-buttons',['submit'=>true,'reset'=>true])
                    </div>
                </div>
                <div id="3" class=" tab-content " >
                    <div class="timing-tabs">
                        <div class="full-tab">
                             <div class="full-tab-head" onclick="openTimeTab(this ,1)" data-target="1">
                                 <span class="full-tab-title">User DashBoard</span>
                                 <span class="full-tab-arrow"><i class="fa fa-angle-down"></i></span>
                             </div>
                             <div class="full-tab-content" data-id="1">
                                 <div class="timing-div">
                                     @include('project-management.project-system.project-dash-board')
                                 </div>
                             </div>
                         </div>
                    </div>
                </div>

    </form>
@endsection

@push('css')
    <link href="{{asset('css/colorbox.css')}}" rel="stylesheet">
@endpush
@push('js')

    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.3/moment.min.js"></script>
    <script src="{{asset('packages/ckeditor/ckeditor.js')}}"></script>
    <script>
        CKEDITOR.replace( 'description' );
    </script>
    <script type="text/javascript" src="{{asset('js/jquery.colorbox-min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/standalonepopup.min.js')}}"></script>

    <script>
        $(function(){
            var hash = location.hash;
            if(hash == "#projectModule"){
                openTab(1);
            };
        });
    </script>
    <script>
        $(function(){

            $("input").on("change", function() {
                this.setAttribute(
                    "data-date",
                    moment(this.value, "YYYY-MM-DD")
                        .format( this.getAttribute("data-date-format") )
                )
            }).trigger("change");


        });
    </script>

    <script type="application/javascript">
        function openTab(tabId) {
            var i, tabcontent;
            tabcontent = document.getElementsByClassName("tab-content");
            for (i = 0; i < tabcontent.length; i++) {
                tabcontent[i].style.display = "none";
            }
            var tabs = document.getElementsByClassName("tab-links");
            for (i = 0; i < tabs.length; i++) {
                tabs[i].className = tabs[i].className.replace(" active", "");
            }
            document.getElementById(tabId).style.display = "block";
            document.getElementById('tab-'+tabId).className += " active";
        }
        // openTab(this, 1);
    </script>
    <script>
        $(function () {
            $('.select2').select2({
                width:'100%'
            });

            $('#organization_id').on('change', function () {
                var id=$(this).val();
                var dataString = '?organization_id='+ id;
                $('#client_manager_id').select2({
                    width: '100%',
                    placeholder: 'Select Contact',
                    ajax: {
                        url: '{{route('ajax-select')}}'+dataString,
                        dataType: 'json',
                        processResults: function (data) {
                            return {
                                results: $.map(data, function(key, value) {
                                    return { id: key, text: value };
                                })
                            };
                        },
                        cache: true
                    }
                });
            })

            $('#form-back').on('click',function () {
                location.href='{{route('project-management.project-system.project.index')}}';
            })
            $('#form-reset').on('click',function () {
                $('#main-form')[0].reset();
                $(".select2").val(null);
                $(".select2").trigger('change');
            })
            $('#form-submit').on('click',function () {
                var count = 0;
                @foreach($fields['update'] as $field)
                if($('#{{$field}}').val() == ''){
                    $('#{{$field}}-error').html('This field is required');
                    count++;
                }else{
                    $('#{{$field}}-error').html('');
                }
                @endforeach

                if(count == 0){
                    var data = new FormData();
                    var content = CKEDITOR.instances.description.getData();

                    //Form data

                    var form_data = $('#main-form').serializeArray();
                    $.each(form_data, function (key, input) {
                        data.append(input.name, input.value);
                    });
                    data.append('_token', '{{csrf_token()}}');
                    data.append('description', content);

                    $.ajax({
                        type: "POST",
                        processData: false,
                        contentType: false,
                        url: '{{route('project-management.project-system.project.update', $project->id)}}',
                        data: data,
                        success: function(data)
                        {
                            console.log(data);
                            if(data.status == 'success'){
                                location.href='{{route('project-management.project-system.project.index')}}';
                            } else{
                                for( var error in  data.messages){
                                    $('#'+error+'-error').html(data.messages[error]);
                                    $('#'+error).focus();
                                }
                            }
                        },
                        error:function( data){
                            console.log(data);
                        }
                    });
                }
            });
        })
    </script>

@endpush