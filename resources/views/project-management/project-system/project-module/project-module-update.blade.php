@extends('layouts.app')

@section('content')
    <form id="main-form" enctype="multipart/form-data">
        <div class="tab">
            <div class="tab-links active" id="tab-1" onclick="openTab(1)">Tasks</div>
            <div class="tab-links" id="tab-2" onclick="openTab(2)">Module Properties</div>
        </div>
        <input type="hidden" name="_method" value="put">
        @csrf
        <div id="1" class="tab-content tab-table" style="display:block;">
            <div class="tab-table">
                <div class="col-md-12 tab-table">
                    @include('layouts.partials.internal-ajax-table', ['route'=>route('project-task.index'), 'params'=>'?project_module_id='.$projectModule->id,'active'=>'project','type' => 'project', 'add_button'=>'ADD A TASK','model'=>new \App\Models\ProjectTask()])
                </div>
            </div>

        </div>
        <div id="2" class=" tab-content tab-content-profile " >
            <div class="row">
                <div class="container">
                    <div class="col-md-11">
                        <div class="form-group">
                            <label for="name" class="control-label">Title<span class="star-required">*</span></label>
                            <input type="text" class="form-control" id="name" name="name" value="{{$projectModule->name}}" >
                            <label id="name-error" class="control-label error"></label>
                        </div>

                        <div class="form-group">
                            <label for="users" class="control-label">Team Leader </label>
                            <select type="text" class="form-control select2" id="users" name="users[]" multiple>
                                @foreach($users as $user)
                                    <option value="{{$user->id}}"
                                    @foreach ($projectModule->users as $item)
                                        {{ $user->id == $item->id ? 'selected' :'' }}
                                            @endforeach
                                    >{{$user->name}}</option>
                                @endforeach
                            </select>
                            <label id="user_id-error" class="control-label error"></label>
                        </div>



                        <div class="form-group">
                            <label for="start_date" class="control-label">Start Date</label>
                            <div class="date-box">
                                <div class="date-icon-box" >
                                    <span class="fa fa-calendar" id="start_date_icon"></span>
                                </div>
                                <input type="date" class="form-control" id="start_date" name="start_date"  data-date-format="DD-MM-YYYY" value="{{$projectModule->start_date}}">
                            </div>
                            <label id="start_date-error" class="control-label error"></label>
                        </div>

                        <div class="form-group">
                            <label for="end_date" class="control-label">End Date</label>
                            <div class="date-box">
                                <div class="date-icon-box" >
                                    <span class="fa fa-calendar" id="end_date_icon"></span>
                                </div>
                                <input type="date" class="form-control" id="end_date" name="end_date"  data-date-format="DD-MM-YYYY" value="{{$projectModule->end_date}}">
                            </div>
                            <label id="end_date-error" class="control-label error"></label>
                        </div>



                        <div class="form-group">
                            <label for="status_id" class="control-label">Module Stage</label>
                            <select type="text" class="form-control select2" id="status_id" name="status_id">
                                @foreach($statuses as $status)
                                    <option value="{{$status->id}}" @if($status->id == $projectModule->status_id) selected @endif>{{$status->name}}</option>
                                @endforeach
                            </select>
                            <label id="status_id-error" class="control-label error"></label>
                        </div>

                        <div class="form-group">
                            <label for="channel" class="control-label">Module Channel</label>
                            <select type="text" class="form-control select2" id="channel" name="channel">
                                    <option value="">Select Channel</option>
                                    <option value="Web" @if( $projectModule->channel == 'Web' ) selected @endif>Web</option>
                                    <option value="Mobile" @if( $projectModule->channel  == 'Mobile') selected @endif>Mobile</option>
                                    <option value="Chatbot" @if( $projectModule->channel == 'Chatbot' ) selected @endif>Chatbot</option>
                                    <option value="Social Media" @if( $projectModule->channel =='ERP' ) selected @endif>Social Media</option>
                                    <option value="ERP" @if( $projectModule->channel =='ERP' ) selected @endif>ERP</option>
                                    <option value="API" @if($projectModule->channel == 'API' ) selected @endif>API</option>

                            </select>
                            <label id="channel-error" class="control-label error"></label>
                        </div>




                    </div>

                </div>
            </div>
            @include('layouts.partials.form-action-buttons',['submit'=>true,'back'=>true])

        </div>

    </form>
@endsection

@push('css')
    <link href="{{asset('css/colorbox.css')}}" rel="stylesheet">
@endpush
@push('js')

    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.3/moment.min.js"></script>
    <script src="{{asset('packages/ckeditor/ckeditor.js')}}"></script>
    <script>
        // CKEDITOR.replace( 'note' );
    </script>
    <script type="text/javascript" src="{{asset('js/jquery.colorbox-min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/standalonepopup.min.js')}}"></script>

    <script>
        $(function(){

            $("input").on("change", function() {
                this.setAttribute(
                    "data-date",
                    moment(this.value, "YYYY-MM-DD")
                        .format( this.getAttribute("data-date-format") )
                )
            }).trigger("change");



        });
    </script>
    <script>
        $(function(){
            var hash = location.hash;
            if(hash == "#projectTask"){
                openTab(1);
            };
        });
    </script>
    <script type="application/javascript">
        function openTab(tabId) {
            var i, tabcontent;
            tabcontent = document.getElementsByClassName("tab-content");
            for (i = 0; i < tabcontent.length; i++) {
                tabcontent[i].style.display = "none";
            }
            var tabs = document.getElementsByClassName("tab-links");
            for (i = 0; i < tabs.length; i++) {
                tabs[i].className = tabs[i].className.replace(" active", "");
            }
            document.getElementById(tabId).style.display = "block";
            document.getElementById('tab-'+tabId).className += " active";
        }
        // openTab(this, 1);
    </script>


    <script>
        $(function () {
            $('.select2').select2({
                width: '100%'
            });
        })
        $('#form-back').click(function () {
            location.href='{{route('project-management.project-system.project.index')}}' + '/' + '{{$projectModule->project_id}}' +'/edit'
        });
        $('#form-reset').on('click',function () {
            $('#main-form')[0].reset();
        })
        $('#form-submit').click(function () {
            var counter = 0;
            @foreach($fields['update'] as $k=>$field)

            if( $('#{{$field}}').val() =='' ){
                $('#{{$field}}-error').text('This field is required');
                counter++;
            }
            @endforeach
            if ( counter > 0 ){
                return;
            }
            // tinyMCE.triggerSave();

            // var content = CKEDITOR.instances.note.getData();
            var data = new FormData();
            //Form data
            var form_data = $('#main-form').serializeArray();
            $.each(form_data, function (key, input) {
                data.append(input.name, input.value);
            });
            data.append('_token', '{{csrf_token()}}');
            // data.append('note', content);
            $.ajax({
                type: "POST",
                processData: false,
                contentType: false,
                url: '{{route('project-module.update', $projectModule->id)}}',
                data: data,
                success: function(data)
                {
                    console.log(data);
                    if(data != null){
                        location.href='{{route('project-management.project-system.project.index')}}/{{$projectModule->project_id}}/edit#projectModule';
                    }
                },
                error:function( data){
                    console.log(data);
                }
            });

        });
    </script>
@endpush
