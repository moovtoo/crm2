@extends('layouts.app')

@section('content')

    <form id="main-form">
        @if(app('request')->has('project_id'))
            <input type="text" name="project_id" value="{{app('request')->get('project_id')}}" hidden>
        @endif
        @csrf
        <div id="1" class="tab-content tab-content-profile" style="display:block;">
            <div class="row">
                <div class="container">
                    <div class="col-md-11">

                        <div class="form-group">
                            <label for="name" class="control-label">Title<span class="star-required">*</span></label>
                            <input type="text" class="form-control" id="name" name="name" >
                            <label id="name-error" class="control-label error"></label>
                        </div>

                        <div class="form-group">
                        <label for="users" class="control-label">Team Leader </label>
                        <select type="text" class="form-control select2" id="users" name="users[]" multiple>
                        <option value="">Select Team Leader</option>
                        @foreach($users as $user)
                        <option value="{{$user->id}}" >{{$user->name}}</option>
                        @endforeach
                        </select>
                        <label id="user_id-error" class="control-label error"></label>
                        </div>



                        <div class="form-group">
                            <label for="start_date" class="control-label">Start Date</label>
                            <div class="date-box">
                                <div class="date-icon-box" >
                                    <span class="fa fa-calendar" id="start_date_icon"></span>
                                </div>
                                <input type="date" class="form-control" id="start_date" name="start_date"  data-date-format="DD-MM-YYYY">
                            </div>
                            <label id="start_date-error" class="control-label error"></label>
                        </div>

                        <div class="form-group">
                            <label for="end_date" class="control-label">End Date</label>
                            <div class="date-box">
                                <div class="date-icon-box" >
                                    <span class="fa fa-calendar" id="end_date_icon"></span>
                                </div>
                                <input type="date" class="form-control" id="end_date" name="end_date"  data-date-format="DD-MM-YYYY">
                            </div>
                            <label id="end_date-error" class="control-label error"></label>
                        </div>

                        <div class="form-group">
                            <label for="status_id" class="control-label">Module Stage</label>
                            <select type="text" class="form-control select2" id="status_id" name="status_id">
                                <option value="">Select Status</option>
                                @foreach($statuses as $status)
                                    <option value="{{$status->id}}" >{{$status->name}}</option>
                                @endforeach
                            </select>
                            <label id="status_id-error" class="control-label error"></label>
                        </div>

                        <div class="form-group">
                            <label for="channel" class="control-label">Module Channel</label>
                            <select type="text" class="form-control select2" id="channel" name="channel">
                                <option value="">Select Channel</option>
                                <option value="Web">Web</option>
                                <option value="Mobile">Mobile</option>
                                <option value="Chatbot">Chatbot</option>
                                <option value="Social Media">Social Media</option>
                                <option value="ERP">ERP</option>
                                <option value="API" >API</option>

                            </select>
                            <label id="channel-error" class="control-label error"></label>
                        </div>




                    </div>

                </div>
            </div>
        </div>
        @include('layouts.partials.form-action-buttons',['submit'=>true,'back'=>true])
    </form>
@endsection

@push('css')
    <link href="{{asset('css/colorbox.css')}}" rel="stylesheet">
@endpush
@push('js')

    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.3/moment.min.js"></script>
    <script src="{{asset('packages/ckeditor/ckeditor.js')}}"></script>
    <script>
        // CKEDITOR.replace( 'note' );
    </script>
    <script type="text/javascript" src="{{asset('js/jquery.colorbox-min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/standalonepopup.min.js')}}"></script>

    <script>
        $(function(){

            $("input").on("change", function() {
                this.setAttribute(
                    "data-date",
                    moment(this.value, "YYYY-MM-DD")
                        .format( this.getAttribute("data-date-format") )
                )
            }).trigger("change");


        });
    </script>
    <script>
        $(function () {
            $('.select2').select2({
                width: '100%'
            });
        })
        $('#form-back').click(function () {
            location.href='{{route('project-module.index')}}'+ '/' + {{app('request')->get('project_id')}} + '/edit'
        });
        $('#form-reset').on('click',function () {
            $('#main-form')[0].reset();
        })
        $('#form-submit').click(function () {
            var counter = 0;
            @foreach($fields['store'] as $k=>$field)

            if( $('#{{$field}}').val() =='' ){
                $('#{{$field}}-error').text('This field is required');
                counter++;
            }
            @endforeach
            if ( counter > 0 ){
                return;
            }
            // tinyMCE.triggerSave();
            var data = new FormData();
            // var content = CKEDITOR.instances.note.getData();

            //Form data
            var form_data = $('#main-form').serializeArray();
            $.each(form_data, function (key, input) {
                data.append(input.name, input.value);
            });
            data.append('_token', '{{csrf_token()}}');
            // data.append('note', content);

            $.ajax({
                type: "POST",
                processData: false,
                contentType: false,
                url: '{{route('project-module.store')}}',
                data: data,
                success: function(data)
                {
                    console.log(data);
                    if(data != null){
                        location.href='{{route('project-management.project-system.project.index')}}'+ '/' + data.projectModule.project_id + '/edit#projectModule';
                    }
                },
                error:function( data){
                    console.log(data);
                }
            });

        });
    </script>
@endpush
