@extends('layouts.main-app')
@section('content')

    <form id="main-form" enctype="multipart/form-data">
        <div class="tab">
            <div class="tab-links active" id="tab-1" onclick="openTab(1)">Proposition Commerciale</div>
            <div class="tab-links" id="tab-2" onclick="openTab(2)">Leads</div>
        </div>
        <input type="hidden" name="_method" value="put">
        @csrf
        <div id="1" class="tab-content tab-table" style="display:block;">

                <div class="tab-table">
                    <div class="col-md-12 tab-table">
                        @include('layouts.partials.show-ajax-table', ['route'=>route('project-management.lead-system.lead.index'), 'params'=>'?type_id=7&status_id=10&status_id=20','custom_order'=>4,'active'=>'lead','type' => 'lead','table'=>'lead1','model'=>new \App\Models\Lead()])
                    </div>
                </div>
        </div>
            <div id="2" class="tab-content tab-table" >
                <div class="tab-table">
                    <div class="col-md-12 tab-table">
                        @include('layouts.partials.show-ajax-table', ['route'=>route('project-management.lead-system.lead.index'), 'params'=>'?status_id[]=10&status_id[]=20&status_id[]=12','custom_order'=>5 ,'active'=>'lead','type' => 'lead','table'=>'lead2', 'model'=>new \App\Models\Lead()])
                    </div>
                </div>

        </div>

        {{--@include('layouts.partials.form-action-buttons',['submit'=>true,'reset'=>true])--}}
    </form>

@endsection

@push('css')
    <link href="{{asset('css/colorbox.css')}}" rel="stylesheet">
@endpush
@push('js')

    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.3/moment.min.js"></script>
    <script src="{{asset('packages/tinymce/tinymce.min.js')}}"></script>

    <script type="text/javascript" src="{{asset('js/jquery.colorbox-min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/standalonepopup.min.js')}}"></script>



    <script type="application/javascript">
        function openTab(tabId) {
            var i, tabcontent;
            tabcontent = document.getElementsByClassName("tab-content");
            for (i = 0; i < tabcontent.length; i++) {
                tabcontent[i].style.display = "none";
            }
            var tabs = document.getElementsByClassName("tab-links");
            for (i = 0; i < tabs.length; i++) {
                tabs[i].className = tabs[i].className.replace(" active", "");
            }
            document.getElementById(tabId).style.display = "block";
            document.getElementById('tab-'+tabId).className += " active";
        }
        // openTab(this, 1);

    </script>
    {{--<script>--}}
        {{--$(function () {--}}
            {{--$('.select2').select2({--}}
                {{--width:'100%'--}}
            {{--});--}}

            {{--$('#form-back').on('click',function () {--}}
                {{--location.href='{{route('project-management.project-system.lead.index')}}';--}}
            {{--})--}}
            {{--$('#form-reset').on('click',function () {--}}
                {{--$('#main-form')[0].reset();--}}
                {{--$(".select2").val(null);--}}
                {{--$(".select2").trigger('change');--}}
            {{--})--}}
            {{--$('#form-submit').on('click',function () {--}}
                {{--var count = 0;--}}
                {{--@foreach($fields['update'] as $field)--}}
                {{--if($('#{{$field}}').val() == ''){--}}
                    {{--$('#{{$field}}-error').html('This field is required');--}}
                    {{--count++;--}}
                {{--}else{--}}
                    {{--$('#{{$field}}-error').html('');--}}
                {{--}--}}
                {{--@endforeach--}}

                {{--if(count == 0){--}}
                    {{--var data = new FormData();--}}
                    {{--//Form data--}}
                    {{--var form_data = $('#main-form').serializeArray();--}}
                    {{--$.each(form_data, function (key, input) {--}}
                        {{--data.append(input.name, input.value);--}}
                    {{--});--}}
                    {{--data.append('_token', '{{csrf_token()}}');--}}
                    {{--$.ajax({--}}
                        {{--type: "POST",--}}
                        {{--processData: false,--}}
                        {{--contentType: false,--}}
                        {{--url: '{{route('project-management.project-system.lead.update', $lead->id)}}',--}}
                        {{--data: data,--}}
                        {{--success: function(data)--}}
                        {{--{--}}
                            {{--console.log(data);--}}
                            {{--if(data.status == 'success'){--}}
                                {{--location.href='{{route('project-management.project-system.lead.index')}}';--}}
                            {{--} else{--}}
                                {{--for( var error in  data.messages){--}}
                                    {{--$('#'+error+'-error').html(data.messages[error]);--}}
                                    {{--$('#'+error).focus();--}}
                                {{--}--}}
                            {{--}--}}
                        {{--},--}}
                        {{--error:function( data){--}}
                            {{--console.log(data);--}}
                        {{--}--}}
                    {{--});--}}
                {{--}--}}
            {{--});--}}
        {{--})--}}
    {{--</script>--}}

@endpush