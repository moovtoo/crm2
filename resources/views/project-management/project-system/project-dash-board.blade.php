    <form id="main-form" enctype="multipart/form-data">

        @csrf

        <div class="row">
            <div class="container">
                <div class="col-md-11">
                    <div class="form-group">
                        <label for="user_id" class="control-label">Filter By </label>
                        <select type="text" class="form-control select2" id="user_id" name="user_id">
                            <option value="">Select User</option>
                            @foreach($usersProject as $user)
                                <option value="{{$user->id}}">{{$user->name}}</option>
                            @endforeach
                        </select>

                        <label id="user_id-error" class="control-label error"></label>
                    </div>
                    <hr>
                </div>
                <div class="col-md-11">

                    {{--<div class="form-group form-group-show">--}}
                        {{--<label for="name" class="control-label">Project Name:</label>--}}
                        {{--<p>{{$project->name}}</p>--}}
                    {{--</div>--}}

                    @foreach($project->getAllRelatedUsers() as $user )
                        <div class="project-user" data-user="{{$user->id}}">
                            <div class="form-group form-group-show">
                                <label for="name" class="control-label">User :</label>
                                <label>{{$user->name}}</label>
                            </div>

                            @foreach($user->RelatedModulesAndTasks($project->id) as  $module )
                                <div class="form-group form-group-show">
                                    {{--<label for="name" class="control-label">Module:</label>--}}
                                    <p class="module-name">{{$module->name}}</p>
                                </div>
                                @foreach( $module->projectTask as $task )
                                    @if ( $task->checkUser($user->id))
                                        <div class="form-group form-group-show">
                                            {{--<label for="name" class="control-label">Task:</label>--}}
                                            <p class="task-name">{{$task->name}} </p> <span class="task-end-date">({{$task->end_date}})</span>
                                        </div>
                                    @endif
                                @endforeach
                            @endforeach
                            <hr>
                        </div>
                    @endforeach

                </div>
            </div>
        </div>
{{--        @include('layouts.partials.form-action-buttons',['submit'=>false,'reset'=>false , 'edit'=>false])--}}

    </form>


@push('css')
    <link href="http://cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/a549aa8780dbda16f6cff545aeabc3d71073911e/build/css/bootstrap-datetimepicker.css" rel="stylesheet"/>
    <link href="{{asset('css/colorbox.css')}}" rel="stylesheet">
@endpush
@push('js')

    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>
    <script src="{{asset('js/datetimepicker.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/jquery.colorbox-min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/standalonepopup.min.js')}}"></script>

    <script src="{{asset('packages/tinymce/tinymce.min.js')}}"></script>
    <script>
        $(function(){
            tinymce.init({
                selector: "textarea",
                plugins: [
                    "advlist autolink lists link image charmap print preview anchor",
                    "searchreplace visualblocks code fullscreen",
                    "insertdatetime media table paste"
                ],
                toolbar: " undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
                file_browser_callback : elFinderBrowser,
                urlconverter_callback: myCustomURLConverter

            });

            function myCustomURLConverter(url, node, on_save, name) {
                // Do some custom URL conversion

                // Return new URL
                return url;
            }

            function elFinderBrowser (field_name, url, type, win) {
                var elfinder_url = '{{route('elfinder.tinymce4')}}';    // use an absolute path!
                tinyMCE.activeEditor.windowManager.open({
                    file: elfinder_url,
                    title: 'Media Library',
                    width: 900,
                    height: 550,
                    resizable: 'yes',
                    //inline: 'yes',
                    popup_css: false,
                    close_previous: 'no'
                }, {
                    setUrl: function (url) {
                        win.document.getElementById(field_name).value = url;
                    },
                    window: win,
                    input: field_name
                });
                return false;
            }

        });
    </script>
    <script>
        ;( function ( document, window, index )
        {
            var inputs = document.querySelectorAll( '.inputfile' );
            Array.prototype.forEach.call( inputs, function( input )
            {
                var label	 = input.nextElementSibling,
                    labelVal = label.innerHTML;

                input.addEventListener( 'change', function( e )
                {
                    var fileName = '';
                    if( this.files && this.files.length > 1 )
                        fileName = ( this.getAttribute( 'data-multiple-caption' ) || '' ).replace( '{count}', this.files.length );
                    else
                        fileName = e.target.value.split( '\\' ).pop();

                    if( fileName )
                        label.querySelector( 'span' ).innerHTML = fileName;
                    else
                        label.innerHTML = labelVal;
                });

                // Firefox bug fix
                input.addEventListener( 'focus', function(){ input.classList.add( 'has-focus' ); });
                input.addEventListener( 'blur', function(){ input.classList.remove( 'has-focus' ); });
            });
        }( document, window, 0 ));
    </script>

    <script>
        $('#user_id').on('change',function () {
            var id = $(this).val();

            if(id != null && id != "" && id != undefined){
                $('.project-user').each(function (key, value) {
                    $(value).hide();
                })
                $('.project-user[data-user="'+id+'"]').show();
            }else{
                $('.project-user').each(function (key, value) {
                    $(value).show();
                })
            }
        })
    </script>

@endpush