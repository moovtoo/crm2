@extends('layouts.app')

@section('content')
    @include('layouts.partials.ajax-tables', ['route'=>Request::url(),'custom_order'=>4,'active'=>'hostings','type' => 'hosting'])

@endsection
