@extends('layouts.app')
@php($slug = 'hosting')
@php($module = \App\Models\Module::query()->where('slug',$slug)->get()->first())
@if( $module != null )
    @php($name = '\App\Models\\'.str_replace('Controller','',$module->controller_name))
    @php($model = new $name())
    @php($fields = $model->fields())
@endif
@section('content')
    <form id="main-form" enctype="multipart/form-data">
        <div class="tab">
            <div class="tab-links active" id="tab-1" onclick="openTab(1)">Hosting Properties</div>
            <div class="tab-links" id="tab-2" onclick="openTab(2)">Cpannel Properties</div>
        </div>
        @csrf
        <div id="1" class="tab-content" style="display:block;">
            <div class="row">
                <div class="container">
                    <div class="col-md-11">
                        <div class="form-group">
                            <label for="name" class="control-label">Name<span class="star-required">*</span></label>
                            <input type="text" class="form-control" id="name" name="name" >
                            <label id="name-error" class="control-label error"></label>
                        </div>
                        <div class="form-group">
                            <label for="status_id" class="control-label">hosting Status</label>
                            <select type="text" class="form-control select2" id="status_id" name="status_id">
                                <option value="">Select Status</option>
                                @foreach($statuses as $status)
                                    <option value="{{$status->id}}" >{{$status->name}}</option>
                                @endforeach
                            </select>
                            <label id="status_id-error" class="control-label error"></label>
                        </div>
                        <div class="form-group">
                            <label for="domain_management_id" class="control-label">Domain Management</label>
                            <select type="text" class="form-control select2" id="domain_management_id" name="domain_management_id">
                                <option value="">Select Domain Management</option>
                                @foreach($domainManagements as $domainManagement)
                                    <option value="{{$domainManagement->id}}" >{{$domainManagement->name}}</option>
                                @endforeach
                            </select>
                            <label id="domain_management_id-error" class="control-label error"></label>
                        </div>
                        <div class="form-group">
                            <label for="type_id" class="control-label">Domain type</label>
                            <select type="text" class="form-control select2" id="type_id" name="type_id">
                                <option value="">Select Domain Type</option>
                                @foreach($domainTypes as $domainType)
                                    <option value="{{$domainType->id}}" >{{$domainType->name}}</option>
                                @endforeach
                            </select>
                            <label id="type_id-error" class="control-label error"></label>
                        </div>
                        <div class="form-group">
                            <label for="technology_id" class="control-label">Technology</label>
                            <select type="text" class="form-control select2" id="technology_id" name="technology_id">
                                <option value="">Select Technology</option>
                                @foreach($technologies as $technology)
                                    <option value="{{$technology->id}}" >{{$technology->name}}</option>
                                @endforeach
                            </select>
                            <label id="technology_id-error" class="control-label error"></label>
                        </div>

                        <div class="form-group">
                            <label for="price" class="control-label">Price<span class="star-required">*</span></label>
                            <input type="text" class="form-control" id="price" name="price" >
                            <label id="price-error" class="control-label error"></label>
                        </div>

                              <div class="form-group">
                            <label for="server_id" class="control-label">Server</label  >
                            <select type="text" class="form-control select2" id="server_id" name="server_id">
                                <option value="">Select Server</option>
                                <option value="1" >OVH</option>
                                <option value="2" >One & One FR</option>
                                <option value="3" >One & One USA</option>
                            </select>
                            <label id="server_id-error" class="control-label error"></label>
                        </div>

                        <div class="form-group">
                            <label for="renewal_date" class="control-label">Renewal Date</label>
                            <div class="date-box">
                                <div class="date-icon-box" >
                                    <span class="fa fa-calendar" id="renewal_date_icon"></span>
                                </div>
                                <input type="date" class="form-control" id="renewal_date" name="renewal_date"  data-date-format="DD-MM-YYYY">
                            </div>
                            <label id="renewal_date-error" class="control-label error"></label>
                        </div>

                        <div class="form-group">
                            <label for="notes" class="control-label">Note</label>
                            <textarea class="form-control" id="notes" name="notes" ></textarea>
                            <label id="notes-error" class="control-label error"></label>
                        </div>



                    </div>

                </div>
            </div>
        </div>
        <div id="2" class="tab-content">
            <div class="row">
                <div class="container">
                    <div class="col-md-11">

                        <div class="form-group">
                            <label for="cpanel_code" class="control-label">Cpanel Name</label>
                            <input type="text" class="form-control" id="cpanel_code" name="cpanel_code" >
                            <label id="cpanel_code-error" class="control-label error"></label>
                        </div>
                        <div class="form-group">
                            <label for="cpanel_password" class="control-label">Cpanel Password</label>
                            <input type="text" class="form-control" id="cpanel_password" name="cpanel_password" >
                            <label id="cpanel_password-error" class="control-label error"></label>
                        </div>
                        <div class="form-group">
                            <label for="database_name" class="control-label">Data Base Name</label>
                            <input type="text" class="form-control" id="database_name" name="database_name" >
                            <label id="database_name-error" class="control-label error"></label>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        @include('layouts.partials.form-action-buttons',['submit'=>true,'reset'=>true])
    </form>
@endsection

@push('css')
    <link href="{{asset('css/colorbox.css')}}" rel="stylesheet">
@endpush
@push('js')

    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.3/moment.min.js"></script>
    <script src="{{asset('packages/ckeditor/ckeditor.js')}}"></script>
    <script>
        CKEDITOR.replace( 'notes' );
    </script>
    <script type="text/javascript" src="{{asset('js/jquery.colorbox-min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/standalonepopup.min.js')}}"></script>

    <script>
        $(function(){

            $("input").on("change", function() {
                this.setAttribute(
                    "data-date",
                    moment(this.value, "YYYY-MM-DD")
                        .format( this.getAttribute("data-date-format") )
                )
            }).trigger("change");



        });
    </script>

    <script type="application/javascript">
        function openTab(tabId) {
            var i, tabcontent;
            tabcontent = document.getElementsByClassName("tab-content");
            for (i = 0; i < tabcontent.length; i++) {
                tabcontent[i].style.display = "none";
            }
            var tabs = document.getElementsByClassName("tab-links");
            for (i = 0; i < tabs.length; i++) {
                tabs[i].className = tabs[i].className.replace(" active", "");
            }
            document.getElementById(tabId).style.display = "block";
            document.getElementById('tab-'+tabId).className += " active";
        }
        // openTab(this, 1);
    </script>
    <script>
        $(function () {
            $('.select2').select2({
                width:'100%'
            });

            $('#form-back').on('click',function () {
                location.href='{{route('project-management.hosting-system.hosting.index')}}';
            })
            $('#form-reset').on('click',function () {
                $('#main-form')[0].reset();
                $(".select2").val(null);
                $(".select2").trigger('change');
            })
            $('#form-submit').on('click',function () {
                var count = 0;
                @foreach($fields['store'] as $field)
                if($('#{{$field}}').val() == ''){
                    $('#{{$field}}-error').html('This field is required');
                    count++;
                }else{
                    $('#{{$field}}-error').html('');
                }
                @endforeach

                if(count == 0){
                    var data = new FormData();

                    //Form data
                    var form_data = $('#main-form').serializeArray();
                    $.each(form_data, function (key, input) {
                        data.append(input.name, input.value);
                    });
                    data.append('_token', '{{csrf_token()}}');
                    $.ajax({
                        type: "POST",
                        processData: false,
                        contentType: false,
                        url: '{{route('project-management.hosting-system.hosting.store')}}',
                        data: data,
                        success: function(data)
                        {
                            console.log(data);
                            if(data.status == 'success'){
                                location.href='{{route('project-management.hosting-system.hosting.index')}}';
                            } else{
                                for( var error in  data.messages){
                                    $('#'+error+'-error').html(data.messages[error]);
                                    $('#'+error).focus();
                                }
                            }
                        },
                        error:function( data){
                            console.log(data);
                        }
                    });
                }
            });
        })
    </script>

@endpush