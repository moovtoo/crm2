@extends('layouts.app')
@section('content')
    <form id="main-form" enctype="multipart/form-data">

        @csrf

        <div id="1" class="tab-content" style="display:block;">
            <div class="row">
                <div class="container" style="margin-left: 0;">
                    <div class="col-md-11">
                        @if(isset($lead->name))
                            <div class="form-group form-group-show">
                                <label for="name" class="control-label">Lead Name: </label>
                                <p>{{$lead->name}}</p>
                            </div>
                            <hr>
                        @endif

                        @if(isset($lead->user))
                            <div class="form-group form-group-show">
                                <label for="user_id" class="control-label">Assigned to: </label>
                                <p>{{$lead->user->name}}</p>
                            </div>
                            <hr>
                        @endif
                            @if(isset($lead->organization))
                                <div class="form-group form-group-show">
                                    <label for="organization_id" class="control-label">Organization: </label>
                                    <p>{{$lead->organization->name}}</p>
                                </div>
                                <hr>
                            @endif
                            @if(isset($lead->origin))
                                <div class="form-group form-group-show">
                                    <label for="origin_id" class="control-label">Origin: </label>
                                    <p>{{$lead->origin->name}}</p>
                                </div>
                                <hr>
                            @endif
                            {{--<label for="contact" class="control-label">Contacts: </label>--}}
                            {{--@foreach($lead->contacts as $contact)--}}
                                {{--<div class="form-group form-group-show">--}}
                                    {{--<a href="{{route('organizations-management.contact-system.contact.index')}}/{{$contact->id}}" target="_blank">{{$contact->name}}</a>--}}
                                {{--</div>--}}
                            {{--@endforeach--}}
                            {{--<hr>--}}
                        @if(isset($lead->created_date))
                            <div class="form-group form-group-show">
                                <label for="created_date" class="control-label">Created Since: </label>
                                <p>{{$lead->created_date}}</p>
                            </div>
                            <hr>
                        @endif
                            @if(isset($lead->status))
                                <div class="form-group form-group-show">
                                    <label for="status_id" class="control-label">Status: </label>
                                    <p>{{$lead->status->name}}</p>
                                </div>
                                <hr>
                            @endif

                        @if(isset($lead->description))
                            <div class="form-group form-group-show">
                                <label for="description" class="control-label">Description: </label>
                                <p>{!!$lead->description!!}</p>
                            </div>
                            <hr>
                        @endif
                    </div>
                </div>
            </div>
        </div>

        @include('layouts.partials.form-action-buttons',['submit'=>false,'reset'=>false, 'edit'=>true])
    </form>

@endsection

@push('css')
    <link href="http://cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/a549aa8780dbda16f6cff545aeabc3d71073911e/build/css/bootstrap-datetimepicker.css" rel="stylesheet"/>
    <link href="{{asset('css/colorbox.css')}}" rel="stylesheet">
@endpush
@push('js')

    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>
    <script src="{{asset('js/datetimepicker.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/jquery.colorbox-min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/standalonepopup.min.js')}}"></script>

    <script src="{{asset('packages/tinymce/tinymce.min.js')}}"></script>
    <script>
        $(function(){
            tinymce.init({
                selector: "textarea",
                plugins: [
                    "advlist autolink lists link image charmap print preview anchor",
                    "searchreplace visualblocks code fullscreen",
                    "insertdatetime media table paste"
                ],
                toolbar: " undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
                file_browser_callback : elFinderBrowser,
                urlconverter_callback: myCustomURLConverter

            });

            function myCustomURLConverter(url, node, on_save, name) {
                // Do some custom URL conversion

                // Return new URL
                return url;
            }

            function elFinderBrowser (field_name, url, type, win) {
                var elfinder_url = '{{route('elfinder.tinymce4')}}';    // use an absolute path!
                tinyMCE.activeEditor.windowManager.open({
                    file: elfinder_url,
                    title: 'Media Library',
                    width: 900,
                    height: 550,
                    resizable: 'yes',
                    //inline: 'yes',
                    popup_css: false,
                    close_previous: 'no'
                }, {
                    setUrl: function (url) {
                        win.document.getElementById(field_name).value = url;
                    },
                    window: win,
                    input: field_name
                });
                return false;
            }

        });
    </script>
    <script>
        ;( function ( document, window, index )
        {
            var inputs = document.querySelectorAll( '.inputfile' );
            Array.prototype.forEach.call( inputs, function( input )
            {
                var label	 = input.nextElementSibling,
                    labelVal = label.innerHTML;

                input.addEventListener( 'change', function( e )
                {
                    var fileName = '';
                    if( this.files && this.files.length > 1 )
                        fileName = ( this.getAttribute( 'data-multiple-caption' ) || '' ).replace( '{count}', this.files.length );
                    else
                        fileName = e.target.value.split( '\\' ).pop();

                    if( fileName )
                        label.querySelector( 'span' ).innerHTML = fileName;
                    else
                        label.innerHTML = labelVal;
                });

                // Firefox bug fix
                input.addEventListener( 'focus', function(){ input.classList.add( 'has-focus' ); });
                input.addEventListener( 'blur', function(){ input.classList.remove( 'has-focus' ); });
            });
        }( document, window, 0 ));
    </script>

    <script>
        $('#form-edit').on('click',function(){
            location.href='{{route('project-management.lead-system.lead.edit',$lead->id)}}';
        })
        $('#form-back').on('click',function () {
            location.href='{{route('project-management.lead-system.lead.index')}}';
        })
    </script>

@endpush