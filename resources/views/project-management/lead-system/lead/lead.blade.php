@extends('layouts.app')
@php($slug = 'lead')
@php($module = \App\Models\Module::query()->where('slug',$slug)->get()->first())
@if( $module != null )
    @php($name = '\App\Models\\'.str_replace('Controller','',$module->controller_name))
    @php($model = new $name())
    @php($fields = $model->fields())
@endif
@section('content')

            <form id="main-form" enctype="multipart/form-data">
                @csrf
                <div id="1" class="tab-content" style="display:block;">
                    <div class="row">
                        <div class="container">
                            <div class="col-md-11">
                                <div class="form-group">
                                    <label for="name" class="control-label">Name<span class="star-required">*</span></label>
                                    <input type="text" class="form-control" id="name" name="name" >
                                    <label id="name-error" class="control-label error"></label>
                                </div>
                                <div class="form-group">
                                    <label for="user_id" class="control-label">Assigned To </label>
                                    <select type="text" class="form-control select2" id="user_id" name="user_id">
                                        {{--<option value="">Select User</option>--}}
                                        @foreach($users as $user)
                                            <option value="{{$user->id}}" @if (auth()->user()->id == $user->id) selected @endif >{{$user->name}}</option>
                                        @endforeach
                                    </select>
                                    <label id="user_id-error" class="control-label error"></label>
                                </div>


                                <div class="form-group">
                                    <label for="organization_id" class="control-label">Organization</label>
                                    <select type="text" class="form-control select2" id="organization_id" name="organization_id">
                                        <option value="">Select Organization</option>
                                        @foreach($organizations as $organization)
                                            <option value="{{$organization->id}}" >{{$organization->name}}</option>
                                        @endforeach
                                    </select>
                                    <label id="organization_id-error" class="control-label error"></label>
                                </div>
                                <div class="form-group">
                                    <label for="origin_id" class="control-label">Origin</label>
                                    <select class="form-control select2" id="origin_id" name="origin_id">
                                        <option value="">Select Origin</option>
                                        @foreach($origins as $origin)
                                            <option value="{{$origin->id}}">{{$origin->name}}</option>
                                        @endforeach
                                    </select>
                                    <label id="origin_id-error" class="control-label error"></label>
                                </div>

                                <div class="form-group">
                                    <label for="contact_id" class="control-label">Contact</label>
                                    <select type="text" class="form-control select2" id="contact_id" name="contact_id">
                                        <option value="">Select Contact</option>
                                        @foreach($contacts as $contact)
                                            <option value="{{$contact->id}}" >{{$contact->name}}</option>
                                        @endforeach
                                    </select>
                                    <label id="contact_id-error" class="control-label error"></label>
                                </div>

                                <div class="form-group">
                                    <label for="created_date" class="control-label">Created Date</label>
                                    <div class="date-box">
                                        <div class="date-icon-box" >
                                            <span class="fa fa-calendar" id="created_date_icon"></span>
                                        </div>
                                        <input type="date" class="form-control" id="created_date" name="created_date"  data-date-format="DD-MM-YYYY">
                                    </div>
                                    <label id="created_date-error" class="control-label error"></label>
                                </div>


                                <div class="form-group">
                                    <label for="status_id" class="control-label">Project Status</label>
                                    <select type="text" class="form-control select2" id="status_id" name="status_id">
                                        <option value="">Select Status</option>

                                        @foreach($statuses as $status)
                                            <option value="{{$status->id}}" >{{$status->name}}</option>
                                        @endforeach
                                    </select>
                                    <label id="status_id-error" class="control-label error"></label>
                                </div>




                                <div class="form-group">
                                    <label for="description" class="control-label">Description</label>
                                    <textarea class="form-control" id="description" name="description"></textarea>
                                    <label id="description-error" class="control-label error"></label>
                                </div>



                            </div>

                        </div>
                    </div>
                </div>
                @include('layouts.partials.form-action-buttons',['submit'=>true,'reset'=>true])
            </form>
@endsection

@push('css')
    <link href="{{asset('css/colorbox.css')}}" rel="stylesheet">
@endpush
@push('js')

    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.3/moment.min.js"></script>
    <script src="{{asset('packages/ckeditor/ckeditor.js')}}"></script>
    <script>
        CKEDITOR.replace( 'description' );
    </script>
    <script type="text/javascript" src="{{asset('js/jquery.colorbox-min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/standalonepopup.min.js')}}"></script>

    <script>
        $(function(){

            $("input").on("change", function() {
                this.setAttribute(
                    "data-date",
                    moment(this.value, "YYYY-MM-DD")
                        .format( this.getAttribute("data-date-format") )
                )
            }).trigger("change");



        });
    </script>

    <script type="application/javascript">
        function openTab(tabId) {
            var i, tabcontent;
            tabcontent = document.getElementsByClassName("tab-content");
            for (i = 0; i < tabcontent.length; i++) {
                tabcontent[i].style.display = "none";
            }
            var tabs = document.getElementsByClassName("tab-links");
            for (i = 0; i < tabs.length; i++) {
                tabs[i].className = tabs[i].className.replace(" active", "");
            }
            document.getElementById(tabId).style.display = "block";
            document.getElementById('tab-'+tabId).className += " active";
        }
        // openTab(this, 1);
    </script>
    <script>
        $(function () {
            $('.select2').select2({
                width:'100%'
            });

            $('#form-back').on('click',function () {
                location.href='{{route('project-management.lead-system.lead.index')}}';
            })
            $('#form-reset').on('click',function () {
                $('#main-form')[0].reset();
                $(".select2").val(null);
                $(".select2").trigger('change');
            })
            $('#form-submit').on('click',function () {
                var count = 0;
                @foreach($fields['store'] as $field)
                if($('#{{$field}}').val() == ''){
                    $('#{{$field}}-error').html('This field is required');
                    count++;
                }else{
                    $('#{{$field}}-error').html('');
                }
                @endforeach

                if(count == 0){
                    // tinyMCE.triggerSave();
                    var content = CKEDITOR.instances.description.getData();

                    var data = new FormData();
                    //Form data
                    var form_data = $('#main-form').serializeArray();
                    $.each(form_data, function (key, input) {
                        data.append(input.name, input.value);

                    });
                    data.append('_token', '{{csrf_token()}}');
                    data.append('description', content);

                    $.ajax({
                        type: "POST",
                        processData: false,
                        contentType: false,
                        url: '{{route('project-management.lead-system.lead.store')}}',
                        data: data,
                        success: function(data)
                        {
                            console.log(data);
                            if(data.status == 'success'){
                                location.href='{{route('project-management.lead-system.lead.index')}}'+'/'+data.lead.id+'/edit#leadAction';
                            } else{
                                for( var error in  data.messages){
                                    $('#'+error+'-error').html(data.messages[error]);
                                    $('#'+error).focus();
                                }
                            }
                        },
                        error:function( data){
                            console.log(data);
                        }
                    });
                }
            });
        })
    </script>

@endpush