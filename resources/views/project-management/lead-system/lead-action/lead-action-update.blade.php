@extends('layouts.app')

@section('content')
        <form id="main-form">
                <input type="hidden" name="_method" value="put">
                @csrf
                <div id="1" class="tab-content tab-content-profile" style="display:block;">
                    <div class="row">
                        <div class="container">
                            <div class="col-md-11">
                                {{--<div class="form-group">--}}
                                    {{--<label for="user_id" class="control-label">Assigned To </label>--}}
                                    {{--<select type="text" class="form-control select2" id="user_id" name="user_id">--}}
                                        {{--@foreach($users as $user)--}}
                                            {{--<option value="{{$user->id}} "@if($user->id == $leadAction->user_id) selected @endif >{{$user->name}}</option>--}}
                                        {{--@endforeach--}}
                                    {{--</select>--}}
                                    {{--<label id="user_id-error" class="control-label error"></label>--}}
                                {{--</div>--}}

                                <div class="form-group">
                                    <label for="contact_id" class="control-label">Contact</label>
                                    <select type="text" class="form-control select2" id="contact_id" name="contact_id">
                                        @foreach($contacts as $contact)
                                            <option value="{{$contact->id}}" @if($contact->id == $leadAction->contact_id) selected @endif  >{{$contact->name}}</option>
                                        @endforeach
                                    </select>
                                    <label id="contact_id-error" class="control-label error"></label>
                                </div>


                                <div class="form-group">
                                    <label for="created_date" class="control-label">Created Date</label>
                                    <div class="date-box">
                                        <div class="date-icon-box" >
                                            <span class="fa fa-calendar" id="created_date_icon"></span>
                                        </div>
                                        <input type="date" class="form-control" id="created_date" name="created_date"  data-date-format="DD-MM-YYYY" value="{{$leadAction->created_date}}">
                                    </div>
                                    <label id="created_date-error" class="control-label error"></label>
                                </div>

                                <div class="form-group">
                                    <label for="status_id" class="control-label">Status</label>
                                    <select type="text" class="form-control select2" id="status_id" name="status_id">
                                        @foreach($statuses as $status)
                                            <option value="{{$status->id}}" @if($status->id == $leadAction->status_id) selected @endif>{{$status->name}}</option>
                                        @endforeach
                                    </select>
                                    <label id="status_id-error" class="control-label error"></label>
                                </div>
                                {{--<div class="form-group">--}}
                                    {{--<label for="lead_id" class="control-label">Belongs to</label>--}}
                                    {{--<select type="text" class="form-control select2" id="lead_id" name="lead_id">--}}
                                        {{--@foreach($leads as $lead)--}}
                                            {{--<option value="{{$lead->id}}" @if($lead->id == $leadAction->lead_id) selected @endif >{{$lead->name}}</option>--}}
                                        {{--@endforeach--}}
                                    {{--</select>--}}
                                    {{--<label id="lead_id-error" class="control-label error"></label>--}}
                                {{--</div>--}}

                                <div class="form-group">
                                    <label for="type_id" class="control-label">Type</label>
                                    <select type="text" class="form-control select2" id="type_id" name="type_id">
                                        <option value="">Select Contact Type</option>
                                        @foreach($contactTypes as $contactType)
                                            <option value="{{$contactType->id}}" @if($contactType->id == $leadAction->type_id) selected @endif>{{$contactType->name}}</option>
                                        @endforeach
                                    </select>
                                    <label id="type_id-error" class="control-label error"></label>
                                </div>




                                <div class="form-group">
                                    <label for="note" class="control-label">Note</label>
                                    <textarea class="form-control" id="note" name="note" >{!! $leadAction->note !!} </textarea>
                                    <label id="note-error" class="control-label error"></label>
                                </div>

                                <div class="form-group">
                                    <label for="attachment" class="control-label">Attachment</label>
                                    <div class="box image-box" >
                                        <a href="" class="popup_selector" data-inputid="attachment">Browse Attachment</a>
                                        <input type="text" id="attachment" name="attachment" ">
                                    </div>
                                   {{--@if(isset($leadAction->attachment) and ($leadAction->attachment != null))--}}
                                        {{--<a type="text" href="{{asset($leadAction->attachment)}}" target="_blank" >{{asset($leadAction->attachment)}}</a>--}}
                                   {{--@endif--}}
                                    <label id="attachment-error" class="control-label error"></label>
                                {{--</div>--}}

                                {{--<div class="form-group">--}}
                                    <label for="att" class="control-label"></label>
                                    @if(isset($leadAction->attachment) and ($leadAction->attachment != null))
                                        <input type="hidden" id="video" name="video" value="{{asset($leadAction->attachment)}}" >
                                        <a type="text" class="form-control" id="att" name="att" href="{{asset($leadAction->attachment)}}">{{asset($leadAction->attachment)}}</a>
                                    @endif
                                    <label id="att-error" class="control-label error"></label>
                                </div>



                            </div>

                        </div>
                    </div>
                </div>
            @include('layouts.partials.form-action-buttons',['submit'=>true,'back'=>true])
        </form>
@endsection

@push('css')
    <link href="{{asset('css/colorbox.css')}}" rel="stylesheet">
@endpush
@push('js')

    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.3/moment.min.js"></script>
    <script src="{{asset('packages/ckeditor/ckeditor.js')}}"></script>
    <script>
        CKEDITOR.replace( 'note' );
    </script>
    <script type="text/javascript" src="{{asset('js/jquery.colorbox-min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/standalonepopup.min.js')}}"></script>

    <script>
        $(function(){

            $("input").on("change", function() {
                this.setAttribute(
                    "data-date",
                    moment(this.value, "YYYY-MM-DD")
                        .format( this.getAttribute("data-date-format") )
                )
            }).trigger("change");



        });
    </script>

    <script>
        $(function () {
            $('.select2').select2({
                width: '100%'
            });
        })
        $('#form-back').click(function () {
            location.href='{{route('project-management.lead-system.lead.index')}}' + '/' + '{{$leadAction->lead_id}}' +'/edit'
        });
        $('#form-reset').on('click',function () {
            $('#main-form')[0].reset();
        })
        $('#form-submit').click(function () {
            var counter = 0;
            @foreach($fields['update'] as $k=>$field)

            if( $('#{{$field}}').val() =='' ){
                $('#{{$field}}-error').text('This field is required');
                counter++;
            }
            @endforeach
            if ( counter > 0 ){
                return;
            }
            // tinyMCE.triggerSave();

            var content = CKEDITOR.instances.note.getData();
            var data = new FormData();
            //Form data
            var form_data = $('#main-form').serializeArray();
            $.each(form_data, function (key, input) {
                data.append(input.name, input.value);
            });
            data.append('_token', '{{csrf_token()}}');
            data.append('note', content);
            $.ajax({
                type: "POST",
                processData: false,
                contentType: false,
                url: '{{route('lead-action.update', $leadAction->id)}}',
                data: data,
                success: function(data)
                {
                    console.log(data);
                    if(data != null){
                        location.href='{{route('project-management.lead-system.lead.index')}}/{{$leadAction->lead_id}}/edit#leadAction';
                    }
                },
                error:function( data){
                    console.log(data);
                }
            });

        });
    </script>
@endpush
