@extends('layouts.app')
@php($slug = 'organization')
@php($module = \App\Models\Module::query()->where('slug',$slug)->get()->first())
@if( $module != null )
    @php($name = '\App\Models\\'.str_replace('Controller','',$module->controller_name))
    @php($model = new $name())
    @php($fields = $model->fields())
@endif
@section('content')
    <form id="main-form" enctype="multipart/form-data">
        <div class="tab">
            <div class="tab-links active" id="tab-1" onclick="openTab(1)">Basic Info</div>
            <div class="tab-links" id="tab-2" onclick="openTab(2)">Link and Phone</div>
        </div>
        <input type="hidden" name="_method" value="put">
        @csrf
        <div id="1" class="tab-content" style="display:block;">
                    <div class="row">
                        <div class="container">
                            <div class="col-md-11">
                                <div class="form-group">
                                    <label for="name" class="control-label">Name<span class="star-required">*</span></label>
                                    <input type="text" class="form-control" id="name" name="name" value="{{$organization->name}}">
                                    <label id="name-error" class="control-label error"></label>
                                </div>
                                <div class="form-group">
                                    <label for="user_id" class="control-label">Assigned to</label>
                                    <select type="text" class="form-control select2" id="user_id" name="user_id">
                                        <option value="">Select User</option>
                                        @foreach($users as $user)
                                            <option value="{{$user->id}}" @if($user->id == $organization->user_id) selected @endif>{{$user->name}}</option>
                                        @endforeach
                                    </select>
                                    <label id="user_id-error" class="control-label error"></label>
                                </div>
                                <div class="form-group">
                                    <label for="sector_id" class="control-label">Sector</label>
                                    <select type="text" class="form-control select2" id="sector_id" name="sector_id">
                                        <option value="">Select Sector</option>
                                        @foreach($sectors as $sector)
                                            <option value="{{$sector->id}}" @if($sector->id == $organization->sector_id) selected @endif>{{$sector->name}}</option>
                                        @endforeach
                                    </select>
                                    <label id="sector_id-error" class="control-label error"></label>
                                </div>
                                <div class="form-group">
                                    <label for="connected_since" class="control-label">Connected since</label>
                                    <div class="date-box">
                                        <div class="date-icon-box" >
                                            <span class="fa fa-calendar" id="date_updated_icon"></span>
                                        </div>
                                        <input type="date" class="form-control" id="connected_since" name="connected_since"  data-date=""   data-date-format="DD-MM-YYYY" value="{{$organization->connected_since}}">
                                    </div>
                                    <label id="connected_since-error" class="control-label error"></label>
                                </div>

                                <div class="form-group">
                                    <label for="address" class="control-label">Address</label>
                                    <textarea class="form-control" id="address" name="address" >{{$organization->address}}</textarea>
                                    <label id="address-error" class="control-label error"></label>
                                </div>

                                <div class="form-group">
                                    <label for="city" class="control-label">City</label>
                                    <input type="text" class="form-control" id="city" name="city" value="{{$organization->city}}">
                                    <label id="city-error" class="control-label error"></label>
                                </div>
                                
                                <div class="form-group">
                                    <label for="zip_code" class="control-label">Zip code</label>
                                    <input type="text" class="form-control" id="zip_code" name="zip_code" value="{{$organization->zip_code}}">
                                    <label id="zip_code-error" class="control-label error"></label>
                                </div>

                                <div class="form-group">
                                    <label for="country_id" class="control-label">Country</label>
                                    <select type="text" class="form-control select2" id="country_id" name="country_id" >
                                        <option value="">Select Country</option>
                                        @foreach($countries as $country)
                                            <option value="{{$country->id}}" @if($organization->country_id == $country->id) selected @endif>{{$country->name}}</option>
                                        @endforeach
                                    </select>
                                    <label id="country_id-error" class="control-label error"></label>
                                </div>

                                <div class="form-group">
                                    <label for="description" class="control-label">Description</label>
                                    <textarea class="form-control" id="description" name="description" >{{$organization->description}}</textarea>
                                    <label id="description-error" class="control-label error"></label>
                                </div>

                                <div class="form-group">
                                    <label for="image" class="control-label">Profile Image</label>
                                    @if(isset($organization->image))
                                        <div class="image-thumb">
                                            <img src="{{asset($organization->image)}}" />
                                            <a href="{{route('organizations-management.organization-system.organization.removeImage',$organization->id)}}" class="image-fav "><img src="{{asset("images/deletemodifypic.png")}}"> </a>
                                        </div>
                                    @endif
                                    <div class="box image-box" >
                                        <a href="" class="popup_selector" data-inputid="image">Browse Image</a>
                                        <input type="text" id="image" name="image" value="">
                                    </div>
                                    <label id="image-error" class="control-label error"></label>
                                </div>

                                <div class="form-group">
                                    <label for="company_image" class="control-label">Company Image</label>
                                    @if(isset($organization->company_image))
                                        <div class="image-thumb">
                                            <img src="{{asset($organization->company_image)}}" />
                                            <a href="{{route('organizations-management.organization-system.organization.removeImage',$organization->id)}}" class="image-fav "><img src="{{asset("images/deletemodifypic.png")}}"> </a>
                                        </div>
                                    @endif
                                    <div class="box image-box" >
                                        <a href="" class="popup_selector" data-inputid="company_image">Browse Image</a>
                                        <input type="text" id="company_image" name="company_image" value="">
                                    </div>
                                    <label id="company_image-error" class="control-label error"></label>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
        <div id="2" class="tab-content" >
            <div class="row">
                <div class="container">
                    <div class="col-md-11">
                        <div class="form-group">
                            <label for="type_id" class="control-label">Phone Number</label>
                            <div class="contact-phones-div">
                                @foreach ($organizationPhones as $phone)
                                    <div class="contact-phone-select">
                                        <select class="form-control select2" id="type_id" name="type_id[]">
                                            <option value="1">Mobile</option>
                                        @foreach($contactTypes as $contactType)
                                                <option value="{{$contactType->id}}" @if($phone->type_id == $contactType->id) selected @endif>{{$contactType->name}}</option>
                                            @endforeach
                                        </select>
                                        <input type="text" class="form-control" id="phone" name="phone[]" value="{{$phone->phone}}">
                                        <div class="add-btn-plus" id="delete-phone" onclick="$(this).parent().remove();">-</div>
                                    </div>
                                @endforeach
                            </div>
                            <div class="add-phone-contant" id="add-phone-btn">
                                <div class="add-btn-plus">+</div><span class="black">Add Phone Number</span>
                            </div>
                            <label id="type_id-error" class="control-label error"></label>
                        </div>
                        <div class="form-group form-bottom top-40">
                            <label for="link" class="control-label">Link</label>
                            <input type="text" class="form-control" id="link" name="link" placeholder="Link" value="{{$organization->link}}">
                            <label id="link-error" class="control-label error"></label>
                        </div>


                </div>
            </div>
        </div>
        </div>
                @include('layouts.partials.form-action-buttons',['submit'=>true,'reset'=>true])
            </form>
@endsection

@push('css')
     <link href="{{asset('css/colorbox.css')}}" rel="stylesheet">
@endpush
@push('js')

    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.3/moment.min.js"></script>
    <script src="{{asset('packages/ckeditor/ckeditor.js')}}"></script>
    <script>
        CKEDITOR.replace( 'description' );
    </script>
    <script type="text/javascript" src="{{asset('js/jquery.colorbox-min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/standalonepopup.min.js')}}"></script>

    <script>
        $(function(){

            $("input").on("change", function() {
                this.setAttribute(
                    "data-date",
                    moment(this.value, "YYYY-MM-DD")
                        .format( this.getAttribute("data-date-format") )
                )
            }).trigger("change");



        });
    </script>

    <script type="application/javascript">
        function openTab(tabId) {
            var i, tabcontent;
            tabcontent = document.getElementsByClassName("tab-content");
            for (i = 0; i < tabcontent.length; i++) {
                tabcontent[i].style.display = "none";
            }
            var tabs = document.getElementsByClassName("tab-links");
            for (i = 0; i < tabs.length; i++) {
                tabs[i].className = tabs[i].className.replace(" active", "");
            }
            document.getElementById(tabId).style.display = "block";
            document.getElementById('tab-'+tabId).className += " active";
        }
        // openTab(this, 1);
    </script>
    <script>
        window.num = 1;
        $(function () {
            $('#add-phone-btn').on('click', function(){
                $('.contact-phones-div').append('<div class="contact-phone-select">'
                    +'<select class="form-control select2" id="select2_'+window.num+'" name="type_id[]">'
                    +'<option value="">Select Type</option>'
                        @foreach($contactTypes as $contactType)
                    +'<option value="'+'{{$contactType->id}}'+'">'+'{{$contactType->name}}'+'</option>'
                        @endforeach
                    +'</select>'
                    +'<input type="text" class="form-control" id="phone" name="phone[]">'
                    +'<div class="add-btn-plus" id="delete-phone" onclick="$(this).parent().remove();">-</div>'
                    +'</div>'
                )
                $('#select2_'+window.num).select2({
                    width:'100%'
                });
                window.num++;
            });

            $('.select2').select2({
                width:'100%'
            });

            $('#form-back').on('click',function () {
                location.href='{{route('organizations-management.organization-system.organization.index')}}';
            })
            $('#form-reset').on('click',function () {
               $('#main-form')[0].reset();
               $(".select2").val(null);
               $(".select2").trigger('change');
            })
            $('#form-submit').on('click',function () {
                var count = 0;
                @foreach($fields['update'] as $field)
                    if($('#{{$field}}').val() == ''){
                        $('#{{$field}}-error').html('This field is required');
                        count++;
                    }else{
                        $('#{{$field}}-error').html('');
                    }
                @endforeach

                if(count == 0){
                    var data = new FormData();
                    var content = CKEDITOR.instances.description.getData();

                    //Form data
                    var form_data = $('#main-form').serializeArray();
                    $.each(form_data, function (key, input) {
                        data.append(input.name, input.value);
                    });
                    data.append('_token', '{{csrf_token()}}');
                    data.append('description', content);

                    $.ajax({
                        type: "POST",
                        processData: false,
                        contentType: false,
                        url: '{{route('organizations-management.organization-system.organization.update', $organization->id)}}',
                        data: data,
                        success: function(data)
                        {
                            console.log(data);
                            if(data.status == 'success'){
                              location.href='{{route('organizations-management.organization-system.organization.index')}}';
                            } else{
                                for( var error in  data.messages){
                                    $('#'+error+'-error').html(data.messages[error]);
                                    $('#'+error).focus();
                                }
                            }
                        },
                        error:function( data){
                            console.log(data);
                        }
                    });
                }
            });
        })
    </script>

@endpush