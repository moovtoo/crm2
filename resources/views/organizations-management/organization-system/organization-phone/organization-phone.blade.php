@extends('layouts.app')
@php($slug = 'organization-phone')
@php($module = \App\Models\Module::query()->where('slug',$slug)->get()->first())
@if( $module != null )
    @php($name = '\App\Models\\'.str_replace('Controller','',$module->controller_name))
    @php($model = new $name())
    @php($fields = $model->fields())
@endif
@section('content')

            <form id="main-form" enctype="multipart/form-data">
                @csrf
                <div id="1" class="tab-content" style="display:block;">
                    <div class="row">
                        <div class="container">
                            <div class="col-md-11">
                                <div class="form-group">
                                    <label for="phone" class="control-label">Phone</label>
                                    <input type="text" class="form-control" id="phone" name="phone" >
                                    <label id="phone-error" class="control-label error"></label>
                                </div>
                                <div class="form-group">
                                    <label for="type_id" class="control-label">Type<span class="star-required">*</span></label>
                                    <select type="text" class="form-control select2" id="type_id" name="type_id">
                                        <option value="">Select Type</option>
                                        @foreach($contactTypes as $contactType)
                                            <option value="{{$contactType->id}}">{{$contactType->name}}</option>
                                        @endforeach
                                    </select>
                                    <label id="type_id-error" class="control-label error"></label>
                                </div>
                                
                                <div class="form-group">
                                    <label for="link" class="control-label">Link<span class="star-required">*</span></label>
                                    <input type="text" class="form-control" id="link" name="link" >
                                    <label id="link-error" class="control-label error"></label>
                                </div>

                                <div class="form-group">
                                    <label for="organization_id" class="control-label">Organization<span class="star-required">*</span></label>
                                    <select type="text" class="form-control select2" id="organization_id" name="organization_id" >
                                        <option value="">Select Organization</option>
                                        @foreach($organizations as $organization)
                                            <option value="{{$organization->id}}">{{$organization->name}}</option>
                                        @endforeach
                                    </select>
                                    <label id="organization_id-error" class="control-label error"></label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @include('layouts.partials.form-action-buttons',['submit'=>true,'reset'=>true])
            </form>
@endsection

@push('css')
     <link href="{{asset('css/colorbox.css')}}" rel="stylesheet">
@endpush
@push('js')

    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.3/moment.min.js"></script>
    <script src="{{asset('packages/ckeditor/ckeditor.js')}}"></script>
    <script>
        CKEDITOR.replace( 'notes' );
    </script>
    <script type="text/javascript" src="{{asset('js/jquery.colorbox-min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/standalonepopup.min.js')}}"></script>

    <script>
        $(function(){

            $("input").on("change", function() {
                this.setAttribute(
                    "data-date",
                    moment(this.value, "YYYY-MM-DD")
                        .format( this.getAttribute("data-date-format") )
                )
            }).trigger("change");



        });
    </script>

    <script type="application/javascript">
        function openTab(tabId) {
            var i, tabcontent;
            tabcontent = document.getElementsByClassName("tab-content");
            for (i = 0; i < tabcontent.length; i++) {
                tabcontent[i].style.display = "none";
            }
            var tabs = document.getElementsByClassName("tab-links");
            for (i = 0; i < tabs.length; i++) {
                tabs[i].className = tabs[i].className.replace(" active", "");
            }
            document.getElementById(tabId).style.display = "block";
            document.getElementById('tab-'+tabId).className += " active";
        }
        // openTab(this, 1);
    </script>
    <script>
        $(function () {
            $('.select2').select2({
                width:'100%'
            });

            $('#form-back').on('click',function () {
                location.href='{{route('organizations-management.organization-system.organization-phone.index')}}';
            })
            $('#form-reset').on('click',function () {
               $('#main-form')[0].reset();
               $(".select2").val(null);
               $(".select2").trigger('change');
            })
            $('#form-submit').on('click',function () {
                var count = 0;
                @foreach($fields['store'] as $field)
                    if($('#{{$field}}').val() == ''){
                        $('#{{$field}}-error').html('This field is required');
                        count++;
                    }else{
                        $('#{{$field}}-error').html('');
                    }
                @endforeach

                if(count == 0){
                    var data = new FormData();
                    var content = CKEDITOR.instances.notes.getData();

                    //Form data
                    var form_data = $('#main-form').serializeArray();
                    $.each(form_data, function (key, input) {
                        data.append(input.name, input.value);
                    });
                    data.append('_token', '{{csrf_token()}}');
                    data.append('notes', content);

                    $.ajax({
                        type: "POST",
                        processData: false,
                        contentType: false,
                        url: '{{route('organizations-management.organization-system.organization-phone.store')}}',
                        data: data,
                        success: function(data)
                        {
                            console.log(data);
                            if(data.status == 'success'){
                              location.href='{{route('organizations-management.organization-system.organization-phone.index')}}';
                            } else{
                                for( var error in  data.messages){
                                    $('#'+error+'-error').html(data.messages[error]);
                                    $('#'+error).focus();
                                }
                            }
                        },
                        error:function( data){
                            console.log(data);
                        }
                    });
                }
            });
        })
    </script>

@endpush