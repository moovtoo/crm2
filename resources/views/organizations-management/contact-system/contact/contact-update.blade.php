@extends('layouts.app')
@php($slug = 'contact')
@php($module = \App\Models\Module::query()->where('slug',$slug)->get()->first())
@if( $module != null )
    @php($name = '\App\Models\\'.str_replace('Controller','',$module->controller_name))
    @php($model = new $name())
    @php($fields = $model->fields())
@endif
@section('content')


            <form id="main-form" enctype="multipart/form-data">
                <div class="tab">
                    <div class="tab-links active" id="tab-1" onclick="openTab(1)">Basic Info</div>
                    <div class="tab-links" id="tab-2" onclick="openTab(2)">Address and Phone</div>
                    <div class="tab-links" id="tab-3" onclick="openTab(3)">Web</div>
                </div>
                <input type="hidden" name="_method" value="put">
                @csrf
                <div id="1" class="tab-content" style="display:block;">
                    <div class="row">
                        <div class="container">
                            <div class="col-md-11">
                                <div class="form-group">
                                    <label for="name" class="control-label">Name<span class="star-required">*</span></label>
                                    <input type="text" class="form-control" id="name" name="name" value="{{$contact->name}}">
                                    <label id="name-error" class="control-label error"></label>
                                </div>
                                <div class="form-group">
                                    <label for="private_email" class="control-label">Private Email</label>
                                    <input type="email" class="form-control" id="private_email" name="private_email" value="{{$contact->private_email}}">
                                    <label id="private_email-error" class="control-label error"></label>
                                </div>
                                <div class="form-group">
                                    <label for="business_email" class="control-label">Business Email</label>
                                    <input type="email" class="form-control" id="business_email" name="business_email" value="{{$contact->business_email}}">
                                    <label id="business_email-error" class="control-label error"></label>
                                </div>
                                <div class="form-group">
                                    <label for="business_position" class="control-label">Business Position</label>
                                    <input type="text" class="form-control" id="business_position" name="business_position" value="{{$contact->business_position}}">
                                    <label id="business_position-error" class="control-label error"></label>
                                </div>
                                <div class="form-group">
                                    <label for="sector_id" class="control-label">Sector</label>
                                    <select type="text" class="form-control select2" id="sector_id" name="sector_id">
                                        <option value="">Select Sector</option>
                                        @foreach($sectors as $sector)
                                            <option value="{{$sector->id}}" @if($sector->id == $contact->sector_id) selected @endif>{{$sector->name}}</option>
                                        @endforeach
                                    </select>
                                    <label id="sector_id-error" class="control-label error"></label>
                                </div>
                                <div class="form-group">
                                    <label for="organization_id" class="control-label">Organization</label>
                                    <select type="text" class="form-control select2" id="organization_id" name="organization_id">
                                        <option value="">Select Organization</option>
                                        @foreach($organizations as $organization)
                                            <option value="{{$organization->id}}" @if($organization->id == $contact->organization_id) selected @endif>{{$organization->name}}</option>
                                        @endforeach
                                    </select>
                                    <label id="organization_id-error" class="control-label error"></label>
                                </div>
                                {{--<div class="form-group">--}}
                                    {{--<label for="country_id" class="control-label">Country</label>--}}
                                    {{--<select type="text" class="form-control select2" id="country_id" name="country_id" >--}}
                                        {{--<option value="">Select Country</option>--}}
                                        {{--@foreach($countries as $country)--}}
                                            {{--<option value="{{$country->id}}" @if($country->id == $contact->country_id) selected @endif>{{$country->name}}</option>--}}
                                        {{--@endforeach--}}
                                    {{--</select>--}}
                                    {{--<label id="country_id-error" class="control-label error"></label>--}}
                                {{--</div>--}}

                                <div class="form-group">
                                    <label for="notes" class="control-label">Notes</label>
                                    <textarea class="form-control" id="notes" name="notes" >{{$contact->notes}}</textarea>
                                    <label id="notes-error" class="control-label error"></label>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div id="2" class="tab-content" >
                    <div class="row">
                        <div class="container">
                            <div class="col-md-11">
                                <div class="form-group">
                                    <label for="type_id" class="control-label">Phone Number</label>
                                    <div class="contact-phones-div">
                                        @foreach ($contactPhones as $phone)
                                            <div class="contact-phone-select">
                                                <select class="form-control select2" id="type_id" name="type_id[]">
                                                    <option value="">Select Type</option>
                                                    @foreach($contactTypes as $contactType)
                                                        <option value="{{$contactType->id}}" @if($phone->type_id == $contactType->id) selected @endif>{{$contactType->name}}</option>
                                                    @endforeach
                                                </select>
                                                <input type="text" class="form-control" id="phone" name="phone[]" value="{{$phone->phone}}">
                                                <div class="add-btn-plus" id="delete-phone" onclick="$(this).parent().remove();">-</div>
                                            </div>
                                        @endforeach
                                    </div>
                                    <div class="add-phone-contant" id="add-phone-btn">
                                        <div class="add-btn-plus">+</div><span class="black">Add Phone Number</span>
                                    </div>
                                    <label id="type_id-error" class="control-label error"></label>
                                </div>
                                <div class="form-group form-bottom top-40">
                                    <label for="address_1" class="control-label">Address</label>
                                    <input type="text" class="form-control" id="address_1" name="address_1" placeholder="Address 1">
                                    <label id="address_1-error" class="control-label error"></label>
                                </div>
                                <div class="form-group form-bottom">
                                    <input type="text" class="form-control" id="address_2" name="address_2" placeholder="Address 2">
                                    <label id="address_2-error" class="control-label error"></label>
                                </div>
                                <div class="form-group form-bottom">
                                    <input type="text" class="form-control" id="zip_code" name="zip_code" placeholder="Zip Code">
                                    <label id="zip_code-error" class="control-label error"></label>
                                </div>
                                <div class="form-group form-bottom">
                                    <select class="form-control select2" id="country_id" name="country_id" >
                                        <option value="">Select Country</option>
                                        @foreach($countries as $country)
                                            <option value="{{$country->id}}" @if($country->id == $contact->country_id) selected @endif>{{$country->name}}</option>
                                        @endforeach
                                    </select>
                                    <label id="country_id-error" class="control-label error"></label>
                                </div>
                                <div class="form-group form-bottom" >
                                    <input type="text" class="form-control" id="city" name="city" placeholder="City">
                                    <label id="city-error" class="control-label error"></label>
                                </div>
                                <div class="form-group form-bottom">
                                    <input type="text" class="form-control" id="state" name="state" placeholder="State">
                                    <label id="state-error" class="control-label error"></label>
                                </div>
                                <div class="form-group form-bottom">
                                    <input type="text" class="form-control" id="po_box" name="po_box" placeholder="PO Box">
                                    <label id="po_box-error" class="control-label error"></label>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div id="3" class="tab-content" >
                    <div class="row">
                        <div class="container">
                            <div class="col-md-11">
                                <div class="form-group">
                                    <label for="website" class="control-label">Website</label>
                                    <input type="text" class="form-control" id="website" name="website"  value="{{$contact->website }}">
                                    <label id="website-error" class="control-label error"></label>
                                </div>
                                <div class="form-group">
                                    <label for="instagram" class="control-label">Instagram</label>
                                    <input type="text" class="form-control" id="instagram" name="instagram"  value="{{ $contact->instagram }}">
                                    <label id="instagram-error" class="control-label error"></label>
                                </div>
                                <div class="form-group">
                                    <label for="facebook" class="control-label">Facebook</label>
                                    <input type="text" class="form-control" id="facebook" name="facebook"  value="{{ $contact->facebook }}">
                                    <label id="facebook-error" class="control-label error"></label>
                                </div>
                                <div class="form-group">
                                    <label for="twitter" class="control-label">Twitter</label>
                                    <input type="text" class="form-control" id="twitter" name="twitter"  value="{{ $contact->twitter }}">
                                    <label id="twitter-error" class="control-label error"></label>
                                </div>
                                <div class="form-group">
                                    <label for="linkedin" class="control-label">Linkedin</label>
                                    <input type="text" class="form-control" id="linkedin" name="linkedin"  value="{{ $contact->linkedin }}">
                                    <label id="linkedin-error" class="control-label error"></label>
                                </div>
                                <div class="form-group">
                                    <label for="pinterest" class="control-label">Pinterest</label>
                                    <input type="text" class="form-control" id="pinterest" name="pinterest"  value="{{$contact->pinterest}}">
                                    <label id="pinterest-error" class="control-label error"></label>
                                </div>



                            </div>

                        </div>
                    </div>
                </div>
                @include('layouts.partials.form-action-buttons',['submit'=>true,'reset'=>true])
            </form>

@endsection

@push('css')
     <link href="{{asset('css/colorbox.css')}}" rel="stylesheet">
@endpush
@push('js')

    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.3/moment.min.js"></script>
    <script src="{{asset('packages/ckeditor/ckeditor.js')}}"></script>

    <script type="text/javascript" src="{{asset('js/jquery.colorbox-min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/standalonepopup.min.js')}}"></script>

    <script>
        $(function(){

            $("input").on("change", function() {
                this.setAttribute(
                    "data-date",
                    moment(this.value, "YYYY-MM-DD")
                        .format( this.getAttribute("data-date-format") )
                )
            }).trigger("change");

            CKEDITOR.replace( 'notes' );


        });
    </script>

    <script type="application/javascript">
        function openTab(tabId) {
            var i, tabcontent;
            tabcontent = document.getElementsByClassName("tab-content");
            for (i = 0; i < tabcontent.length; i++) {
                tabcontent[i].style.display = "none";
            }
            var tabs = document.getElementsByClassName("tab-links");
            for (i = 0; i < tabs.length; i++) {
                tabs[i].className = tabs[i].className.replace(" active", "");
            }
            document.getElementById(tabId).style.display = "block";
            document.getElementById('tab-'+tabId).className += " active";
        }
        // openTab(this, 1);
    </script>
    <script>
        window.num = 1;
        $(function () {
            $('#add-phone-btn').on('click', function(){
                $('.contact-phones-div').append('<div class="contact-phone-select">'
                            +'<select class="form-control select2" id="select2_'+window.num+'" name="type_id[]">'
                            +'<option value="">Select Type</option>'
                            @foreach($contactTypes as $contactType)
                                +'<option value="'+'{{$contactType->id}}'+'">'+'{{$contactType->name}}'+'</option>'
                            @endforeach
                            +'</select>'
                            +'<input type="text" class="form-control" id="phone" name="phone[]">'
                            +'<div class="add-btn-plus" id="delete-phone" onclick="$(this).parent().remove();">-</div>'
                            +'</div>'
                        )
                $('#select2_'+window.num).select2({
                    width:'100%'
                });
                window.num++;
            });

            $('.select2').select2({
                width:'100%'
            });

            $('#form-back').on('click',function () {
                location.href='{{route('organizations-management.contact-system.contact.index')}}';
            })
            $('#form-reset').on('click',function () {
               $('#main-form')[0].reset();
               $(".select2").val(null);
               $(".select2").trigger('change');
            })
            $('#form-submit').on('click',function () {
                var count = 0;
                @foreach($fields['store'] as $field)
                    if($('#{{$field}}').val() == ''){
                        $('#{{$field}}-error').html('This field is required');
                        count++;
                    }else{
                        $('#{{$field}}-error').html('');
                    }
                @endforeach

                if(count == 0){
                    // tinyMCE.triggerSave();
                    var content = CKEDITOR.instances.notes.getData();

                    var data = new FormData();
                    //Form data
                    var form_data = $('#main-form').serializeArray();
                    $.each(form_data, function (key, input) {
                        data.append(input.name, input.value);
                    });
                    data.append('_token', '{{csrf_token()}}');
                    data.append('notes', content);

                    $.ajax({
                        type: "POST",
                        processData: false,
                        contentType: false,
                        url: '{{route('organizations-management.contact-system.contact.update', $contact->id)}}',
                        data: data,
                        success: function(data)
                        {
                            console.log(data);
                            if(data.status == 'success'){
                              location.href='{{route('organizations-management.contact-system.contact.index')}}';
                            } else{
                                for( var error in  data.messages){
                                    $('#'+error+'-error').html(data.messages[error]);
                                    $('#'+error).focus();
                                }
                            }
                        },
                        error:function( data){
                            console.log(data);
                        }
                    });
                }
            });
        })
    </script>

@endpush