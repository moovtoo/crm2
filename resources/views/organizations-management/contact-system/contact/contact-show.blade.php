@extends('layouts.app')
@section('content')
    <form id="main-form" enctype="multipart/form-data">

        @csrf

        <div id="1" class="tab-content" style="display:block;">
            <div class="row">
                <div class="container" style="margin-left: 0;">
                    <div class="col-md-11">
                        @if(isset($contact->name))
                        <div class="form-group form-group-show">
                            <label for="name" class="control-label">Contact Name: </label>
                            <p>{{$contact->name}}</p>
                        </div>
                        <hr>
                        @endif
                            @if(isset($contact->private_email))
                        <div class="form-group form-group-show">
                                <label for="private_email" class="control-label">Private Email: </label>
                                <p>{{$contact->private_email}}</p>
                        </div>
                        <hr>
                            @endif
                                @if(isset($contact->business_email))
                        <div class="form-group form-group-show">
                                <label for="business_email" class="control-label">Business Email: </label>
                                <p>{{$contact->business_email}}</p>
                        </div>
                        <hr>
                                @endif
                                    @if(isset($contact->business_position))
                        <div class="form-group form-group-show">
                                <label for="business_position" class="control-label">Business Position: </label>
                                <p>{{$contact->business_position}}</p>
                        </div>
                        <hr>
                                    @endif
                     @if(isset($contact->sector))
                            <div class="form-group form-group-show">
                                <label for="sector_id" class="control-label">Sector: </label>
                                <p>{{$contact->sector->name}}</p>
                            </div>
                            <hr>
                    @endif
                        @if(isset($contact->organization))
                        <div class="form-group form-group-show">
                                <label for="organization_id" class="control-label">Organization: </label>
                                <p>{{$contact->organization->name}}</p>
                        </div>
                        <hr>
                        @endif
                            @if(isset($contact->notes))
                        <div class="form-group form-group-show">
                                <label for="notes" class="control-label">Notes: </label>
                                <p>{!!  $contact->notes!!}</p>
                        </div>
                        <hr>
                            @endif

                        @foreach ($contactPhones as $phone)
                        <div class="form-group form-group-show">
                            @foreach($contactTypes as $contactType)
                                <label  @if($phone->type_id == $contactType->id)  >{{$contactType->name}}:</label>@endif
                            @endforeach
                                {{--<label for="phone" class="control-label">Phone: </label>--}}
                                <p>{{$phone->phone}}</p>
                        </div>
                        <hr>
                        @endforeach
                            @if(isset($contact->address_1))
                            <div class="form-group form-group-show">
                            <label for="address_1" class="control-label">Address: </label>
                            <p>{{$contact->address_1}}</p>
                        </div>
                        <hr>
                            @endif
                            @if(isset($contact->address_2))
                                <div class="form-group form-group-show">
                            <label for="address_2" class="control-label">Address-2: </label>
                            <p>{{$contact->address_2}}</p>
                        </div>
                        <hr>
                            @endif
                            @if(isset($contact->zip_code))
                                <div class="form-group form-group-show">
                            <label for="zip_code" class="control-label">Zip Code: </label>
                            <p>{{$contact->zip_code}}</p>
                        </div>
                        <hr>
                        {{--<div class="form-group form-group-show">--}}
                            {{--<label for="country_id" class="control-label">Country Name: </label>--}}
                            {{--<p>{{$contact->country->name}}</p>--}}
                        {{--</div>--}}
                        {{--<hr>--}}
                            @endif
                            @if(isset($contact->city))
                        <div class="form-group form-group-show">
                            <label for="city" class="control-label">City Name: </label>
                            <p>{{$contact->city}}</p>
                        </div>
                        <hr>
                            @endif
                            @if(isset($contact->state))
                        <div class="form-group form-group-show">
                            <label for="state" class="control-label">State Name: </label>
                            <p>{{$contact->state}}</p>
                        </div>
                        <hr>
                            @endif
                            @if(isset($contact->po_box))
                        <div class="form-group form-group-show">
                            <label for="po_box" class="control-label">po_box: </label>
                            <p>{{$contact->po_box}}</p>
                        </div>
                        <hr>
                            @endif
                            @if(isset($contact->website))
                        <div class="form-group form-group-show">
                            <label for="website" class="control-label">Website: </label>
                            <p>{{$contact->website}}</p>
                        </div>
                        <hr>
                            @endif
                            @if(isset($contact->instagram))
                        <div class="form-group form-group-show">
                            <label for="instagram" class="control-label">Instagram: </label>
                            <p>{{$contact->instagram}}</p>
                        </div>
                        <hr>
                            @endif
                            @if(isset($contact->facebook))
                        <div class="form-group form-group-show">
                            <label for="facebook" class="control-label">Facebook: </label>
                            <p>{{$contact->facebook}}</p>
                        </div>
                        <hr>
                            @endif
                            @if(isset($contact->twitter))
                        <div class="form-group form-group-show">
                            <label for="twitter" class="control-label">Twitter: </label>
                            <p>{{$contact->twitter}}</p>
                        </div>
                        <hr>
                            @endif
                            @if(isset($contact->linkedin))
                        <div class="form-group form-group-show">
                            <label for="linkedin" class="control-label">Linkedin: </label>
                            <p>{{$contact->linkedin}}</p>
                        </div>
                        <hr>
                            @endif
                            @if(isset($contact->pinterest))
                        <div class="form-group form-group-show">
                            <label for="pinterest" class="control-label">Pinterest: </label>
                            <p>{{$contact->pinterest}}</p>
                        </div>
                            @endif



                    </div>
                </div>
            </div>
        </div>

        @include('layouts.partials.form-action-buttons',['submit'=>false,'reset'=>false, 'edit'=>true])
    </form>

@endsection

@push('css')
    <link href="http://cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/a549aa8780dbda16f6cff545aeabc3d71073911e/build/css/bootstrap-datetimepicker.css" rel="stylesheet"/>
    <link href="{{asset('css/colorbox.css')}}" rel="stylesheet">
@endpush
@push('js')

    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>
    <script src="{{asset('js/datetimepicker.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/jquery.colorbox-min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/standalonepopup.min.js')}}"></script>

    <script src="{{asset('packages/tinymce/tinymce.min.js')}}"></script>
    <script>
        $(function(){
            tinymce.init({
                selector: "textarea",
                plugins: [
                    "advlist autolink lists link image charmap print preview anchor",
                    "searchreplace visualblocks code fullscreen",
                    "insertdatetime media table paste"
                ],
                toolbar: " undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
                file_browser_callback : elFinderBrowser,
                urlconverter_callback: myCustomURLConverter

            });

            function myCustomURLConverter(url, node, on_save, name) {
                // Do some custom URL conversion

                // Return new URL
                return url;
            }

            function elFinderBrowser (field_name, url, type, win) {
                var elfinder_url = '{{route('elfinder.tinymce4')}}';    // use an absolute path!
                tinyMCE.activeEditor.windowManager.open({
                    file: elfinder_url,
                    title: 'Media Library',
                    width: 900,
                    height: 550,
                    resizable: 'yes',
                    //inline: 'yes',
                    popup_css: false,
                    close_previous: 'no'
                }, {
                    setUrl: function (url) {
                        win.document.getElementById(field_name).value = url;
                    },
                    window: win,
                    input: field_name
                });
                return false;
            }

        });
    </script>
    <script>
        ;( function ( document, window, index )
        {
            var inputs = document.querySelectorAll( '.inputfile' );
            Array.prototype.forEach.call( inputs, function( input )
            {
                var label	 = input.nextElementSibling,
                    labelVal = label.innerHTML;

                input.addEventListener( 'change', function( e )
                {
                    var fileName = '';
                    if( this.files && this.files.length > 1 )
                        fileName = ( this.getAttribute( 'data-multiple-caption' ) || '' ).replace( '{count}', this.files.length );
                    else
                        fileName = e.target.value.split( '\\' ).pop();

                    if( fileName )
                        label.querySelector( 'span' ).innerHTML = fileName;
                    else
                        label.innerHTML = labelVal;
                });

                // Firefox bug fix
                input.addEventListener( 'focus', function(){ input.classList.add( 'has-focus' ); });
                input.addEventListener( 'blur', function(){ input.classList.remove( 'has-focus' ); });
            });
        }( document, window, 0 ));
    </script>

    <script>
        $('#form-edit').on('click',function(){
            location.href='{{route('organizations-management.contact-system.contact.edit',$contact->id)}}';
        })
        $('#form-back').on('click',function () {
            location.href='{{route('organizations-management.contact-system.contact.index')}}';
        })
    </script>

@endpush