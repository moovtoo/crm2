@extends('layouts.app')

@section('content')
            @include('layouts.partials.ajax-tables', ['route'=>Request::url(),'active'=>'contact-phones','type' => 'contact-phone'])
@endsection
