@extends('layouts.app')
@php($model = new \App\Models\Api())
@php($fields = $model->fields())
@section('content')

            <form id="main-form">
                @csrf
                <div id="1" class="tab-content tab-content-profile" style="display:block;">
                    <div class="row">
                        <div class="container">
                            <div class="col-md-11">
                                <div class="form-group">
                                    <label for="name" class="control-label">Name<span class="star-required">*</span></label>
                                    <input type="text" class="form-control" id="name" name="name"  >
                                    <label id="name-error" class="control-label error"></label>
                                </div>
                                <div class="form-group">
                                    <label for="module_id" class="control-label">Module</label>
                                    <select type="text" class="form-control select2" id="module_id" name="module_id" >
                                        <option value="" >Select Module</option>
                                    @foreach($modules as $module)
                                            <option value="{{$module->id}}"  >{{$module->name}}</option>
                                        @endforeach
                                    </select>
                                    <label id="module_id-error" class="control-label error"></label>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                @include('layouts.partials.form-action-buttons',['submit'=>true,'reset'=>true])
            </form>

@endsection

@push('css')

    <link href="http://cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/a549aa8780dbda16f6cff545aeabc3d71073911e/build/css/bootstrap-datetimepicker.css" rel="stylesheet"/>
    <link href="{{asset('css/colorbox.css')}}" rel="stylesheet">
@endpush
@push('js')


    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>

    <script type="text/javascript" src="{{asset('js/jquery.colorbox-min.js')}}"></script>

    <script>
        $(function () {
            $('.select2').select2({
                width:'100%'
            });


            $('#form-back').click(function () {
                location.href='{{route('status.index')}}'
            });
            $('#form-reset').on('click',function () {
                $('#main-form')[0].reset();
                $(".select2").val(null);
                $(".select2").trigger('change');
            })
            $('#form-submit').on('click',function () {
                var count = 0;
                @foreach($fields['store'] as $field)
                if($('#{{$field}}').val() == ''){
                    $('#{{$field}}-error').html('This field is required');
                    count++;
                }else{
                    $('#{{$field}}-error').html('');
                }
                @endforeach

                if(count == 0){
                    $.ajax({
                        type: "POST",
                        url: '{{route('status.store')}}',
                        data: $('#main-form').serialize(),
                        dataType: 'json',
                        success: function(data)
                        {
                            if(data.status == 'success'){
                                location.href='{{route('status.index')}}';
                            } else{
                                for( var error in  data.messages){
                                    $('#'+error+'-error').html(data.messages[error]);
                                }
                            }
                            console.log(data);
                            // if ( data != null )
                            //
                        },
                        error:function( data){
                            console.log(data);
                        }
                    });
                }
            });
        })
    </script>

@endpush