@extends('layouts.app')
@php($model = new \App\Models\Domain())
@php($fields = $model->fields())
@section('content')

            <form id="main-form">
                @csrf
                <div id="1" class="tab-content tab-content-profile" style="display:block;">
                    <div class="row">
                        <div class="container">
                            <div class="col-md-11">
                                <div class="form-group">
                                    <label for="name" class="control-label">{{__('cms.configuration.domains.domain.domain-name')}}<span class="star-required">*</span></label>
                                    <div style="display:flex;">
                                        <input type="text" class="form-control" id="name" name="name" >
                                        @if(app('request')->input('id'))
                                            <input type="text" class="form-control" value=".{{ App\Models\Domain::query()->where('id',app('request')->input('id'))->get()->first()->name}}" disabled>
                                        @endif
                                    </div>
                                    <label id="name-error" class="control-label error"></label>
                                </div>
                            </div>
                            @if(app('request')->input('id'))
                                <input name="parent_id" id="parent_id" value="{{app('request')->input('id')}}" hidden>
                                <input name="type" id="type" value="sub" hidden>
                            @else
                            <input name="type" id="type" value="domain" hidden>
                            @endif

                        </div>
                    </div>
                </div>
                @include('layouts.partials.form-action-buttons',['submit'=>true,'back'=>true])
            </form>
@endsection

@push('css')

@endpush
@push('js')

    <script>
        $(function () {
            $('.select2').select2({
                width: '100%'
            });
        })
        $('#form-back').click(function () {
            location.href='{{route('domain.index')}}'
        });
        $('#form-reset').on('click',function () {
            $('#main-form')[0].reset();
            $(".select2").val(null);
            $(".select2").trigger('change');
        })
        $('#form-submit').click(function () {
            console.log('clicked');
            var counter = 0;
            @foreach($fields['store'] as $k=>$field)

            if( $('#{{$field}}').val() =='' ){
                $('#{{$field}}-error').text('This field is required');
                counter++;
            }
            @endforeach
            if ( counter > 0 ){
                return;
            }

            $.ajax({
                type: "POST",
                url: '{{route('domain.store')}}',
                data: $('#main-form').serialize(),
                success: function(data)
                {
                    console.log(data);
                    if ( data != null )
                        location.href='{{route('domain.index')}}';
                },
                error:function( data){
                    console.log(data);
                }
            });

        });
    </script>
@endpush
