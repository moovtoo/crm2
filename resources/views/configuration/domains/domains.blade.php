@extends('layouts.app')

@section('content')
           @include('layouts.partials.ajax-tables', ['route'=>Request::url(),'active'=>'domains', 'type' => 'domain'])

@endsection
