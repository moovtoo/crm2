@extends('layouts.app')
@php($model = new \App\Models\Domain())
@php($fields = $model->fields())
@section('content')
            <form id="main-form">
                @csrf
                <div id="1" class="tab-content tab-content-profile" style="display:block;">
                    <div class="row">
                        <div class="container">
                            <div class="col-md-11">
                                <div class="form-group">
                                    <label for="name" class="control-label">{{__('cms.configuration.domains.domain-update.domain-name')}}<span class="star-required">*</span></label>
                                    <div style="display:flex;">
                                        <input type="text" class="form-control" id="name" name="name" value="{{$domain->name}}">
                                        @if($domain->type == "sub")
                                            <input type="text" class="form-control" value=".{{ $domain->parent->name}}" disabled>
                                        @endif
                                    </div>
                                    <label id="name-error" class="control-label error"></label>
                                </div>
                            </div>
                            @if($domain->type == "sub")
                                {{-- <div class="col-md-11">
                                    <div class="form-group"> --}}
                                        {{-- <label for="parent_id" class="control-label">Parent Domain</label> --}}
                                        <input name="parent_id" name="parent_id" value="{{$domain->parent_id}}" hidden>
                                        {{-- <select type="text" class="form-control select2" id="parent_id" name="parent_id" disabled hidden>
                                            <option value="">Select Domain</option>
                                            @if( isset($domains) and $domains->count() > 0 )
                                                @foreach($domains as $dom)
                                                    <option value="{{$dom->id}}" @if($domain->parent_id == $dom->id) selected @endif >{{$dom->name}}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                        <label id="parent_id-error" class="control-label error"></label> --}}
                                    {{-- </div>
                                </div> --}}
                            @endif
                            {{-- <div class="col-md-11">
                                <div class="form-group">
                                    <label for="type" class="control-label">Domain Type</label> --}}
                                    <input name="type" id="type" value="{{$domain->type}}" hidden>
                                    {{-- <select type="text" class="form-control select2" id="type" name="type" disabled hidden>
                                        <option value="">Select Type</option>
                                        <option value="domain" @if ( $domain->type == 'domain')selected @endif>Domain</option>
                                        <option value="sub" @if ( $domain->type == 'sub')selected @endif>Sub Domain</option>
                                    </select>
                                    <label id="type-error" class="control-label error"></label> --}}
                                {{-- </div>
                            </div> --}}
                        </div>
                    </div>
                </div>
                @include('layouts.partials.form-action-buttons',['update'=>true,'back'=>true, 'reset'=>true])
            </form>
@endsection

@push('css')

@endpush
@push('js')

    <script>
        $(function () {
            $('.select2').select2({
                width: '100%'
            });
        })
        $('#form-back').click(function () {
            location.href='{{route('domain.index')}}'
        });
        $('#form-reset').on('click',function () {
            $('#main-form')[0].reset();
            $("#parent_id").val({{$domain->parent_id}});
            $("#type").val('{{$domain->type}}');
            $(".select2").trigger('change');
        })
        $('#form-update').click(function () {
            console.log('clicked');
            var counter = 0;
            @foreach($fields['store'] as $k=>$field)

            if( $('#{{$field}}').val() =='' ){
                $('#{{$field}}-error').text('This field is required');
                counter++;
            }
            @endforeach
            if ( counter > 0 ){
                return;
            }

            $.ajax({
                type: "PUT",
                url: '{{route('domain.update',$domain->id)}}',
                data: $('#main-form').serialize(),
                success: function(data)
                {
                    console.log(data);
                    if ( data != null )
                        location.href='{{route('domain.index')}}';
                },
                error:function( data){
                    console.log(data);
                }
            });

        });
    </script>
@endpush
