@extends('layouts.app')
@section('content')

            <form id="main-form" enctype="multipart/form-data">

                @csrf

                <div id="1" class="tab-content" style="display:block;">
                    <div class="row">
                        <div class="container" style="margin-left: 0;">
                            <div class="col-md-11">
                                <div class="form-group form-group-show">
                                    <label for="name" class="control-label">{{__('cms.configuration.apis.api-show.name')}}</label>
                                    <p>{{$api->name}}</p>
                                </div>
                                <hr>
                                <div class="form-group form-group-show">
                                    <label for="custom_select" class="control-label">{{__('cms.configuration.apis.api-show.custom_select')}}</label>
                                    <p>{{$api->custom_select}}</p>
                                </div>
                                <hr>
                                <div class="form-group form-group-show">
                                    <label for="custom_condition" class="control-label">{{__('cms.configuration.apis.api-show.custom_condition')}}</label>
                                    <p>{{$api->custom_condition}}</p>
                                </div>
                                <hr>
                                <div class="form-group form-group-show">
                                    <label for="module_id" class="control-label">{{__('cms.configuration.apis.api-show.module_id')}}</label>
                                    <p>{{$api->module->name}}</p>
                                </div>
                                <hr>
                                <div class="form-group form-group-show">
                                    <label for="version" class="control-label">{{__('cms.configuration.apis.api-show.version')}}</label>
                                    <p>{{$api->version}}</p>
                                </div>
                                <hr>
                                <div class="form-group form-group-show">
                                    <label for="type" class="control-label">{{__('cms.configuration.apis.api-show.type')}}</label>
                                    <p>{{$api->type}}</p>
                                </div>
                                <hr>
                                <div class="form-group form-group-show">
                                    <label for="order_by" class="control-label">{{__('cms.configuration.apis.api-show.order_by')}}</label>
                                    <p>{{$api->order_by}}</p>
                                </div>
                                <hr>
                                <div class="form-group form-group-show">
                                    <label for="paginate" class="control-label">{{__('cms.configuration.apis.api-show.paginate')}}</label>
                                    <p>{{$api->paginate}}</p>
                                </div>
                                <hr>
                                <div class="form-group form-group-show">
                                    <label for="authenticated" class="control-label">{{__('cms.configuration.apis.api-show.authenticated')}}</label>
                                    <p>{{$api->authenticated}}</p>
                                </div>
                                <hr>

                            </div>
                        </div>
                    </div>
                </div>

                @include('layouts.partials.form-action-buttons',['submit'=>false,'reset'=>false, 'edit'=>true])
            </form>

@endsection

@push('css')
    <link href="http://cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/a549aa8780dbda16f6cff545aeabc3d71073911e/build/css/bootstrap-datetimepicker.css" rel="stylesheet"/>
    <link href="{{asset('css/colorbox.css')}}" rel="stylesheet">
@endpush
@push('js')

    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>
    <script src="{{asset('js/datetimepicker.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/jquery.colorbox-min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/standalonepopup.min.js')}}"></script>

    <script src="{{asset('packages/tinymce/tinymce.min.js')}}"></script>
    <script>
        $(function(){
            tinymce.init({
                selector: "textarea",
                plugins: [
                    "advlist autolink lists link image charmap print preview anchor",
                    "searchreplace visualblocks code fullscreen",
                    "insertdatetime media table paste"
                ],
                toolbar: " undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
                file_browser_callback : elFinderBrowser,
            });

            function elFinderBrowser (field_name, url, type, win) {
                var elfinder_url = '{{route('elfinder.tinymce4')}}';    // use an absolute path!
                tinyMCE.activeEditor.windowManager.open({
                    file: elfinder_url,
                    title: 'Media Library',
                    width: 900,
                    height: 550,
                    resizable: 'yes',
                    //inline: 'yes',
                    popup_css: false,
                    close_previous: 'no'
                }, {
                    setUrl: function (url) {
                        win.document.getElementById(field_name).value = url;
                    },
                    window: win,
                    input: field_name
                });
                return false;
            }

        });
    </script>
    <script>
        ;( function ( document, window, index )
        {
            var inputs = document.querySelectorAll( '.inputfile' );
            Array.prototype.forEach.call( inputs, function( input )
            {
                var label	 = input.nextElementSibling,
                    labelVal = label.innerHTML;

                input.addEventListener( 'change', function( e )
                {
                    var fileName = '';
                    if( this.files && this.files.length > 1 )
                        fileName = ( this.getAttribute( 'data-multiple-caption' ) || '' ).replace( '{count}', this.files.length );
                    else
                        fileName = e.target.value.split( '\\' ).pop();

                    if( fileName )
                        label.querySelector( 'span' ).innerHTML = fileName;
                    else
                        label.innerHTML = labelVal;
                });

                // Firefox bug fix
                input.addEventListener( 'focus', function(){ input.classList.add( 'has-focus' ); });
                input.addEventListener( 'blur', function(){ input.classList.remove( 'has-focus' ); });
            });
        }( document, window, 0 ));
    </script>

    <script>
        $('#form-edit').on('click',function(){
            location.href='{{route('api-generator.edit',$api->id)}}';
        })
        $('#form-back').on('click',function () {
            location.href='{{route('api-generator.index')}}';
        })
    </script>

@endpush