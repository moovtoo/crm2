@extends('layouts.app')
@php($model = new \App\Models\Api())
@php($fields = $model->fields())
@section('content')

            <form id="main-form">
                @csrf
                <div id="1" class="tab-content tab-content-profile" style="display:block;">
                    <div class="row">
                        <div class="container">
                            <div class="col-md-11">
                                <div class="form-group">
                                    <label for="name" class="control-label">{{__('cms.configuration.apis.api.name')}}<span class="star-required">*</span></label>
                                    <input type="text" class="form-control" id="name" name="name" >
                                    <label id="name-error" class="control-label error"></label>
                                </div>
                                <div class="form-group">
                                    <label for="custom_select" class="control-label">{{__('cms.configuration.apis.api.custom_select')}}</label>
                                    <textarea type="text" class="form-control editor-area" id="custom_select" name="custom_select" ></textarea>
                                    <label id="custom_select-error" class="control-label error"></label>
                                </div>
                                <div class="form-group">
                                    <label for="custom_condition" class="control-label">{{__('cms.configuration.apis.api.custom_condition')}}</label>
                                    <textarea type="text" class="form-control editor-area" id="custom_condition" name="custom_condition" ></textarea>
                                    <label id="custom_condition-error" class="control-label error"></label>
                                </div>
                                <div class="form-group">
                                    <label for="module_id" class="control-label">{{__('cms.configuration.apis.api.module')}}</label>
                                    <select type="text" class="form-control select2" id="module_id" name="module_id" >
                                        <option value="">Select Module</option>
                                        @foreach($modules as $module)
                                            <option value="{{$module->id}}">{{$module->name}}</option>
                                        @endforeach
                                    </select>
                                    <label id="module_id-error" class="control-label error"></label>
                                </div>
                                <div class="form-group">
                                    <label for="version" class="control-label">{{__('cms.configuration.apis.api.version')}}</label>
                                    <input type="text" class="form-control editor-area" id="version" value="v1" name="version" >
                                    <label id="version-error" class="control-label error"></label>
                                </div>

                                <div class="form-group">
                                    <label for="type" class="control-label">{{__('cms.configuration.apis.api.type')}}</label>
                                    <input type="text" class="form-control editor-area" id="type" name="type" value="GET" style='text-transform:uppercase' >
                                    <label id="type-error" class="control-label error"></label>
                                </div>
                                <div class="form-group">
                                    <label for="order_by" class="control-label">{{__('cms.configuration.apis.api.order_by')}}</label>
                                    <input type="text" class="form-control editor-area" id="order_by" value="order" name="order_by" >
                                    <label id="order_by-error" class="control-label error"></label>
                                </div>
                                <div class="form-group">
                                    <label for="paginate" class="control-label">{{__('cms.configuration.apis.api.paginate')}}</label>
                                    <input type="number" class="form-control editor-area" id="paginate" name="paginate" value="0" >
                                    <label id="paginate-error" class="control-label error"></label>
                                </div>

                                <div class="form-group">
                                    <label for="authenticated" class="control-label">{{__('cms.configuration.apis.api.authenticated')}}</label>
                                    <select type="text" class="form-control select2" id="authenticated" name="authenticated" >
                                        <option value="">Select Authenticated</option>
                                        <option value="1">{{__('cms.configuration.apis.api.yes')}}</option>
                                        <option value="0">{{__('cms.configuration.apis.api.no')}}</option>
                                    </select>
                                    <label id="authenticated-error" class="control-label error"></label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @include('layouts.partials.form-action-buttons',['submit'=>true,'back'=>true])
            </form>

@endsection

@push('css')

@endpush
@push('js')

    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.3/moment.min.js"></script>
    <script src="{{asset('packages/tinymce/tinymce.min.js')}}"></script>

    <script type="text/javascript" src="{{asset('js/jquery.colorbox-min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/standalonepopup.min.js')}}"></script>
    <script>
        $(function () {
            $('.select2').select2({
                width: '100%'
            });
        })
        $('#form-back').click(function () {
            location.href='{{route('api-generator.index')}}'
        });
        $('#form-reset').on('click',function () {
            $('#main-form')[0].reset();
            $(".select2").val(null);
            $(".select2").trigger('change');
        })
        $('#form-submit').click(function () {
            console.log('clicked');
            var counter = 0;
            @foreach($fields['store'] as $k=>$field)

            if( $('#{{$field}}').val() =='' ){
                $('#{{$field}}-error').text('This field is required');
                counter++;
            }
            @endforeach
            if ( counter > 0 ){
                return;
            }

            $.ajax({
                type: "POST",
                url: '{{route('api-generator.store')}}',
                data: $('#main-form').serialize(),
                success: function(data)
                {
                    console.log(data);
                    if ( data != null )
                        location.href='{{route('api-generator.index')}}';
                },
                error:function( data){
                    console.log(data);
                }
            });

        });
    </script>
@endpush
