@extends('layouts.app')
        @php($model = new \App\Models\Api())
        @php($fields = $model->fields())
        @section('content')


            <form id="main-form" enctype="multipart/form-data">
                <div class="tab">
                    <div class="tab-links active" id="tab-1" onclick="openTab(1)">API Properties</div>
                    <div class="tab-links" id="tab-2" onclick="openTab(2)">API Arguments</div>
                </div>
                <input type="hidden" name="_method" value="put">
                @csrf

            <div id="1" class="tab-content" style="display:block;">
                <div class="row">
                    <div class="container">
                        <div class="col-md-11">
                            <div class="form-group">
                                <label for="name" class="control-label">{{__('cms.configuration.apis.api.name')}}<span class="star-required">*</span></label>
                                <input type="text" class="form-control" id="name" name="name" value="{{$api->name}}">
                                <label id="name-error" class="control-label error"></label>
                            </div>
                            <div class="form-group">
                                <label for="custom_select" class="control-label">{{__('cms.configuration.apis.api.custom_select')}}</label>
                                <textarea type="text" class="form-control editor-area" id="custom_select" name="custom_select" value="{{$api->custom_select}}"></textarea>
                                <label id="custom_select-error" class="control-label error"></label>
                            </div>
                            <div class="form-group">
                                <label for="custom_condition" class="control-label">{{__('cms.configuration.apis.api.custom_condition')}}</label>
                                <textarea type="text" class="form-control editor-area" id="custom_condition" name="custom_condition" >{!! $api->custom_condition !!}</textarea>
                                <label id="custom_condition-error" class="control-label error"></label>
                            </div>
                            <div class="form-group">
                                <label for="module_id" class="control-label">{{__('cms.configuration.apis.api.module')}}</label>
                                <select type="text" class="form-control select2" id="module_id" name="module_id" >
                                    <option value="">Select Module</option>
                                    @foreach($modules as $module)
                                        <option value="{{$module->id}}" @if($api->module_id == $module->id) selected @endif>{{$module->name}}</option>
                                    @endforeach
                                </select>
                                <label id="module_id-error" class="control-label error"></label>
                            </div>
                            <div class="form-group">
                                <label for="version" class="control-label">{{__('cms.configuration.apis.api.version')}}</label>
                                <input type="text" class="form-control editor-area" id="version" value="{{$api->version}}" name="version" >
                                <label id="version-error" class="control-label error"></label>
                            </div>
                            <div class="form-group">
                                <label for="type" class="control-label">{{__('cms.configuration.apis.api.type')}}</label>
                                <input type="text" class="form-control editor-area" id="type" name="type" style='text-transform:uppercase' value="{{$api->type}}">
                                <label id="type-error" class="control-label error"></label>
                            </div>
                            <div class="form-group">
                                <label for="order_by" class="control-label">{{__('cms.configuration.apis.api.order_by')}}</label>
                                <input type="text" class="form-control editor-area" id="order_by" name="order_by" value="{{$api->order_by}}">
                                <label id="order_by-error" class="control-label error"></label>
                            </div>
                            <div class="form-group">
                                <label for="paginate" class="control-label">{{__('cms.configuration.apis.api.paginate')}}</label>
                                <input type="number" class="form-control editor-area" id="paginate" name="paginate" value="{{$api->paginate}}" >
                                <label id="paginate-error" class="control-label error"></label>
                            </div>
                            <div class="form-group">
                                <label for="authenticated" class="control-label">{{__('cms.configuration.apis.api.authenticated')}}</label>
                                <select type="text" class="form-control select2" id="authenticated" name="authenticated" >
                                    <option value="">Select Authenticated</option>
                                    <option value="1" @if($api->authenticated == 1) selected @endif>{{__('cms.configuration.apis.api.yes')}}</option>
                                    <option value="0" @if($api->authenticated == 0) selected @endif >{{__('cms.configuration.apis.api.no')}}</option>
                                </select>
                                <label id="authenticated-error" class="control-label error"></label>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <div id="2" class="tab-content" >
                <div class="row">
                    <div class="container">
                        <div class="col-md-11">
                            @include('layouts.partials.internal-ajax-table', ['route'=>route('api-argument.index'), 'params'=>'?api_id='.$api->id,'active'=>'api','type' => 'api', 'add_button'=>'ADD AN ARGUMENT','model'=>new \App\Models\ApiArgument()])
                        </div>
                    </div>
                </div>
            </div>

                @include('layouts.partials.form-action-buttons',['submit'=>true,'reset'=>true])
            </form>


@endsection

@push('css')
    <link href="http://cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/a549aa8780dbda16f6cff545aeabc3d71073911e/build/css/bootstrap-datetimepicker.css" rel="stylesheet"/>
    <link href="{{asset('css/colorbox.css')}}" rel="stylesheet">
@endpush
@push('js')

    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>
    <script src="{{asset('js/datetimepicker.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/jquery.colorbox-min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/standalonepopup.min.js')}}"></script>

    <script src="{{asset('packages/tinymce/tinymce.min.js')}}"></script>
    <script type="application/javascript">
        function openTab(tabId) {
            var i, tabcontent;
            tabcontent = document.getElementsByClassName("tab-content");
            for (i = 0; i < tabcontent.length; i++) {
                tabcontent[i].style.display = "none";
            }
            var tabs = document.getElementsByClassName("tab-links");
            for (i = 0; i < tabs.length; i++) {
                tabs[i].className = tabs[i].className.replace(" active", "");
            }
            document.getElementById(tabId).style.display = "block";
            document.getElementById('tab-'+tabId).className += " active";
        }
        // openTab(this, 1);
    </script>

    <script>
        $(function () {
            $('.select2').select2({
                width:'100%'
            });
            
            $('#form-back').on('click',function () {
                location.href='{{route('api-generator.index')}}';
            })
            $('#form-reset').on('click',function () {
                $('#main-form')[0].reset();
            })
            $('#form-submit').on('click',function () {
                var count = 0;
                @foreach($fields['update'] as $field)
                    if($('#{{$field}}').val() == ''){
                        $('#{{$field}}-error').html('This field is required');
                        count++;
                    }else{
                        $('#{{$field}}-error').html('');
                    }
                @endforeach

                @foreach($fields['update'] as $field)
                if($('#{{$field}}').val() == ''){
                    openTab($('#{{$field}}-error').parent().parent().parent().parent().parent()[0].id);
                    return;
                }
                @endforeach
                if(count == 0){
                    // tinyMCE.triggerSave();
                    var data = new FormData();
                    //Form data
                    var form_data = $('#main-form').serializeArray();
                    $.each(form_data, function (key, input) {
                        data.append(input.name, input.value);
                    });
                    data.append('_token', '{{csrf_token()}}');
                    $.ajax({
                        type: "POST",
                        url: '{{route('api-generator.update',$api->id)}}',
                        processData: false,
                        contentType: false,
                        data: data,
                        success: function(data)
                        {
                            console.log(data);
                            if(data.status == 'success'){
                              location.href='{{route('api-generator.index')}}';
                            } else{
                                for( var error in  data.messages){
                                    $('#'+error+'-error').html(data.messages[error]);
                                    openTab($('#'+error+'-error').parent().parent().parent().parent().parent()[0].id);
                                    $('#'+error).focus();
                                }
                            }
                        },
                        error:function( data){
                            console.log(data);
                        }
                    });
                }
            });
        })
    </script>

@endpush