@extends('layouts.app')

@section('content')
            <form id="main-form">
                <input type="hidden" name="_method" value="put">
                @csrf
                <div id="1" class="tab-content tab-content-profile" style="display:block;">
                    <div class="row">
                        <div class="container">
                            <div class="col-md-11">
                                <div class="form-group">
                                    <label for="key" class="control-label">Key<span class="star-required">*</span></label>
                                    <input type="text" class="form-control" id="key" name="key" value="{{$apiArgument->key}}">
                                    <label id="key-error" class="control-label error"></label>
                                </div>
                            </div>
                            <div class="col-md-11">
                                <div class="form-group">
                                    <label for="required" class="control-label">Required</label>
                                    <select type="text" class="form-control select2" id="required" name="required">
                                        <option value="">Select Required</option>
                                        <option value="1" @if($apiArgument->required == '1') selected @endif>Yes</option>
                                        <option value="0" @if($apiArgument->required == '0') selected @endif>No</option>
                                    </select>
                                    <label id="required-error" class="control-label error"></label>
                                </div>
                            </div>
                            <div class="col-md-11">
                                <div class="form-group">
                                    <label for="type" class="control-label">Type</label>
                                    <select type="text" class="form-control select2" id="type" name="type">
                                        <option value="">Select Required</option>
                                        <option value="or" @if($apiArgument->type == 'or') selected @endif>Or</option>
                                        <option value="and" @if($apiArgument->type == 'and') selected @endif>And</option>
                                    </select>
                                    <label id="type-error" class="control-label error"></label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @include('layouts.partials.form-action-buttons',['submit'=>true,'back'=>true])
            </form>

@endsection

@push('css')

@endpush
@push('js')

    <script>
        $(function () {
            $('.select2').select2({
                width: '100%'
            });
        })
        $('#form-back').click(function () {
            location.href='{{route('api-argument.index')}}'
        });
        $('#form-reset').on('click',function () {
            $('#main-form')[0].reset();
        })
        $('#form-submit').click(function () {
            var counter = 0;
            @foreach($fields['update'] as $k=>$field)

            if( $('#{{$field}}').val() =='' ){
                $('#{{$field}}-error').text('This field is required');
                counter++;
            }
            @endforeach
            if ( counter > 0 ){
                return;
            }

            $.ajax({
                type: "POST",
                url: '{{route('api-argument.update', $apiArgument->id)}}',
                data: $('#main-form').serialize(),
                success: function(data)
                {
                    console.log(data);
                    if(data != null){
                        location.href='{{route('api-generator.index')}}'+ '/' + data.apiArgument.api_id + '/edit';
                    }
                },
                error:function( data){
                    console.log(data);
                }
            });

        });
    </script>
@endpush
