@extends('layouts.app')

@section('content')

            <form id="main-form">
                @csrf
                <div id="1" class="tab-content tab-content-profile" style="display:block;">
                    <div class="row">
                        <div class="container">
                            <div class="col-md-11">
                                <div class="form-group">
                                    <label for="domain_name" class="control-label">{{__('cms.configuration.configurations.domain.domain-name')}}<span class="star-required">*</span></label>
                                    <input type="text" class="form-control" id="domain_name" name="domain_name" >
                                    <label id="domain_name-error" class="control-label error"></label>
                                </div>
                            </div>
                            <div class="col-md-11">
                                <div class="form-group">
                                    <label for="domain_name" class="control-label">{{__('cms.configuration.configurations.domain.domain-name')}}<span class="star-required">*</span></label>
                                    <input type="text" class="form-control" id="domain_name" name="domain_name" >
                                    <label id="domain_name-error" class="control-label error"></label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @include('layouts.partials.form-action-buttons',['submit'=>true,'back'=>false])
            </form>
@endsection

@push('css')

@endpush
@push('js')

@endpush
