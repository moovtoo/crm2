@extends('layouts.app')

@section('content')

            <form id="main-form">
                @csrf
                <div id="1" class="tab-content tab-content-profile" style="display:block;">
                    <div class="row">
                        <div class="container">
                            <div class="col-md-11">
                                <div class="form-group">
                                    <label for="name" class="control-label">Name<span class="star-required">*</span></label>
                                    <div style="display:flex;">
                                        <input type="text" class="form-control" name="name" id="name">
                                    </div>
                                    <label id="name-error" class="control-label error"></label>
                                </div>
                            </div>
                            <div class="col-md-11">
                                <div class="form-group">
                                    <label for="flag" class="control-label">Flag<span class="star-required">*</span></label>
                                    <div class="box image-box" >
                                        <a href="" class="popup_selector" data-inputid="flag">Browse Image</a>
                                        <input type="text" id="flag" name="flag" value="">
                                    </div>
                                    <label id="flag-error" class="control-label error"></label>
                                </div>
                            </div>
                            <div class="col-md-11">
                                <div class="form-group">
                                    <label for="code" class="control-label">Code<span class="star-required">*</span></label>
                                    <div style="display:flex;">
                                        <input type="text" class="form-control" name="code" id="code">
                                    </div>
                                    <label id="code-error" class="control-label error"></label>
                                </div>
                            </div>
                            <div class="col-md-11">
                                <div class="form-group">
                                    <label for="message" class="control-label">Message<span class="star-required">*</span></label>
                                    <div style="display:flex;">
                                        <input type="text" class="form-control" name="message" id="message">
                                    </div>
                                    <label id="message-error" class="control-label error"></label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @include('layouts.partials.form-action-buttons',['submit'=>true,'back'=>true])
            </form>

@endsection

@push('css')

    <link href="http://cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/a549aa8780dbda16f6cff545aeabc3d71073911e/build/css/bootstrap-datetimepicker.css" rel="stylesheet"/>
    <link href="{{asset('css/colorbox.css')}}" rel="stylesheet">
@endpush
@push('js')


    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>
    <script src="{{asset('js/datetimepicker.js')}}"></script>

    <script type="text/javascript" src="{{asset('js/jquery.colorbox-min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/standalonepopup.min.js')}}"></script>


    <script>
        $(function () {
            $('.select2').select2({
                width:'100%'
            });
            
            $('#form-back').on('click',function () {
                location.href='{{route('configuration.language')}}';
            })
            $('#form-reset').on('click',function () {
               $('#main-form')[0].reset();
            })
            $('#form-submit').on('click',function () {
                var count = 0;
                @foreach($fields['store'] as $field)
                    if($('#{{$field}}').val() == ''){
                        $('#{{$field}}-error').html('This field is required');
                        count++;
                    }else{
                        $('#{{$field}}-error').html('');
                    }
                @endforeach

                if(count == 0){
                    var data = new FormData();
                    //Form data
                    var form_data = $('#main-form').serializeArray();
                    $.each(form_data, function (key, input) {
                        data.append(input.name, input.value);
                    });
                    data.append('_token', '{{csrf_token()}}');
                    $.ajax({
                        type: "POST",
                        processData: false,
                        contentType: false,
                        url: '{{route('configuration.language.store')}}',
                        data: data,
                        success: function(data)
                        {
                            console.log(data);
                            if(data.status == 'success'){
                              location.href='{{route('configuration.language')}}';
                            } else{
                                for( var error in  data.messages){
                                    $('#'+error+'-error').html(data.messages[error]);
                                }
                            }
                        },
                        error:function( data){
                            console.log(data);
                        }
                    });
                }
            });
            
        })
    </script>
@endpush
