@extends('layouts.app')

@section('content')
            <form id="main-form">
                @csrf
                <div id="1" class="tab-content tab-content-profile" style="display:block;">
                    <div class="row">
                        <div class="container">
                            <div class="col-md-11">
                                @foreach ($locales as $locale)
                                    <div class="col-md-4 col-sm-6 col-xs-12">
                                        <div class="full-menu-box language-box @if($settings['language'] == $locale->code) active @endif">
                                            <div class="full-menu-box-image language ">
                                                <h4>{{ $locale->message }}</h4>
                                                <img class="language-flag" src="{{asset("$locale->flag")}}">
                                            </div>
                                            <div class="full-menu-box-link-box">
                                                <a class="full-menu-box-button-blue language-btn"  href="{{route('configuration.language.change', $locale->code)}}" >{{$locale->name}}</a>
                                            </div>
                                            <div class="active-check">
                                                <i class="fa fa-check"></i>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                    <div class="full-menu-box language-box">
                                        <div class="full-menu-box-image language">
                                            <h4>{{__('cms.configuration.configurations.language.add-new-locale')}}</h4>
                                        </div>
                                        <div class="full-menu-box-link-box">
                                            <a class="full-menu-box-button-blue language-btn"  href="{{route('configuration.language.create')}}" >Create</a>
                                        </div>
                                        <div class="active-check">
                                            <i class="fa fa-check"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>

@endsection

@push('css')

    <link href="http://cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/a549aa8780dbda16f6cff545aeabc3d71073911e/build/css/bootstrap-datetimepicker.css" rel="stylesheet"/>
@endpush
@push('js')


    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>
    <script src="{{asset('js/datetimepicker.js')}}"></script>


    <script>
        $(function(){
            $.FroalaEditor.DefineIcon('imageInfo', {NAME: 'info'});
            $.FroalaEditor.RegisterCommand('imageInfo', {
                title: 'Info',
                focus: false,
                undo: false,
                refreshAfterCallback: false,
                callback: function () {
                    var $img = this.image.get();
                    alert($img.attr('src'));
                }
            });

        });
    </script>
    <script>
        ;( function ( document, window, index )
        {
            var inputs = document.querySelectorAll( '.inputfile' );
            Array.prototype.forEach.call( inputs, function( input )
            {
                var label	 = input.nextElementSibling,
                    labelVal = label.innerHTML;

                input.addEventListener( 'change', function( e )
                {
                    var fileName = '';
                    if( this.files && this.files.length > 1 )
                        fileName = ( this.getAttribute( 'data-multiple-caption' ) || '' ).replace( '{count}', this.files.length );
                    else
                        fileName = e.target.value.split( '\\' ).pop();

                    if( fileName )
                        label.querySelector( 'span' ).innerHTML = fileName;
                    else
                        label.innerHTML = labelVal;
                });

                // Firefox bug fix
                input.addEventListener( 'focus', function(){ input.classList.add( 'has-focus' ); });
                input.addEventListener( 'blur', function(){ input.classList.remove( 'has-focus' ); });
            });
        }( document, window, 0 ));
    </script>
    <script>
        $(function () {
            $('.select2').select2({
                width:'100%'
            });
            
            $('#datetimepicker1').datetimepicker(); 

            
        })
    </script>
@endpush
