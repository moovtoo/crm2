@extends('layouts.app')

@section('content')

            <form id="main-form">
                @csrf
                <div id="1" class="tab-content tab-content-profile" style="display:block;">
                    <div class="row">
                        <div class="container">
                            <div class="col-md-11">
                                <div class="form-group">
                                    <label for="cms_title" class="control-label">{{__('cms.configuration.configurations.branding.cms-title')}}</label>
                                    <input type="text" class="form-control" id="cms_title" name="cms_title" @if(isset($settings['cms_title'])) value="{{$settings['cms_title']}}" @endif>
                                    <label id="cms_title-error" class="control-label error"></label>
                                </div>
                                <div class="form-group">
                                    <div>
                                        @if( isset($settings['logo']))
                                            <div class="image-thumb-user">
                                                <img id="logo-photo" src="{{asset($settings['logo'])}}" class="user-image user-image-hover">
                                                <a href="{{route('configuration.remove','logo')}}" class="image-fav-user"><img src="{{asset("images/delete.svg")}}"> </a>
                                            </div>
                                        @else
                                            <img id="logo-photo" src="{{asset('images/Avatar.svg')}}" class="user-image">
                                        @endif
                                    </div>
                                    {{--@if( isset($settings['logo']))--}}
                                        {{--<div class="image-thumb">--}}
                                            {{--<img src="{{asset($settings['logo'])}}" id="logo-photo"/>--}}
                                            {{--<a href="{{route('configuration.remove','logo')}}" class="image-fav x-delete"><i class="fa fa-remove"></i> </a>--}}
                                        {{--</div>--}}
                                    {{--@endif--}}
                                        <label for="logo" class="control-label">{{__('cms.configuration.configurations.branding.logo')}}</label>
                                        <div class="box image-box" >
                                        <a href="" class="popup_selector" data-inputid="logo">{{__('cms.configuration.configurations.branding.browse-image')}}</a>
                                        <input type="text" id="logo" name="logo" value="">
                                    </div>

                                </div>
                                <div class="form-group">
                                    @if( isset($settings['background']))
                                    <div class="image-thumb">
                                        <img src="{{asset($settings['background'])}}" id="background-photo"/>
                                        <a href="{{route('configuration.remove','background')}}" class="image-fav x-delete"><i class="fa fa-remove"></i> </a>
                                    </div>
                                    @else
                                        <img src="" id="background-photo" style="display:none"/>
                                    @endif
                                        <label for="background" class="control-label">{{__('cms.configuration.configurations.branding.background')}} <span class="size-required">(1600x800px)</span></label>
                                        <div class="box image-box" >
                                        <a href="" class="popup_selector" data-inputid="background">{{__('cms.configuration.configurations.branding.browse-image')}}</a>
                                        <input type="text" id="background" name="background" value="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    @if( isset($settings['favicon']))
                                        <div class="image-favicon">
                                            <img src="{{asset($settings['favicon'])}}" id="favicon-photo"/>
                                            <a href="{{route('configuration.remove','favicon')}}" class="image-fav x-delete"><i class="fa fa-remove"></i> </a>
                                        </div>
                                    @else
                                        <img src="" id="favicon-photo" style="display:none;"/>
                                    @endif
                                        <label for="favicon" class="control-label">{{__('cms.configuration.configurations.branding.favicon')}} <span class="size-required">(16x16px)</span></label>
                                        <div class="box image-box" >
                                        <a href="" class="popup_selector" data-inputid="favicon">{{__('cms.configuration.configurations.branding.browse-image')}}</a>
                                        <input type="text" id="favicon" name="favicon" value="">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @include('layouts.partials.form-action-buttons',['submit'=>true,'back'=>false])
            </form>

@endsection

@push('css')

    <link href="{{asset('css/colorbox.css')}}" rel="stylesheet">
@endpush
@push('js')

    <script type="text/javascript" src="{{asset('js/jquery.colorbox-min.js')}}"></script>
    {{-- <script type="text/javascript" src="{{asset('js/standalonepopup.min.js')}}"></script> --}}

    <script>
        function processSelectedFile(e,t){
            $("#"+t).val(e);
           document.getElementById(t+'-photo').src = e.replace('public','/public/storage');
           $('#'+t+'-photo').css('display','block');
        }
        $(document).on("click",".popup_selector",function(e){
            e.preventDefault();
            var t=$(this).attr("data-inputid");
            var n="/elfinder/popup/";
            var r=n+t;
            $.colorbox({
                href:r,
                fastIframe:true,
                iframe:true,
                width:"70%",
                height:"70%"
            })
        })
    
    </script>
    <script >
        $(function () {
            
            $('#form-back').click(function () {
                location.href='{{route('configuration')}}'
            });
            $('#form-reset').on('click',function () {
                $('#main-form')[0].reset();
            })
            $('#form-submit').click(function () {
                $.ajax({
                    url: "{{route('configuration.branding.store')}}",
                    type:'POST',
                    data: {
                        "_token": "{{ csrf_token() }}",
                        "logo": $('#logo').val(),
                        "background": $('#background').val(),
                        "favicon": $('#favicon').val(),
                        'cms_title': $('#cms_title').val(),
                    },
                    success: function(data){
                        // $('#main-view').html(data);
                        location.href='{{route('configuration.branding')}}';
                    },
                    error: function (data) {
                        console.log(data);
                    }
                });
            });

        })
    </script>

@endpush
