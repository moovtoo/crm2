@extends('layouts.app')

@section('content')

            <form id="main-form">
                @csrf
                <div id="1" class="tab-content tab-content-profile" style="display:block;">
                    <div class="row">
                        <div class="container">
                            <div class="col-md-11">
                                <div class="form-group row form-group-profile">

                                    <div class="col-md-2" >
                                        @if(isset($settings['profile']) and $settings['profile']!= null)
                                            <div class="image-thumb-user">
                                                <img id="display-photo" src="{{asset($settings['profile'])}}" class="user-image user-image-hover">
                                                <a href="{{route('configuration.remove','profile')}}" class="image-fav-user">
                                                    <img src="{{asset("images/delete.svg")}}"> </a>
                                            </div>
                                        @else
                                            <img id="display-photo" src="{{asset('images/Avatar.svg')}}" class="user-image cursor-pointer popup_selector " data-inputid="profile">
                                        @endif
                                    </div>
                                    <div class="col-md-10 display-flex">
                                        <div class="add-btn add-btn-profile pull-left" id="add-button">
                                            <a class="add-btn-plus popup_selector" data-inputid="profile">+</a>
                                            <input type="text" id="profile" name="profile" value="" hidden>
                                            <div class="add-btn-title popup_selector" data-inputid="profile">{{__('cms.configuration.configurations.super-user.add-modify-profile')}}</div>
                                        </div>
                                    </div>
                                    {{-- <label for="photo" class="control-label">Profile Picture<span class="star-required">*</span></label>
                                    <div class="box" >
                                        <input type="file" name="profile" id="profile" class="inputfile inputfile-6" data-multiple-caption="{count} files selected" multiple />
                                        <label for="photo"><strong>Browse Image&hellip;</strong><span></span> </label>
                                    </div> --}}
                                </div>
                                <div class="form-group">
                                    <label for="name" class="control-label">{{__('cms.configuration.configurations.super-user.name')}}<span class="star-required">*</span></label>
                                    <input type="text" class="form-control" id="name" name="name" @if(isset($settings['name'])) value="{{$settings['name']}}" @endif>
                                    <label id="name-error" class="control-label error"></label>
                                </div>
                                <div class="form-group">
                                    <label for="email" class="control-label">{{__('cms.configuration.configurations.super-user.email')}}<span class="star-required">*</span></label>
                                    <input type="email" class="form-control" id="email" name="email" @if(isset($settings['email'])) value="{{$settings['email']}}" @endif>
                                    <label id="email-error" class="control-label error"></label>
                                </div>
                                {{--<div class="form-group">--}}
                                    {{--<label for="password" class="control-label">Password<span class="star-required">*</span></label>--}}
                                    {{--<input type="password" class="form-control" id="password" name="password">--}}
                                    {{--<label id="password-error" class="control-label error"></label>--}}
                                {{--</div>--}}
                                <div class="form-group">
                                    <label for="old_password" class="control-label">{{__('cms.configuration.configurations.super-user.current-password')}}</label>
                                    <input type="password" class="form-control" id="old_password" name="old_password"  >
                                    <label id="old_password-error" class="control-label error"></label>
                                </div>
                                <div class="form-group">
                                    <label for="password" class="control-label">{{__('cms.configuration.configurations.super-user.new-password')}}</label>
                                    <input type="password" class="form-control" id="password" name="password"  >
                                    <label id="password-error" class="control-label error"></label>
                                </div>
                                <div class="form-group">
                                    <label for="password_confirmation" class="control-label">{{__('cms.configuration.configurations.super-user.confirm-password')}}</label>
                                    <input type="password" class="form-control" id="password_confirmation" name="password_confirmation"  >
                                    <label id="password_confirmation-error" class="control-label error"></label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @include('layouts.partials.form-action-buttons',['submit'=>true,'reset'=>true])
            </form>
@endsection

@push('css')

    <link href="http://cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/a549aa8780dbda16f6cff545aeabc3d71073911e/build/css/bootstrap-datetimepicker.css" rel="stylesheet"/>
    <link href="{{asset('css/colorbox.css')}}" rel="stylesheet">
@endpush
@push('js')


    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>
    <script src="{{asset('js/datetimepicker.js')}}"></script>

    <script type="text/javascript" src="{{asset('js/jquery.colorbox-min.js')}}"></script>
    <script>
            function processSelectedFile(e,t){
                $("#"+t).val(e);
                document.getElementById('display-photo').src = e.replace('public','/public/storage');
            }
            $(document).on("click",".popup_selector",function(e){
                e.preventDefault();
                var t=$(this).attr("data-inputid");
                var n="/elfinder/popup/";
                var r=n+t;
                $.colorbox({
                    href:r,
                    fastIframe:true,
                    iframe:true,
                    width:"70%",
                    height:"70%"
                })
            })
        
        </script>

    <script>
        $(function(){
            $.FroalaEditor.DefineIcon('imageInfo', {NAME: 'info'});
            $.FroalaEditor.RegisterCommand('imageInfo', {
                title: 'Info',
                focus: false,
                undo: false,
                refreshAfterCallback: false,
                callback: function () {
                    var $img = this.image.get();
                    alert($img.attr('src'));
                }
            });

        });
    </script>
    
    <script>
        ;( function ( document, window, index )
        {
            var inputs = document.querySelectorAll( '.inputfile' );
            Array.prototype.forEach.call( inputs, function( input )
            {
                var label	 = input.nextElementSibling,
                    labelVal = label.innerHTML;

                input.addEventListener( 'change', function( e )
                {
                    var fileName = '';
                    if( this.files && this.files.length > 1 )
                        fileName = ( this.getAttribute( 'data-multiple-caption' ) || '' ).replace( '{count}', this.files.length );
                    else
                        fileName = e.target.value.split( '\\' ).pop();

                    if( fileName )
                        label.querySelector( 'span' ).innerHTML = fileName;
                    else
                        label.innerHTML = labelVal;
                });

                // Firefox bug fix
                input.addEventListener( 'focus', function(){ input.classList.add( 'has-focus' ); });
                input.addEventListener( 'blur', function(){ input.classList.remove( 'has-focus' ); });
            });
        }( document, window, 0 ));
    </script>
    <script>
        $(function () {
            $('.select2').select2({
                width:'100%'
            });
            
            $('#datetimepicker1').datetimepicker(); 
            $('#form-back').click(function () {
                location.href='{{route('configuration.super-user')}}'
            });
            $('#form-reset').on('click',function () {
                $('#main-form')[0].reset();
            })
            $('#form-submit').click(function () {
                var data = new FormData();
                //Form data
                var form_data = $('#main-form').serializeArray();
                $.each(form_data, function (key, input) {
                    data.append(input.name, input.value);
                });
                // //File data
                // var file_data = $('input[name="logo"]')[0].files;
                // for (var i = 0; i < file_data.length; i++) {
                //     data.append("logo", file_data[i]);
                // }
                $.ajax({
                    url: "{{route('configuration.super-user.store')}}",
                    type:'POST',
                    data: data,
                    processData: false,
                    contentType: false,
                    success: function(data){
                            if(data.status == 'success'){
                                location.href='{{route('configuration.super-user')}}';
                            } else{
                                for( var error in  data.messages){
                                    $('#'+error+'-error').html(data.messages[error]);
                                }
                            }
//                         console.log(data);
                    },
                    error: function (data) {
                        console.log(data);
                    }
                });
            });
        })
    </script>
@endpush
