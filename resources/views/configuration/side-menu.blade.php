<div class="col-sm-4 col-md-3 col-lg-2 sidebar">
    <div class="mini-submenu">
        <i class="fa fa-angle-double-right"></i>
    </div>
    <span class="pull-right" id="slide-submenu">
        <i class="fa fa-angle-double-left"></i>
    </span>
    <div class="list-group">
        <div class="side-menu-user">
            <img src="@if(isset(Auth::user()->photo) and Auth::user()->photo != '') {{asset(Auth::user()->photo)}} @else {{asset('images/Avatar.svg')}} @endif" class="img-responsive header-user-photo">
            <a href="{{route ('profile')}}">
                Hello {{ Auth::user()->name }}
            </a>
        </div>
        <span href="#" class="list-group-item main-title">
            {{-- <span class="pull-left">
                <img src="{{asset('images/content-management.png')}}"></i>
            </span> --}}
            <a href="javascript:void(0)" class="title" id="configurations">
                CMS Configuration
            </a>
        </span>
        <div id="list-configurations" @if(str_contains(Request::path(), 'cms/configuration')) style="display: block" @endif>
            <div>
                <a href="{{route('configuration.branding')}}" class="list-group-item @if(Request::is('cms/configuration/branding')) active @endif">
                    Branding Management
                </a>
            </div>
            {{--<div>--}}
                {{--<a href="{{route('configuration.super-user')}}" class="list-group-item @if(Request::is('cms/configuration/super-user')) active @endif">--}}
                    {{--Super User Management--}}
                {{--</a>--}}
            {{--</div>--}}
            <div>
                <a href="{{route('configuration.language')}}" class="list-group-item @if(Request::is('cms/configuration/language')) active @endif">
                    CMS Language
                </a>
            </div>
            <div>
                <a href="{{route('domain.index')}}" class="list-group-item @if(Request::is('cms/configuration/domain*')) active @endif">
                    Domains
                </a>
            </div>
            <div>
                <a href="{{route('api-generator.index')}}" class="list-group-item @if(Request::is('cms/configuration/api-generator')) active @endif">
                    API Generator
                </a>
            </div>
            <div>
                <a href="{{route('status.index')}}" class="list-group-item @if(Request::is('cms/configuration/status')) active @endif">
                    Statuses
                </a>
            </div>
            <div>
                <a href="javascript:void(0)" class="list-group-item @if(Request::is('cms/configuration/logs')) active @endif">
                    Logs & Notifications management
                </a>
            </div>
            <div>
                <a href="javascript:void(0)" class="list-group-item @if(Request::is('cms/configuration/emails')) active @endif">
                    Emails management
                </a>
            </div>
            <div>
                <a href="javascript:void(0)" class="list-group-item @if(Request::is('cms/configuration/servers')) active @endif">
                    Servers
                </a>
            </div>
        </div>
    </div>
</div>