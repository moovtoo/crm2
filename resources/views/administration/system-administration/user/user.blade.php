@extends('layouts.app')
@php($slug = 'user')
@php($module = \App\Models\Module::query()->where('slug',$slug)->get()->first())
@if( $module != null )
    @php($name = '\App\Models\\'.str_replace('Controller','',$module->controller_name))
    @php($model = new $name())
    @php($fields = $model->fields())
@endif
@section('content')

            <form id="main-form">
                @csrf
                <div id="1" class="tab-content tab-content-profile" style="display:block;">
                    <div class="row">
                        <div class="container">
                            <div class="col-md-11">
                                <div class="form-group row form-group-profile">
                                    <div class="col-md-2">
                                        <div class="image-thumb-user">
                                            <img id="photo-display" src="{{asset("images/Avatar.svg")}}" class="user-image user-image-hover">
                                        </div>
                                    </div>
                                    <div class="col-md-10 display-flex">
                                        <div class="add-btn add-btn-profile pull-left" id="add-button">
                                            <a class="add-btn-plus popup_selector" data-inputid="photo">+</a>
                                            <input type="text" id="photo" name="photo" value="" hidden>
                                            <div class="add-btn-title popup_selector" data-inputid="photo" >{{__('cms.administration.system-administration.users.user.add_modify_profile_image')}}</div>
                                        </div>
                                    </div>
                                    {{-- <label for="photo" class="control-label">Profile Picture<span class="star-required">*</span></label>
                                    <div class="box" >
                                        <input type="file" name="profile" id="profile" class="inputfile inputfile-6" data-multiple-caption="{count} files selected" multiple />
                                        <label for="photo"><strong>Browse Image&hellip;</strong><span></span> </label>
                                    </div> --}}
                                </div>
                                <div class="form-group">
                                    <label for="name" class="control-label">{{__('cms.administration.system-administration.users.user.full-name')}}<span class="star-required">*</span></label>
                                    <input type="text" class="form-control" id="name" name="name"  >
                                    <label id="name-error" class="control-label error"></label>
                                </div>
                                <div class="form-group">
                                    <label for="email" class="control-label">{{__('cms.administration.system-administration.users.user.email')}}<span class="star-required">*</span></label>
                                    <input type="email" class="form-control" id="email" name="email" >
                                    <label id="email-error" class="control-label error"></label>
                                </div>
                                <div class="form-group">
                                    <label for="password" class="control-label">{{__('cms.administration.system-administration.users.user.password')}}<span class="star-required">*</span></label>
                                    <input type="password" class="form-control" id="password" name="password">
                                    <label id="password-error" class="control-label error"></label>
                                </div>
                                <div class="form-group">
                                    <label for="status" class="control-label">{{__('cms.administration.system-administration.users.user.status')}}<span class="star-required">*</span></label>
                                    <select type="text" class="form-control select2" id="status" name="status" >
                                        <option value="">Select Status</option>
                                        <option value="active">Active</option>
                                        <option value="inactive">Inactive</option>
                                    </select>
                                    <label id="status-error" class="control-label error"></label>
                                </div>
                                <div class="form-group">
                                    <label for="role_id" class="control-label">{{__('cms.administration.system-administration.users.user.role')}}<span class="star-required">*</span></label>
                                    <select type="text" class="form-control select2" id="role_id" name="roles[]" multiple>
                                        @foreach($roles as $role)
                                            <option value="{{$role->id}}">{{$role->name}}</option>
                                        @endforeach
                                    </select>
                                    <label id="role_id-error" class="control-label error"></label>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                @include('layouts.partials.form-action-buttons',['submit'=>true,'reset'=>true])
            </form>


@endsection

@push('css')

    <link href="http://cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/a549aa8780dbda16f6cff545aeabc3d71073911e/build/css/bootstrap-datetimepicker.css" rel="stylesheet"/>
    <link href="{{asset('css/colorbox.css')}}" rel="stylesheet">
@endpush
@push('js')


    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>
    <script src="{{asset('js/datetimepicker.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/jquery.colorbox-min.js')}}"></script>
    <script>
        function processSelectedFile(e,t){
            $("#"+t).val(e);
           document.getElementById('photo-display').src = e.replace('public','/public/storage');
           $('.image-thumb-user').append('<a href="javascript:void(0)" onclick="removeImage()" class="image-fav-user"><img src="{{asset("images/delete.svg")}}"> </a>');
        }
        $(document).on("click",".popup_selector",function(e){
            e.preventDefault();
            var t=$(this).attr("data-inputid");
            var n="/elfinder/popup/";
            var r=n+t;
            $.colorbox({
                href:r,
                fastIframe:true,
                iframe:true,
                width:"70%",
                height:"70%"
            })
        })

        function removeImage(){
            $("#photo").val('');
            document.getElementById('photo-display').src = "{{asset('images/Avatar.svg')}}";
            $('.image-fav-user').remove();

        }
    </script>

    <script>
        $(function () {
            $('.select2').select2({
                width:'100%'
            });

            $('#datetimepicker1').datetimepicker();
            
            $('#form-back').click(function () {
                location.href='{{route('administration.system-administration.user.index')}}'
            });
            $('#form-reset').on('click',function () {
                $('#main-form')[0].reset();
                $(".select2").val(null);
                $(".select2").trigger('change');
            })
            $('#form-submit').on('click',function () {
                var count = 0;
                @foreach($fields['store'] as $field)
                if($('#{{$field}}').val() == ''){
                    $('#{{$field}}-error').html('This field is required');
                    count++;
                }else{
                    $('#{{$field}}-error').html('');
                }
                @endforeach

                if(count == 0){
                    $.ajax({
                        type: "POST",
                        url: '{{route('administration.system-administration.user.store')}}',
                        data: $('#main-form').serialize(),
                        dataType: 'json',
                        success: function(data)
                        {
                          if(data.status == 'success'){
                              location.href='{{route('administration.system-administration.user.index')}}';
                          } else{
                              for( var error in  data.messages){
                                  $('#'+error+'-error').html(data.messages[error]);
                              }
                          }
                            console.log(data);
                           // if ( data != null )
                            //
                        },
                        error:function( data){
                            console.log(data);
                        }
                    });
                }
            });
        })
    </script>

@endpush