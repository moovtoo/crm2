@extends('layouts.app')

@section('content')
            @include('layouts.partials.ajax-tables', ['route'=>Request::url(),'active'=>'users', 'type'=>'user'])

@endsection
