@extends('layouts.app')
@php($slug = 'user')
@php($module = \App\Models\Module::query()->where('slug',$slug)->get()->first())
@if( $module != null )
@php($name = '\App\Models\\'.str_replace('Controller','',$module->controller_name))
@php($model = new $name())
@php($fields = $model->fields())
@endif
@section('content')

            <form id="main-form" enctype="multipart/form-data">
                <div class="tab">
                    <div class="tab-links active" id="tab-1" onclick="openTab(1)">{{__('cms.administration.system-administration.users.user-update.user-basic-info')}}</div>
                    <div class="tab-links" id="tab-2" onclick="openTab(2)">{{__('cms.administration.system-administration.users.user-update.user-properties')}}</div>
                    {{--<button class="tab-links" onclick="openTab(this, 3)">Article SEO</button>--}}
                </div>
                <input type="hidden" name="_method" value="put">
                @csrf

                <div id="1" class="tab-content" style="display:block;">
                    <div class="row">
                        <div class="container">
                            <div class="col-md-11">
                                <div class="form-group row form-group-profile">
                                    <div class="col-md-2" >
                                        @if(isset($editUser->photo) and $editUser->photo != null)
                                            <div class="image-thumb-user">
                                                <img id="photo-display" src="{{asset("$editUser->photo")}}" class="user-image user-image-hover">
                                                <a href="{{route('administration.system-administration.user.removeImage',$editUser->id)}}" class="image-fav-user"><img src="{{asset("images/delete.svg")}}"> </a>
                                            </div>
                                        @else
                                            <img id="photo-display" src="{{asset('images/Avatar.svg')}}" class="user-image">
                                        @endif
                                    </div>
                                    <div class="col-md-10 display-flex">
                                        <div class="add-btn add-btn-profile pull-left" id="add-button">
                                            <a class="add-btn-plus popup_selector" data-inputid="photo">+</a>
                                            <input type="text" id="photo" name="photo" value="" hidden>
                                            <div class="add-btn-title popup_selector" data-inputid="photo" >{{__('cms.administration.system-administration.users.user-update.add-modify-profile-image')}}</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="name" class="control-label">{{__('cms.administration.system-administration.users.user-update.user-name')}}<span class="star-required">*</span></label>
                                    <input type="text" class="form-control" id="name" name="name" value="{{$editUser->name}}">
                                    <label id="name-error" class="control-label error"></label>
                                </div>

                                <div class="form-group">
                                    <label for="email" class="control-label">{{__('cms.administration.system-administration.users.user-update.email')}}<span class="star-required">*</span></label>
                                    <input type="text" class="form-control" id="email" name="email" value="{{$editUser->email}}" readonly>
                                    <label id="email-error" class="control-label error"></label>
                                </div>

                                {{-- <div class="form-group">
                                    <label for="old_password" class="control-label">{{__('cms.administration.system-administration.users.user-update.current-password')}}</label>
                                    <input type="password" class="form-control" id="old_password" name="old_password"  >
                                    <label id="old_password-error" class="control-label error"></label>
                                </div>
                                <div class="form-group">
                                    <label for="password" class="control-label">{{__('cms.administration.system-administration.users.user-update.new-password')}}</label>
                                    <input type="password" class="form-control" id="password" name="password"  >
                                    <label id="password-error" class="control-label error"></label>
                                </div>
                                <div class="form-group">
                                    <label for="password_confirmation" class="control-label">{{__('cms.administration.system-administration.users.user-update.confirm-password')}}</label>
                                    <input type="password" class="form-control" id="password_confirmation" name="password_confirmation"  >
                                    <label id="password_confirmation-error" class="control-label error"></label>
                                </div> --}}

                                <div class="form-group">
                                    <label for="status" class="control-label">{{__('cms.administration.system-administration.users.user-update.status')}}</label>
                                    <select type="text" class="form-control select2" id="status" name="status" value="{{$editUser->status}}">
                                        <option value="Active" @if($editUser->status == "Active") selected @endif>Active</option>
                                        <option value="Inactive" @if($editUser->status == "Inactive") selected @endif>InActive</option>
                                    </select>
                                    <label id="status-error" class="control-label error"></label>
                                </div>
                                <div class="form-group">
                                    <label for="role_id" class="control-label">{{__('cms.administration.system-administration.users.user-update.role')}}</label>
                                    <select type="text" class="form-control select2" id="role_id" name="roles[]" multiple>
                                        <option val="">Select Role</option>
                                        @foreach($roles as $role)
                                            <option value="{{$role->id}}"
                                                    @foreach ($editUser->roles as $rol)
                                                    @if($rol->id == $role->id) selected @endif
                                                    @endforeach
                                            >{{$role->name}}</option>
                                        @endforeach
                                    </select>
                                    <label id="role_id-error" class="control-label error"></label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="2" class="tab-content">
                    <div class="row">
                        <div class="container"  style="margin-left: 0;">
                            <div class="col-md-11">
                                <div class="form-group">
                                    <label for="position" class="control-label">{{__('cms.administration.system-administration.users.user-update.user-position')}}</label>
                                    <input type="text" class="form-control" id="position" name="position"  value="{{ $editUser->position }}" >
                                    <label id="position-error" class="control-label error"></label>
                                </div>
                                <div class="form-group">
                                    <label for="bio" class="control-label">{{__('cms.administration.system-administration.users.user-update.user-bio')}}</label>
                                    <textarea type="text" class="form-control editor-area" id="bio" name="bio"  >{!! $editUser->bio !!}</textarea>
                                    <label id="bio-error" class="control-label error"></label>
                                </div>

                                <div class="form-group">
                                    <label for="facebook" class="control-label">{{__('cms.administration.system-administration.users.user-update.facebook')}}</label>
                                    <input type="text" class="form-control" id="facebook" name="facebook"  value="{{ $editUser->facebook }}">
                                    <label id="facebook-error" class="control-label error"></label>
                                </div>
                                <div class="form-group">
                                    <label for="twitter" class="control-label">{{__('cms.administration.system-administration.users.user-update.twitter')}}</label>
                                    <input type="text" class="form-control" id="twitter" name="twitter"  value="{{ $editUser->twitter }}">
                                    <label id="twitter-error" class="control-label error"></label>
                                </div>
                                <div class="form-group">
                                    <label for="linkedin" class="control-label">{{__('cms.administration.system-administration.users.user-update.linkedin')}}</label>
                                    <input type="text" class="form-control" id="linkedin" name="linkedin"  value="{{ $editUser->linkedin }}">
                                    <label id="linkedin-error" class="control-label error"></label>
                                </div>
                                <div class="form-group">
                                    <label for="instagram" class="control-label">{{__('cms.administration.system-administration.users.user-update.instagram')}}</label>
                                    <input type="text" class="form-control" id="instagram" name="instagram"  value="{{ $editUser->instagram }}">
                                    <label id="instagram-error" class="control-label error"></label>
                                </div>
                                <div class="form-group">
                                    <label for="website" class="control-label">{{__('cms.administration.system-administration.users.user-update.website')}}</label>
                                    <input type="text" class="form-control" id="website" name="website"  value="{{ $editUser->website }}">
                                    <label id="website-error" class="control-label error"></label>
                                </div>
                                <div class="form-group">
                                    <label for="blog" class="control-label">{{__('cms.administration.system-administration.users.user-update.blog')}}</label>
                                    <input type="text" class="form-control" id="blog" name="blog" value="{{ $editUser->blog }}" >
                                    <label id="blog-error" class="control-label error"></label>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

                @include('layouts.partials.form-action-buttons',['submit'=>true,'reset'=>true])
            </form>



@endsection

@push('css')

    <link href="http://cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/a549aa8780dbda16f6cff545aeabc3d71073911e/build/css/bootstrap-datetimepicker.css" rel="stylesheet"/>
    <link href="{{asset('css/colorbox.css')}}" rel="stylesheet">
@endpush
@push('js')


    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>
    <script src="{{asset('js/datetimepicker.js')}}"></script>
    
    <script type="text/javascript" src="{{asset('js/jquery.colorbox-min.js')}}"></script>

    <script src="{{asset('packages/tinymce/tinymce.min.js')}}"></script>
    <script>
        function processSelectedFile(e,t){
            console.log(e);
            $("#"+t).val(e);
            document.getElementById('photo-display').src = e.replace('public','/public/storage');
        }
        $(document).on("click",".popup_selector",function(e){
            e.preventDefault();
            var t=$(this).attr("data-inputid");
            var n="/elfinder/popup/";
            var r=n+t;
            $.colorbox({
                href:r,
                fastIframe:true,
                iframe:true,
                width:"70%",
                height:"70%"
            })
        })

    </script>

    <script>
        $(function(){
            tinymce.init({
                selector: "textarea",
                height:"400",
                plugins: [
                    "advlist autolink lists link  charmap print preview anchor",
                    "searchreplace visualblocks code fullscreen",
                    "insertdatetime media table contextmenu paste"
                ],
                toolbar: "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link",

            });



        });
    </script>


    <script type="application/javascript">
        function openTab(tabId) {
            var i, tabcontent;
            tabcontent = document.getElementsByClassName("tab-content");
            for (i = 0; i < tabcontent.length; i++) {
                tabcontent[i].style.display = "none";
            }
            var tabs = document.getElementsByClassName("tab-links");
            for (i = 0; i < tabs.length; i++) {
                tabs[i].className = tabs[i].className.replace(" active", "");
            }
            document.getElementById(tabId).style.display = "block";
            document.getElementById('tab-'+tabId).className += " active";
        }
        // openTab(this, 1);
    </script>
    <script>
        $(function () {
            $('.select2').select2({
                width:'100%'
            });

            $('#datetimepicker1').datetimepicker();

            $('#form-back').click(function () {
                location.href='{{route('administration.system-administration.user.index')}}'
            });
            $('#form-reset').on('click',function () {
                $('#main-form')[0].reset();
            })
            $('#form-submit').on('click',function () {
                var count = 0;
                @foreach($fields['update'] as $field)
                if($('#{{$field}}').val() == ''){
                    $('#{{$field}}-error').html('This field is required');
                    count++;
                }else{
                    $('#{{$field}}-error').html('');
                }
                @endforeach

                @foreach($fields['update'] as $field)
                if($('#{{$field}}').val() == ''){
                    openTab($('#{{$field}}-error').parent().parent().parent().parent().parent()[0].id);
                    return;
                }
                @endforeach

                if(count == 0){
                    // CKEDITOR.updateElement();
                    // CKEDITOR.triggerSave();
                    var data = new FormData();
                    //Form data
                    var form_data = $('#main-form').serializeArray();
                    $.each(form_data, function (key, input) {
                        data.append(input.name, input.value);
                    });
                    data.append('_token', '{{csrf_token()}}');
                    $.ajax({
                        type: "POST",
                        processData: false,
                        contentType: false,
                        url: '{{route('administration.system-administration.user.update',$editUser->id)}}',
                        data: data,
                        dataType:'json',
                        success: function(data)
                        {
                            if(data.status == 'success'){
                                location.href='{{route('administration.system-administration.user.index')}}';
                            } else{
                                for( var error in  data.messages){
                                    $('#'+error+'-error').html(data.messages[error]);
                                    openTab($('#'+error+'-error').parent().parent().parent().parent().parent()[0].id);
                                    $('#'+error).focus();
                                }
                            }
                            // if ( data != null )
                                {{--location.href='{{route('user.index')}}';--}}
                        },
                        error:function( data){
                            console.log(data);
                        }
                    });
                }
            });
        })
    </script>

@endpush
