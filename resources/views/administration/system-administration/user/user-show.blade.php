@extends('layouts.app')
@section('content')

            <form id="main-form" enctype="multipart/form-data">

                @csrf

            <div id="1" class="tab-content" style="display:block;">
                <div class="row">
                    <div class="container">
                        <div class="col-md-11">
                            
                            <div class="form-group ">
                                <label for="meta_image" class="control-label">{{__('cms.administration.system-administration.users.user-show.user-photo')}}</label>
                                @if(isset($user->photo))
                                    <div class="image-thumb">
                                        <img src="{{asset($user->photo)}}" />
                                    </div>
                                @endif
                            </div>
                            <hr>
                            <div class="form-group form-group-show">
                                <label for="name" class="control-label">{{__('cms.administration.system-administration.users.user-show.user-name')}}</label>
                                <p>{{$user->name}}</p>
                            </div>
                            <hr>
                            <div class="form-group form-group-show">
                                <label for="users" class="control-label">{{__('cms.administration.system-administration.users.user-show.email')}}</label>
                                <p>{{$user->email}}</p>
                            </div>
                            <hr>
                            <div class="form-group form-group-show">
                                <label for="date_created" class="control-label">{{__('cms.administration.system-administration.users.user-show.status')}}</label>
                                <p>{{$user->status}}</p>
                            </div>
                            <hr>
                            <div class="form-group form-group-show">
                                <label for="date_updated" class="control-label">{{__('cms.administration.system-administration.users.user-show.role')}}</label>
                                <p>{{$user->role->name}}</p>
                            </div>
                            <hr>
                            <div class="form-group form-group-show">
                                <label for="status" class="control-label">{{__('cms.administration.system-administration.users.user-show.position')}}</label>
                                <p>{{$user->position}}</p>
                            </div>
                            <hr>
                            <div class="form-group">
                                <label for="sites" class="control-label">{{__('cms.administration.system-administration.users.user-show.bio')}}</label>
                                <p>{!! $user->bio !!}</p>
                            </div>
                            <hr>
                            <div class="form-group form-group-show">
                                <label for="description" class="control-label">{{__('cms.administration.system-administration.users.user-show.facebook')}}</label>
                                <p>{{ $user->facebook }}</p>
                            </div>
                            <hr>
                            <div class="form-group form-group-show">
                                <label for="description" class="control-label">{{__('cms.administration.system-administration.users.user-show.twitter')}}</label>
                                <p>{{ $user->twitter }}</p>
                            </div>
                            <hr>
                            <div class="form-group form-group-show">
                                <label for="description" class="control-label">{{__('cms.administration.system-administration.users.user-show.linkedin')}}</label>
                                <p>{{ $user->linkedin }}</p>
                            </div>
                            <hr>
                            <div class="form-group form-group-show">
                                <label for="description" class="control-label">{{__('cms.administration.system-administration.users.user-show.instagram')}}</label>
                                <p>{{ $user->instagram }}</p>
                            </div>
                            <hr>
                            <div class="form-group form-group-show">
                                <label for="description" class="control-label">{{__('cms.administration.system-administration.users.user-show.website')}}</label>
                                <p>{{ $user->website }}</p>
                            </div>
                            <hr>
                            <div class="form-group form-group-show">
                                <label for="description" class="control-label">{{__('cms.administration.system-administration.users.user-show.blog')}}</label>
                                <p>{{ $user->blog }}</p>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

                @include('layouts.partials.form-action-buttons',['submit'=>false,'reset'=>false, 'edit'=>true])
            </form>


@endsection

@push('css')
    <link href="http://cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/a549aa8780dbda16f6cff545aeabc3d71073911e/build/css/bootstrap-datetimepicker.css" rel="stylesheet"/>
    <link href="{{asset('css/colorbox.css')}}" rel="stylesheet">
@endpush
@push('js')

    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>
    <script src="{{asset('js/datetimepicker.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/jquery.colorbox-min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/standalonepopup.min.js')}}"></script>

    <script src="{{asset('packages/tinymce/tinymce.min.js')}}"></script>
    <script>
        $(function(){
            tinymce.init({
                selector: "textarea",
                plugins: [
                    "advlist autolink lists link image charmap print preview anchor",
                    "searchreplace visualblocks code fullscreen",
                    "insertdatetime media table paste"
                ],
                toolbar: " undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
                file_browser_callback : elFinderBrowser,
            });

            function elFinderBrowser (field_name, url, type, win) {
                var elfinder_url = '{{route('elfinder.tinymce4')}}';    // use an absolute path!
                tinyMCE.activeEditor.windowManager.open({
                    file: elfinder_url,
                    title: 'Media Library',
                    width: 900,
                    height: 550,
                    resizable: 'yes',
                    //inline: 'yes',
                    popup_css: false,
                    close_previous: 'no'
                }, {
                    setUrl: function (url) {
                        win.document.getElementById(field_name).value = url;
                    },
                    window: win,
                    input: field_name
                });
                return false;
            }

        });
    </script>
    <script>
        ;( function ( document, window, index )
        {
            var inputs = document.querySelectorAll( '.inputfile' );
            Array.prototype.forEach.call( inputs, function( input )
            {
                var label	 = input.nextElementSibling,
                    labelVal = label.innerHTML;

                input.addEventListener( 'change', function( e )
                {
                    var fileName = '';
                    if( this.files && this.files.length > 1 )
                        fileName = ( this.getAttribute( 'data-multiple-caption' ) || '' ).replace( '{count}', this.files.length );
                    else
                        fileName = e.target.value.split( '\\' ).pop();

                    if( fileName )
                        label.querySelector( 'span' ).innerHTML = fileName;
                    else
                        label.innerHTML = labelVal;
                });

                // Firefox bug fix
                input.addEventListener( 'focus', function(){ input.classList.add( 'has-focus' ); });
                input.addEventListener( 'blur', function(){ input.classList.remove( 'has-focus' ); });
            });
        }( document, window, 0 ));
    </script>

    <script>
        $('#form-edit').on('click',function(){
            location.href='{{route('administration.system-administration.user.edit',$user->id)}}';
        })
        $('#form-back').click(function () {
            location.href='{{route('administration.system-administration.user.index')}}'
        });
    </script>

@endpush