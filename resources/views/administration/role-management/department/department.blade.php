@extends('layouts.app')
@php($slug = 'department')
@php($module = \App\Models\Module::query()->where('slug',$slug)->get()->first())
@if( $module != null )
    @php($name = '\App\Models\\'.str_replace('Controller','',$module->controller_name))
    @php($model = new $name())
    @php($fields = $model->fields())
@endif

@section('content')


            <form id="main-form">
                @csrf
                <div id="1" class="tab-content tab-content-profile" style="display:block;">
                    <div class="row">
                        <div class="container">
                            <div class="col-md-11">
                                <div class="form-group">
                                    <label for="name" class="control-label">Name<span class="star-required">*</span></label>
                                    <input type="text" class="form-control" id="name" name="name"  >
                                    <label id="name-error" class="control-label error"></label>
                                </div>
                                <div class="form-group">
                                    <label for="tags" class="control-label">Tags</label>
                                    <select type="text" class="form-control select2" id="tags" name="tags[]" multiple>
                                    </select>
                                    <label id="tags-error" class="control-label error"></label>
                                </div>
                                <div class="form-group">
                                    <label for="color" class="control-label">Color<span class="star-required">*</span></label>
                                    <input type="text" class="form-control" id="color" name="color"  >
                                    <label id="color-error" class="control-label error"></label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @include('layouts.partials.form-action-buttons',['submit'=>true,'reset'=>true])
            </form>


@endsection
@push('css')
    <link href="http://cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/a549aa8780dbda16f6cff545aeabc3d71073911e/build/css/bootstrap-datetimepicker.css" rel="stylesheet"/>
    <link href="{{asset('css/colorbox.css')}}" rel="stylesheet">
@endpush
@push('js')

    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>
    <script src="{{asset('js/datetimepicker.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/jquery.colorbox-min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/standalonepopup.min.js')}}"></script>

    <script src="{{asset('packages/tinymce/tinymce.min.js')}}"></script>
    <script>
        $(function(){
            tinymce.init({
                selector: "textarea",
                height:"400",
                plugins: [
                    "advlist autolink lists link image charmap print preview anchor",
                    "searchreplace visualblocks code fullscreen",
                    "insertdatetime media table paste"
                ],
                toolbar: " undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
                file_browser_callback : elFinderBrowser,
            });

            function elFinderBrowser (field_name, url, type, win) {
                var elfinder_url = '{{route('elfinder.tinymce4')}}';    // use an absolute path!
                tinyMCE.activeEditor.windowManager.open({
                    file: elfinder_url,
                    title: 'Media Library',
                    width: 900,
                    height: 550,
                    resizable: 'yes',
                    //inline: 'yes',
                    popup_css: false,
                    close_previous: 'no'
                }, {
                    setUrl: function (url) {
                        win.document.getElementById(field_name).value = url;
                    },
                    window: win,
                    input: field_name
                });
                return false;
            }


        });
    </script>
    <script>
        $(function () {
            $('.select2').select2({
                width:'100%',
                tags: true
            });

            $('#date_created').datetimepicker({
                allowInputToggle: true
            });
            $('#date_updated').datetimepicker({
                allowInputToggle: true
            });

            $('#form-back').click(function () {
                location.href='{{route('administration.role-management.department.index')}}'
            });
            $('#form-reset').on('click',function () {
                $('#main-form')[0].reset();
            })
            $('#form-submit').on('click',function () {
                var count = 0;
                @foreach($fields['store'] as $field)
                if($('#{{$field}}').val() == ''){
                    $('#{{$field}}-error').html('This field is required');
                    count++;
                }else{
                    $('#{{$field}}-error').html('');
                }
                @endforeach

                if(count == 0){

                    var data = new FormData();
                    //Form data
                    var form_data = $('#main-form').serializeArray();
                    $.each(form_data, function (key, input) {
                        data.append(input.name, input.value);
                    });
                    data.append('_token', '{{csrf_token()}}');
                    $.ajax({
                        type: "POST",
                        processData: false,
                        contentType: false,
                        url: '{{route('administration.role-management.department.store')}}',
                        data: data,
                        success: function(data)
                        {
                            console.log(data);
                            if ( data != null )
                                location.href='{{route('administration.role-management.department.index')}}';
                        },
                        error:function( data){
                            console.log(data);
                        }
                    });
                }
            });
        })
    </script>

@endpush
