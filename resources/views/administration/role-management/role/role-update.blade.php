@extends('layouts.app')
@php($slug = 'role')
@php($module = \App\Models\Module::query()->where('slug',$slug)->get()->first())
@if( $module != null )
@php($name = '\App\Models\\'.str_replace('Controller','',$module->controller_name))
@php($model = new $name())
@php($fields = $model->fields())
@endif

@section('content')

            <form id="main-form" enctype="multipart/form-data">
                <input type="hidden" name="_method" value="put">
                @csrf
                <div id="1" class="tab-content tab-content-profile" style="display:block;">
                    <div class="row">
                        <div class="container">
                            <div class="col-md-11">

                                <div class="form-group">
                                    <label for="name" class="control-label">{{__('cms.administration.role-management.roles.role-update.role_name')}}</label>
                                    <input type="text" class="form-control" id="name" name="name"  value="{{ $role->name }}" >
                                    <label id="position-error" class="control-label error"></label>
                                </div>
                                <div class="form-group">
                                    <label for="description" class="control-label">{{__('cms.administration.role-management.roles.role-update.description')}}</label>
                                    <textarea type="text" class="form-control editor-area" id="description" name="description"  >{{$role->description}}</textarea>
                                    <label id="bio-error" class="control-label error"></label>
                                </div>

                                <div>
                                    <div class="form-group">
                                        <label for="name" class="control-label">{{__('cms.administration.role-management.roles.role-update.permissions')}}</label>

                                    </div>
                                    <table class="role-table">
                                        <tr>
                                            <th>Module Name</th>
                                            <th><input type="checkbox" class="role-col-check" data-target="add" />&nbsp;Add</th>
                                            <th><input type="checkbox" class="role-col-check" data-target="show" />&nbsp;Show</th>
                                            <th><input type="checkbox" class="role-col-check" data-target="edit" />&nbsp;Edit</th>
                                            <th><input type="checkbox" class="role-col-check" data-target="delete" />&nbsp;Delete</th>
                                            <th><input type="checkbox" class="role-col-check" data-target="status" />&nbsp;Status</th>
                                        </tr>
                                        @foreach($parents as $par)
                                            @if(($par->children()->count() == 0 and $par->parentChildren()->count() == 0) or $par->parent != null)

                                            @else
                                                <tr>
                                                    <td colspan="6" class="role-dropdown-toggle" data-target="{{$par->id}}">
                                                        <input type="checkbox" class="role-parent-checkbox" data-parent-target="{{$par->id}}" />&nbsp;<span>{{$par->name}}<i class="fa fa-angle-down"></i></span>
                                                    </td>
                                                </tr>
                                                @include('layouts.partials.role-table-root', ['parent'=>$par])
                                            @endif
                                        @endforeach
                                        <tr>
                                            <td><input type="checkbox" class="role-checkbox" data-target="general-config"/>&nbsp;General Configurations</td>
                                            <td><input type="checkbox" name="permissions[1000][add]" data-col-check="add" data-check="general-config"
                                                @if (isset($role))
                                                    @if( $role->module(1000)!= null and $role->module(1000)->add == 1 ) checked @endif
                                                @endif
                                                />&nbsp;</td>

                                            <td><input type="checkbox" name="permissions[1000][show_all]" data-col-check="show" data-check="general-config"data-check="general-config"
                                                @if (isset($role))
                                                    @if( $role->module(1000)!= null and $role->module(1000)->show_all == 1 ) checked @endif
                                                @endif
                                                />
                                                &nbsp;<span class="role-permission-label">All records</span> <br/>
                                                <input type="checkbox" name="permissions[1000][show]" data-col-check="show" data-check="general-config"/> <span class="role-permission-label"data-check="general-config"
                                                @if (isset($role))
                                                    @if( $role->module(1000)!= null and $role->module(1000)->show == 1 ) checked @endif
                                                @endif
                                                >
                                                Only user record</span></td>

                                            <td><input type="checkbox" name="permissions[1000][edit_all]" data-col-check="edit" data-check="general-config"data-check="general-config"
                                                @if (isset($role))
                                                    @if( $role->module(1000)!= null and $role->module(1000)->edit_all == 1 ) checked @endif
                                                @endif
                                                />
                                                &nbsp;<span class="role-permission-label">All records </span><br/>
                                                <input type="checkbox" name="permissions[1000][edit]" data-col-check="edit" data-check="general-config"/> <span class="role-permission-label"data-check="general-config"
                                                @if (isset($role))
                                                    @if( $role->module(1000)!= null and $role->module(1000)->edit == 1 ) checked @endif
                                                @endif
                                                >
                                                Only user record</span></td>

                                            <td><input type="checkbox" name="permissions[1000][delete_all]" data-col-check="delete" data-check="general-config"data-check="general-config"
                                                @if (isset($role))
                                                    @if( $role->module(1000)!= null and $role->module(1000)->delete_all == 1 ) checked @endif
                                                @endif
                                                />
                                                &nbsp;<span class="role-permission-label">All records</span> <br/>
                                                <input type="checkbox" name="permissions[1000][delete]" data-col-check="delete" data-check="general-config"data-check="general-config"
                                                @if (isset($role))
                                                    @if( $role->module(1000)!= null and $role->module(1000)->delete == 1 ) checked @endif
                                                @endif
                                                />
                                                <span class="role-permission-label">Only user record</span></td>

                                            <td><input type="checkbox" name="permissions[1000][status]" data-col-check="status" data-check="general-config"data-check="general-config"
                                                @if (isset($role))
                                                    @if( $role->module(1000)!= null and $role->module(1000)->status == 1 ) checked @endif
                                                @endif
                                                />
                                             </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @include('layouts.partials.form-action-buttons',['submit'=>true,'reset'=>true])
            </form>


@endsection

@push('css')

    <link href="http://cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/a549aa8780dbda16f6cff545aeabc3d71073911e/build/css/bootstrap-datetimepicker.css" rel="stylesheet"/>
    <link href="{{asset('css/colorbox.css')}}" rel="stylesheet">
@endpush
@push('js')

    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>
    <script src="{{asset('js/datetimepicker.js')}}"></script>

    <script type="text/javascript" src="{{asset('js/jquery.colorbox-min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/standalonepopup.min.js')}}"></script>
    <script src="{{asset('packages/ckeditor/ckeditor.js')}}"></script>
    <script>
        CKEDITOR.replace( 'description' );
    </script>

    <script>
        $(function () {
            $('.select2').select2({
                width:'100%'
            });

            $('#form-back').click(function () {
                location.href='{{route('administration.role-management.role.index')}}'
            });
            $('#form-reset').on('click',function () {
                $('#main-form')[0].reset();
            })
            $('#form-submit').on('click',function () {
                var count = 0;
                @foreach($fields['update'] as $field)
                if($('#{{$field}}').val() == ''){
                    $('#{{$field}}-error').html('This field is required');
                    count++;
                }else{
                    $('#{{$field}}-error').html('');
                }
                @endforeach

                if(count == 0){
                    var data = new FormData();
                    var content = CKEDITOR.instances.description.getData();

                    //Form data
                    var form_data = $('#main-form').serializeArray();
                    $.each(form_data, function (key, input) {
                        data.append(input.name, input.value);
                    });

                    data.append('_token', '{{csrf_token()}}');
                    data.append('description', content);

                    $.ajax({
                        type: "POST",
                        processData: false,
                        contentType: false,
                        url: '{{route('administration.role-management.role.update',$role->id)}}',
                        data: data,
                        success: function(data)
                        {
                            console.log(data);
                            if ( data != null )
                                location.href='{{route('administration.role-management.role.index')}}';
                        },
                        error:function( data){
                            console.log(data);
                        }
                    });
                }
            });
        })
    </script>

@endpush