@extends('layouts.app')

@section('content')

            <form id="main-form" enctype="multipart/form-data">
                @csrf
                <div id="1" class="tab-content" style="display:block;">
                    <div class="row">
                        <div class="container">
                            <div class="col-md-11">
                                <div class="form-group form-group-show">
                                    <label for="name" class="control-label">{{__('cms.administration.role-management.roles.role-show.role_name')}}</label>
                                    <p>{{$role->name}}</p>
                                </div>
                                <hr>
                                <div class="form-group form-group-show">
                                    <label for="name" class="control-label">{{__('cms.administration.role-management.roles.role-show.description')}}</label>
                                    <p>{!!$role->description!!}</p>
                                </div>
                                <hr>
                                <div>
                                    <div class="form-group">
                                        <label for="name" class="control-label">{{__('cms.administration.role-management.roles.role-show.permissions')}}</label>

                                    </div>
                                    <table class="role-table">
                                        <tr>
                                            <th>Module Name</th>
                                            <th>Add</th>
                                            <th>Show</th>
                                            <th>Edit</th>
                                            <th>Delete</th>
                                            <th>Status</th>
                                        </tr>
                                        @foreach($parents as $par)
                                            @if(($par->children()->count() == 0 and $par->parentChildren()->count() == 0) or $parent->parent != null)

                                            @else
                                                <tr>
                                                    <td colspan="6" class="role-dropdown-toggle" data-target="{{$par->id}}">
                                                        <span>{{$par->name}}<i class="fa fa-angle-down"></i></span>
                                                    </td>
                                                </tr>
                                                @include('layouts.partials.role-show-table-root', ['parent'=>$par])
                                            @endif
                                        @endforeach
                                        <tr>
                                            <td>General Configurations</td>
                                            <td>@if (isset($role))
                                                    @if( $role->module(1000)!= null and $role->module(1000)->add == 1 ) 
                                                        <i class="fa fa-check"></i> 
                                                    @else 
                                                        <i class="fa fa-close"></i> 
                                                    @endif
                                                @endif
                                                &nbsp;</td>

                                            <td>@if (isset($role))
                                                    @if( $role->module(1000)!= null and $role->module(1000)->show_all == 1 )  
                                                        <i class="fa fa-check"></i> 
                                                    @else 
                                                        <i class="fa fa-close"></i> 
                                                    @endif
                                                @endif
                                                &nbsp;<span class="role-permission-label">All records</span> <br/>
                                                <span class="role-permission-label">
                                                @if (isset($role))
                                                    @if( $role->module(1000)!= null and $role->module(1000)->show == 1 )  
                                                        <i class="fa fa-check"></i> 
                                                    @else 
                                                        <i class="fa fa-close"></i> 
                                                    @endif
                                                @endif
                                                Only user record</span></td>

                                            <td>@if (isset($role))
                                                    @if( $role->module(1000)!= null and $role->module(1000)->edit_all == 1 )  
                                                        <i class="fa fa-check"></i> 
                                                    @else 
                                                        <i class="fa fa-close"></i> 
                                                    @endif
                                                @endif
                                                &nbsp;<span class="role-permission-label">All records </span><br/>
                                                <span class="role-permission-label" >
                                                @if (isset($role))
                                                    @if( $role->module(1000)!= null and $role->module(1000)->edit == 1 )  
                                                        <i class="fa fa-check"></i> 
                                                    @else 
                                                        <i class="fa fa-close"></i> 
                                                    @endif
                                                @endif
                                                Only user record</span></td>

                                            <td>@if (isset($role))
                                                    @if( $role->module(1000)!= null and $role->module(1000)->delete_all == 1 )  
                                                        <i class="fa fa-check"></i> 
                                                    @else 
                                                        <i class="fa fa-close"></i> 
                                                    @endif
                                                @endif
                                                &nbsp;<span class="role-permission-label">All records</span> <br/>
                                                @if (isset($role))
                                                    @if( $role->module(1000)!= null and $role->module(1000)->delete == 1 )  
                                                        <i class="fa fa-check"></i> 
                                                    @else 
                                                        <i class="fa fa-close"></i> 
                                                    @endif
                                                @endif
                                                <span class="role-permission-label">Only user record</span></td>

                                            <td>@if (isset($role))
                                                    @if( $role->module(1000)!= null and $role->module(1000)->status == 1 )  
                                                        <i class="fa fa-check"></i> 
                                                    @else 
                                                        <i class="fa fa-close"></i> 
                                                    @endif
                                                @endif
                                             </td>
                                        </tr>
                                    </table>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

                @include('layouts.partials.form-action-buttons',['submit'=>false,'reset'=>false, 'edit'=>true])
            </form>


@endsection

@push('css')
    <link href="http://cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/a549aa8780dbda16f6cff545aeabc3d71073911e/build/css/bootstrap-datetimepicker.css" rel="stylesheet"/>
    <link href="{{asset('css/colorbox.css')}}" rel="stylesheet">
@endpush
@push('js')

    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>
    <script src="{{asset('js/datetimepicker.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/jquery.colorbox-min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/standalonepopup.min.js')}}"></script>

    <script src="{{asset('packages/tinymce/tinymce.min.js')}}"></script>
    <script>
        $(function(){
            tinymce.init({
                selector: "textarea",
                plugins: [
                    "advlist autolink lists link image charmap print preview anchor",
                    "searchreplace visualblocks code fullscreen",
                    "insertdatetime media table paste"
                ],
                toolbar: " undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
                file_browser_callback : elFinderBrowser,
            });

            function elFinderBrowser (field_name, url, type, win) {
                var elfinder_url = '{{route('elfinder.tinymce4')}}';    // use an absolute path!
                tinyMCE.activeEditor.windowManager.open({
                    file: elfinder_url,
                    title: 'Media Library',
                    width: 900,
                    height: 550,
                    resizable: 'yes',
                    //inline: 'yes',
                    popup_css: false,
                    close_previous: 'no'
                }, {
                    setUrl: function (url) {
                        win.document.getElementById(field_name).value = url;
                    },
                    window: win,
                    input: field_name
                });
                return false;
            }

        });
    </script>
    <script>
        ;( function ( document, window, index )
        {
            var inputs = document.querySelectorAll( '.inputfile' );
            Array.prototype.forEach.call( inputs, function( input )
            {
                var label	 = input.nextElementSibling,
                    labelVal = label.innerHTML;

                input.addEventListener( 'change', function( e )
                {
                    var fileName = '';
                    if( this.files && this.files.length > 1 )
                        fileName = ( this.getAttribute( 'data-multiple-caption' ) || '' ).replace( '{count}', this.files.length );
                    else
                        fileName = e.target.value.split( '\\' ).pop();

                    if( fileName )
                        label.querySelector( 'span' ).innerHTML = fileName;
                    else
                        label.innerHTML = labelVal;
                });

                // Firefox bug fix
                input.addEventListener( 'focus', function(){ input.classList.add( 'has-focus' ); });
                input.addEventListener( 'blur', function(){ input.classList.remove( 'has-focus' ); });
            });
        }( document, window, 0 ));
    </script>

    <script>
        $('#form-edit').on('click',function(){
            location.href='{{route('administration.role-management.role.edit',$role->id)}}';
        })
        $('#form-back').click(function () {
            location.href='{{route('administration.role-management.role.index')}}'
        });
    </script>

@endpush