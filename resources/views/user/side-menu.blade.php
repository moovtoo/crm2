<div class="col-sm-4 col-md-3 col-lg-2 sidebar">
    <div class="mini-submenu">
        <i class="fa fa-angle-double-right"></i>
    </div>
    <span class="pull-right" id="slide-submenu">
        <i class="fa fa-angle-double-left"></i>
    </span>
    <div class="list-group">

            <div class="side-menu-user">
                <img src="@if(isset(Auth::user()->photo) and Auth::user()->photo != '') {{asset(Auth::user()->photo)}} @else {{asset('images/Avatar.svg')}} @endif" class="img-responsive header-user-photo">
                <a href="{{route ('profile')}}">
                    Hello {{ Auth::user()->name }}
                </a>
            </div>
        <div id="list-configurations" @if(str_contains(Request::path(), '/user')) style="display: block" @endif>
            <div>
                <a href="{{route('profile')}}" class="list-group-item @if(Request::is('cms/profile')) active @endif">
                    Edit Profile
                </a>
            </div>

            <div>
                <a href="#" class="list-group-item @if(Request::is('cms/notification-center')) active @endif">
                    Notification Center
                </a>
            </div>

            <div>
                <a href="#" class="list-group-item @if(Request::is('cms/messages')) active @endif">
                    Messages
                </a>
            </div>

            <div>
                <a href="#" class="list-group-item @if(Request::is('cms/emails')) active @endif">
                    Email
                </a>
            </div>

        </div>
    </div>
</div>