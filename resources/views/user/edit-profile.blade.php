@extends('layouts.app')
@section('content')
        <form id="main-form" enctype="multipart/form-data">

                @csrf

                <div id="1" class="tab-content" style="display:block;">
                    <div class="row">
                        <div class="container">
                            <div class="col-md-11">
                                <div class="form-group row form-group-profile">
                                    <div class="col-md-2" >
                                        @if(isset($user->photo) and $user->photo != null)
                                            <div class="image-thumb-user">
                                                <img id="photo-display" src="{{asset("$user->photo")}}" class="user-image user-image-hover">
                                                <a href="{{route('profile.removeImage')}}" class="image-fav-user"><img src="{{asset("images/delete.svg")}}"> </a>
                                            </div>
                                        @else
                                            <img id="photo-display" src="{{asset('images/Avatar.svg')}}" class="user-image">
                                        @endif
                                    </div>
                                    <div class="col-md-10 display-flex">
                                        <div class="add-btn add-btn-profile pull-left" id="add-button">
                                            <a class="add-btn-plus popup_selector" data-inputid="photo">+</a>
                                            <input type="text" id="photo" name="photo" value="" hidden>
                                            <div class="add-btn-title popup_selector" data-inputid="photo" >{{__('cms.administration.system-administration.users.user-update.add-modify-profile-image')}}</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="name" class="control-label">{{__('cms.user.edit-profile.user-name')}}</label>
                                    <input type="text" class="form-control" id="name" name="name" value="{{$user->name}}">
                                    <label id="name-error" class="control-label error"></label>
                                </div>



                                <div class="form-group">
                                    <label for="old_password" class="control-label">{{__('cms.user.edit-profile.current-password')}}</label>
                                    <input type="password" class="form-control" id="old_password" name="old_password"  >
                                    <label id="old_password-error" class="control-label error"></label>
                                </div>
                                <div class="form-group">
                                    <label for="password" class="control-label">{{__('cms.user.edit-profile.new-password')}}</label>
                                    <input type="password" class="form-control" id="password" name="password"  >
                                    <label id="password-error" class="control-label error"></label>
                                </div>
                                <div class="form-group">
                                    <label for="password_confirmation" class="control-label">{{__('cms.user.edit-profile.confirm-password')}}</label>
                                    <input type="password" class="form-control" id="password_confirmation" name="password_confirmation"  >
                                    <label id="password_confirmation-error" class="control-label error"></label>
                                </div>




                            </div>
                        </div>
                    </div>
                </div>

            @include('layouts.partials.form-action-buttons',['submit'=>true,'reset'=>true])
        </form>

@endsection

@push('css')

    <link href="http://cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/a549aa8780dbda16f6cff545aeabc3d71073911e/build/css/bootstrap-datetimepicker.css" rel="stylesheet"/>
    <link href="{{asset('css/colorbox.css')}}" rel="stylesheet">
@endpush

@push('js')

    <script type="text/javascript" src="{{asset('js/jquery.colorbox-min.js')}}"></script>
    
    <script>
        function processSelectedFile(e,t){
            console.log(e);
            $("#"+t).val(e);
            document.getElementById('photo-display').src = e.replace('public','/public/storage');
        }
        $(document).on("click",".popup_selector",function(e){
            e.preventDefault();
            var t=$(this).attr("data-inputid");
            var n="/elfinder/popup/";
            var r=n+t;
            $.colorbox({
                href:r,
                fastIframe:true,
                iframe:true,
                width:"70%",
                height:"70%"
            })
        })

    </script>
    <script>
        $(function () {

            $('#form-back').click(function () {
                location.href='{{route('home')}}'
            });
            $('#form-reset').on('click',function () {
                $('#main-form')[0].reset();
            })
            $('#form-submit').on('click',function () {
                var count = 0;
                if(count == 0){
                    var data = new FormData();
                    //Form data
                    var form_data = $('#main-form').serializeArray();
                    $.each(form_data, function (key, input) {
                        data.append(input.name, input.value);
                    });
                    data.append('_token', '{{csrf_token()}}');
                    $.ajax({
                        type: "POST",
                        processData: false,
                        contentType: false,
                        url: '{{route('profile.update')}}',
                        data: data,
                        dataType:'json',
                        success: function(data)
                        {
                            if(data.status == 'success'){
                                location.href='{{route('profile')}}';
                            } else{
                                for( var error in  data.messages){
                                    $('#'+error+'-error').html(data.messages[error]);
                                }
                            }
                            // if ( data != null )
                            {{--location.href='{{route('user.index')}}';--}}
                        },
                        error:function( data){
                            console.log(data);
                        }
                    });
                }
            });
        })
    </script>

@endpush