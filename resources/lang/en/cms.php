<?php
/**
 * Created by PhpStorm.
 * User: rabeearkadan
 * Date: 1/7/19
 * Time: 12:59 PM
 */

return [
    'administration.system-administration.users.user.add_modify_profile_image' =>'ADD / MODIFY PROFILE PICTURE',
    'administration.role-management.roles.role.role_name' =>'Role Name',
    'administration.role-management.roles.role.description' =>'Description',
    'administration.role-management.roles.role-update.role_name' =>'Role Name',
    'administration.role-management.roles.role-update.description' =>'Role Description',
    'administration.role-management.roles.role-update.permissions' =>'Choose Permissions',
    'administration.role-management.roles.role-update.module-name' =>'Module Name',
    'administration.role-management.roles.role-update.add' =>'Add',
    'administration.role-management.roles.role-update.show' =>'Show',
    'administration.role-management.roles.role-update.edit' =>'Edit',
    'administration.role-management.roles.role-update.delete' =>'Delete',
    'administration.role-management.roles.role-update.status' =>'Status',

    'ecommerce.e-commerce-content.producer.producer.name' =>'Producer Name',
    'ecommerce.e-commerce-content.producer.producer.commercial_name' =>'Producer Commerial Name',
    'ecommerce.e-commerce-content.producer.producer.profile' =>'Producer Profile',
    'ecommerce.e-commerce-content.producer.producer.cover' =>'Producer Cover',
    'ecommerce.e-commerce-content.producer.producer.date_update' =>'Date Updated',
    'ecommerce.e-commerce-content.producer.producer.date_created' =>'Date Created',
    'ecommerce.e-commerce-content.producer.producer.status' =>'Status',
    'ecommerce.e-commerce-content.producer.producer.browse_image' =>'Browse Image',
    'ecommerce.e-commerce-content.producer.producer.producer_description' =>'Producer Description',
    'ecommerce.e-commerce-content.producer.producer.facebook' =>'Facebook',
    'ecommerce.e-commerce-content.producer.producer.twitter' =>'Twitter',
    'ecommerce.e-commerce-content.producer.producer.website' =>'Website',
    'ecommerce.e-commerce-content.producer.producer.address' =>'Address',
    'ecommerce.e-commerce-content.producer.producer.city' =>'City',
    'ecommerce.e-commerce-content.producer.producer.country' =>'Country',
    'ecommerce.e-commerce-content.producer.producer.phone' =>'Phone',
    'ecommerce.e-commerce-content.producer.producer.email' =>'ُEmail',
    'ecommerce.e-commerce-content.producer.producer-show.name' =>'Producer Name',
    'ecommerce.e-commerce-content.producer.producer-show.date_update' =>'Date Updated',
    'ecommerce.e-commerce-content.producer.producer-show.date_created' =>'Date Created',
    'ecommerce.e-commerce-content.producer.producer-show.status' =>'Status',
    'ecommerce.e-commerce-content.producer.producer-show.commercial_name' =>'Producer Commerial Name',
    'ecommerce.e-commerce-content.producer.producer-show.profile' =>'Producer Profile',
    'ecommerce.e-commerce-content.producer.producer-show.cover' =>'Producer Cover',
    'ecommerce.e-commerce-content.producer.producer-show.producer_description' =>'Producer Description',
    'ecommerce.e-commerce-content.producer.producer-show.facebook' =>'Facebook',
    'ecommerce.e-commerce-content.producer.producer-show.twitter' =>'Twitter',
    'ecommerce.e-commerce-content.producer.producer-show.website' =>'Website',
    'ecommerce.e-commerce-content.producer.producer-show.address' =>'Address',
    'ecommerce.e-commerce-content.producer.producer-show.city' =>'City',
    'ecommerce.e-commerce-content.producer.producer-show.country' =>'Country',
    'ecommerce.e-commerce-content.producer.producer-show.phone' =>'Phone',
    'ecommerce.e-commerce-content.producer.producer-show.email' =>'ُEmail',
    'ecommerce.e-commerce-content.producer.producer-update.date_update' =>'Date Updated',
    'ecommerce.e-commerce-content.producer.producer-update.date_created' =>'Date Created',
    'ecommerce.e-commerce-content.producer.producer-update.status' =>'Status',
    'ecommerce.e-commerce-content.producer.producer-update.name' =>'Producer Name',
    'ecommerce.e-commerce-content.producer.producer-update.commercial_name' =>'Producer Commerial Name',
    'ecommerce.e-commerce-content.producer.producer-update.profile' =>'Producer Profile',
    'ecommerce.e-commerce-content.producer.producer-update.cover' =>'Producer Cover',
    'ecommerce.e-commerce-content.producer.producer-update.producer_description' =>'Producer Description',
    'ecommerce.e-commerce-content.producer.producer-update.facebook' =>'Facebook',
    'ecommerce.e-commerce-content.producer.producer-update.twitter' =>'Twitter',
    'ecommerce.e-commerce-content.producer.producer-update.website' =>'Website',
    'ecommerce.e-commerce-content.producer.producer-update.address' =>'Address',
    'ecommerce.e-commerce-content.producer.producer-update.city' =>'City',
    'ecommerce.e-commerce-content.producer.producer-update.country' =>'Country',
    'ecommerce.e-commerce-content.producer.producer-update.phone' =>'Phone',
    'ecommerce.e-commerce-content.producer.producer-update.email' =>'ُEmail',

    'administration.role-management.roles.role-show.role_name' =>'Role Name',
    'administration.role-management.roles.role-show.description' =>'Role Description',
    'administration.role-management.roles.role-show.permissions' =>'Choose Permissions',
    'administration.role-management.roles.role-show.module-name' =>'Module Name',
    'administration.role-management.roles.role-show.add' =>'Add',
    'administration.role-management.roles.role-show.show' =>'Show',
    'administration.role-management.roles.role-show.edit' =>'Edit',
    'administration.role-management.roles.role-show.delete' =>'Delete',
    'administration.role-management.roles.role-show.status' =>'Status',

    'administration.system-administration.users.user.full-name' =>'Full Name',
    'administration.system-administration.users.user.email' =>'Email',
    'administration.system-administration.users.user.password' =>'Password',
    'administration.system-administration.users.user.status'=>'Status',
    'administration.system-administration.users.user.role'=>'Role',
    'administration.system-administration.users.user-show.user-photo'=>'User Photo :',
    'administration.system-administration.users.user-show.user-name'=>'User Name :',
    'administration.system-administration.users.user-show.email'=>'Email :',
    'administration.system-administration.users.user-show.status'=>'Status :',
    'administration.system-administration.users.user-show.role'=>'Role :',
    'administration.system-administration.users.user-show.position'=>'Position :',
    'administration.system-administration.users.user-show.bio'=>'Bio :',
    'administration.system-administration.users.user-show.facebook'=>'Facebook :',
    'administration.system-administration.users.user-show.twitter'=>'Twitter :',
    'administration.system-administration.users.user-show.linkedin'=>'Linkedin :',
    'administration.system-administration.users.user-show.instagram'=>'Instagram :',
    'administration.system-administration.users.user-show.website'=>'Website :',
    'administration.system-administration.users.user-show.blog'=>'Blog :',
    'administration.system-administration.users.user-update.user-basic-info'=>'User Basic Info',
    'administration.system-administration.users.user-update.user-properties'=>'User Properties',
    'administration.system-administration.users.user-update.add-modify-profile-image'=>'ADD / MODIFY PROFILE PICTURE',
    'administration.system-administration.users.user-update.user-name'=>'User Name',
    'administration.system-administration.users.user-update.email'=>'Email',
    'administration.system-administration.users.user-update.new-password'=>'New Password',
    'user.edit-profile.new-password'=>'New Password',
    'user.edit-profile.current-password'=>'Current Password',
    'administration.system-administration.users.user-update.current-password'=>'Current Password',
    'administration.system-administration.users.user-update.confirm-password'=>'Confirm Password',
    'user.edit-profile.confirm-password'=>'Confirm Password',
    'administration.system-administration.users.user-update.status'=>'Status',
    'administration.system-administration.users.user-update.role'=>'Role',
    'administration.system-administration.users.user-update.select-role'=>'Select role',
    'administration.system-administration.users.user-update.user-position'=>'User Position',
    'administration.system-administration.users.user-update.user-bio'=>'User Bio',
    'administration.system-administration.users.user-update.facebook'=>'Facebook',
    'administration.system-administration.users.user-update.twitter'=>'Twitter',
    'administration.system-administration.users.user-update.linkedin'=>'Linkedin',
    'administration.system-administration.users.user-update.instagram'=>'Instagram',
    'administration.system-administration.users.user-update.website'=>'Website',
    'administration.system-administration.users.user-update.blog'=>'Blog',
    'home.content-management-system'=>'Content Management System',
    'home.configurations'=>'CONFIGURATIONS',
    'home.get-started'=>'Get Started',
    'home.administration'=>'ADMINISTRATION',
    'home.get-started'=>'Get Started',
    'home.content-management'=>'CONTENT MANAGEMENT',
    'configuration.configurations.branding.cms-title'=>'CMS Title',
    'configuration.configurations.branding.logo'=>'Logo',
    'configuration.configurations.branding.browse-image'=>'Browse Image...',
    'configuration.configurations.branding.background'=>'Background',
    'configuration.configurations.branding.favicon'=>'Favicon',
    'configuration.configurations.domain.domain-name'=>'Domain Name',
    'configuration.domains.domain.domain-name'=>'Domain Name',
    'configuration.domains.domain-update.domain-name'=>'Domain Name',
    'configuration.configurations.language.add-new-locale'=>'Add New Locale',
    'configuration.configurations.language.your-cms-in-english'=>'Your CMS in english',
    'configuration.configurations.language.your-cms-in-france'=>'Votre CMS en Français',
    'configuration.configurations.super-user.add-modify-profile'=>'ADD / MODIFY PROFILE PICTURE',
    'configuration.configurations.super-user.name'=>'Name',
    'configuration.configurations.super-user.email'=>'Email',
    'configuration.configurations.super-user.current-password'=>'Current Password',
    'configuration.configurations.super-user.new-password'=>'New Password',
    'configuration.configurations.super-user.confirm-password'=>'Confirm Password',
    'content-management.media-content.article.article.article_title'=>'Article Title',
    'content-management.media-content.article.article.authros'=>'Authors',
    'content-management.media-content.article.article.date_created'=>'Date Created',
    'content-management.media-content.article.article.date_update'=>'Date Update',
    'content-management.media-content.article.article.main_photo'=>'Main Photo',
    'content-management.media-content.article.article.browse_image'=>'Browse Image...',
    'content-management.media-content.article.article.status'=>'Status',
    'content-management.media-content.article.article.locale'=>'Locale',
    'content-management.media-content.article.article.site'=>'Site',
    'content-management.media-content.article.article.categories'=>'Categories',
    'content-management.media-content.article.article.tags'=>'Tags',
    'content-management.media-content.article.article.article_short_description'=>'Article Short Description',
    'content-management.media-content.article.article.article_content'=>'Article Content',
    'content-management.media-content.article.article.meta_title'=>'Meta Title',
    'content-management.media-content.article.article.meta_photo'=>'Meta Photo',
    'content-management.media-content.article.article.meta_url'=>'Meta Url',
    'content-management.media-content.article.article.meta_type'=>'Meta Type',
    'content-management.media-content.article.article.meta_description'=>'Meta Description',
    'content-management.media-content.article.article-show.article_title'=>'Article Title :',
    'content-management.media-content.article.article-show.authors'=>'Authors :',
    'content-management.media-content.article.article.authors'=>'Authors :',
    'content-management.media-content.article.article-show.date_created'=>'Date Created :',
    'content-management.media-content.article.article-show.date_updated'=>'Date Updated :',
    'content-management.media-content.article.article-show.main_photo'=>'Main Photo :',
    'content-management.media-content.article.article-show.status'=>'Status :',
    'content-management.media-content.article.article-show.locale'=>'Locale :',
    'content-management.media-content.article.article-show.site'=>'Site :',
    'content-management.media-content.article.article-show.categories'=>'Categories :',
    'content-management.media-content.article.article-show.article_short_description'=>'Article Short Description :',
    'content-management.media-content.article.article-show.article_content'=>'Article Content :',
    'content-management.media-content.article.article-show.meta_title'=>'Meta Title :',
    'content-management.media-content.article.article-show.meta_photo'=>'Meta Photo :',
    'content-management.media-content.article.article-show.meta_url'=>'Meta Url :',
    'content-management.media-content.article.article-show.meta_type'=>'Meta Type :',
    'content-management.media-content.article.article-show.meta_description'=>'Meta Description :',
    'content-management.media-content.article.article-update.article_title'=>'Article Title',
    'content-management.media-content.article.article-update.authors'=>'Authors',
    'content-management.media-content.article.article-update.date_created'=>'Date Created',
    'content-management.media-content.article.article-update.date_updated'=>'Date Updated',
    'content-management.media-content.article.article-update.main_photo'=>'Main Photo',
    'content-management.media-content.article.article-update.browse_image'=>'Browse Image...',
    'content-management.media-content.article.article-update.status'=>'Status',
    'content-management.media-content.article.article-update.select_status'=>'Select Status',
    'content-management.media-content.article.article-update.locale'=>'Locale',
    'content-management.media-content.article.article-update.site'=>'Site',
    'content-management.media-content.article.article-update.categories'=>'Categories',
    'content-management.media-content.article.article-update.tags'=>'Tags',
    'content-management.media-content.article.article-update.article_short_description'=>'Article Short Description',
    'content-management.media-content.article.article-update.article_content'=>'Article Content',
    'content-management.media-content.article.article-update.meta_title'=>'Meta Title',
    'content-management.media-content.article.article-update.meta_photo'=>'Meta Photo',
    'content-management.media-content.article.article-update.meta_url'=>'Meta Url',
    'content-management.media-content.article.article-update.meta_type'=>'Meta Type',
    'content-management.media-content.article.article-update.meta_description'=>'Meta Description',
    'content-management.media-content.referential.category.category.category_name'=>'Category Name',
    'content-management.media-content.referential.category.category-update.category_name'=>'Category Name',
    'content-management.media-content.referential.category.category-update.select_parent_category'=>'Select Parent Category',
    'content-management.media-content.referential.category.category.category_type'=>'Category Type',
    'content-management.media-content.referential.category.category-update.category_type'=>'Category Type',
    'content-management.media-content.referential.category.category.parent_category'=>'Parent Category',
    'content-management.media-content.referential.category.category-update.parent_category'=>'Parent Category',
    'content-management.media-content.referential.category.category.select_type'=>'Select Type',
    'content-management.media-content.referential.category.category-update.select_type'=>'Select Type',
    'content-management.media-content.referential.tag.tag.tag_name'=>'Tag Name',
    'content-management.media-content.referential.tag.tag.locale'=>'Locale',
    'content-management.media-content.referential.tag.tag-show.tag_name'=>'Tag Name :',
    'content-management.media-content.referential.tag.tag-show.locale'=>'Locale :',
    'content-management.media-content.referential.tag.tag-update.tag_name'=>'Tag Name',
    'content-management.media-content.referential.tag.tag-update.locale'=>'Locale',
    'user.edit-profile.user-name'=>'User Name',
    'content-management.media-content.video.video.video_title'=>'Video Title',
    'content-management.media-content.video.video.photo'=>'Photo',
    'content-management.media-content.video.video.browse_image'=>'Browse Image...',
    'content-management.media-content.video.video.browse_video'=>'Browse Video...',
    'content-management.media-content.video.video.video'=>'Video',
    'content-management.media-content.video.video.date_created'=>'Date Created',
    'content-management.media-content.video.video.video_description'=>'Video Description',
    'content-management.media-content.video.video.tags'=>'Tags',
    'content-management.media-content.video.video.status'=>'Status',
    'content-management.media-content.video.video.meta_title'=>'Meta Title',
    'content-management.media-content.video.video.meta_photo'=>'Meta Photo',
    'content-management.media-content.video.video.meta_url'=>'Meta Url',
    'content-management.media-content.video.video.meta_type'=>'Meta Type',
    'content-management.media-content.video.video.meta_description'=>'Meta Description',
    'content-management.media-content.video.video-update.video_title'=>'Video Title',
    'content-management.media-content.video.video-update.photo'=>'Photo',
    'content-management.media-content.video.video-update.browse_image'=>'Browse Image...',
    'content-management.media-content.video.video-update.video'=>'Video',
    'content-management.media-content.video.video-update.date_created'=>'Date Created',
    'content-management.media-content.video.video-update.video_description'=>'Video Description',
    'content-management.media-content.video.video-update.tags'=>'Tags',
    'content-management.media-content.video.video-update.status'=>'Status',
    'content-management.media-content.video.video-update.meta_title'=>'Meta Title',
    'content-management.media-content.video.video-update.meta_photo'=>'Meta Photo',
    'content-management.media-content.video.video-update.meta_url'=>'Meta Url',
    'content-management.media-content.video.video-update.meta_type'=>'Meta Type',
    'content-management.media-content.video.video-update.meta_description'=>'Meta Description',
    'content-management.media-content.video.video-show.video_title'=>'Video Title :',
    'content-management.media-content.video.video-show.date_created'=>'Date Created :',
    'content-management.media-content.video.video-show.main_photo'=>'Main Photo :',
    'content-management.media-content.video.video-show.video_link'=>'Video Link :',
    'content-management.media-content.video.video-show.status'=>'Status :',
    'content-management.media-content.video.video-show.video_description'=>'Video Description :',
    'content-management.media-content.video.video-show.meta_title'=>'Meta Title :',
    'content-management.media-content.video.video-show.meta_photo'=>'Meta Photo :',
    'content-management.media-content.video.video-show.meta_url'=>'Meta Url :',
    'content-management.media-content.video.video-show.meta_type'=>'Meta Type :',
    'content-management.media-content.video.video-show.meta_description'=>'Meta Description :',
    'content-management.media-content.configuration.default_locale'=>'Default Locale',
    'content-management.media-content.configuration.default_author'=>'Default Author',
    'content-management.media-content.configuration.default_category'=>'Default Category',
    'content-management.media-content.configuration.select_default_category'=>'Select Default Category',
    'content-management.media-content.configuration.select_locale'=>'Select Locale',
    'content-management.media-content.configuration.default_status'=>'Default Status',
    'content-management.media-content.configuration.default_domain'=>'Default Domain',
    'ecommerce.e-commerce-content.category.category.name'=>'Name',
    'ecommerce.e-commerce-content.category.category.date_created'=>'Date Created',
    'ecommerce.e-commerce-content.category.category.date_update'=>'Date Update',
    'ecommerce.e-commerce-content.category.category.browse_image'=>'Browse Image...',
    'ecommerce.e-commerce-content.category.category.background'=>'Background',
    'ecommerce.e-commerce-content.category.category.color'=>'Color',
    'ecommerce.e-commerce-content.category.category.parent'=>'Parent',
    'ecommerce.e-commerce-content.category.category.category_description'=>'Category Description',
    'ecommerce.e-commerce-content.category.category-show.category_title'=>'Category Title :',
    'ecommerce.e-commerce-content.category.category-show.parent_category'=>'Parent Category :',
    'ecommerce.e-commerce-content.category.category-show.date_created'=>'Date Created :',
    'ecommerce.e-commerce-content.category.category-show.date_update'=>'Date Update :',
    'ecommerce.e-commerce-content.category.category-show.category_icon'=>'Category Icon :',
    'ecommerce.e-commerce-content.category.category-show.category_background'=>'Category Background :',
    'ecommerce.e-commerce-content.category.category-show.category_color'=>'Category Color :',
    'ecommerce.e-commerce-content.category.category-show.category_description'=>'Category Description :',
    'ecommerce.e-commerce-content.category.category-update.name'=>'Name',
    'ecommerce.e-commerce-content.category.category-update.date_created'=>'Date Created',
    'ecommerce.e-commerce-content.category.category-update.date_update'=>'Date Update',
    'ecommerce.e-commerce-content.category.category-update.icon'=>'Icon',
    'ecommerce.e-commerce-content.category.category-update.browse_image'=>'Browse Image...',
    'ecommerce.e-commerce-content.category.category-update.background'=>'Background',
    'ecommerce.e-commerce-content.category.category-update.color'=>'Color',
    'ecommerce.e-commerce-content.category.category-update.parent'=>'Parent',
    'ecommerce.e-commerce-content.category.category-update.category_description'=>'Category Description',
    'ecommerce.e-commerce-content.product.product.name'=>'Name',
    'ecommerce.e-commerce-content.product.product-update.name'=>'Name',
    'ecommerce.e-commerce-content.product.product.sku'=>'Sku',
    'ecommerce.e-commerce-content.product.product-update.sku'=>'Sku',
    'ecommerce.e-commerce-content.product.product.date_created'=>'Date Created',
    'ecommerce.e-commerce-content.product.product-update.date_created'=>'Date Created',
    'ecommerce.e-commerce-content.product.product.date_update'=>'Date Update',
    'ecommerce.e-commerce-content.product.product-update.date_update'=>'Date Update',
    'ecommerce.e-commerce-content.product.product.price'=>'Price',
    'ecommerce.e-commerce-content.product.product-update.price'=>'Price',
    'ecommerce.e-commerce-content.product.product.cost'=>'Cost',
    'ecommerce.e-commerce-content.product.product-update.cost'=>'Cost',
    'ecommerce.e-commerce-content.product.product.quantity'=>'Quantity',
    'ecommerce.e-commerce-content.product.product-update.quantity'=>'Quantity',
    'ecommerce.e-commerce-content.product.product.main_photo'=>'Main Photo',
    'ecommerce.e-commerce-content.product.product-update.main_photo'=>'Main Photo',
    'ecommerce.e-commerce-content.product.product.browse_image'=>'Browse Image...',
    'ecommerce.e-commerce-content.product.product-update.browse_image'=>'Browse Image...',
    'ecommerce.e-commerce-content.product.product.status'=>'Status',
    'ecommerce.e-commerce-content.product.product-update.status'=>'Status',
    'ecommerce.e-commerce-content.product.product.locale'=>'Locale',
    'ecommerce.e-commerce-content.product.product-update.locale'=>'Locale',
    'ecommerce.e-commerce-content.product.product-update.select_locale'=>'Select Locale',
    'ecommerce.e-commerce-content.product.product.categories'=>'Categories',
    'ecommerce.e-commerce-content.product.product-update.categories'=>'Categories',
    'ecommerce.e-commerce-content.product.product.tags'=>'Tags',
    'ecommerce.e-commerce-content.product.product-update.tags'=>'Tags',
    'ecommerce.e-commerce-content.product.product.product_short_description'=>'Product Short Description',
    'ecommerce.e-commerce-content.product.product-update.product_short_description'=>'Product Short Description',
    'ecommerce.e-commerce-content.product.product-show.product_short_description'=>'Product Short Description',
    'ecommerce.e-commerce-content.product.product.product_description'=>'Product Description',
    'ecommerce.e-commerce-content.product.product-update.product_description'=>'Product Description',
    'ecommerce.e-commerce-content.product.product.product_ingredients'=>'Product Ingredients',
    'ecommerce.e-commerce-content.product.product-update.product_ingredients'=>'Product Ingredients',
    'ecommerce.e-commerce-content.product.product.product_instructions'=>'Product Instructions',
    'ecommerce.e-commerce-content.product.product-update.product_instructions'=>'Product Instructions',
    'ecommerce.e-commerce-content.product.product.product_tips'=>'Product Tips',
    'ecommerce.e-commerce-content.product.product-update.product_tips'=>'Product Tips',
    'ecommerce.e-commerce-content.product.product.meta_title'=>'Meta Title',
    'ecommerce.e-commerce-content.product.product-update.meta_title'=>'Meta Title',
    'ecommerce.e-commerce-content.product.product.meta_photo'=>'Meta Photo',
    'ecommerce.e-commerce-content.product.product-update.meta_photo'=>'Meta Photo',
    'ecommerce.e-commerce-content.product.product.meta_url'=>'Meta Url',
    'ecommerce.e-commerce-content.product.product-update.meta_url'=>'Meta Url',
    'ecommerce.e-commerce-content.product.product.meta_type'=>'Meta Type',
    'ecommerce.e-commerce-content.product.product-update.meta_type'=>'Meta Type',
    'ecommerce.e-commerce-content.product.product.meta_description'=>'Meta Description',
    'ecommerce.e-commerce-content.product.product-update.meta_description'=>'Meta Description',
    'ecommerce.e-commerce-content.product.product-show.product_title'=>'Product Title :',
    'ecommerce.e-commerce-content.product.product-show.product_sku'=>'Product Sku :',
    'ecommerce.e-commerce-content.product.product-show.product_price'=>'Product Price :',
    'ecommerce.e-commerce-content.product.product-show.product_cost'=>'Product Cost :',
    'ecommerce.e-commerce-content.product.product-show.product_quantity'=>'Product Quantity :',
    'ecommerce.e-commerce-content.product.product-show.date_created'=>'Date Created :',
    'ecommerce.e-commerce-content.product.product-show.date_update'=>'Date Update :',
    'ecommerce.e-commerce-content.product.product-show.main_photo'=>'Main Photo :',
    'ecommerce.e-commerce-content.product.product-show.status'=>'Status :',
    'ecommerce.e-commerce-content.product.product-show.locale'=>'Locale :',
    'ecommerce.e-commerce-content.product.product-show.categories'=>'Categories :',
    'ecommerce.e-commerce-content.product.product-show.product_ingredients'=>'Product Ingredients :',
    'ecommerce.e-commerce-content.product.product-show.product_tips'=>'Product Tips :',
    'ecommerce.e-commerce-content.product.product-show.product_instructions'=>'Product Instructions :',
    'ecommerce.e-commerce-content.product.product-show.product_description'=>'Product Description :',
    'ecommerce.e-commerce-content.product.product-show.meta_title'=>'Meta Title :',
    'ecommerce.e-commerce-content.product.product-show.meta_photo'=>'Meta Photo :',
    'ecommerce.e-commerce-content.product.product-show.meta_url'=>'Meta Url :',
    'ecommerce.e-commerce-content.product.product-show.meta_type'=>'Meta Type :',
    'ecommerce.e-commerce-content.product.product-show.meta_description'=>'Meta Description :',

    'ecommerce.e-commerce-content.brand.brand.browse_image' =>'Browse Image...',
    'ecommerce.e-commerce-content.brand.brand-update.browse_image' =>'Browse Image...',
    'ecommerce.e-commerce-content.brand.brand.brand_image' =>'Photo',
    'ecommerce.e-commerce-content.brand.brand-update.brand_image' =>'Photo',
    'ecommerce.e-commerce-content.brand.brand-show.photo' =>'Photo :',
    'ecommerce.e-commerce-content.brand.brand.brand_name' =>'Name',
    'ecommerce.e-commerce-content.brand.brand-update.brand_name' =>'Name',
    'ecommerce.e-commerce-content.brand.brand-show.brand_name' =>'Name :',
    'ecommerce.e-commerce-content.brand.brand.parent_brand' =>'Parent Brand',
    'ecommerce.e-commerce-content.brand.brand-update.parent_brand' =>'Parent Brand',
    'ecommerce.e-commerce-content.brand.brand-show.parent_brand' =>'Parent Brand :',
    'ecommerce.e-commerce-content.brand.brand-show.created_at' =>'Date Created :',
    'ecommerce.e-commerce-content.brand.brand-show.updated_at' =>'Date Updated :',
    'ecommerce.e-commerce-content.order.order.order_reference' =>'Order Reference',
    'ecommerce.e-commerce-content.order.order-show.order_reference' =>'Order Reference',
    'ecommerce.e-commerce-content.order.order-update.order_reference' =>'Order Reference',
    'ecommerce.e-commerce-content.order.order.customer' =>'Customer',
    'ecommerce.e-commerce-content.order.order-show.customer' =>'Customer',
    'ecommerce.e-commerce-content.order.order-update.customer' =>'Customer',
    'ecommerce.e-commerce-content.order.order.order_status' =>'Order Status',
    'ecommerce.e-commerce-content.order.order-show.order_status' =>'Order Status',
    'ecommerce.e-commerce-content.order.order-update.order_status' =>'Order Status',
    'ecommerce.e-commerce-content.order.order.payment' =>'Payment',
    'ecommerce.e-commerce-content.order.order-show.payment' =>'Payment',
    'ecommerce.e-commerce-content.order.order-update.payment' =>'Payment',
    'ecommerce.e-commerce-content.order.order.discounts' =>'Discounts',
    'ecommerce.e-commerce-content.order.order-show.discounts' =>'Discounts',
    'ecommerce.e-commerce-content.order.order-update.discounts' =>'Discounts',
    'ecommerce.e-commerce-content.order.order.total_products' =>'Total Products',
    'ecommerce.e-commerce-content.order.order-show.total_products' =>'Total Products',
    'ecommerce.e-commerce-content.order.order-update.total_products' =>'Total Products',
    'ecommerce.e-commerce-content.order.order.tax' =>'Tax',
    'ecommerce.e-commerce-content.order.order-show.tax' =>'Tax',
    'ecommerce.e-commerce-content.order.order-update.tax' =>'Tax',
    'ecommerce.e-commerce-content.order.order.total' =>'Total',
    'ecommerce.e-commerce-content.order.order-show.total' =>'Total',
    'ecommerce.e-commerce-content.order.order-update.total' =>'Total',
    'ecommerce.e-commerce-content.order.order.total_paid' =>'Total Paid',
    'ecommerce.e-commerce-content.order.order-show.total_paid' =>'Total Paid',
    'ecommerce.e-commerce-content.order.order-update.total_paid' =>'Total Paid',


    'configuration.apis.api.name' =>'Api Name',
    'configuration.apis.api.custom_select' =>'Custom Select',
    'configuration.apis.api.custom_condition' =>'Custom Condition',
    'configuration.apis.api.module' =>'Module',
    'configuration.apis.api.version' =>'Version',
    'configuration.apis.api.type' =>'Type',
    'configuration.apis.api.order_by' =>'Order By',
    'configuration.apis.api.paginate' =>'Paginate',
    'configuration.apis.api.authenticated' =>'Authenticated',
    'configuration.apis.api.yes' =>'YES',
    'configuration.apis.api.no' =>'NO',

    'configuration.apis.api-show.name' =>'Api Name :',
    'configuration.apis.api-show.custom_select' =>'Custom Select :',
    'configuration.apis.api-show.custom_condition' =>'Custom Condition :',
    'configuration.apis.api-show.module_id' =>'Module :',
    'configuration.apis.api-show.version' =>'Version :',
    'configuration.apis.api-show.type' =>'Type :',
    'configuration.apis.api-show.order_by' =>'Order By :',
    'configuration.apis.api-show.paginate' =>'Paginate :',
    'configuration.apis.api-show.authenticated' =>'Authenticated :',


    'content-management.media-content.referential.category.category-show.category_name' =>'Category Name :',
    'content-management.media-content.referential.category.category-show.parent_id' =>'Parent :',
    'content-management.media-content.referential.category.category-show.type' =>'Type :',








];