<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('{version}/{slug}' , 'Configuration\ApiController@apis');
Route::post('{version}/{slug}', 'Configuration\ApiController@apis');
Route::delete('{version}/{slug}' , 'Configuration\ApiController@apis');
Route::put('{version}/{slug}','Configuration\ApiController@apis');
