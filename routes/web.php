<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

function getUrlParent($parent_id ){
    if ( $parent_id == null )
        return'';
    $pm = \App\Models\ParentModule::find($parent_id);
    if ( $pm->parent_id == null){
        return $pm->slug.'/';
    }
    else {
       return   getUrlParent($pm->parent_id). $pm->slug.'/';
    }

}


function getUrlController($parent_id ){
    if ( $parent_id == null )
        return'';
    $pm = \App\Models\ParentModule::find($parent_id);
    if ( $pm->parent_id == null) {
        return $pm->controller_name.'\\';
    }
    else {
        return   getUrlController($pm->parent_id). $pm->controller_name.'\\';
    }

}

function getUrlName($parent_id ){
    if ( $parent_id == null )
        return'';
    $pm = \App\Models\ParentModule::find($parent_id);
    if ( $pm->parent_id == null) {
        return $pm->slug.'.';
    }
    else {
        return   getUrlName($pm->parent_id). $pm->slug.'.';
    }

}

Route::get('/', function () {
    return redirect('/cms');
})->name('welcome');

Auth::routes();
Route::get('cms/login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('cms/login', 'Auth\LoginController@login')->name('login');

Route::post('/contact-us/mail','HomeController@mailContact')->name('contact.post');
Route::get('/ajax-select', 'HomeController@ajaxSelect')->name('ajax-select');

Route::group([ 'middleware' => ['roles'], 'prefix'=>'cms'], function () {
    $modules = \App\Models\Module::all();
    foreach ( $modules as $module ){

        Route::resource( getUrlParent($module->parent_id).$module->slug, getUrlController($module->parent_id).$module->controller_name,
            ['as' => substr(getUrlName($module->parent_id), 0, -1)]);
        Route::get(getUrlParent($module->parent_id).$module->slug.'/{id}/remove-image',
            getUrlController($module->parent_id).$module->controller_name.'@removeImage')
            ->name(getUrlName($module->parent_id).$module->slug.'.removeImage');

        Route::get(getUrlParent($module->parent_id).$module->slug.'/{id}/edit/authorize',
            getUrlController($module->parent_id).$module->controller_name.'@editAuthorise')
            ->name(getUrlName($module->parent_id).$module->slug.'.edit-authorise');
        Route::get(getUrlParent($module->parent_id).$module->slug.'/{user}/up', getUrlController($module->parent_id).$module->controller_name.'@up')
            ->name(getUrlName($module->parent_id).$module->slug.'.up');
        Route::get(getUrlParent($module->parent_id).$module->slug.'/{user}/down', getUrlController($module->parent_id).$module->controller_name.'@down')
            ->name(getUrlName($module->parent_id).$module->slug.'.down');
    }
    Route::get('/', 'HomeController@index')->name('home');
    $pmodules = \App\Models\ParentModule::query()->whereNull('parent_id')->get();
    foreach ($pmodules as $mod){

//        \Illuminate\Support\Facades\Log::info($mod->hasConfig());
        Route::get($mod->slug, $mod->controller_name.'\\'.$mod->controller_name.'Controller@show')->name($mod->slug);
        if ( $mod->hasConfig()->count() > 0){
            Route::get($mod->slug.'/configuration', $mod->controller_name.'\\'.$mod->controller_name.'Controller@configuration')
                ->name($mod->slug.'.configuration');
            Route::post($mod->slug.'/configuration', $mod->controller_name.'\\'.$mod->controller_name.'Controller@configuration')
                ->name($mod->slug.'.configuration.store');
        }
    }

    Route::group([ 'prefix'=>'configuration','middleware' => ['roles'] ], function () {
        Route::get('remove/{key}','Configuration\ConfigurationController@removeImage')->name('configuration.remove');
        Route::get('branding','Configuration\ConfigurationController@indexBranding')->name('configuration.branding');
        Route::post('branding','Configuration\ConfigurationController@brandingStore')->name('configuration.branding.store');
        Route::get('domain','Configuration\ConfigurationController@indexDomain')->name('configuration.domain');
        Route::post('domain','Configuration\ConfigurationController@domainStore')->name('configuration.domain.store');
        Route::get('super-user','Configuration\ConfigurationController@indexSuperUser')->name('configuration.super-user');
        Route::post('super-user','Configuration\ConfigurationController@superUserStore')->name('configuration.super-user.store');
        
        Route::resource('api-argument', 'Configuration\ApiArgumentController');
        Route::get('api-argument/{api-argument}/up', 'Configuration\ApiArgumentController@up')->name('api-argument.up');
        Route::get('api-argument/{api-argument}/down', 'Configuration\ApiArgumentController@down')->name('api-argument.down');Route::get('api-argument/{id}/edit/authorize','Configuration\ApiArgumentController@editAuthorise')->name('api-argument.edit-authorise');


        Route::resource('contact-phone', 'OrganizationsManagement\ContactSystem\ContactPhoneController');
        Route::get('contact-phone/{contact-phone}/up', 'OrganizationsManagement\ContactSystem\ContactPhoneController@up')->name('contact-phone.up');
        Route::get('contact-phone/{contact-phone}/down', 'OrganizationsManagement\ContactSystem\ContactPhoneController@down')->name('contact-phone.down');Route::get('lead-action/{id}/edit/authorize','ProjectManagement\ProjectSystem\LeadActionController@editAuthorise')->name('lead-action.edit-authorise');



        Route::resource('api-generator','Configuration\ApiController');
        Route::get('api-generator/{api}/up', 'Configuration\ApiController@up')->name('api-generator.up');
        Route::get('api-generator/{api}/down', 'Configuration\ApiController@down')->name('api-generator.down');
        Route::get('api-generator/{id}/edit/authorize','Configuration\ApiController@editAuthorise')->name('api-generator.edit-authorise');

        Route::resource('domain', 'Configuration\DomainController');
        Route::get('domain/{domain}/up', 'Configuration\DomainController@up')->name('domain.up');
        Route::get('domain/{domain}/down', 'Configuration\DomainController@down')->name('domain.down');
        Route::get('domain/{id}/edit/authorize','Configuration\DomainController@editAuthorise')->name('domain.edit-authorise');

        Route::resource('status', 'Configuration\StatusController');
        Route::get('status/{status}/up', 'Configuration\StatusController@up')->name('status.up');
        Route::get('status/{status}/down', 'Configuration\StatusController@down')->name('status.down');
        Route::get('status/{id}/edit/authorize','Configuration\StatusController@editAuthorise')->name('status.edit-authorise');



        Route::get('language','Configuration\ConfigurationController@indexLanguage')->name('configuration.language');
        Route::get('language/change/{language}','Configuration\ConfigurationController@languageChange')->name('configuration.language.change');
        Route::get('language/create','Configuration\ConfigurationController@createLang')->name('configuration.language.create');
        Route::post('language/store','Configuration\ConfigurationController@storeLanguage')->name('configuration.language.store');


    });
    Route::group(['middleware' => ['roles'] ], function () {

        Route::resource('project-management/dashboard', 'ProjectManagement\ProjectSystem\DashboardController');
        Route::get('project-management/dashboard', 'ProjectManagement\ProjectSystem\DashboardController@index')->name('project-management/dashboard.index');

        Route::get('user','ProfileController@edit')->name('profile');
        Route::get('user/remove-image','ProfileController@removeImage')->name('profile.removeImage');
        Route::post('user/update','ProfileController@update')->name('profile.update');

        Route::resource('lead-action', 'ProjectManagement\LeadSystem\LeadActionController');
        Route::get('lead-action/{lead-action}/up', 'ProjectManagement\LeadSystem\LeadActionController@up')->name('lead-action.up');
        Route::get('lead-action/{lead-action}/down', 'ProjectManagement\LeadSystem\LeadActionController@down')->name('lead-action.down');
        Route::get('lead-action/{id}/edit/authorize','ProjectManagement\LeadSystem\LeadActionController@editAuthorise')->name('lead-action.edit-authorise');

        Route::resource('project-module', 'ProjectManagement\ProjectSystem\ProjectModuleController');
        Route::get('project-module/{project-module}/up', 'ProjectManagement\ProjectSystem\ProjectModuleController@up')->name('project-module.up');
        Route::get('project-module/{project-module}/down', 'ProjectManagement\ProjectSystem\ProjectModuleController@down')->name('project-module.down');
        Route::get('project-module/{id}/edit/authorize','ProjectManagement\ProjectSystem\ProjectModuleController@editAuthorise')->name('project-module.edit-authorise');

        Route::resource('project-task', 'ProjectManagement\ProjectSystem\ProjectTaskController');
        Route::get('project-task/{project-task}/up', 'ProjectManagement\ProjectSystem\ProjectTaskController@up')->name('project-task.up');
        Route::get('project-task/{project-task}/down', 'ProjectManagement\ProjectSystem\ProjectTaskController@down')->name('project-task.down');
        Route::get('project-task/{id}/edit/authorize','ProjectManagement\ProjectSystem\ProjectTaskController@editAuthorise')->name('project-task.edit-authorise');

    });

});
